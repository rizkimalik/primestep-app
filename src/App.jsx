import React, { Suspense } from "react";
import { useSelector } from "react-redux";
import { Switch } from "react-router-dom";

import Login from "views/pages/Login";
import { authUser } from "app/slice/sliceAuth";
import SplashScreen from "views/components/SplashScreen";
import { PrivateRoute, ProtectedRoutes, PublicRoute } from "router";

function App() {
    const { token } = useSelector(authUser);
    const isAuth = Boolean(token);

    return (
        <Suspense fallback={<SplashScreen />}>
            <Switch>
                <PublicRoute path='/login' isAuth={isAuth}>
                    <Login />
                </PublicRoute>

                <PrivateRoute path="/" isAuth={isAuth}>
                    <ProtectedRoutes />
                </PrivateRoute>
            </Switch>
        </Suspense>
    )
}

export default App;
