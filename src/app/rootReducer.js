import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from "redux";

import sliceUser from './slice/sliceUser';
import sliceAuth from './slice/sliceAuth';
import sliceMenu from './slice/sliceMenu';
import sliceNotification from './slice/sliceNotification';
import sliceMasterData from './slice/sliceMasterData';
import slicePackageCategory from './slice/slicePackageCategory';
import sliceDocument from './slice/sliceDocument';
import sliceDocumentStatus from './slice/sliceDocumentStatus';
import sliceProcurementMethod from './slice/sliceProcurementMethod';
import sliceProcurementReview from './slice/sliceProcurementReview';
import sliceDashboard from './slice/sliceDashboard';
import sliceProcurement from './slice/sliceProcurement';
import sliceDMF from './slice/sliceDMF';
import sliceInstitution from './slice/sliceInstitution';
import sliceStartupResearch from './slice/sliceStartupResearch';
import sliceStrengthenCapability from './slice/sliceStrengthenCapability';
import sliceIssuesChallenges from './slice/sliceIssuesChallenges';
import sliceGenderAction from './slice/sliceGenderAction';
import sliceCategoryResearch from './slice/sliceCategoryResearch';
import sliceCategoryFramework from './slice/sliceCategoryFramework';
import sliceDisbursement from './slice/sliceDisbursement';
import sliceContract from './slice/sliceContract';
import sliceRAB from './slice/sliceRAB';
import sliceEvaluator from './slice/sliceEvaluator';
import sliceStrengthenComponent from './slice/sliceStrengthenComponent';
import sliceStrengthenCategory from './slice/sliceStrengthenCategory';
import slicePaguContract from './slice/slicePaguContract';
import sliceLaporanDokumen from './slice/sliceLaporanDokumen';

const persistConfig = {
    key: 'root',
    version: 1,
    storage
}

const reducer = combineReducers({
    authUser: sliceAuth.reducer,
})
const persistedReducer = persistReducer(persistConfig, reducer);

const rootReducer = {
    persistedReducer,
    auth: sliceAuth.reducer,
    mainmenu: sliceMenu.reducer,
    notification: sliceNotification.reducer,
    user: sliceUser.reducer,
    master: sliceMasterData.reducer,
    package_category: slicePackageCategory.reducer,
    document: sliceDocument.reducer,
    document_status: sliceDocumentStatus.reducer,
    procurement_method: sliceProcurementMethod.reducer,
    procurement_review: sliceProcurementReview.reducer,
    dashboard: sliceDashboard.reducer,
    procurement: sliceProcurement.reducer,
    dmf: sliceDMF.reducer,
    institution: sliceInstitution.reducer,
    startup_research: sliceStartupResearch.reducer,
    strengthen_capability: sliceStrengthenCapability.reducer,
    issues_challenges: sliceIssuesChallenges.reducer,
    gender_action: sliceGenderAction.reducer,
    category_focus: sliceCategoryResearch.reducer,
    category_framework: sliceCategoryFramework.reducer,
    disbursement: sliceDisbursement.reducer,
    contract: sliceContract.reducer,
    rab: sliceRAB.reducer,
    evaluator: sliceEvaluator.reducer,
    strengthen_component: sliceStrengthenComponent.reducer,
    strengthen_category: sliceStrengthenCategory.reducer,
    pagu_contract: slicePaguContract.reducer,
    laporan_dokumen: sliceLaporanDokumen.reducer,
}

export default rootReducer;
