import { createSlice } from "@reduxjs/toolkit";
import { apiEvaluator_List } from "app/services/apiEvaluator";


const sliceEvaluator = createSlice({
    name: "evaluator",
    initialState: {
        evaluator_list: [],
    },
    extraReducers: {
        [apiEvaluator_List.fulfilled]: (state, action) => {
            state.evaluator_list = action.payload
        },
    },
});

export default sliceEvaluator;