import { createSlice } from "@reduxjs/toolkit";
import {
    apiMasterUserLevel,
    apiMasterInstitution,
    apiMasterPackageCategory,
    apiMasterCatDocStatus,
    apiMaster_DocUploadedList,
    apiMasterProcurementMethod,
    apiMasterProcurementReview,
    apiMasterCategoryResearch,
    apiMaster_CategoryFramework,
    apiMaster_DMFPerformance,
    apiMaster_DMFIndicator,
    apiMaster_CategoryTKT,
    apiMaster_IndicatorTKT,
    apiMaster_StrengthenComponent,
    apiMaster_StrengthenCategory,
} from "app/services/apiMasterData";

const sliceMasterData = createSlice({
    name: "master",
    initialState: {
        user_level: [],
        institution: [],
        category_package: [],
        category_doc_status: [],
        docs_uploaded_list: [],
        category_method: [],
        category_review: [],
        category_focus: [],
        category_framework: [],
        dmf_performance: [],
        dmf_indicator: [],
        tkt_category: [],
        tkt_indicator: [],
        strengthen_component: [],
        strengthen_category: [],
    },
    extraReducers: {
        [apiMasterUserLevel.fulfilled]: (state, action) => {
            state.user_level = action.payload.data
        },
        [apiMasterInstitution.fulfilled]: (state, action) => {
            state.institution = action.payload.data
        },
        [apiMasterPackageCategory.fulfilled]: (state, action) => {
            state.category_package = action.payload.data
        },
        [apiMasterCatDocStatus.fulfilled]: (state, action) => {
            state.category_doc_status = action.payload.data
        },
        [apiMaster_DocUploadedList.fulfilled]: (state, action) => {
            state.docs_uploaded_list = action.payload.data
        },
        [apiMasterProcurementMethod.fulfilled]: (state, action) => {
            state.category_method = action.payload.data
        },
        [apiMasterProcurementReview.fulfilled]: (state, action) => {
            state.category_review = action.payload.data
        },
        [apiMasterCategoryResearch.fulfilled]: (state, action) => {
            state.category_focus = action.payload.data
        },
        [apiMaster_CategoryFramework.fulfilled]: (state, action) => {
            state.category_framework = action.payload.data
        },
        [apiMaster_DMFPerformance.fulfilled]: (state, action) => {
            state.dmf_performance = action.payload.data
        },
        [apiMaster_DMFIndicator.fulfilled]: (state, action) => {
            state.dmf_indicator = action.payload.data
        },
        [apiMaster_CategoryTKT.fulfilled]: (state, action) => {
            state.tkt_category = action.payload.data
        },
        [apiMaster_IndicatorTKT.fulfilled]: (state, action) => {
            state.tkt_indicator = action.payload.data
        },
        [apiMaster_StrengthenComponent.fulfilled]: (state, action) => {
            state.strengthen_component = action.payload.data
        },
        [apiMaster_StrengthenCategory.fulfilled]: (state, action) => {
            state.strengthen_category = action.payload.data
        },
    },
});

export default sliceMasterData;