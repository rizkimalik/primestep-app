import { createSlice } from "@reduxjs/toolkit";
import { apiContractDetail } from "app/services/apiContract";

const sliceContract = createSlice({
    name: "contract",
    initialState: {
        contract_detail: {},
    },
    extraReducers: {
        [apiContractDetail.fulfilled]: (state, action) => {
            state.contract_detail = action.payload.data
        },
    },
});

export default sliceContract;