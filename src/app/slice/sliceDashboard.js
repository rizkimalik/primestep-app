import { createSlice } from "@reduxjs/toolkit";
import {
    apiDashboard_TotalProgress,
    apiDashboard_LoanMilestone,
    apiDashboard_LoanProgressPermonth,
    //? Procurement Packages Total
    apiDashboard_ProcurementPackagesUI,
    apiDashboard_ProcurementPackagesUGM,
    apiDashboard_ProcurementPackagesITB,
    apiDashboard_ProcurementPackagesIPB,
    apiDashboard_ProcurementPackagesPMU,
    //? evalautor dash
    apiDashboard_EvaluatorTotal,
    apiDashboard_EvaluatorData,
} from "app/services/apiDashboard";

const sliceDashboard = createSlice({
    name: "dashboard",
    initialState: {
        total_progress: {},
        loan_milestone: {},
        loan_progress_permonth: {},
        //? Procurement Packages Total
        procurement_packages_ui: {},
        procurement_packages_ugm: {},
        procurement_packages_itb: {},
        procurement_packages_ipb: {},
        procurement_packages_pmu: {},
        //? evalautor dash
        evaluator_total: {},
        evaluator_data: [],
    },
    extraReducers: {
        [apiDashboard_TotalProgress.fulfilled]: (state, action) => {
            state.total_progress = action.payload.data
        },
        [apiDashboard_LoanMilestone.fulfilled]: (state, action) => {
            state.loan_milestone = action.payload.data
        },
        [apiDashboard_LoanProgressPermonth.fulfilled]: (state, action) => {
            state.loan_progress_permonth = action.payload.data
        },
        //? Procurement Packages Total
        [apiDashboard_ProcurementPackagesUI.fulfilled]: (state, action) => {
            state.procurement_packages_ui = action.payload.data
        },
        [apiDashboard_ProcurementPackagesUGM.fulfilled]: (state, action) => {
            state.procurement_packages_ugm = action.payload.data
        },
        [apiDashboard_ProcurementPackagesITB.fulfilled]: (state, action) => {
            state.procurement_packages_itb = action.payload.data
        },
        [apiDashboard_ProcurementPackagesIPB.fulfilled]: (state, action) => {
            state.procurement_packages_ipb = action.payload.data
        },
        [apiDashboard_ProcurementPackagesPMU.fulfilled]: (state, action) => {
            state.procurement_packages_pmu = action.payload.data
        }, 
        //? Evaluator Dashboard
        [apiDashboard_EvaluatorTotal.fulfilled]: (state, action) => {
            state.evaluator_total = action.payload.data
        },
        [apiDashboard_EvaluatorData.fulfilled]: (state, action) => {
            state.evaluator_data = action.payload
        },
    },
});

export default sliceDashboard;