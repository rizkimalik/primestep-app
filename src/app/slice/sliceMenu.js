import { createSlice } from "@reduxjs/toolkit";
import { getMainMenu, apiMenuAccess,apiModulAccess, apiMenu, apiModul } from "app/services/apiMenu";

const sliceMenu = createSlice({
    name: "mainmenu",
    initialState: {
        main_menu: [],
        menu_access: [],
        modul_access: [],
        menu: [],
        modul: [],
    },
    extraReducers: {
        [getMainMenu.fulfilled]: (state, action) => {
            state.main_menu = action.payload.data
        },
        [apiMenuAccess.fulfilled]: (state, action) => {
            state.menu_access = action.payload.data
        },
        [apiModulAccess.fulfilled]: (state, action) => {
            state.modul_access = action.payload.data
        },
        [apiMenu.fulfilled]: (state, action) => {
            state.menu = action.payload.data
        },
        [apiModul.fulfilled]: (state, action) => {
            state.modul = action.payload.data
        },
    },
});

export default sliceMenu;