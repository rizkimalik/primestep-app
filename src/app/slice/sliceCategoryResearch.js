import { createSlice } from "@reduxjs/toolkit";
import { apiCategoryResearch } from "app/services/apiCategoryResearch";

const sliceCategoryResearch = createSlice({
    name: "category_focus",
    initialState: {
        category_focus: [],
    },
    extraReducers: {
        [apiCategoryResearch.fulfilled]: (state, action) => {
            state.category_focus = action.payload
        },
    },
});

export default sliceCategoryResearch;