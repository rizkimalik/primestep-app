import { createSlice } from "@reduxjs/toolkit";
import {
    apiProcurementPackageList,
    apiProcurement_InformationStatus,
    apiProcurementPackageShow,
    apiProcurementPackageDetailList,
    apiProcurementPackageDetailShow,
    apiListDocsUploaded,
    apiDocStatusList,
    apiPackageStatusDetail,
} from "app/services/apiProcurement";


const sliceProcurement = createSlice({
    name: "procurement",
    initialState: {
        package_show: {},
        information_status: {},
        package_detail_show: {},
        package_status_detail: {},
        package_list: [],
        package_detail_list: [],
        list_docs_uploaded: [],
        list_docs_status: [],
    },
    extraReducers: {
        [apiProcurementPackageList.fulfilled]: (state, action) => {
            state.package_list = action.payload
        },
        [apiProcurement_InformationStatus.fulfilled]: (state, action) => {
            state.information_status = action.payload.data[0]
        },
        [apiProcurementPackageShow.fulfilled]: (state, action) => {
            state.package_show = action.payload.data
        },
        [apiProcurementPackageDetailList.fulfilled]: (state, action) => {
            state.package_detail_list = action.payload
        },
        [apiProcurementPackageDetailShow.fulfilled]: (state, action) => {
            state.package_detail_show = action.payload.data
        },
        [apiListDocsUploaded.fulfilled]: (state, action) => {
            state.list_docs_uploaded = action.payload
        },
        [apiDocStatusList.fulfilled]: (state, action) => {
            state.list_docs_status = action.payload.data
        },
        [apiPackageStatusDetail.fulfilled]: (state, action) => {
            state.package_status_detail = action.payload.data
        },
    },
});
export default sliceProcurement;