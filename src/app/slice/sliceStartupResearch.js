import { createSlice } from "@reduxjs/toolkit";
import {
    apiStartupResearch_StartupList,
    apiStartupResearch_ResearchList,
    apiStartup_AccelerationList,
    apiStartup_AccelerationSelect,
    apiStartupResearch_InformationStatus,
    apiStartupResearch_Detail,
    apiStartupResearch_Show,
    apiListDocsUploaded,
    apiDocStatusList,
    apiPackageStatusDetail,
    apiStartupResearch_SearchUsulan,
    apiStartupResearch_ContractList,
    apiStartupResearch_RecommendedList,
    apiInnovationMember,
    apiInnovation_OuterList,
    apiInnovation_PartnerList,
} from "app/services/apiStartupResearch";

const sliceStartupResearch = createSlice({
    name: "startup_research",
    initialState: {
        startups: [],
        research: [],
        startup_acceleration: [],
        startup_acceleration_select: [],
        usulan_list: [],
        data_detail: {},
        startup_research_detail: {},
        information_status: {},
        //? status & docs
        list_docs_uploaded: [],
        list_docs_status: [],
        package_status_detail: {},
        contract_list: [],
        recommend_list: [],
        //? innovation member & outer
        innovation_members: [],
        innovation_outers: [],
        innovation_partners: [],
    },
    extraReducers: {
        [apiStartupResearch_StartupList.fulfilled]: (state, action) => {
            state.startups = action.payload
        },
        [apiStartupResearch_ResearchList.fulfilled]: (state, action) => {
            state.research = action.payload
        },
        [apiStartup_AccelerationList.fulfilled]: (state, action) => {
            state.startup_acceleration = action.payload
        },
        [apiStartup_AccelerationSelect.fulfilled]: (state, action) => {
            state.startup_acceleration_select = action.payload
        },
        [apiStartupResearch_InformationStatus.fulfilled]: (state, action) => {
            state.information_status = action.payload.data[0]
        },
        [apiStartupResearch_Show.fulfilled]: (state, action) => {
            state.startup_research_detail = action.payload.data
        },
        [apiStartupResearch_Detail.fulfilled]: (state, action) => {
            state.data_detail = action.payload.data
        },
        [apiStartupResearch_SearchUsulan.fulfilled]: (state, action) => {
            state.usulan_list = action.payload
        },
        [apiStartupResearch_RecommendedList.fulfilled]: (state, action) => {
            state.recommend_list = action.payload
        },

        //? doc & status
        [apiListDocsUploaded.fulfilled]: (state, action) => {
            state.list_docs_uploaded = action.payload
        },
        [apiDocStatusList.fulfilled]: (state, action) => {
            state.list_docs_status = action.payload.data
        },
        [apiPackageStatusDetail.fulfilled]: (state, action) => {
            state.package_status_detail = action.payload.data
        },
        [apiStartupResearch_ContractList.fulfilled]: (state, action) => {
            state.contract_list = action.payload.data
        },

        //? innovation member, outer, partner
        [apiInnovationMember.fulfilled]: (state, action) => {
            state.innovation_members = action.payload
        },
        [apiInnovation_OuterList.fulfilled]: (state, action) => {
            state.innovation_outers = action.payload.data
        },
        [apiInnovation_PartnerList.fulfilled]: (state, action) => {
            state.innovation_partners = action.payload.data
        },
    },
});

export default sliceStartupResearch;