import { createSlice } from "@reduxjs/toolkit";
import { apiDocumentStatusList, apiDocumentStatusShow } from "app/services/apiDocumentStatus";

const sliceDocumentStatus = createSlice({
    name: "document_status",
    initialState: {
        document_status_detail: {},
        document_status: [],
    },
    extraReducers: {
        [apiDocumentStatusList.fulfilled]: (state, action) => {
            state.document_status = action.payload
        },
        [apiDocumentStatusShow.fulfilled]: (state, action) => {
            state.document_status_detail = action.payload.data
        },
    },
});

export default sliceDocumentStatus;