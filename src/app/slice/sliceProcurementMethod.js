import { createSlice } from "@reduxjs/toolkit";
import { apiProcurementMethodList } from "app/services/apiProcurementMethod";

const sliceProcurementMethod = createSlice({
    name: "procurement_method",
    initialState: {
        procurement_method: [],
    },
    extraReducers: {
        [apiProcurementMethodList.fulfilled]: (state, action) => {
            state.procurement_method = action.payload
        },
    },
});

export default sliceProcurementMethod;