import { createSlice } from "@reduxjs/toolkit";
import { apiRAB_List } from "app/services/apiRAB";

const sliceRAB = createSlice({
    name: "rab",
    initialState: {
        rab_list: [],
    },
    extraReducers: {
        [apiRAB_List.fulfilled]: (state, action) => {
            state.rab_list = action.payload
        },
    },
});

export default sliceRAB;