import { createSlice } from "@reduxjs/toolkit";
import { apiInstitutionList } from "app/services/apiInstitution";


const sliceInstitution = createSlice({
    name: "institution",
    initialState: {
        institutions: [],
    },
    extraReducers: {
        [apiInstitutionList.fulfilled]: (state, action) => {
            state.institutions = action.payload
        },
    },
});

export default sliceInstitution;