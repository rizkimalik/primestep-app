import { createSlice } from "@reduxjs/toolkit";
import { apiPaguContract } from "app/services/apiPaguContract";

const slicePaguContract = createSlice({
    name: "pagu_contract",
    initialState: {
        pagu_contract_list: [],
    },
    extraReducers: {
        [apiPaguContract.fulfilled]: (state, action) => {
            state.pagu_contract_list = action.payload
        },
    
    },
});

export default slicePaguContract;