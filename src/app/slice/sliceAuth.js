import { createSlice } from '@reduxjs/toolkit'

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        id: '',
        name: '',
        username: '',
        token: '',
        user_level: '',
        email_address: '',
        institution: '',
        login: '',

        //? set store data default
        store_data: {
            institution: 'UI',
            year: new Date().getFullYear(),
        },
    },
    reducers: {
        setAuth: (state, action) => {
            state.id = action.payload.id;
            state.name = action.payload.name;
            state.username = action.payload.username;
            state.user_level = action.payload.user_level;
            state.institution = action.payload.institution;
            state.token = action.payload.token;
            state.login = action.payload.login;
        },
        setStoreData: (state, action) => {
            state.store_data = action.payload;
        }
    },
})

// dvalue from store in the slice file. For example: `useSelector((state) => state.counter.value)`
export const authUser = state => state.persistedReducer.authUser;

//export actions
export const { setAuth, setStoreData } = authSlice.actions;

//export reducer
export default authSlice;