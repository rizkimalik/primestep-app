import { createSlice } from "@reduxjs/toolkit";
import { apiProcurementReviewList } from "app/services/apiProcurementReview";

const sliceProcurementReview = createSlice({
    name: "procurement_review",
    initialState: {
        procurement_review: [],
    },
    extraReducers: {
        [apiProcurementReviewList.fulfilled]: (state, action) => {
            state.procurement_review = action.payload
        },
    },
});

export default sliceProcurementReview;