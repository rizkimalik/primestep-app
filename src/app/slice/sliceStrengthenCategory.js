import { createSlice } from "@reduxjs/toolkit";
import { apiStrengthenCategory } from "app/services/apiStrengthenCategory";

const sliceStrengthenCategory = createSlice({
    name: "strengthen_category",
    initialState: {
        strengthen_category: [],
    },
    extraReducers: {
        [apiStrengthenCategory.fulfilled]: (state, action) => {
            state.strengthen_category = action.payload
        },
    },
});

export default sliceStrengthenCategory;