import { createSlice } from "@reduxjs/toolkit";
import { apiDisbursementList, apiDisbursementShow } from "app/services/apiDisbursement";


const sliceDisbursement = createSlice({
    name: "disbursement",
    initialState: {
        disbursement_list: [],
        disbursement_detail: {},
    },
    extraReducers: {
        [apiDisbursementList.fulfilled]: (state, action) => {
            state.disbursement_list = action.payload.data
        },
        [apiDisbursementShow.fulfilled]: (state, action) => {
            state.disbursement_detail = action.payload.data
        },
    },
});

export default sliceDisbursement;