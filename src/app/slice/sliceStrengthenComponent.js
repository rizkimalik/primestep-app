import { createSlice } from "@reduxjs/toolkit";
import { apiStrengthenComponent } from "app/services/apiStrengthenComponent";

const sliceStrengthenComponent = createSlice({
    name: "strengthen_component",
    initialState: {
        strengthen_component: [],
    },
    extraReducers: {
        [apiStrengthenComponent.fulfilled]: (state, action) => {
            state.strengthen_component = action.payload
        },
    },
});

export default sliceStrengthenComponent;