import { createSlice } from "@reduxjs/toolkit";
import { apiStartupList } from "app/services/apiStartup";

const sliceStartup = createSlice({
    name: "startup",
    initialState: {
        startups: [],
    },
    extraReducers: {
        [apiStartupList.fulfilled]: (state, action) => {
            state.startups = action.payload
        },
    },
});

export default sliceStartup;