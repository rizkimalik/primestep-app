import { createSlice } from "@reduxjs/toolkit";
import { apiDMF_PerformanceHeader, apiDMF_IndicatorHeader, apiDMF_DetailData, apiDMF_IndicatorMaster } from "app/services/apiDMF";

const sliceDMF = createSlice({
    name: "dmf",
    initialState: {
        dmf_performance_header: [],
        dmf_indicator_header: [],
        dmf_data_detail: [],
        dmf_indicator_list: [],
    },
    extraReducers: {
        [apiDMF_PerformanceHeader.fulfilled]: (state, action) => {
            state.dmf_performance_header = action.payload.data
        },
        [apiDMF_IndicatorHeader.fulfilled]: (state, action) => {
            state.dmf_indicator_header = action.payload.data
        },
        [apiDMF_DetailData.fulfilled]: (state, action) => {
            state.dmf_data_detail = action.payload
        },
        [apiDMF_IndicatorMaster.fulfilled]: (state, action) => {
            state.dmf_indicator_list = action.payload
        },
    },
});

export default sliceDMF;