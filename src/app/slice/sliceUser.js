import { createSlice } from "@reduxjs/toolkit";
import { apiUserList, apiUserLevel } from "app/services/apiUser";

const sliceUser = createSlice({
    name: "user",
    initialState: {
        users: [],
        user_level: [],
    },
    extraReducers: {
        [apiUserList.fulfilled]: (state, action) => {
            state.users = action.payload
        },
        [apiUserLevel.fulfilled]: (state, action) => {
            state.user_level = action.payload.data
        },
    },
});

export default sliceUser;