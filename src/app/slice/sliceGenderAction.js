import { createSlice } from "@reduxjs/toolkit";
import { 
    apiGenderActionPivot,
    apiGenderActionMaster,
    apiGenderActionHeader,
    apiGenderActionDetail,
    apiGenderActionMasterShow,
    apiGenderAction_FileUploadList,
} from "app/services/apiGenderAction";

const sliceGenderAction = createSlice({
    name: "gender_action",
    initialState: {
        gender_action_pivot: [],
        gender_action_master: [],
        gender_action_header: [],
        gender_action_detail: [],
        gender_action_master_show: {},
        gap_file_upload: [],
    },
    extraReducers: {
        [apiGenderActionPivot.fulfilled]: (state, action) => {
            state.gender_action_pivot = action.payload.data
        },
        [apiGenderActionMaster.fulfilled]: (state, action) => {
            state.gender_action_master = action.payload
        },
        [apiGenderActionHeader.fulfilled]: (state, action) => {
            state.gender_action_header = action.payload.data
        },
        [apiGenderActionDetail.fulfilled]: (state, action) => {
            state.gender_action_detail = action.payload
        },
        [apiGenderActionMasterShow.fulfilled]: (state, action) => {
            state.gender_action_master_show = action.payload.data
        },
        [apiGenderAction_FileUploadList.fulfilled]: (state, action) => {
            state.gap_file_upload = action.payload
        },
    },
});

export default sliceGenderAction;