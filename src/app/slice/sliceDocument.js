import { createSlice } from "@reduxjs/toolkit";
import { apiDocument_UploadList } from "app/services/apiDocument";

const sliceDocument = createSlice({
    name: "document",
    initialState: {
        document_list: [],
    },
    extraReducers: {
        [apiDocument_UploadList.fulfilled]: (state, action) => {
            state.document_list = action.payload.data
        },
    
    },
});

export default sliceDocument;