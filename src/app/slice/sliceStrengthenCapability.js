import { createSlice } from "@reduxjs/toolkit";
import { 
    apiStrengthenCapability_List,
    apiStrengthenCapability_Show,
    apiStrengthenCapability_InformationStatus,
    apiContractDetail,
    apiPackageStatusDetail,
    apiStrengthen_ContractList,
    apiStrengthenCapability_VerifiedList,
    apiDMFIndicator_List,

    /** Api GOI - PNBP */
    apiStrengthenPNBP_List,
    apiStrengthenPNBP_InformationStatus,
} from "app/services/apiStrengthenCapability";

const sliceStrengthenCapability = createSlice({
    name: "strengthen_capability",
    initialState: {
        strengthen_capability: [],
        strengthen_capability_detail: {},
        strengthen_verified: [],
        // status & docs
        information_status: {},
        contract_detail: {},
        package_status_detail: {},
        // contrak
        contract_list: [],
        dmf_indicator_list: [],

        /** Data GOI - PNBP */
        goi_pnbp: [],
        information_status_pnbp: {},
    },
    extraReducers: {
        [apiStrengthenCapability_List.fulfilled]: (state, action) => {
            state.strengthen_capability = action.payload
        },
        [apiStrengthenCapability_InformationStatus.fulfilled]: (state, action) => {
            state.information_status = action.payload.data[0]
        },
        [apiStrengthenCapability_Show.fulfilled]: (state, action) => {
            state.strengthen_capability_detail = action.payload.data
        },
        [apiContractDetail.fulfilled]: (state, action) => {
            state.contract_detail = action.payload.data
        },
        [apiPackageStatusDetail.fulfilled]: (state, action) => {
            state.package_status_detail = action.payload.data
        },
        [apiStrengthen_ContractList.fulfilled]: (state, action) => {
            state.contract_list = action.payload.data
        },
        [apiStrengthenCapability_VerifiedList.fulfilled]: (state, action) => {
            state.strengthen_verified = action.payload.data
        },
        [apiDMFIndicator_List.fulfilled]: (state, action) => {
            state.dmf_indicator_list = action.payload.data
        },

        /**
         *  GOI - PNBP
         */
        [apiStrengthenPNBP_List.fulfilled]: (state, action) => {
            state.goi_pnbp = action.payload
        },
        [apiStrengthenPNBP_InformationStatus.fulfilled]: (state, action) => {
            state.information_status_pnbp = action.payload.data[0]
        },
    },
});

export default sliceStrengthenCapability;