import { createSlice } from "@reduxjs/toolkit";
import { apiLaporanDokumen_RND, apiLaporan_StatusProposal } from "app/services/apiLaporanDokumen";

const sliceLaporanDokumen = createSlice({
    name: "laporan_dokumen",
    initialState: {
        laporan_dokumen_rnd: [],
        laporan_status_proposal: [],
    },
    extraReducers: {
        [apiLaporanDokumen_RND.fulfilled]: (state, action) => {
            state.laporan_dokumen_rnd = action.payload.data
        },
        [apiLaporan_StatusProposal.fulfilled]: (state, action) => {
            state.laporan_status_proposal = action.payload.data
        },
    },
});

export default sliceLaporanDokumen;