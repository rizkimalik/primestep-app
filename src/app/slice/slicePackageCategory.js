import { createSlice } from "@reduxjs/toolkit";
import { apiPackageCategoryList } from "app/services/apiPackageCategory";

const slicePackageCategory = createSlice({
    name: "package_category",
    initialState: {
        package_category: [],
    },
    extraReducers: {
        [apiPackageCategoryList.fulfilled]: (state, action) => {
            state.package_category = action.payload
        },
    },
});

export default slicePackageCategory;