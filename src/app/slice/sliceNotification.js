import { createSlice } from '@reduxjs/toolkit'
import { apiNotificationProposal, apiNotificationDocument } from 'app/services/apiNotification';

const sliceNotification = createSlice({
    name: 'notification',
    initialState: {
        notification_proposal: [],
        notification_document: [],
    },

    extraReducers: {
        [apiNotificationProposal.fulfilled]: (state, action) => {
            state.notification_proposal = action.payload.data
        },
        [apiNotificationDocument.fulfilled]: (state, action) => {
            state.notification_document = action.payload.data
        },
    },
})

//export reducer
export default sliceNotification;