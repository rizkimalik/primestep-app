import { createSlice } from "@reduxjs/toolkit";
import { apiCategoryFramework } from "app/services/apiCategoryFramework";

const sliceCategoryFramework = createSlice({
    name: "category_framework",
    initialState: {
        category_framework: [],
    },
    extraReducers: {
        [apiCategoryFramework.fulfilled]: (state, action) => {
            state.category_framework = action.payload
        },
    },
});

export default sliceCategoryFramework;