import { createSlice } from "@reduxjs/toolkit";
import { 
    apiIssuesChallengesList,
    apiIssuesChallengesShow,
} from "app/services/apiIssuesChallenges";

const sliceIssuesChallenges = createSlice({
    name: "issues_challenges",
    initialState: {
        issues_challenges: [],
        issue_detail: {},
    },
    extraReducers: {
        [apiIssuesChallengesList.fulfilled]: (state, action) => {
            state.issues_challenges = action.payload
        },
        [apiIssuesChallengesShow.fulfilled]: (state, action) => {
            state.issue_detail = action.payload.data
        },
    },
});

export default sliceIssuesChallenges;