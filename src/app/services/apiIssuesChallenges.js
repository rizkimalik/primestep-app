import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiIssuesChallengesList = createAsyncThunk(
    "issues_challenges/apiIssuesChallengesList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/issues_challenges/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiIssuesChallengesShow = createAsyncThunk(
    "issues_challenges/apiIssuesChallengesShow",
    async ({ id }) => {
        const res = await axios.get(`/issues_challenges/show/${id}`);
        return res.data;
    }
)

export const apiIssuesChallengesInsert = createAsyncThunk(
    "issues_challenges/apiIssuesChallengesInsert",
    async (param) => {
        const res = await axios.post('/issues_challenges/insert', param);
        return res.data;
    }
)

export const apiIssuesChallengesUpdate = createAsyncThunk(
    "issues_challenges/apiIssuesChallengesUpdate",
    async (param) => {
        const res = await axios.put('/issues_challenges/update', param);
        return res.data;
    }
)

export const apiIssuesChallengesDelete = createAsyncThunk(
    "issues_challenges/apiIssuesChallengesDelete",
    async ({ id }) => {
        const res = await axios.delete(`/issues_challenges/delete/${id}`);
        return res.data;
    }
)