import axios from "axios";
import CustomStore from "devextreme/data/custom_store";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const apiUserList = createAsyncThunk(
    "user/apiUserList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/user`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiUserShow = createAsyncThunk(
    "user/apiUserShow",
    async ({ id }) => {
        const res = await axios.get(`/user/show/${id}`);
        return res.data;
    }
)

export const apiUserInsert = createAsyncThunk(
    "user/apiUserInsert",
    async (param) => {
        const res = await axios.post('/user/store', JSON.stringify(param));
        return res.data;
    }
)

export const apiUserUpdate = createAsyncThunk(
    "user/apiUserUpdate",
    async (param) => {
        const res = await axios.put('/user/update', param);
        return res.data;
    }
)

export const apiResetPassword = createAsyncThunk(
    "user/apiResetPassword",
    async (param) => {
        const res = await axios.put('/user/reset_password', param);
        return res.data;
    }
)

export const apiUserDelete = createAsyncThunk(
    "user/apiUserDelete",
    async ({ id }) => {
        const res = await axios.delete(`/user/delete/${id}`);
        return res.data;
    }
)

export const apiUserLevel = createAsyncThunk(
    "user/apiUserLevel",
    async () => {
        const res = await axios.get('/master/user_level');
        return res.data;
    }
)
