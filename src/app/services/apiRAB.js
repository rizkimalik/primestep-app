import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiRAB_List = createAsyncThunk(
    "rab/apiRAB_List",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/rab/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/rab/update`, JSON.stringify(params))
                    .then((result) => console.log(result.statusText))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/rab/delete/${id}`)
                    .then((result) => console.log(result.statusText))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiRAB_UploadExcel = createAsyncThunk(
    "rab/apiRAB_UploadExcel",
    async (param) => {
        const formData = new FormData();
        formData.append('excel_file', param.excel_file[0]);
        formData.append('modul', param.modul);
        formData.append('year', param.year);
        formData.append('institution', param.institution);
        formData.append('package_no', param.package_no);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/rab/document/upload', formData, config);
        return result.data;
    }
)

export const apiRAB_Detail = createAsyncThunk(
    "rab/apiRAB_Detail",
    async (param) => {
        const result = await axios.post('/rab/detail', param);
        return result.data;
    }
)