import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiMasterUserLevel = createAsyncThunk(
    "master/apiMasterUserLevel",
    async () => {
        const res = await axios.get('/master/user_level');
        return res.data;
    }
)

export const apiMasterInstitution = createAsyncThunk(
    "master/apiMasterInstitution",
    async () => {
        const res = await axios.get('/master/institution');
        return res.data;
    }
)

export const apiMasterCatDocStatus = createAsyncThunk(
    "master/apiMasterCatDocStatus",
    async (param) => {
        const res = await axios.post('/master/category_doc_status', param);
        return res.data;
    }
)

export const apiMaster_DocUploadedList = createAsyncThunk(
    "master/apiMaster_DocUploadedList",
    async (param) => {
        const res = await axios.post('/master/docs_uploaded_list', param);
        return res.data;
    }
)

export const apiMasterPackageCategory = createAsyncThunk(
    "master/apiMasterPackageCategory",
    async () => {
        const res = await axios.get('/master/category_package');
        return res.data;
    }
)

export const apiMasterProcurementMethod = createAsyncThunk(
    "master/apiMasterProcurementMethod",
    async (param) => {
        const res = await axios.post('/master/category_proc_method', param);
        return res.data;
    }
)

export const apiMasterProcurementReview = createAsyncThunk(
    "master/apiMasterProcurementReview",
    async (param) => {
        const res = await axios.post('/master/category_proc_review', param);
        return res.data;
    }
)

export const apiMasterCategoryResearch = createAsyncThunk(
    "master/apiMasterCategoryResearch",
    async (param) => {
        const res = await axios.post('/master/category_focus', param);
        return res.data;
    }
)

export const apiMaster_CategoryFramework = createAsyncThunk(
    "master/apiMaster_CategoryFramework",
    async (param) => {
        const res = await axios.post('/master/category_framework', param);
        return res.data;
    }
)

export const apiMaster_DMFPerformance = createAsyncThunk(
    "master/apiMaster_DMFPerformance",
    async (param) => {
        const res = await axios.post('/master/dmf/performance', param);
        return res.data;
    }
)

export const apiMaster_DMFIndicator = createAsyncThunk(
    "master/apiMaster_DMFIndicator",
    async (param) => {
        const res = await axios.post('/master/dmf/indicator', param);
        return res.data;
    }
)

export const apiMaster_CategoryTKT = createAsyncThunk(
    "master/apiMaster_CategoryTKT",
    async (param) => {
        const res = await axios.post('/master/tkt/category', param);
        return res.data;
    }
)

export const apiMaster_IndicatorTKT = createAsyncThunk(
    "master/apiMaster_IndicatorTKT",
    async (param) => {
        const res = await axios.post('/master/tkt/indicator', param);
        return res.data;
    }
)

export const apiMaster_StrengthenComponent = createAsyncThunk(
    "master/apiMaster_StrengthenComponent",
    async (param) => {
        const res = await axios.post('/master/strengthen/component', param);
        return res.data;
    }
)

export const apiMaster_StrengthenCategory = createAsyncThunk(
    "master/apiMaster_StrengthenCategory",
    async (param) => {
        const res = await axios.post('/master/strengthen/category', param);
        return res.data;
    }
)

export const apiMaster_PaguContract = createAsyncThunk(
    "master/apiMaster_PaguContract",
    async (param) => {
        const res = await axios.post('/master/pagu', param);
        return res.data;
    }
)


