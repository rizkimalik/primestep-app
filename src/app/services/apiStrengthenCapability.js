import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

//? component 3
export const apiStrengthenCapability_List = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_List",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/strengthen_capability/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStrengthenCapability_InformationStatus = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_InformationStatus",
    async (param) => {
        const res = await axios.post('/strengthen_capability/information/status', param);
        return res.data;
    }
)

export const apiStrengthenCapability_Show = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_Show",
    async ({ id }) => {
        const res = await axios.get(`/strengthen_capability/show/${id}`);
        return res.data;
    }
)

export const apiStrengthenCapability_Insert = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_Insert",
    async (param) => {
        const res = await axios.post('/strengthen_capability/insert', param);
        return res.data;
    }
)

export const apiStrengthenCapability_Update = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_Update",
    async (param) => {
        const res = await axios.post('/strengthen_capability/update', param);
        return res.data;
    }
)

export const apiStrengthenCapability_Delete = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_Delete",
    async ({ id }) => {
        const res = await axios.delete(`/strengthen_capability/delete/${id}`);
        return res.data;
    }
)

export const apiStrengthenCapability_VerifiedList = createAsyncThunk(
    "strengthen_capability/apiStrengthenCapability_VerifiedList",
    async (param) => {
        const result = await axios.post('/strengthen_capability/verified/list', param);
        return result.data;

        // const store = new CustomStore({
        //     key: 'id',
        //     load: async (options) => {
        //         let params = {
        //             ...param,
        //             ...options
        //         }

        //         return await axios.post(`/strengthen_capability/verified/list`, params)
        //             .then(({ data }) => ({
        //                 data: data.data,
        //                 totalCount: data.total,
        //             }))
        //             .catch((error) => { throw new Error(error); });
        //     },
        // });

        // return store;
    }
)


//? status & document
export const apiUpdate_StatusProgress = createAsyncThunk(
    "strengthen_capability/apiUpdate_StatusProgress",
    async (param) => {
        const result = await axios.post('/strengthen_capability/update/status/progress', param);
        return result.data;
    }
)

export const apiContractDetail = createAsyncThunk(
    "strengthen_capability/apiContractDetail",
    async (param) => {
        const res = await axios.post('/strengthen_capability/contract/detail', JSON.stringify(param));
        return res.data;
    }
)

export const apiDocStatusList = createAsyncThunk(
    "strengthen_capability/apiDocStatusList",
    async () => {
        const res = await axios.post(`/strengthen_capability/docs/status/list`);
        return res.data;
    }
)

export const apiPackageStatusDetail = createAsyncThunk(
    "strengthen_capability/apiPackageStatusDetail",
    async (param) => {
        const result = await axios.post('/strengthen_capability/status/detail', param);
        return result.data;
    }
)

// ? dokumen verifikasi
export const apiStrengthen_StatusVerificationDocument = createAsyncThunk(
    "strengthen_capability/apiStrengthen_StatusVerificationDocument",
    async (param) => {
        const result = await axios.post('/strengthen_capability/status/verifikasi/document', param);
        return result.data;
    }
)

export const apiStrengthen_StatusRevisiDocument = createAsyncThunk(
    "strengthen_capability/apiStrengthen_StatusRevisiDocument",
    async (param) => {
        const result = await axios.post('/strengthen_capability/status/revisi/document', param);
        return result.data;
    }
)

export const apiStrengthen_HistoryVerificationDocument = createAsyncThunk(
    "strengthen_capability/apiStrengthen_HistoryVerificationDocument",
    async (param) => {
        const result = await axios.post('/strengthen_capability/history/verification/document', param);
        return result.data;
    }
)

export const apiCheck_VerificationDocument = createAsyncThunk(
    "strengthen_capability/apiCheck_VerificationDocument",
    async (param) => {
        const result = await axios.post('/strengthen_capability/check/verification/document', param);
        return result.data;
    }
)

//? contract
export const apiStrengthen_StatusContract = createAsyncThunk(
    "strengthen_capability/apiStrengthen_StatusContract",
    async (param) => {
        const result = await axios.post('/strengthen_capability/status/update/contract', param);
        return result.data;
    }
)

export const apiStrengthen_StatusWithdrawal = createAsyncThunk(
    "strengthen_capability/apiStrengthen_StatusWithdrawal",
    async (param) => {
        const result = await axios.post('/strengthen_capability/status/update/withdrawal', param);
        return result.data;
    }
)


//? list Contract API
export const apiStrengthen_ContractList = createAsyncThunk(
    "strengthen_capability/apiStrengthen_ContractList",
    async (param) => {
        const result = await axios.post('/strengthen_capability/contract/list', param);
        return result.data;
    }
)

export const apiDisbursement_Perquarter = createAsyncThunk(
    "strengthen_capability/apiDisbursement_Perquarter",
    async (param) => {
        const result = await axios.post('/strengthen_capability/disbursement/perquarter', param);
        return result.data;
    }
)

export const apiDMFIndicator_List = createAsyncThunk(
    "strengthen_capability/apiDMFIndicator_List",
    async () => {
        const result = await axios.post('/strengthen_capability/dmf/indicator');
        return result.data;
    }
)


/**
 *  Export Data Modul
 */

export const apiExport_Excel = createAsyncThunk(
    "strengthen_capability/apiExport_Excel",
    async (param) => {
        const result = await axios.post('/strengthen_capability/export/excel', param);
        return result.data;
    }
)


/**
 *  Modul GOI / PNBP
 */
export const apiStrengthenPNBP_List = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_List",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/strengthen_pnbp/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStrengthenPNBP_InformationStatus = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_InformationStatus",
    async (param) => {
        const res = await axios.post('/strengthen_pnbp/information/status', param);
        return res.data;
    }
)


export const apiStrengthenPNBP_Show = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_Show",
    async ({ id }) => {
        const res = await axios.get(`/strengthen_pnbp/show/${id}`);
        return res.data;
    }
)

export const apiStrengthenPNBP_Insert = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_Insert",
    async (param) => {
        const res = await axios.post('/strengthen_pnbp/insert', param);
        return res.data;
    }
)

export const apiStrengthenPNBP_Update = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_Update",
    async (param) => {
        const res = await axios.post('/strengthen_pnbp/update', param);
        return res.data;
    }
)

export const apiStrengthenPNBP_Delete = createAsyncThunk(
    "strengthen_capability/apiStrengthenPNBP_Delete",
    async ({ id }) => {
        const res = await axios.delete(`/strengthen_pnbp/delete/${id}`);
        return res.data;
    }
)

export const apiPNBP_ExportExcel = createAsyncThunk(
    "strengthen_capability/apiPNBP_ExportExcel",
    async (param) => {
        const result = await axios.post('/strengthen_pnbp/export/excel', param);
        return result.data;
    }
)