import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiEvaluator_List = createAsyncThunk(
    "evaluator/apiEvaluator_List",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/evaluator/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)


export const apiEvaluator_Detail = createAsyncThunk(
    "evaluator/apiEvaluator_Detail",
    async (param) => {
        const res = await axios.post(`/evaluator/detail`, param);
        return res.data;
    }
)

export const apiEvaluator_InsertUpdate = createAsyncThunk(
    "evaluator/apiEvaluator_InsertUpdate",
    async (param) => {
        let result = '';
        if (param.id) {
            result = await axios.post('/evaluator/update', param);
        } else {
            result = await axios.post('/evaluator/insert', param);
        }
        return result.data;
    }
)

export const apiEvaluator_Delete = createAsyncThunk(
    "evaluator/apiEvaluator_Delete",
    async ({ id }) => {
        const res = await axios.delete(`/evaluator/delete/${id}`);
        return res.data;
    }
)