import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiGenderActionPivot = createAsyncThunk(
    "gender_action/apiGenderActionPivot",
    async (param) => {
        const res = await axios.post('/gender_action/pivot', param);
        return res.data;
    }
)

export const apiGenderActionHeader = createAsyncThunk(
    "gender_action/apiGenderActionHeader",
    async (param) => {
        const res = await axios.post('/gender_action/header', param);
        return res.data;
    }
)

export const apiGenderActionInsert = createAsyncThunk(
    "gender_action/apiGenderActionInsert",
    async (param) => {
        const formData = new FormData();
        formData.append('gap_id', param.gap_id);
        formData.append('piu', param.piu);
        formData.append('year', param.year);
        formData.append('amount', param.amount);
        formData.append('gender', param.gender);
        formData.append('action_date', param.action_date);
        formData.append('comments', param.comments);
        // file data
        formData.append('document_file', param?.document_file[0]);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const res = await axios.post('/gender_action/detail/insert', formData, config);
        return res.data;
    }
)

export const apiGenderActionDetail = createAsyncThunk(
    "gender_action/apiGenderActionDetail",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/gender_action/detail/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/gender_action/detail/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/gender_action/detail/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiGenderAction_FileUpload = createAsyncThunk(
    "gender_action/apiGenderAction_FileUpload",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('status', param.status);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const res = await axios.post('/gender_action/file/upload', formData, config);
        return res.data;
    }
)

export const apiGenderAction_FileUploadList = createAsyncThunk(
    "gender_action/apiGenderAction_FileUploadList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/gender_action/file/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/gender_action/file/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)


//? master data GAP
export const apiGenderActionMaster = createAsyncThunk(
    "gender_action/apiGenderActionMaster",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/gender_action/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/gender_action/insert`, JSON.stringify(values))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/gender_action/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/gender_action/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiGenderActionMasterShow = createAsyncThunk(
    "gender_action/apiGenderActionMasterShow",
    async ({ id }) => {
        const res = await axios.get(`/gender_action/show/${id}`);
        return res.data;
    }
)
