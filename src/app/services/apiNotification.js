import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiNotificationProposal = createAsyncThunk(
    "notification/apiNotificationProposal",
    async (param) => {
        const res = await axios.post('/notification/proposal', param);
        return res.data;
    }
)

export const apiNotificationDocument = createAsyncThunk(
    "notification/apiNotificationDocument",
    async (param) => {
        const res = await axios.post('/notification/document', param);
        return res.data;
    }
)

