import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiDisbursementMain = createAsyncThunk(
    "disbursement/apiDisbursementMain",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/disbursement/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/disbursement/insert`, JSON.stringify(values))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/disbursement/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/disbursement/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiDisbursementList = createAsyncThunk(
    "disbursement/apiDisbursementList",
    async (param) => {
        const res = await axios.post('/disbursement/list', param);
        return res.data;


    }
)

export const apiDisbursementUpdateInsert = createAsyncThunk(
    "disbursement/apiDisbursementUpdateInsert",
    async (param) => {
        if (param.id) {
            const res = await axios.post('/disbursement/update', param);
            return res.data;
        } else {
            const res = await axios.post('/disbursement/insert', param);
            return res.data;
        }

    }
)

export const apiDisbursementShow = createAsyncThunk(
    "disbursement/apiDisbursementShow",
    async ({ id }) => {
        const res = await axios.get(`/disbursement/show/${id}`);
        return res.data;
    }
)

export const apiDisbursementDelete = createAsyncThunk(
    "disbursement/apiDisbursementDelete",
    async ({ id }) => {
        const res = await axios.delete(`/disbursement/delete/${id}`);
        return res.data;
    }
)
