import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiDocumentStatusList = createAsyncThunk(
    "document_status/apiDocumentStatusList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/document_status`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/document_status/insert`, JSON.stringify(values))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/document_status/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/document_status/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiDocumentStatusShow = createAsyncThunk(
    "document_status/apiDocumentStatusShow",
    async ({ id }) => {
        const res = await axios.get(`/document_status/show/${id}`);
        return res.data;
    }
)
