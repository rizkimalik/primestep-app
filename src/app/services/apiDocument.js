import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
// import CustomStore from "devextreme/data/custom_store";

export const apiDocument_UploadList = createAsyncThunk(
    "document/apiDocument_UploadList",
    async (param) => {
        const res = await axios.post(`/document/list`, param);
        return res.data;
    }
)

export const apiDocument_UploadDelete = createAsyncThunk(
    "document/apiDocument_UploadDelete",
    async ({ id }) => {
        const res = await axios.delete(`/document/delete/${id}`);
        return res.data;
    }
)

export const apiDocument_Upload = createAsyncThunk(
    "document/apiDocument_Upload",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_for', param.document_for);
        formData.append('document_name', param.document_name);
        formData.append('document_section', param?.document_section);
        formData.append('status', param.status);
        formData.append('description', param.description);
        formData.append('institution', param.institution);
        formData.append('user_upload', param.user_upload);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/document/upload', formData, config);
        return result.data;
    }
)

export const apiDocument_Update = createAsyncThunk(
    "document/apiDocument_Update",
    async (param) => {
        const res = await axios.post(`/document/update`, param);
        return res.data;
    }
)


// export const apiDocument_UploadList = createAsyncThunk(
//     "document/apiDocument_UploadList",
//     async (param) => {
//         const store = new CustomStore({
//             key: 'id',
//             load: async (options) => {
//                 let params = {
//                     ...param,
//                     ...options
//                 }

//                 return await axios.post(`/document/list`, params)
//                     .then(({ data }) => ({
//                         data: data.data,
//                         totalCount: data.total,
//                     }))
//                     .catch((error) => { throw new Error(error); });
//             },
//         });

//         return store;
//     }
// )