import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

//? component 2
export const apiStartupResearch_StartupList = createAsyncThunk(
    "startup_research/apiStartupResearch_StartupList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/startup/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStartupResearch_ResearchList = createAsyncThunk(
    "startup_research/apiStartupResearch_ResearchList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/research/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStartup_AccelerationList = createAsyncThunk(
    "startup_research/apiStartup_AccelerationList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/startup/acceleration`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStartup_AccelerationSelect = createAsyncThunk(
    "startup_research/apiStartup_AccelerationSelect",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/select/acceleration`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStartupResearch_InformationStatus = createAsyncThunk(
    "startup_research/apiStartupResearch_InformationStatus",
    async (param) => {
        const res = await axios.post('/startup_research/information/status', param);
        return res.data;
    }
)

export const apiStartupResearch_Detail = createAsyncThunk(
    "startup_research/apiStartupResearch_Detail",
    async (param) => {
        const res = await axios.post('/startup_research/detail', param);
        return res.data;
    }
)

export const apiStartupResearch_Show = createAsyncThunk(
    "startup_research/apiStartupResearch_Show",
    async ({ id }) => {
        const res = await axios.get(`/startup_research/show/${id}`);
        return res.data;
    }
)

export const apiStartupResearch_Insert = createAsyncThunk(
    "startup_research/apiStartupResearch_Insert",
    async (param) => {
        const res = await axios.post('/startup_research/insert', param);
        return res.data;
    }
)

export const apiStartupResearch_Update = createAsyncThunk(
    "startup_research/apiStartupResearch_Update",
    async (param) => {
        const res = await axios.post('/startup_research/update', param);
        return res.data;
    }
)

export const apiStartupResearch_Delete = createAsyncThunk(
    "startup_research/apiStartupResearch_Delete",
    async ({ id }) => {
        const res = await axios.delete(`/startup_research/delete/${id}`);
        return res.data;
    }
)

export const apiStartupResearch_SearchUsulan = createAsyncThunk(
    "startup_research/apiStartupResearch_SearchUsulan",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/search/usulan`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiStartupResearch_RecommendedList = createAsyncThunk(
    "startup_research/apiStartupResearch_RecommendedList",
    async (param) => {
        const result = await axios.post(`/startup_research/recommended/list`, param);
        return result.data.data;

        // const store = new CustomStore({
        //     key: 'id',
        //     load: async (options) => {
        //         let params = {
        //             ...param,
        //             ...options
        //         }

        //         return await axios.post(`/startup_research/recommended/list`, params)
        //             .then(({ data }) => ({
        //                 data: data.data,
        //                 totalCount: data.data.length,
        //             }))
        //             .catch((error) => { throw new Error(error); });
        //     },
        // });

        // return store;
    }
)

export const apiCheck_VerificationProposal = createAsyncThunk(
    "startup_research/apiCheck_VerificationProposal",
    async (param) => {
        const result = await axios.post('/startup_research/check/verification/proposal', param);
        return result.data;
    }
)

export const apiCheck_VerificationDocument = createAsyncThunk(
    "startup_research/apiCheck_VerificationDocument",
    async (param) => {
        const result = await axios.post('/startup_research/check/verification/document', param);
        return result.data;
    }
)

//? Tahap Usulan
export const apiUpdateStatus_TahapUsulan = createAsyncThunk(
    "startup_research/apiUpdateStatus_TahapUsulan",
    async (param) => {
        const result = await axios.post('/startup_research/status/tahap_usulan', param);
        return result.data;
    }
)

export const apiStepUpdate_TargetPendapatan = createAsyncThunk(
    "startup_research/apiStepUpdate_TargetPendapatan",
    async (param) => {
        const result = await axios.post('/startup_research/step/update/target', param);
        return result.data;
    }
)

//? Tahap Verifikasi & Kontrak
export const apiUpdateStatus_TahapVerification = createAsyncThunk(
    "startup_research/apiUpdateStatus_TahapVerification",
    async (param) => {
        const result = await axios.post('/startup_research/status/tahap_verifikasi', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusPersiapan = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusPersiapan",
    async (param) => {
        const result = await axios.post('/startup_research/status/update/persiapan', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusVerificationDocument = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusVerificationDocument",
    async (param) => {
        const result = await axios.post('/startup_research/status/verifikasi/document', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusVerificationProposal = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusVerificationProposal",
    async (param) => {
        const result = await axios.post('/startup_research/status/verifikasi/proposal', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusRevisiDocument = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusRevisiDocument",
    async (param) => {
        const result = await axios.post('/startup_research/status/revisi/document', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusRevisiProposal = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusRevisiProposal",
    async (param) => {
        const result = await axios.post('/startup_research/status/revisi/proposal', param);
        return result.data;
    }
)

export const apiStartupResearch_HistoryVerificationDocument = createAsyncThunk(
    "startup_research/apiStartupResearch_HistoryVerificationDocument",
    async (param) => {
        const result = await axios.post('/startup_research/history/verification/document', param);
        return result.data;
    }
)

export const apiStartupResearch_HistoryVerificationProposal = createAsyncThunk(
    "startup_research/apiStartupResearch_HistoryVerificationProposal",
    async (param) => {
        const result = await axios.post('/startup_research/history/verification/proposal', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusContract = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusContract",
    async (param) => {
        const result = await axios.post('/startup_research/status/update/contract', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusWithdrawal = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusWithdrawal",
    async (param) => {
        const result = await axios.post('/startup_research/status/update/withdrawal', param);
        return result.data;
    }
)

export const apiStartupResearch_StatusFinish = createAsyncThunk(
    "startup_research/apiStartupResearch_StatusFinish",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_name', param.document_name);
        formData.append('status', param.status);
        formData.append('submodul', param.submodul);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/startup_research/status/update/finish', formData, config);
        return result.data;
    }
)

//? document api
export const apiInsertDocsUploaded = createAsyncThunk(
    "startup_research/apiInsertDocsUploaded",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_name', param.document_name);
        formData.append('status', param.status);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const res = await axios.post('/startup_research/docs/uploaded/insert', formData, config);
        return res.data;
    }
)
export const apiListDocsUploaded = createAsyncThunk(
    "startup_research/apiListDocsUploaded",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/docs/uploaded`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiDoscUploadedDelete = createAsyncThunk(
    "startup_research/apiDoscUploadedDelete",
    async ({ id }) => {
        const res = await axios.delete(`/startup_research/docs/delete/${id}`);
        return res.data;
    }
)

export const apiDocStatusList = createAsyncThunk(
    "startup_research/apiDocStatusList",
    async () => {
        const res = await axios.post(`/startup_research/docs/status/list`);
        return res.data;
    }
)

export const apiPackageStatusDetail = createAsyncThunk(
    "startup_research/apiPackageStatusDetail",
    async (param) => {
        const result = await axios.post('/startup_research/status/detail', param);
        return result.data;
    }
)

//? list Contract API
export const apiStartupResearch_ContractList = createAsyncThunk(
    "startup_research/apiStartupResearch_ContractList",
    async (param) => {
        const result = await axios.post('/startup_research/contract/list', param);
        return result.data;
    }
)

export const apiStartupResearch_AssignEvaluator = createAsyncThunk(
    "startup_research/apiStartupResearch_AssignEvaluator",
    async (param) => {
        const result = await axios.post('/startup_research/assign/evaluator', param);
        return result.data;
    }
)

//? api member
export const apiInnovationMember = createAsyncThunk(
    "startup_research/apiInnovationMember",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup_research/innovation_member/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                let params = {
                    ...param,
                    ...values,
                }
                return await axios.post(`/startup_research/innovation_member/insert`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/startup_research/innovation_member/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/startup_research/innovation_member/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiInnovation_MemberDetail = createAsyncThunk(
    "startup_research/apiInnovation_MemberDetail",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_member/detail', param);
        return result.data;
    }
)

export const apiInnovation_MemberInsertUpdate = createAsyncThunk(
    "startup_research/apiInnovation_MemberInsertUpdate",
    async (param) => {
        let result = '';
        if (param.id) {
            result = await axios.post('/startup_research/innovation_member/update', param);
        } else {
            result = await axios.post('/startup_research/innovation_member/insert', param);
        }
        return result.data;
    }
)

export const apiInnovation_MemberDelete = createAsyncThunk(
    "startup_research/apiInnovation_MemberDelete",
    async ({ id }) => {
        const res = await axios.delete(`/startup_research/innovation_member/delete/${id}`);
        return res.data;
    }
)


//? api outer / Luaran - tahap 2
export const apiInnovation_OuterList = createAsyncThunk(
    "startup_research/apiInnovation_OuterList",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_outer/list', param);
        return result.data;
    }
)

export const apiInnovation_OuterDetail = createAsyncThunk(
    "startup_research/apiInnovation_OuterDetail",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_outer/detail', param);
        return result.data;
    }
)

export const apiInnovation_OuterInsertUpdate = createAsyncThunk(
    "startup_research/apiInnovation_OuterInsertUpdate",
    async (param) => {
        let result = '';
        if (param.id) {
            result = await axios.post('/startup_research/innovation_outer/update', param);
        } else {
            result = await axios.post('/startup_research/innovation_outer/insert', param);
        }
        return result.data;
    }
)

export const apiInnovation_OuterDelete = createAsyncThunk(
    "startup_research/apiInnovation_OuterDelete",
    async ({ id }) => {
        const res = await axios.delete(`/startup_research/innovation_outer/delete/${id}`);
        return res.data;
    }
)

export const apiInnovation_OuterDocument = createAsyncThunk(
    "startup_research/apiInnovation_OuterDocument",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('outer_code', param.outer_code);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/startup_research/innovation_outer/document/upload', formData, config);
        return result.data;
    }
)


//? Luaran Publikasi
export const apiInnovation_OutertUpdatePublication = createAsyncThunk(
    "startup_research/apiInnovation_OutertUpdatePublication",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_outer/update/publication', param);
        return result.data;
    }
)

export const apiInnovation_OutertDetailPublication = createAsyncThunk(
    "startup_research/apiInnovation_OutertDetailPublication",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_outer/detail/publication', param);
        return result.data;
    }
)

//? api Target Luaran - Inkubasi
export const apiOuterIncubation_Detail = createAsyncThunk(
    "startup_research/apiOuterIncubation_Detail",
    async (param) => {
        const result = await axios.post('/startup_research/outer_incubation/detail', param);
        return result.data;
    }
)

export const apiOuterIncubation_InsertUpdate = createAsyncThunk(
    "startup_research/apiOuterIncubation_InsertUpdate",
    async (param) => {
        const result = await axios.post('/startup_research/outer_incubation/insert_update', param);
        return result.data;
    }
)


//? api partner / Dok Pendukung - tahap 4
export const apiInnovation_PartnerList = createAsyncThunk(
    "startup_research/apiInnovation_PartnerList",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_partner/list', param);
        return result.data;
    }
)

export const apiInnovation_PartnerDetail = createAsyncThunk(
    "startup_research/apiInnovation_PartnerDetail",
    async (param) => {
        const result = await axios.post('/startup_research/innovation_partner/detail', param);
        return result.data;
    }
)

export const apiInnovation_PartnerInsertUpdate = createAsyncThunk(
    "startup_research/apiInnovation_PartnerInsertUpdate",
    async (param) => {
        let result = '';
        if (param.id) {
            result = await axios.post('/startup_research/innovation_partner/update', param);
        } else {
            result = await axios.post('/startup_research/innovation_partner/insert', param);
        }
        return result.data;
    }
)

export const apiInnovation_PartnerDelete = createAsyncThunk(
    "startup_research/apiInnovation_PartnerDelete",
    async ({ id }) => {
        const res = await axios.delete(`/startup_research/innovation_partner/delete/${id}`);
        return res.data;
    }
)

//? history TKT
export const apiHistoryTKT_Insert = createAsyncThunk(
    "startup_research/apiHistoryTKT_Insert",
    async (param) => {
        const result = await axios.post('/startup_research/history_tkt/insert', param);
        return result.data;
    }
)

export const apiHistoryTKT_List = createAsyncThunk(
    "startup_research/apiHistoryTKT_List",
    async (param) => {
        const result = await axios.post('/startup_research/history_tkt/list', param);
        return result.data;
    }
)

/**
 *  Export Data Modul
 */

export const apiExport_Excel = createAsyncThunk(
    "startup_research/apiExport_Excel",
    async (param) => {
        const result = await axios.post('/startup_research/export/excel', param);
        return result.data;
    }
)
