import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiContractUpdate = createAsyncThunk(
    "contract/apiContractUpdate",
    async (param) => {
        const result = await axios.post('/contract/update', param);
        return result.data;
    }
)

export const apiContractDetail = createAsyncThunk(
    "contract/apiContractDetail",
    async (param) => {
        const res = await axios.post('/contract/detail', param);
        return res.data;
    }
)