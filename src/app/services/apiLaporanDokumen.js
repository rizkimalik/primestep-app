import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const apiLaporanDokumen_RND = createAsyncThunk(
    "laporan_dokumen/apiLaporanDokumen_RND",
    async (param) => {
        const res = await axios.post('/laporan/rnd', param);
        return res.data;
    }
)

export const apiLaporan_StatusProposal = createAsyncThunk(
    "laporan_dokumen/apiLaporan_StatusProposal",
    async (param) => {
        const res = await axios.post('/laporan/proposal', param);
        return res.data;
    }
)