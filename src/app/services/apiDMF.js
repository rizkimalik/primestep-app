import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiDMF_DataPivot = createAsyncThunk(
    "dmf/apiDMF_DataPivot",
    async (param) => {
        const res = await axios.post('/dmf/data/pivot', param);
        return res.data;
    }
)

export const apiDMF_PerformanceHeader = createAsyncThunk(
    "dmf/apiDMF_PerformanceHeader",
    async (param) => {
        const res = await axios.post('/dmf/performance/header', param);
        return res.data;
    }
)

export const apiDMF_IndicatorHeader = createAsyncThunk(
    "dmf/apiDMF_IndicatorHeader",
    async (param) => {
        const res = await axios.post('/dmf/indicator/header', param);
        return res.data;
    }
)

export const apiDMF_DetailInsert = createAsyncThunk(
    "dmf/apiDMF_DetailInsert",
    async (param) => {
        const res = await axios.post('/dmf/detail/insert', param);
        return res.data;
    }
)

export const apiDMF_DetailData = createAsyncThunk(
    "dmf/apiDMF_DetailData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/dmf/detail/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/dmf/detail/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/dmf/detail/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

//? master data Indicator
export const apiDMF_IndicatorMaster = createAsyncThunk(
    "dmf/apiDMF_IndicatorMaster",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/dmf/indicator/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/dmf/indicator/insert`, JSON.stringify(values))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/dmf/indicator/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/dmf/indicator/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)