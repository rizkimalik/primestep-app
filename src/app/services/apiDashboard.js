import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from 'devextreme/data/custom_store';

export const apiDashboard_TotalProgress = createAsyncThunk(
    "dashboard/apiDashboard_TotalProgress",
    async (params) => {
        const res = await axios.post('/dashboard/total/disbursed_and_packages', params);
        return res.data;
    }
)

export const apiDashboard_LoanMilestone = createAsyncThunk(
    "dashboard/apiDashboard_LoanMilestone",
    async (params) => {
        const res = await axios.post('/dashboard/loan_milestone', params);
        return res.data;
    }
)

export const apiDashboard_LoanProgressPermonth= createAsyncThunk(
    "dashboard/apiDashboard_LoanProgressPermonth",
    async (params) => {
        const res = await axios.post('/dashboard/loan_progress_permonth', params);
        return res.data;
    }
)

//? Procurement Packages Total
export const apiDashboard_ProcurementPackagesUI = createAsyncThunk(
    "dashboard/apiDashboard_ProcurementPackagesUI",
    async (params) => {
        const res = await axios.post('/dashboard/total/procurement_progress', params);
        return res.data;
    }
)

export const apiDashboard_ProcurementPackagesUGM = createAsyncThunk(
    "dashboard/apiDashboard_ProcurementPackagesUGM",
    async (params) => {
        const res = await axios.post('/dashboard/total/procurement_progress', params);
        return res.data;
    }
)

export const apiDashboard_ProcurementPackagesITB = createAsyncThunk(
    "dashboard/apiDashboard_ProcurementPackagesITB",
    async (params) => {
        const res = await axios.post('/dashboard/total/procurement_progress', params);
        return res.data;
    }
)

export const apiDashboard_ProcurementPackagesIPB = createAsyncThunk(
    "dashboard/apiDashboard_ProcurementPackagesIPB",
    async (params) => {
        const res = await axios.post('/dashboard/total/procurement_progress', params);
        return res.data;
    }
)

export const apiDashboard_ProcurementPackagesPMU = createAsyncThunk(
    "dashboard/apiDashboard_ProcurementPackagesPMU",
    async (params) => {
        const res = await axios.post('/dashboard/total/procurement_progress', params);
        return res.data;
    }
)

export const apiDashboard_EvaluatorTotal = createAsyncThunk(
    "dashboard/apiDashboard_EvaluatorTotal",
    async (params) => {
        const res = await axios.post('/dashboard/evaluator/total', params);
        return res.data;
    }
)

export const apiDashboard_EvaluatorData = createAsyncThunk(
    "dashboard/apiDashboard_EvaluatorData",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/dashboard/evaluator/data`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)


