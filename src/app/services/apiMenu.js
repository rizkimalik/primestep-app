import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl, apiHeaders } from "app/config";

export const getMainMenu = createAsyncThunk(
    "mainmenu/main_menu",
    async ({ user_level }) => {
        const res = await axios.get(`/menu/main_menu/${user_level}`);
        return res.data;
    }
);

export const apiMenu = createAsyncThunk(
    "mainmenu/apiMenu",
    async () => {
        const res = await axios.get(`/menu/menu`);
        return res.data;
    }
);

export const apiModul = createAsyncThunk(
    "mainmenu/apiModul",
    async ({ menu_id }) => {
        const res = await axios.get(`/menu/menu_modul/${menu_id}`);
        return res.data;
    }
);

export const apiMenuAccess = createAsyncThunk(
    "mainmenu/apiMenuAccess",
    async ({ user_level }) => {
        const res = await axios.get(`/menu/menu_access?user_level=${user_level}`);
        return res.data;
    }
);

export const apiModulAccess = createAsyncThunk(
    "mainmenu/apiModulAccess",
    async ({ user_level, menu_id }) => {
        const res = await axios.get(`/menu/modul_access?user_level=${user_level}&menu_id=${menu_id}`);
        return res.data;
    }
);

export const apiMenuAccessInsert = createAsyncThunk(
    "mainmenu/apiMenuAccessInsert",
    async (param) => {
        const res = await axios.post(`/menu/store_access`, param);
        return res.data;
    }
);

export const apiMenuAccessDelete = createAsyncThunk(
    "mainmenu/apiMenuAccessDelete",
    async ({ id }) => {
        const res = await axios.delete(`/menu/delete_access/${id}`);
        return res.data;
    }
);



// export const menu_access = createApi({
//     reducerPath: 'menu_access',
//     baseQuery: fetchBaseQuery({
//         baseUrl,
//         prepareHeaders: (headers, { getState }) => {
//             const token = getState().persistedReducer.authUser.token;
//             if (token) {
//                 headers.set('Authorization', `Bearer ${token}`)
//             }
//             return headers
//         }
//     }),
//     endpoints: (builder) => ({
//         // getMenu: builder.query({
//         //     query: () => queryRequest(`/menu/menu`),
//         // }),
//         // getMenuModul: builder.query({
//         //     query: (menu_id) => queryRequest(`/menu/menu_modul/${menu_id}`),
//         // }),
//         // getMenuSubModul: builder.query({
//         //     query: (menu_modul_id) => queryRequest(`/menu/menu_submodul/${menu_modul_id}`),
//         // }),
//         // getMenuAccess: builder.query({
//         //     query: (user_level) => queryRequest(`/menu/menu_access?user_level=${user_level}`),
//         // }),
//         // getModulAccess: builder.query({
//         //     query: ({ user_level, menu_id }) => queryRequest(`/menu/modul_access?user_level=${user_level}&menu_id=${menu_id}`),
//         // }),
//         createMenuAccess: builder.mutation({
//             query: (body) => ({
//                 url: '/menu/store_access',
//                 method: 'POST',
//                 headers: apiHeaders,
//                 body,
//             }),
//         }),
//         deleteMenuAccess: builder.mutation({
//             query: (id) => ({
//                 url: `/menu/delete_access/${id}`,
//                 method: 'DELETE',
//                 headers: apiHeaders,
//             }),
//         }),
//     })
// });

// export const {
//     useGetMenuQuery,
//     useGetMenuModulQuery,
//     useGetMenuSubModulQuery,
//     useGetMenuAccessQuery,
//     useGetModulAccessQuery,
//     useCreateMenuAccessMutation,
//     useDeleteMenuAccessMutation,
// } = menu_access;
// export default menu_access;
