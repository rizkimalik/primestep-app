import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiPaguContract = createAsyncThunk(
    "pagu_contract/apiPaguContract",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/pagu/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/pagu/insert`, JSON.stringify(values))
                    .then((result) => console.log(result.status))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/pagu/update`, JSON.stringify(params))
                    .then((result) => console.log(result.status))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/pagu/delete/${id}`)
                    .then((result) => console.log(result.status))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)
