import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiProcurementPackageList = createAsyncThunk(
    "procurement/apiProcurementPackageList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/procurement/package/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiProcurement_InformationStatus = createAsyncThunk(
    "procurement/apiProcurement_InformationStatus",
    async (param) => {
        const res = await axios.post('/procurement/package/information/status', param);
        return res.data;
    }
)

export const apiProcurementPackageShow = createAsyncThunk(
    "procurement/apiProcurementPackageShow",
    async ({ id }) => {
        const res = await axios.get(`/procurement/package/show/${id}`);
        return res.data;
    }
)

export const apiProcurementPackageInsert = createAsyncThunk(
    "procurement/apiProcurementPackageInsert",
    async (param) => {
        const res = await axios.post('/procurement/package/insert', param);
        return res.data;
    }
)

export const apiProcurementPackageUpdate = createAsyncThunk(
    "procurement/apiProcurementPackageUpdate",
    async (param) => {
        const res = await axios.put('/procurement/package/update', param);
        return res.data;
    }
)

export const apiProcurementPackageDelete = createAsyncThunk(
    "procurement/apiProcurementPackageDelete",
    async ({ id }) => {
        const res = await axios.delete(`/procurement/package/delete/${id}`);
        return res.data;
    }
)

//package detail
export const apiProcurementPackageDetailList = createAsyncThunk(
    "procurement/apiProcurementPackageDetailList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/procurement/package/detail_list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiProcurementPackageDetailShow = createAsyncThunk(
    "procurement/apiProcurementPackageDetailShow",
    async ({ id }) => {
        const res = await axios.get(`/procurement/package/detail_show/${id}`);
        return res.data;
    }
)

export const apiProcurementPackageDetailInsert = createAsyncThunk(
    "procurement/apiProcurementPackageDetailInsert",
    async (param) => {
        const res = await axios.post('/procurement/package/detail_insert', param);
        return res.data;
    }
)

export const apiProcurementPackageDetailUpdate = createAsyncThunk(
    "procurement/apiProcurementPackageDetailUpdate",
    async (param) => {
        const res = await axios.put('/procurement/package/detail_update', param);
        return res.data;
    }
)

export const apiProcurementPackageDetailDelete = createAsyncThunk(
    "procurement/apiProcurementPackageDetailDelete",
    async ({ id }) => {
        const res = await axios.delete(`/procurement/package/detail_delete/${id}`);
        return res.data;
    }
)

export const apiPackageCategoryList = createAsyncThunk(
    "procurement/apiPackageCategoryList",
    async () => {
        const res = await axios.get(`/procurement/package/category_list`);
        return res.data;
    }
)

export const apiPackageStatusDetail = createAsyncThunk(
    "procurement/apiPackageStatusDetail",
    async (param) => {
        const result = await axios.post('/procurement/package/status/detail', param);
        return result.data;
    }
)

//?  status & dokumen
export const apiProcurement_UpdateStatus = createAsyncThunk(
    "procurement/apiProcurement_UpdateStatus",
    async (param) => {
        const result = await axios.post('/procurement/package/update/status', param);
        return result.data;
    }
)

export const apiProcurementStatusPlanning = createAsyncThunk(
    "procurement/apiProcurementStatusPlanning",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_name', param.document_name);
        formData.append('status', param.status);
        formData.append('description', param.description);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/procurement/package/update/planning', formData, config);
        return result.data;
    }
)

export const apiProcurementStatusImplementation = createAsyncThunk(
    "procurement/apiProcurementStatusImplementation",
    async (param) => {
        const result = await axios.post('/procurement/package/update/implementation', param);
        return result.data;
    }
)

export const apiProcurementStatusContract = createAsyncThunk(
    "procurement/apiProcurementStatusContract",
    async (param) => {
        const result = await axios.post('/procurement/package/update/contract', param);
        return result.data;
    }
)

export const apiProcurementStatusFinish = createAsyncThunk(
    "procurement/apiProcurementStatusFinish",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_name', param.document_name);
        formData.append('status', param.status);
        formData.append('description', param.description);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/procurement/package/update/finish', formData, config);
        return result.data;
    }
)

export const apiInsertDocsUploaded = createAsyncThunk(
    "procurement/apiInsertDocsUploaded",
    async (param) => {
        const formData = new FormData();
        formData.append('document_file', param.document_file[0]);
        formData.append('package_no', param.package_no);
        formData.append('document_name', param.document_name);
        formData.append('status', param.status);
        formData.append('description', param.description);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const res = await axios.post('/procurement/package/docs/uploaded/insert', formData, config);
        return res.data;
    }
)
export const apiListDocsUploaded = createAsyncThunk(
    "procurement/apiListDocsUploaded",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/procurement/package/docs/uploaded`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiDoscUploadedDelete = createAsyncThunk(
    "procurement/apiDoscUploadedDelete",
    async ({ id }) => {
        const res = await axios.delete(`/procurement/package/docs/delete/${id}`);
        return res.data;
    }
)

export const apiDocStatusList = createAsyncThunk(
    "procurement/apiDocStatusList",
    async () => {
        const res = await axios.post(`/procurement/package/docs/status`);
        return res.data;
    }
)


/**
 *  Export Data Modul
 */

export const apiExport_Excel = createAsyncThunk(
    "procurement/apiExport_Excel",
    async (param) => {
        const result = await axios.post('/procurement/export/excel', param);
        return result.data;
    }
)
