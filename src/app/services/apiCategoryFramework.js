import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiCategoryFramework = createAsyncThunk(
    "category_framework/apiCategoryFramework",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/category_framework`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
            insert: async (values) => {
                return await axios.post(`/category_framework/insert`, JSON.stringify(values))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            update: async (key, values) => {
                let params = {
                    ...{ id: key },
                    ...values,
                }

                return await axios.post(`/category_framework/update`, JSON.stringify(params))
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
            remove: async (id) => {
                return await axios.delete(`/category_framework/delete/${id}`)
                    .then((result) => console.log(result))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)