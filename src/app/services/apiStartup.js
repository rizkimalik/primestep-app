import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiStartupList = createAsyncThunk(
    "startup/apiStartupList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/startup`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)