import axios from "axios";

const baseurl_sosialapi = import.meta.env.VITE_REST_API_SOCIALMEDIA;
const baseUrl = import.meta.env.VITE_REST_API_URL;
const urlAttachment = baseUrl + '/attachment';
const apiHeaders = {
    'Content-Type': 'application/json',
}

const axiosDefault = (token) => {
    axios.defaults.baseURL = baseUrl;
    axios.defaults.credentials = 'include';
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}


export { axiosDefault, baseUrl, baseurl_sosialapi, apiHeaders, urlAttachment }