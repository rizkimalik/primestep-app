import { lazy } from 'react';

const routes = [
    //? setting users, privillage
    {
        path: 'user',
        exact: true,
        component: lazy(() => import('views/pages/settings/user/UserList')),
    }, {
        path: 'user/create',
        exact: false,
        component: lazy(() => import('views/pages/settings/user/UserCreate')),
    }, {
        path: 'user/show/:id',
        exact: false,
        component: lazy(() => import('views/pages/settings/user/UserDetail')),
    }, {
        path: 'user/:id/edit',
        exact: false,
        component: lazy(() => import('views/pages/settings/user/UserEdit')),
    },

    //? dashboards
    {
        path: 'dashboard',
        exact: true,
        component: lazy(() => import('views/pages/dashboard/Dashboard')),
    },

    //? procurement
    {
        path: 'procurement',
        exact: true,
        component: lazy(() => import('views/pages/procurement/ProcurementPackageMain')),
    }, {
        path: 'procurement/create',
        exact: false,
        component: lazy(() => import('views/pages/procurement/ProcurementPackageCreate')),
    }, {
        path: 'procurement/show/:id',
        exact: false,
        component: lazy(() => import('views/pages/procurement/ProcurementPackageEdit')),
    }, {
        path: 'procurement/status/:id',
        exact: false,
        component: lazy(() => import('views/pages/procurement/MainFormVerification')),
    },

    //? startup-research
    {
        path: 'startup',
        exact: true,
        component: lazy(() => import('views/pages/startup-research/StartupMain')),
    },{
        path: 'startup-acceleration',
        exact: true,
        component: lazy(() => import('views/pages/startup-research/StartupAcceleration')),
    }, {
        path: 'research',
        exact: true,
        component: lazy(() => import('views/pages/startup-research/ResearchMain')),
    }, {
        path: 'startup-research/create/:modul',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/MainFormCreate')),
    }, {
        path: 'startup-research/edit/:modul/:package_no',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/MainFormEdit')),
    }, {
        path: 'startup-research/status/:modul',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/MainFormVerification')),
    },{
        path: 'startup-research/verification/:modul/:package_no',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/status/VerificationProposal')),
    }, {
        path: 'startup-research/contract',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/MainContract')),
    }, {
        path: 'startup-research/contract-status',
        exact: false,
        component: lazy(() => import('views/pages/startup-research/MainFormContract')),
    },

    //? strengthen-capability
    {
        path: 'strengthen-capability',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenCapabilityMain')),
    },
    {
        path: 'strengthen-capability/create',
        exact: false,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenCapabilityCreate')),
    }, {
        path: 'strengthen-capability/show/:id',
        exact: false,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenCapabilityEdit')),
    }, {
        path: 'strengthen-capability/status',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/MainFormVerification')),
    }, {
        path: 'strengthen-capability/contract',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/MainContract')),
    },{
        path: 'strengthen-capability/contract-status',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/MainFormContract')),
    },

    //? strengthen-PNBP-GOI
    {
        path: 'goi-pnbp',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenPNBPMain')),
    },{
        path: 'goi-pnbp/create',
        exact: false,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenPNBPCreate')),
    },{
        path: 'goi-pnbp/edit/:id',
        exact: true,
        component: lazy(() => import('views/pages/strengthen-capability/StrengthenPNBPEdit')),
    },

    //? DMF
    {
        path: 'dmf/outcome',
        exact: true,
        component: lazy(() => import('views/pages/dmf/Outcome')),
    }, {
        path: 'dmf/outcome/data',
        exact: false,
        component: lazy(() => import('views/pages/dmf/OutcomeDataDetail')),
    }, {
        path: 'dmf/outcome/create/:performance_id',
        exact: false,
        component: lazy(() => import('views/pages/dmf/OutcomeAddDetail')),
    }, {
        path: 'dmf/output',
        exact: true,
        component: lazy(() => import('views/pages/dmf/Output')),
    }, {
        path: 'dmf/output/data',
        exact: false,
        component: lazy(() => import('views/pages/dmf/OutputDataDetail')),
    }, {
        path: 'dmf/output/create/:performance_id',
        exact: false,
        component: lazy(() => import('views/pages/dmf/OutputAddDetail')),
    },

    //? gender-action
    {
        path: 'gender-action',
        exact: true,
        component: lazy(() => import('views/pages/gender-action/GenderActionPlantMain')),
    }, {
        path: 'gender-action/detail',
        exact: true,
        component: lazy(() => import('views/pages/gender-action/GenderActionDetailList')),
    }, {
        path: 'gender-action/create/:gap_id',
        exact: true,
        component: lazy(() => import('views/pages/gender-action/GenderActionCreate')),
    },

    //? issue-challenge
    {
        path: 'issue-challenge',
        exact: true,
        component: lazy(() => import('views/pages/issues-challenges/IssuesChallengesMain')),
    }, {
        path: 'issue-challenge/create',
        exact: true,
        component: lazy(() => import('views/pages/issues-challenges/IssuesChallengesCreate')),
    }, {
        path: 'issue-challenge/:id',
        exact: true,
        component: lazy(() => import('views/pages/issues-challenges/IssuesChallengesEdit')),
    },

    //? settings
    {
        path: 'setting/user',
        exact: false,
        component: lazy(() => import('views/pages/settings/user/UserList')),
    }, {
        path: 'setting/privillage',
        exact: true,
        component: lazy(() => import('views/pages/settings/privillage/UserPrivillage')),
    }, {
        path: 'setting/institution',
        exact: true,
        component: lazy(() => import('views/pages/settings/institution/InstitutionList')),
    },
    //? setting procurement
    {
        path: 'setting/category',
        exact: true,
        component: lazy(() => import('views/pages/settings/procurement/PackageCategoryList')),
    }, {
        path: 'setting/method',
        exact: true,
        component: lazy(() => import('views/pages/settings/procurement/ProcurementMethodList')),
    }, {
        path: 'setting/review',
        exact: true,
        component: lazy(() => import('views/pages/settings/procurement/ProcurementReviewList')),
    }, {
        path: 'setting/document_status',
        exact: true,
        component: lazy(() => import('views/pages/settings/procurement/DocumentStatusList')),
    },
    //? setting rnd
    {
        path: 'setting/rnd',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/CategoryResearch')),
    },
    {
        path: 'setting/rnd/bidang-fokus',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/CategoryResearch')),
    }, {
        path: 'setting/rnd/framework',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/CategoryFramework')),
    }, {
        path: 'setting/rnd/document-startup',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/DocumentStartup')),
    }, {
        path: 'setting/rnd/document-research',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/DocumentResearch')),
    }, {
        path: 'setting/rnd/document-contract',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/DocumentContract')),
    },{
        path: 'setting/rnd/pagu',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/PaguContract')),
    }, {
        path: 'setting/evaluator',
        exact: true,
        component: lazy(() => import('views/pages/settings/rnd/Evaluator')),
    },
    //? setting gap
    {
        path: 'setting/gap',
        exact: true,
        component: lazy(() => import('views/pages/settings/gap/GenderActionPlan')),
    }, {
        path: 'setting/dmf/indicator',
        exact: true,
        component: lazy(() => import('views/pages/settings/dmf/DMFIndicator')),
    },
    //? setting strengthen capability building
    {
        path: 'setting/strengthen',
        exact: true,
        component: lazy(() => import('views/pages/settings/strengthen/DocumentStatusList')),
    },{
        path: 'setting/strengthen/document_status',
        exact: true,
        component: lazy(() => import('views/pages/settings/strengthen/DocumentStatusList')),
    },{
        path: 'setting/strengthen/component',
        exact: true,
        component: lazy(() => import('views/pages/settings/strengthen/StrengthenComponent')),
    },{
        path: 'setting/strengthen/category',
        exact: true,
        component: lazy(() => import('views/pages/settings/strengthen/StrengthenCategory')),
    },{
        path: 'setting/strengthen/pagu',
        exact: true,
        component: lazy(() => import('views/pages/settings/strengthen/PaguContract')),
    },

    //? laporan dokumen
    {
        path: 'laporan-rnd',
        component: lazy(() => import('views/pages/laporan/DokumenRND')),
        exact: true,
    },{
        path: 'laporan-kemajuan',
        component: lazy(() => import('views/pages/laporan/LaporanKemajuan')),
        exact: true,
    },{
        path: 'laporan-akhir',
        component: lazy(() => import('views/pages/laporan/LaporanAkhir')),
        exact: true,
    },{
        path: 'laporan-proposal',
        component: lazy(() => import('views/pages/laporan/StatusProposal')),
        exact: true,
    },

    //? custom route
    {
        path: '/',
        component: lazy(() => import('views/pages/dashboard/Dashboard')),
        exact: true,
    },
    // {
    //     path: 'pivot',
    //     component: lazy(() => import('views/pages/pivot/PivotGridSimple')),
    //     exact: true,
    // },

];

export default routes;