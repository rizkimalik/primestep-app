import React from 'react'

function MendawaiLogo({ className, colorLogo }) {
    if (colorLogo === 'white') {
        return <><img src="./assets/image/logo-putih.png" alt="Logo-Icon" className={className} /></>
    }
    else if(colorLogo === 'black') {
        return <><img src="./assets/image/logo-hitam.png" alt="Logo-Icon" className={className} /></>
    }
    else{
        return <><img src="./assets/image/logo-putih.png" alt="Logo-Icon" className={className} /></>
    }
}

export default MendawaiLogo
