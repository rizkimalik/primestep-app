import React from 'react';
import FormGroup from "./FormGroup";

const FormSelect = ({ children, register, name, className, label, rules, errors, onChange }) => {
    return (
        <FormGroup label={label}>
            <select
                name={name}
                className={className}
                onChange={onChange}
                {...register(name, { ...rules })}
            >
                {children}
            </select>
            {errors && <span className="form-text text-danger">* {label}</span>}
        </FormGroup>
    )
}

export default FormSelect;