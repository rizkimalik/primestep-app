export async function ValidateDocumentUpload(arr1, arr2) {
    // Iterate through the arrays and compare each element
    for (let i = 0; i < arr1.length; i++) {

        // Sort the arrays
        const sortedArr1 = arr1.slice().sort(sortByDocument);
        const sortedArr2 = arr2.slice().sort(sortByDocument);

        // Compare each object in the array by stringifying them
        if (JSON.stringify(sortedArr1[i]?.document_name) !== JSON.stringify(sortedArr2[i]?.document_name)) {
            return false;
        }
    }

    // If all elements are equal, return true
    return true;
}

// Helper function to sort by 'document' property
function sortByDocument(a, b) {
    if (a.document_name < b.document_name) return -1;
    if (a.document_name > b.document_name) return 1;
    return 0;
}