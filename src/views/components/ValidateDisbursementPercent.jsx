function ValidateDisbursementPercent(data) {
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
        sum += data[i].disbursement_percent;
    }
    // return sum <= 100;
    return sum;
}

export const ValidateDisbursementValue = (data) => {
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
        sum += data[i].disbursement_value;
    }
    return sum;
}

export default ValidateDisbursementPercent
