function ConvertTextToLinks(text) {
    // Regular expression to match URLs (https or http)
    const urlRegex = /(https?:\/\/[^\s]+)/g;

    return text.replace(urlRegex, (url) => {
        // Create an anchor element with the URL and display text
        const link = document.createElement('a');
        link.href = url;
        link.textContent = url;
        link.target = '_blank';
        return link.outerHTML;
    });
}

export default ConvertTextToLinks