
export const currency_to_number = (currencyString) => {
    // Check if currencyString is a string
    if (typeof currencyString !== 'string') {
        return currencyString; // or any other appropriate value
    }

    // Remove non-numeric characters (except for the decimal point)
    // Parse the numeric string to a float (you can use parseInt for integers)
    const numericString = currencyString.replace(/[^0-9.]/g, '');
    const numericValue = parseFloat(numericString);
    return numericValue;
}