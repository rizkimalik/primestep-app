import React from 'react'
import { NavLink } from 'react-router-dom'

const ButtonCancel = ({ to, className }) => {
    return (
        <NavLink to={to} className={`${className ? className : "btn btn-sm btn-secondary m-1"}`}>
            Kembali
        </NavLink>
    )
}

export default ButtonCancel
