import React from 'react';
import { Link } from 'react-router-dom'

const ButtonConfig = ({ to }) => {
    return (
        <Link to={to} className="btn btn-xs btn-icon btn-circle btn-light-primary m-1" title="pengaturan">
            <i className="fa fa-cog fa-xs" />
        </Link>
    )
}

export default ButtonConfig
