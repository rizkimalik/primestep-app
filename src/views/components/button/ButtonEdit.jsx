import React from 'react'
import { NavLink } from 'react-router-dom'

const ButtonEdit = ({to}) => {
    return (
        <NavLink to={to} className="btn btn-sm btn-clean btn-icon btn-hover-warning" data-toggle="tooltip" title="Button Edit">
            <i className="fas fa-edit fa-sm"></i>
        </NavLink>
    )
}

export default ButtonEdit
