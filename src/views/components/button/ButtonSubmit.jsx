import React from 'react'
import Icons from '../Icons'

const ButtonSubmit = ({ className }) => {
    return (
        <button type="submit" className={`${className ? className : "btn btn-primary font-weight-bolder btn-sm m-1"}`}>
            <Icons iconName="save" className="svg-icon svg-icon-sm" />
            Simpan Perubahan
        </button>
    )
}

export default ButtonSubmit
