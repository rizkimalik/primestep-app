import React from 'react'

const ButtonDelete = ({ onClick }) => {
    return (
        <button onClick={onClick} type="button" className="btn btn-sm btn-clean btn-icon btn-hover-danger" data-toggle="tooltip" title="Button Delete">
            <i className="fas fa-trash-alt fa-sm"></i>
        </button>
    )
}

export default ButtonDelete
