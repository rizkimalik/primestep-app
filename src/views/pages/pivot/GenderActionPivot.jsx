import React, { useEffect } from 'react'
import PivotGrid, { FieldChooser } from 'devextreme-react/pivot-grid';
import PivotGridDataSource from 'devextreme/ui/pivot_grid/data_source';
import { useDispatch, useSelector } from 'react-redux';
import { apiGenderActionPivot } from 'app/services/apiGenderAction';

function GenderActionPivot() {
    const dispatch = useDispatch();
    const { gender_action_pivot } = useSelector(state => state.gender_action);

    useEffect(() => {
        dispatch(apiGenderActionPivot({ gap_id: '1'}))
    }, [dispatch]);

    const dataSource = new PivotGridDataSource({
        fields: [
            {
                caption: 'PIU',
                width: 100,
                dataField: 'piu',
                area: 'row',
                expanded: true,
            },
            {
                caption: 'Gender',
                dataField: 'gender',
                width: 100,
                area: 'row',
                selector(data) {
                    return `Peserta ${data.gender}`;
                },
            },
            {
                dataField: 'action_date',
                dataType: 'date',
                area: 'column',
            },
            {
                caption: 'Gender Action',
                dataField: 'count',
                dataType: 'number',
                summaryType: 'sum',
                area: 'data',
            },
        ],
        store: gender_action_pivot,
    });

    return (
        <div>
            <PivotGrid
                id="gender_action_pivot"
                dataSource={dataSource}
                allowSortingBySummary={true}
                allowSorting={true}
                allowFiltering={true}
                allowExpandAll={true}
                // height={440}
                showBorders={true}
            >
                <FieldChooser enabled={false} />
            </PivotGrid>

            {
                // JSON.stringify(gender_action_pivot)
                // JSON.stringify(gender_action_pivot.filter(data.year === '2023').reduce((total, data) => total + data.count, 0))
                // JSON.stringify(gender_action_pivot.filter(data => data.gender === 'Wanita' && data.year === '2023').reduce((total, data) => total + data.count, 0))
            }
            <h5>RATIO</h5>
            <h5>STATUS</h5>
        </div>
    )
}

export default GenderActionPivot