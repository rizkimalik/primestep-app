import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { ButtonRefresh } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import {
    apiDashboard_EvaluatorTotal,
    apiDashboard_EvaluatorData,
} from 'app/services/apiDashboard';
import { authUser } from 'app/slice/sliceAuth';

function DashboardEvaluator() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [year, setYear] = useState(new Date().getFullYear());
    const [status, setStatus] = useState('menunggu');
    const {
        evaluator_total,
        evaluator_data,
    } = useSelector(state => state.dashboard);

    useEffect(async () => {
        await dispatch(apiDashboard_EvaluatorTotal({ username: auth.username, get_year: year }))
        await dispatch(apiDashboard_EvaluatorData({ username: auth.username, get_year: year, status: status }))
    }, [dispatch, auth, year, status]);


    const onSubmitByYear = async (year) => {
        setYear(year);
        await dispatch(apiDashboard_EvaluatorTotal({ username: auth.username, get_year: year }))
        await dispatch(apiDashboard_EvaluatorData({ username: auth.username, get_year: year, status: status }))
    }

    const onClickByStatus = async (status) => {
        setStatus(status);
        await dispatch(apiDashboard_EvaluatorData({ username: auth.username, get_year: year, status }))
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="PRIMESTeP Project Management Dashboard" modul_name="">
                <span className="text-muted">Tahun:</span>
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <Link to="#2023" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2023' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2023')}>2023</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2024" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2024' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2024')}>2024</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2025" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2025' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2025')}>2025</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2026" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2026' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2026')}>2026</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2027" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2027' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2027')}>2027</Link>
                    </li>
                </ul>
                <ButtonRefresh onClick={() => ''} />
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-3 col-md-6">
                        <a href="#" onClick={() => onClickByStatus('menunggu')} data-toggle="modal" className="card card-custom border gutter-b rounded-lg wave wave-animate-slower wave-primary">
                            <div className="card-body">
                                <div className="d-flex align-items-center justify-content-between mb-4">
                                    <span className="svg-icon svg-icon-primary svg-icon-3x ml-n2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <title>Stockholm-icons / Home / Timer</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs />
                                            <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                <rect x={0} y={0} width={24} height={24} />
                                                <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3" />
                                                <path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000" />
                                                <path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000" />
                                                <path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000" />
                                            </g>
                                        </svg>
                                    </span>
                                    <div className="font-weight-bolder font-size-h1 text-dark">{Number(evaluator_total.total_menunggu)}</div>
                                </div>
                                <a href="#" data-toggle="modal" className="font-weight-bolder font-size-md mt-1">Usulan Belum Dievaluasi </a>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <a href="#" onClick={() => onClickByStatus('revisi')} className="card card-custom gutter-b border rounded-lg wave wave-animate-slower wave-warning">
                            <div className="card-body">
                                <div className="d-flex align-items-center justify-content-between mb-4">
                                    <span className="svg-icon svg-icon-warning svg-icon-3x ml-n2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <title>Stockholm-icons / Code / Lock-overturning</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs />
                                            <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                <rect x={0} y={0} width={24} height={24} />
                                                <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3" />
                                                <path d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z" fill="#000000" />
                                            </g>
                                        </svg>
                                    </span>
                                    <div className="font-weight-bolder font-size-h1 text-dark">{Number(evaluator_total.total_revisi)}</div>
                                </div>
                                <a href="#" data-toggle="modal" className="text-warning font-weight-bolder font-size-md mt-1">Usulan Direvisi</a>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <a href="#" onClick={() => onClickByStatus('rekomendasi')} className="card card-custom gutter-b border rounded-lg wave wave-animate-slow wave-success">
                            <div className="card-body">
                                <div className="d-flex align-items-center justify-content-between mb-4">
                                    <span className="svg-icon svg-icon-success svg-icon-3x ml-n2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <title>Stockholm-icons / General / Like</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs />
                                            <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                <rect x={0} y={0} width={24} height={24} />
                                                <path d="M9,10 L9,19 L10.1525987,19.3841996 C11.3761964,19.7920655 12.6575468,20 13.9473319,20 L17.5405883,20 C18.9706314,20 20.2018758,18.990621 20.4823303,17.5883484 L21.231529,13.8423552 C21.5564648,12.217676 20.5028146,10.6372006 18.8781353,10.3122648 C18.6189212,10.260422 18.353992,10.2430672 18.0902299,10.2606513 L14.5,10.5 L14.8641964,6.49383981 C14.9326895,5.74041495 14.3774427,5.07411874 13.6240179,5.00562558 C13.5827848,5.00187712 13.5414031,5 13.5,5 L13.5,5 C12.5694044,5 11.7070439,5.48826024 11.2282564,6.28623939 L9,10 Z" fill="#000000" />
                                                <rect fill="#000000" opacity="0.3" x={2} y={9} width={5} height={11} rx={1} />
                                            </g>
                                        </svg>
                                    </span>
                                    <div className="font-weight-bolder font-size-h1 text-dark">{Number(evaluator_total.total_rekomendasi)}</div>
                                </div>
                                <a href="#" data-toggle="modal" className="text-success font-weight-bolder font-size-md mt-1">Usulan Direkomendasi</a>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <a href="#" onClick={() => onClickByStatus('ditolak')} className="card card-custom gutter-b border rounded-lg wave wave-animate-slow wave-danger">
                            <div className="card-body">
                                <div className="d-flex align-items-center justify-content-between mb-4">
                                    <span className="svg-icon svg-icon-danger svg-icon-3x ml-n2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <title>Stockholm-icons / Code / Error-circle</title>
                                            <desc>Created with Sketch.</desc>
                                            <defs />
                                            <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                                <rect x={0} y={0} width={24} height={24} />
                                                <circle fill="#000000" opacity="0.3" cx={12} cy={12} r={10} />
                                                <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000" />
                                            </g>
                                        </svg>
                                    </span>
                                    <div className="font-weight-bolder font-size-h1 text-dark">{Number(evaluator_total.total_ditolak)}</div>
                                </div>
                                <a href="#" data-toggle="modal" className="text-danger font-weight-bolder font-size-md mt-1">Usulan Ditolak</a>
                            </div>
                        </a>
                    </div>
                </div>

                <h3 className="font-size-lg text-dark font-weight-bold mb-4">Data Usulan {status === 'menunggu' ? 'Belum Dievaluasi' : status === 'revisi' ? 'Revisi' : status === 'ditolak' ? 'Ditolak' : 'Rekomendasi'}</h3>
                <div className="row">
                    <div className="col-lg-12">
                        <DataGrid
                            dataSource={evaluator_data}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={100}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Judul Proposal" dataField="proposal_name" width={300} cellRender={({ value, data }) => {
                                let modul = data.type === 'S' ? 'startup' : 'research';
                                return <Link to={`/startup-research/verification/${modul}/${data.package_no}`}  className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</Link>
                            }} />
                            <Column caption="PIU" dataField="piu" width={80} />
                            <Column caption="Bidang Fokus" dataField="focus_name" />
                            <Column caption="Nama Ketua" dataField="leader_name" />
                            <Column caption="Tahun Usulan" dataField="proposed_year" />
                            <Column
                                caption="Besaran Dana"
                                dataField="value_idr"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                            <Column caption="TKT" dataField="tkt_value" />
                            <Column caption="Durasi Kegiatan" dataField="tkt_duration" cellRender={({ value }) => {
                                return <span>{value} Tahun</span>
                            }} />
                            <Column caption="Status" dataField="recommend" cellRender={({ data }) => {
                                return data.recommend === 0
                                    ? <span className="label label-inline label-warning">Revisi</span>
                                    : data.recommend === 1
                                        ? <span className="label label-inline label-success">Rekomendasi</span>
                                        : data.recommend === 2
                                            ? <span className="label label-inline label-danger">Ditolak</span>
                                            : <span className="label label-inline">Menunggu</span>
                            }} />
                        </DataGrid>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default DashboardEvaluator
