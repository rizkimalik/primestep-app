import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import * as echarts from 'echarts';

import { ButtonRefresh } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import {
    apiDashboard_TotalProgress,
    apiDashboard_LoanMilestone,
    apiDashboard_LoanProgressPermonth,
    apiDashboard_ProcurementPackagesUI,
    apiDashboard_ProcurementPackagesUGM,
    apiDashboard_ProcurementPackagesITB,
    apiDashboard_ProcurementPackagesIPB,
    // apiDashboard_ProcurementPackagesPMU,
} from 'app/services/apiDashboard';
import { authUser } from 'app/slice/sliceAuth';

const logo_ui = './assets/image/logo-ui.png';
const logo_itb = './assets/image/logo-itb.png';
const logo_ipb = './assets/image/logo-ipb.png';
const logo_ugm = './assets/image/logo-ugm.png';
const logo_pmu = './assets/image/logo-pmu.png';

function DashboardPIU() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [year, setYear] = useState(new Date().getFullYear());
    const refProjectStatistics = useRef();
    const refLoanDisbursment = useRef();
    const refProcurementUI = useRef();
    const refProcurementUGM = useRef();
    const refProcurementIPB = useRef();
    const refProcurementITB = useRef();
    const {
        total_progress,
        loan_milestone,
        loan_progress_permonth,
        procurement_packages_ui,
        procurement_packages_ugm,
        procurement_packages_itb,
        procurement_packages_ipb,
    } = useSelector(state => state.dashboard);
    const { loan_disbursement, loan_withdrawal } = loan_progress_permonth;

    useEffect(() => {
        dispatch(apiDashboard_LoanMilestone({ auth }));
        dispatch(apiDashboard_LoanProgressPermonth({ auth, get_year: new Date().getFullYear() }));
        dispatch(apiDashboard_TotalProgress({ auth, get_year: new Date().getFullYear() }));
        dispatch(apiDashboard_ProcurementPackagesUI({ get_year: new Date().getFullYear(), institution: 'UI' }));
        dispatch(apiDashboard_ProcurementPackagesUGM({ get_year: new Date().getFullYear(), institution: 'UGM' }));
        dispatch(apiDashboard_ProcurementPackagesITB({ get_year: new Date().getFullYear(), institution: 'ITB' }));
        dispatch(apiDashboard_ProcurementPackagesIPB({ get_year: new Date().getFullYear(), institution: 'IPB' }));
    }, [dispatch]);

    useEffect(() => {
        function LoadrefProjectStatistics() {
            const colors = ['#5470c6', '#fc8452', '#3ba272', '#ee6666', '#9a60b4', '#91cc75', '#fac858'];
            const chartProjectStatistics = echarts.init(refProjectStatistics.current);
            const option = {
                title: {
                    text: ''
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    orient: 'horizontal',
                    top: 'bottom'
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    data: ['2023', '2024', '2025', '2026', '2027']
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                series: [{
                    type: 'bar',
                    // data: []
                    data: [{
                        value: 120,
                        itemStyle: {
                            color: colors[0]
                        },
                        label: { show: true, position: 'insideBottom', distance: '30', rotate: '90', formatter: '{c}' },
                    }, {
                        value: 210,
                        itemStyle: {
                            color: colors[1]
                        },
                        label: { show: true, position: 'insideBottom', distance: '30', rotate: '90', formatter: '{c}' },
                    }, {
                        value: 235,
                        itemStyle: {
                            color: colors[2]
                        },
                        label: { show: true, position: 'insideBottom', distance: '30', rotate: '90', formatter: '{c}' },
                    }, {
                        value: 178,
                        itemStyle: {
                            color: colors[3]
                        },
                        label: { show: true, position: 'insideBottom', distance: '30', rotate: '90', formatter: '{c}' },
                    }, {
                        value: 350,
                        itemStyle: {
                            color: colors[4]
                        },
                        label: { show: true, position: 'insideBottom', distance: '30', rotate: '90', formatter: '{c}' },
                    }]
                }]
            }

            option && chartProjectStatistics.setOption(option);
        }
        LoadrefProjectStatistics();

        function ChartProgressPermonth() {
            const chartLoanDisbursement = echarts.init(refLoanDisbursment.current);
            const colors = ['#5470c6', '#fc8452', '#3ba272', '#ee6666', '#9a60b4', '#91cc75', '#fac858'];
            const option = {
                title: {
                    text: ''
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Withdrawal', 'Disbursement']
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    name: 'Withdrawal',
                    type: 'line',
                    itemStyle: { color: colors[3] },
                    data: []
                }, {
                    name: 'Disbursement',
                    type: 'line',
                    itemStyle: { color: colors[4] },
                    data: []
                }]
            }

            let data_disbursement = [];
            let data_withdrawal = [];
            for (let i = 0; i < loan_disbursement?.length; i++) {
                data_disbursement.push({ value: loan_disbursement[i].total_disbursment });
            }
            for (let i = 0; i < loan_withdrawal?.length; i++) {
                data_withdrawal.push({ value: loan_withdrawal[i].total_withdrawal });
            }

            option.series[0].data = data_disbursement;
            option.series[1].data = data_withdrawal;
            option && chartLoanDisbursement.setOption(option);
        }
        ChartProgressPermonth();

        function ChartProcurementPackages() {
            (auth.institution === 'UI' || auth.user_level === 'Admin') &&
                echarts.init(refProcurementUI.current).setOption({
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: '{b} : {c} ({d}%)'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left'
                    },
                    series: [{
                        type: 'pie',
                        radius: ['30%', '60%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 30,
                                fontWeight: 'bold'
                            }
                        },
                        // data: [],
                        data: [{
                            name: 'Sisa Paket',
                            value: procurement_packages_ui.total_paket_sisa,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Aktif',
                            value: procurement_packages_ui.total_paket_aktif,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Selesai',
                            value: procurement_packages_ui.total_paket_selesai,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }],
                    }]
                });
            (auth.institution === 'UGM' || auth.user_level === 'Admin') &&
                echarts.init(refProcurementUGM.current).setOption({
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: '{b} : {c} ({d}%)'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left'
                    },
                    series: [{
                        type: 'pie',
                        radius: ['30%', '60%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 30,
                                fontWeight: 'bold'
                            }
                        },
                        data: [{
                            name: 'Sisa Paket',
                            value: procurement_packages_ugm.total_paket_sisa,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Aktif',
                            value: procurement_packages_ugm.total_paket_aktif,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Selesai',
                            value: procurement_packages_ugm.total_paket_selesai,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }],
                    }]
                });
            (auth.institution === 'IPB' || auth.user_level === 'Admin') &&
                echarts.init(refProcurementIPB.current).setOption({
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: '{b} : {c} ({d}%)'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left'
                    },
                    series: [{
                        type: 'pie',
                        radius: ['30%', '60%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 30,
                                fontWeight: 'bold'
                            }
                        },
                        // data: [],
                        data: [{
                            name: 'Sisa Paket',
                            value: procurement_packages_ipb.total_paket_sisa,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Aktif',
                            value: procurement_packages_ipb.total_paket_aktif,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Selesai',
                            value: procurement_packages_ipb.total_paket_selesai,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }],
                    }]
                });
            (auth.institution === 'ITB' || auth.user_level === 'Admin') &&
                echarts.init(refProcurementITB.current).setOption({
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: '{b} : {c} ({d}%)'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left'
                    },
                    series: [{
                        type: 'pie',
                        radius: ['30%', '60%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 30,
                                fontWeight: 'bold'
                            }
                        },
                        // data: [],
                        data: [{
                            name: 'Sisa Paket',
                            value: procurement_packages_itb.total_paket_sisa,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Aktif',
                            value: procurement_packages_itb.total_paket_aktif,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }, {
                            name: 'Paket Selesai',
                            value: procurement_packages_itb.total_paket_selesai,
                            label: { show: true, position: 'inner', formatter: '({c}) {d} %' },
                        }],
                    }]
                });
        }
        ChartProcurementPackages();
    });

    const onSubmitByYear = (year) => {
        setYear(year);
        dispatch(apiDashboard_TotalProgress({ auth, get_year: year }));
        dispatch(apiDashboard_LoanProgressPermonth({ auth, get_year: year }));
        dispatch(apiDashboard_ProcurementPackagesUI({ get_year: year, institution: 'UI' }));
        dispatch(apiDashboard_ProcurementPackagesUGM({ get_year: year, institution: 'UGM' }));
        dispatch(apiDashboard_ProcurementPackagesITB({ get_year: year, institution: 'ITB' }));
        dispatch(apiDashboard_ProcurementPackagesIPB({ get_year: year, institution: 'IPB' }));
        // dispatch(apiDashboard_ProcurementPackagesPMU({ get_year: year, institution: 'PMU' }));
    }

    return (
        <MainContent>
            <SubHeader active_page="Dashboard" menu_name="PRIMESTeP Project Management Dashboard" modul_name="">
                <span className="text-muted">Tahun:</span>
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <Link to="#2023" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2023' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2023')}>2023</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2024" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2024' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2024')}>2024</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2025" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2025' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2025')}>2025</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2026" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2026' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2026')}>2026</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#2027" className={`nav-link py-2 px-4 font-weight-bolder ${year == '2027' && 'active'}`} data-toggle="tab" onClick={() => onSubmitByYear('2027')}>2027</Link>
                    </li>
                </ul>
                <ButtonRefresh onClick={() => ''} />
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-6 col-md-6">
                        <div className="card card-custom gutter-b rounded-lg wave wave-animate-slower wave-primary">
                            <div className="card-body">
                                <span className="svg-icon svg-icon-primary svg-icon-3x ml-n2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                            <rect x={0} y={0} width={24} height={24} />
                                            <circle fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5" />
                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x={3} y={3} width={18} height={7} rx={1} />
                                            <path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                </span>
                                <div className="font-weight-bolder font-size-h2 mt-2">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(total_progress.total_funds_disbursement))}</div>
                                <a href="#" data-toggle="modal" className="font-weight-bolder font-size-md mt-1">Total Funds Disbursed</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <div className="card card-custom gutter-b border rounded-lg wave wave-animate-slow wave-success">
                            <div className="card-body">
                                <span className="svg-icon svg-icon-success svg-icon-3x ml-n2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                            <rect x={0} y={0} width={24} height={24} />
                                            <path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fillRule="nonzero" />
                                            <path d="M8.7295372,14.6839411 C8.35180695,15.0868534 7.71897114,15.1072675 7.31605887,14.7295372 C6.9131466,14.3518069 6.89273254,13.7189711 7.2704628,13.3160589 L11.0204628,9.31605887 C11.3857725,8.92639521 11.9928179,8.89260288 12.3991193,9.23931335 L15.358855,11.7649545 L19.2151172,6.88035571 C19.5573373,6.44687693 20.1861655,6.37289714 20.6196443,6.71511723 C21.0531231,7.05733733 21.1271029,7.68616551 20.7848828,8.11964429 L16.2848828,13.8196443 C15.9333973,14.2648593 15.2823707,14.3288915 14.8508807,13.9606866 L11.8268294,11.3801628 L8.7295372,14.6839411 Z" fill="#000000" fillRule="nonzero" opacity="0.3" />
                                        </g>
                                    </svg>
                                </span>

                                <div className="font-weight-bolder font-size-h2 mt-2">{total_progress.total_research_startup}</div>
                                <a href="#" data-toggle="modal" className="text-success font-weight-bolder font-size-md mt-1">Research & Startup Incubation</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-3">
                        <div className="card card-custom gutter-b border rounded-lg wave wave-animate-slower wave-danger">
                            <div className="card-body">
                                <span className="svg-icon svg-icon-danger svg-icon-3x ml-n2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                            <rect x={0} y={0} width={24} height={24} />
                                            <path d="M4,9.67471899 L10.880262,13.6470401 C10.9543486,13.689814 11.0320333,13.7207107 11.1111111,13.740321 L11.1111111,21.4444444 L4.49070127,17.526473 C4.18655139,17.3464765 4,17.0193034 4,16.6658832 L4,9.67471899 Z M20,9.56911707 L20,16.6658832 C20,17.0193034 19.8134486,17.3464765 19.5092987,17.526473 L12.8888889,21.4444444 L12.8888889,13.6728275 C12.9050191,13.6647696 12.9210067,13.6561758 12.9368301,13.6470401 L20,9.56911707 Z" fill="#000000" />
                                            <path d="M4.21611835,7.74669402 C4.30015839,7.64056877 4.40623188,7.55087574 4.5299008,7.48500698 L11.5299008,3.75665466 C11.8237589,3.60013944 12.1762411,3.60013944 12.4700992,3.75665466 L19.4700992,7.48500698 C19.5654307,7.53578262 19.6503066,7.60071528 19.7226939,7.67641889 L12.0479413,12.1074394 C11.9974761,12.1365754 11.9509488,12.1699127 11.9085461,12.2067543 C11.8661433,12.1699127 11.819616,12.1365754 11.7691509,12.1074394 L4.21611835,7.74669402 Z" fill="#000000" opacity="0.3" />
                                        </g>
                                    </svg>
                                </span>
                                <div className="font-weight-bolder font-size-h2 mt-2">{total_progress.total_procurement_packages}</div>
                                <a href="#" data-toggle="modal" className="text-danger font-weight-bolder font-size-md mt-1">Procurement Package</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mb-8">
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-header ribbon ribbon-top ribbon-ver p-5">
                                <div className="ribbon-target bg-warning" style={{ top: '-2px', right: 20 }}>
                                    <i className="fas fa-hand-holding-usd text-white" />
                                </div>
                                <h5 className="font-weight-bolder text-dark">Loan Milestone</h5>
                            </div>
                            <div className="card-body p-5">
                                <table className="table table-bordered" style={{ width: '100%' }}>
                                    <tbody>
                                        <tr>
                                            <td width={50}>Loan Agreement Date</td>
                                            <td width={50}>
                                                <span className="font-weight-bolder text-dark">{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(loan_milestone?.loan_agreement_date ? new Date(loan_milestone?.loan_agreement_date) : '')}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Loan Effective Date</td>
                                            <td>
                                                <span className="font-weight-bolder text-dark">{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(loan_milestone?.loan_effective_date ? new Date(loan_milestone?.loan_effective_date) : '')}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Original Loan Closing Date</td>
                                            <td>
                                                <span className="font-weight-bolder text-dark">{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(loan_milestone?.loan_closing_date ? new Date(loan_milestone?.loan_closing_date) : '')}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Elapsed Time as of</td>
                                            <td><span className="font-weight-bolder text-dark">0.00%</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="card">
                            <div className="card-header ribbon ribbon-top ribbon-ver p-5">
                                <div className="ribbon-target bg-info" style={{ top: '-2px', right: 20 }}>
                                    <i className="fas fa-dollar-sign text-white" />
                                </div>
                                <h5 className="font-weight-bolder text-dark">Loan Amount</h5>
                            </div>
                            <div className="card-body p-5">
                                <table className="table table-bordered" style={{ width: '100%' }}>
                                    <tbody>
                                        <tr>
                                            <td width={50}>Original Load ADB (JPY)</td>
                                            <td width={50}>
                                                {
                                                    auth.user_level === 'PIU'
                                                        ? <span className="font-weight-bolder text-dark">{new Intl.NumberFormat("jp-JP", { style: "currency", currency: "JPY" }).format(Number(loan_milestone?.alocation_plan))}</span>
                                                        : <span className="font-weight-bolder text-dark">{new Intl.NumberFormat("jp-JP", { style: "currency", currency: "JPY" }).format(Number(loan_milestone?.loan_origin_adb))}</span>
                                                }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total Withdrawal</td>
                                            <td>
                                                <span className="font-weight-bolder text-dark">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(loan_milestone?.total_withdrawal))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Loan Disbursement</td>
                                            <td>
                                                <span className="font-weight-bolder text-dark">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(loan_milestone?.total_disbursement))}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Progress Variance</td><td>-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mb-8">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header p-5">
                                <h3 className="font-weight-bolder text-dark">Loan Progress</h3>
                                <div className="font-weight-bolder">Total comparison between disbursement and withdrawal</div>
                            </div>
                            <div className="card-body p-5" style={{ height: '350px', width: '100%' }} ref={refLoanDisbursment} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-8">
                        <div className="card">
                            <div className="card-header p-5">
                                <h3 className="font-weight-bolder text-dark">Progress Variance</h3>
                                <div className="font-weight-bolder">Financial progress including budget utilization and disbursement projections</div>
                            </div>
                            <div className="card-body p-5" style={{ height: '350px', width: '100%' }} ref={refProjectStatistics} />
                        </div>
                    </div>

                    {
                        auth.institution === 'UI' &&
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-header ribbon ribbon-clip ribbon-right p-5">
                                    <div className="ribbon-target" style={{ top: 20 }}>
                                        <span className="ribbon-inner bg-warning" /><span className="font-size-h3 font-weight-bolder">{procurement_packages_ui.total_paket}</span>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-start">
                                        <div className="symbol symbol-50 symbol-circle d-block mr-4">
                                            <img src={logo_ui} height={45} width={45} alt="logo_ui" />
                                        </div>
                                        <div>
                                            <h3 className="font-weight-bolder text-dark">UI Procurement</h3>
                                            <div className="font-weight-bolder">Procurement Packages</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0" style={{ height: '350px', width: '100%' }} ref={refProcurementUI} />
                            </div>
                        </div>
                    }
                    {
                        auth.institution === 'UGM' &&
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-header ribbon ribbon-clip ribbon-right p-5">
                                    <div className="ribbon-target" style={{ top: 20 }}>
                                        <span className="ribbon-inner bg-info" /><span className="font-size-h3 font-weight-bolder">{procurement_packages_ugm.total_paket}</span>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-start">
                                        <div className="symbol symbol-50 symbol-circle d-block mr-4">
                                            <img src={logo_ugm} height={45} width={45} alt="logo_ugm" />
                                        </div>
                                        <div>
                                            <h3 className="font-weight-bolder text-dark">UGM Procurement</h3>
                                            <div className="font-weight-bolder">Procurement Packages</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0" style={{ height: '350px', width: '100%' }} ref={refProcurementUGM} />
                            </div>
                        </div>
                    }
                    {
                        auth.institution === 'IPB' &&
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-header ribbon ribbon-clip ribbon-right p-5">
                                    <div className="ribbon-target" style={{ top: 20 }}>
                                        <span className="ribbon-inner bg-primary" /><span className="font-size-h3 font-weight-bolder">{procurement_packages_ipb.total_paket}</span>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-start">
                                        <div className="symbol symbol-50 symbol-circle d-block mr-4">
                                            <img src={logo_ipb} height={45} width={45} alt="logo_ipb" />
                                        </div>
                                        <div>
                                            <h3 className="font-weight-bolder text-dark">IPB Procurement</h3>
                                            <div className="font-weight-bolder">Procurement Packages</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0" style={{ height: '350px', width: '100%' }} ref={refProcurementIPB} />
                            </div>
                        </div>
                    }
                    {
                        auth.institution === 'ITB' &&
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-header ribbon ribbon-clip ribbon-right p-5">
                                    <div className="ribbon-target" style={{ top: 20 }}>
                                        <span className="ribbon-inner bg-success" /><span className="font-size-h3 font-weight-bolder">{procurement_packages_itb.total_paket}</span>
                                    </div>
                                    <div className="d-flex align-items-center justify-content-start">
                                        <div className="symbol symbol-50 symbol-circle d-block mr-4">
                                            <img src={logo_itb} height={45} width={45} alt="logo_itb" />
                                        </div>
                                        <div>
                                            <h3 className="font-weight-bolder text-dark">ITB Procurement</h3>
                                            <div className="font-weight-bolder">Procurement Packages</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0" style={{ height: '350px', width: '100%' }} ref={refProcurementITB} />
                            </div>
                        </div>
                    }
                </div>
            </Container>
        </MainContent>
    )
}

export default DashboardPIU
