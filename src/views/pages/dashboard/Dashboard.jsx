import React from 'react'
import { useSelector } from 'react-redux';

import { authUser } from 'app/slice/sliceAuth';
import DashboardPMU from './DashboardPMU';
import DashboardPIU from './DashboardPIU';
import DashboardEvaluator from './DashboardEvaluator';

function Dashboard() {
    const auth = useSelector(authUser);

    return (
        <div>
            {
                (auth.user_level === 'Admin' || auth.user_level === 'PMU') && 
                <DashboardPMU />
            }
            {
                (auth.user_level === 'PIU') && 
                <DashboardPIU />
            }
            {
                auth.user_level === 'Evaluator' && 
                <DashboardEvaluator />
            }
        </div>
    )
}

export default Dashboard