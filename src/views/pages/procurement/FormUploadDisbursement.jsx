import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Column, DataGrid, Pager, Paging } from 'devextreme-react/data-grid';

import ModalDisbursement from './ModalDisbursement';
import ListDocsUploaded from './ListDocsUploaded';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiListDocsUploaded,
    apiInsertDocsUploaded,
    apiProcurement_UpdateStatus,
} from 'app/services/apiProcurement';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDisbursementList, apiDisbursementDelete } from 'app/services/apiDisbursement';
import { apiContractDetail } from 'app/services/apiContract';
import ValidateDisbursementPercent from 'views/components/ValidateDisbursementPercent';

function FormUploadDisbursement({ packages, setNavigate }) {
    const dispatch = useDispatch();
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const [id, setDisburseID] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { package_no, contract_no, procurement_method } = packages;
    const { contract_detail } = useSelector(state => state.contract);
    const { disbursement_list } = useSelector(state => state.disbursement);
    const { docs_uploaded_list } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'procurement', status: 'Pencairan', method: procurement_method }));
    }, [dispatch, procurement_method]);

    useEffect(() => {
        dispatch(apiDisbursementList({ contract_no }));
    }, [dispatch, contract_no]);

    useEffect(() => {
        dispatch(apiContractDetail({ package_no }));
    }, [dispatch, package_no]);

    useEffect(() => {
        reset({ package_no });
    }, [reset, package_no]);

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiInsertDocsUploaded(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil simpan data.');
                dispatch(apiListDocsUploaded({ package_no, status: 'Pencairan' }));
                setLoading('');
                setFileDoc('');
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    async function deleteDataHandler(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                await dispatch(apiDisbursementDelete({ id }))
                await dispatch(apiDisbursementList({ contract_no }));
                Swal.fire({
                    title: "Sukses Menghapus.",
                    text: `Data telah terhapus!`,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            }
        });
    }

    const onSubmitNextBtn = () => {
        if (contract_detail && ValidateDisbursementPercent(disbursement_list) == 100) {
            setNavigate('4');
            dispatch(apiProcurement_UpdateStatus({ package_no, status: 'Pencairan' }));
        }
        else {
            SwalAlertError('Gagal!', '*Tahap Pencairan 100% & Unggah Dokumen Kelengkapan Pencairan.');
        }
    }

    return (
        <div>
            {contract_detail && <ModalDisbursement id={id} contract_detail={contract_detail} apiDisbursementList={apiDisbursementList} />}
            <div className="mb-4">
                <h3 className="font-size-lg text-dark font-weight-bold">3.1 Informasi Kontrak:</h3>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Nama Penyedia </label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{contract_detail?.contract_vendor}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">No Kontrak PKS</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{contract_detail?.contract_no}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">No Kontrak ADB</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{contract_detail?.contract_no_adb}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Nilai Kontrak (Rp.)</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(contract_detail ? contract_detail.contract_value : 0)}</label>
                </div>
            </div>

            <div className="separator separator-dashed separator-border-2 my-4" />
            <h3 className="font-size-lg text-dark font-weight-bold mb-0">3.2 Tahap Pencairan:</h3>
            <div className="d-flex justify-content-end mb-2">
                <button type="button" onClick={() => setDisburseID('')} data-toggle="modal" data-target="#modalTambahPencairan" className="btn btn-sm btn-primary">Tambah Pencairan</button>
            </div>
            <DataGrid
                dataSource={disbursement_list}
                remoteOperations={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                columnMinWidth={80}
                onInitNewRow={(e) => {
                    e.data.contract_no = contract_no;
                }}
            >
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Opsi" dataField="id" width={80} cellRender={({ data }) => {
                    return <div>
                        <button onClick={() => deleteDataHandler(data.id)} className="btn btn-sm btn-clean btn-icon btn-hover-danger">
                            <i className="fas fa-trash-alt icon-sm"></i>
                        </button>
                        <button onClick={() => setDisburseID(data.id)} data-toggle="modal" data-target="#modalTambahPencairan" className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                            <i className="fas fa-edit icon-sm"></i>
                        </button>
                    </div>
                }} />
                <Column caption="Nomor Kontrak" dataField="contract_no" visible={false} allowEditing={false} />
                <Column caption="Pencairan %" dataField="disbursement_percent" />
                <Column
                    caption="Jumlah Pencairan"
                    dataField="disbursement_value"
                    format="fixedPoint"
                    cellRender={({ value }) => {
                        return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                    }}
                />
                <Column caption="Tanggal BAST" dataField="disbursement_date" dataType="datetime" />
            </DataGrid>

            <div className="separator separator-dashed separator-border-2 my-4" />
            <h3 className="font-size-lg text-dark font-weight-bold my-4">3.3 Unggah Dokumen Kelengkapan Pencairan:</h3>
            <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                <input type="hidden" defaultValue={package_no} {...register("package_no", { required: true })} />
                <input type="hidden" defaultValue="Pencairan" {...register("status", { required: true })} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen * :</label>
                            <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen * :</label>
                            <div className={`custom-file ${file_doc && 'hide'}`}>
                                <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                            </div>
                            <div className={`${(!file_doc) && 'hide'}`}>
                                <div className="form-control alert alert-custom alert-light-dark fade show p-2" role="alert">
                                    <div className="alert-text text-truncate" title={file_doc.name}>{file_doc.name} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(file_doc.size)})</div>
                                    <div className="alert-close">
                                        <button type="button" onClick={() => setFileDoc('')} className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i className="ki ki-close" title="Hapus Berkas" /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={package_no} status='Pencairan' />

            <div className="separator separator-dashed separator-border-2 my-4" />
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('2')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div>
        </div>
    )
}

export default FormUploadDisbursement