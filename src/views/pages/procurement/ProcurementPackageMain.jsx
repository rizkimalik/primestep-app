import React, { useEffect } from 'react';
import Swal from 'sweetalert2';
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import InformationHeader from './InformationHeader';
import { ButtonCreate } from 'views/components/button';
import { authUser, setStoreData } from 'app/slice/sliceAuth';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card'
import {
    apiProcurementPackageList,
    apiProcurementPackageDelete,
    apiProcurement_InformationStatus,
    apiExport_Excel,
} from 'app/services/apiProcurement';

function ProcurementPackageMain() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { store_data } = useSelector(state => state.auth);
    const { package_list } = useSelector(state => state.procurement);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiProcurementPackageList({ user_level: auth.user_level, institution: store_data.institution, adv_year: store_data.year }));
        }
        else {
            dispatch(apiProcurementPackageList({ user_level: auth.user_level, institution: auth.institution, adv_year: store_data.year }))
        }
    }, [dispatch, auth, store_data]);

    function handlerDeleteData(row) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiProcurementPackageDelete({ id: row.id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Sukses Menghapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiProcurementPackageList({ user_level: auth.user_level, institution: row.institution_code, adv_year: row.advertisment_year }));
                    dispatch(apiProcurement_InformationStatus({ institution: row.institution_code, adv_year: row.advertisment_year }));
                }
            }
        });
    }

    const onExportExcel = async () => {
        const { payload } = await dispatch(apiExport_Excel({ institution: store_data.institution, adv_year: store_data.year }))
        if (payload?.data?.length > 0) {
            const data_export = payload.data;
            const workbook = new Workbook();
            const worksheet = workbook.addWorksheet('Main sheet');
            worksheet.columns = [
                { header: 'Nomor Paket', key: 'package_no' },
                { header: 'Paket Pengadaan', key: 'general_description' },
                { header: 'No Kontrak PKS', key: 'contract_no' },
                { header: 'Nilai Kontrak', key: 'contract_value' },
                { header: 'Nama Penyedia', key: 'contract_vendor' },
                { header: 'Nilai Estimasi', key: 'est_value_idr' },
                { header: 'Triwulan', key: 'advertisment_quarter' },
                { header: 'Metode', key: 'procurement_method' },
                { header: 'Review', key: 'review' },
                { header: 'Prosedur Penawaran', key: 'type_proposal' },
                { header: 'Status', key: 'status' },
            ]
            worksheet.addRows(data_export);
            worksheet.autoFilter = 'A1:K1';
            worksheet.eachRow(function (row, rowNumber) {
                row.eachCell((cell, colNumber) => {
                    if (rowNumber === 1) {
                        cell.fill = {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: { argb: 'f5b914' }
                        }
                    }
                })
                row.commit();
            });

            workbook.xlsx.writeBuffer().then((buffer) => {
                saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Procurement - ${store_data.institution} ${store_data.year}.xlsx`);
            });
        } else {
            SwalAlertError('Aksi Gagal', 'Gagal cetak ke berkas excel.')
        }
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <Link to={`/procurement/show/${row.id}`} title="Edit Data" className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                    <i className="fas fa-edit fa-sm"></i>
                </Link>
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(row)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <Link to={`/procurement/status/${row.id}`} className="btn btn-sm btn-clean btn-icon btn-hover-success" title="Verifikasi Dokumen">
                    <i className="fas fa-upload fa-sm"></i>
                </Link>
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Procurement" menu_name="Paket Pengadaan" modul_name="">
                <span className="font-weight-bolder text-muted">Daftar Paket Pengadaan {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <InformationHeader
                            auth={auth}
                            store_data={store_data}
                            setStoreData={setStoreData}
                        />

                        <div className="d-flex justify-content-between mb-2">
                            <h3>2. Daftar Paket Pengadaan {store_data.institution}</h3>
                            <div>
                                <button type="button" onClick={() => onExportExcel()} className="btn btn-sm btn-light-success font-weight-bolder mr-2"><i className="fas fa-print icon-sm"></i> Cetak Excel</button>
                                {
                                    auth.institution === 'PMU' &&
                                    <ButtonCreate text="Tambah Paket Pengadaan" to={`/procurement/create?institution=${store_data.institution}&year=${store_data.year}`} />
                                }
                            </div>
                        </div>
                        <DataGrid
                            dataSource={package_list}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {
                                auth.institution === 'PMU' &&
                                <Column caption="Aksi" dataField="id" fixed={false} cellRender={componentButtonActions} />
                            }
                            <Column caption="Nomor Paket" dataField="package_no" />
                            <Column caption="Paket Pengadaan" dataField="general_description" width={250} />
                            <Column caption="No Kontrak PKS" dataField="contract_no" />
                            <Column
                                caption="Nilai Kontrak"
                                dataField="contract_value"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                            <Column caption="Nama Penyedia" dataField="contract_vendor" />
                            <Column caption="Nilai Estimasi" dataField="est_value_idr" cellRender={({ value }) => {
                                return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                            }} />
                            <Column caption="Triwulan" dataField="advertisment_quarter" cellRender={({ data }) => {
                                return <span>{data.advertisment_quarter} / {data.advertisment_year}</span>
                            }} />
                            <Column caption="Metode" dataField="procurement_method" />
                            <Column caption="Review" dataField="review" />
                            <Column caption="Prosedur Penawaran" dataField="type_proposal" />
                            <Column caption="Status" dataField="status" cellRender={({ value }) => {
                                if (value === 'Persiapan' || value === 'Pelaksanaan' || value === 'Pencairan') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Selesai') {
                                    return <span className="label label-inline label-success font-weight-bolder">{value}</span>
                                } else {
                                    return <span className="label label-inline font-weight-bolder">Usulan</span>
                                }
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ProcurementPackageMain