import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons';
import ModalDocsUpload from './ModalDocsUpload';
import { urlAttachment } from 'app/config';
import { apiListDocsUploaded, apiDoscUploadedDelete } from 'app/services/apiProcurement';

function ListDocsUploaded({ package_no, status }) {
    const dispatch = useDispatch();
    const [document_data, setDocData] = useState('');
    const { list_docs_uploaded } = useSelector(state => state.procurement);

    useEffect(() => {
        dispatch(apiListDocsUploaded({ package_no, status }))
    }, [dispatch, package_no]);

    async function handlerDeleteData(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                await dispatch(apiDoscUploadedDelete({ id }))
                await dispatch(apiListDocsUploaded({ package_no, status }))
                Swal.fire({
                    title: "Sukses Menghapus.",
                    text: `Data telah terhapus!`,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            }
        });
    }

    function componentButtonActions(data) {
        const { id } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" onClick={() => handlerDeleteData(id)} className="btn btn-clean btn-icon btn-hover-danger btn-sm mx-1" title="Hapus Berkas Dokumen" aria-expanded="false">
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <button type="button" onClick={() => setDocData(data.row.data)} data-toggle="modal" data-target="#modalDocsUpload" className="btn btn-sm btn-clean btn-icon btn-hover-warning mx-1" title="Edit Data" aria-expanded="false">
                    <i className="fas fa-edit fa-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <div>
            {document_data && <ModalDocsUpload document_data={document_data} status={status} />}
            <DataGrid
                dataSource={list_docs_uploaded}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true,
                    summary: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                wordWrapEnabled={true}
                columnMinWidth={80}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Aksi" dataField="id" width={100} cellRender={componentButtonActions} />
                <Column caption="Nama Dokumen" dataField="document_name" />
                <Column caption="Tautan Berkas" dataField="filename" width={250} cellRender={({ value, data }) => {
                    return <a href={urlAttachment + '/procurement/' + value} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {value} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(data.filesize)})</a>
                }} />
                <Column caption="Keterangan" dataField="description" />
                <Column caption="Tanggal Unggah" dataField="created_at" cellRender={({ value }) => {
                    return <span>{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(value))}</span>
                }} />
            </DataGrid>
        </div>
    )
}

export default ListDocsUploaded