import React from 'react'
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

function ProcurementPackageDetail(props) {
    const { detail } = props.data.data;

    return (
        <div>
            <h5>Package Detail</h5>
            <DataGrid
                dataSource={detail}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true,
                    summary: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={100}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[10, 20, 50]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="No Package Lot" dataField="package_no_lot" />
                <Column caption="General Description" dataField="general_description" width={250} />
                <Column caption="Estimated Value">
                    <Column
                        dataField="est_value_idr"
                        caption="IDR"
                        format="fixedPoint"
                        cellRender={({ value }) => {
                            return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                        }}
                    />
                    <Column
                        dataField="est_value_usd"
                        caption="USD"
                        format="fixedPoint"
                        cellRender={({ value }) => {
                            return <span>{new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" }).format(value)}</span>
                        }}
                    />
                    <Column
                        dataField="est_value_jpy"
                        caption="JPY"
                        format="fixedPoint"
                        cellRender={({ value }) => {
                            return <span>{new Intl.NumberFormat("ja-JP", { style: "currency", currency: "JPY" }).format(value)}</span>
                        }}
                    />
                </Column>
            </DataGrid>
        </div>
    )
}

export default ProcurementPackageDetail