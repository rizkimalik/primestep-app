import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';
import { Link, useLocation } from 'react-router-dom';

import { authUser } from 'app/slice/sliceAuth';
import { FormInput, FormSelect } from 'views/components/form';
import { Card, CardBody } from 'views/components/card';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiProcurementPackageInsert } from 'app/services/apiProcurement';
import { apiMasterInstitution, apiMasterPackageCategory, apiMasterProcurementMethod, apiMasterProcurementReview } from 'app/services/apiMasterData';

function ProcurementPackageCreate() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const search = new URLSearchParams(useLocation().search);
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();
    const { institution, category_package, category_method, category_review } = useSelector(state => state.master)

    // useEffect(() => {
    //     reset({ project_no: '55063-001', status: 'Usulan' }); // no project ADB
    // }, []);

    useEffect(() => {
        reset({ institution_code: auth.institution }); // no project ADB
    }, [auth]);

    useEffect(() => {
        dispatch(apiMasterInstitution());
        dispatch(apiMasterPackageCategory());
        dispatch(apiMasterProcurementMethod());
        dispatch(apiMasterProcurementReview());
    }, [dispatch]);

    const onSubmitInsertPackage = async (data) => {
        try {
            const { payload } = await dispatch(apiProcurementPackageInsert(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data paket.')
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Procurement" menu_name="Tambah Paket Pengadaan" modul_name="">
                <Link to={`/procurement`} className="btn btn-light btn-sm font-weight-bolder">
                    Paket Pengadaan {search.get('institution')} - Tahun {search.get('year')}
                </Link>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Tambah Paket Pengadaan</h3>
                                <form onSubmit={handleSubmit(onSubmitInsertPackage)} className="form">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <FormSelect
                                                name="category_code"
                                                label="Kategori Paket"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.category_code}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                {
                                                    category_package.map((item, index) => {
                                                        return <option value={item.category_code} key={index}>{item.category_code} - {item.category_description}</option>
                                                    })
                                                }
                                            </FormSelect>
                                        </div>
                                        <div className="col-lg-6">
                                            {/* jika bukan user PMU: 'hide' */}
                                            <div className={`${auth.institution !== 'PMU' && 'hide'}`}>
                                                <FormSelect
                                                    name="institution_code"
                                                    label="Institusi"
                                                    className="form-control"
                                                    register={register}
                                                    rules={{ required: true }}
                                                    errors={errors.institution_code}
                                                    onChange={() => ''}
                                                >
                                                    <option value="">-- Pilih --</option>
                                                    {
                                                        institution.map((item, index) => {
                                                            return <option value={item.institution_code} key={index}>{item.institution_code}</option>
                                                        })
                                                    }
                                                </FormSelect>
                                            </div>
                                        </div>
                                        {/* <div className="col-lg-4">
                                            <FormInput
                                                name="package_no"
                                                type="text"
                                                label="Nomor Paket"
                                                className="form-control"
                                                placeholder="Nomor Paket"
                                                register={register}
                                                rules={{ required: true }}
                                                readOnly={false}
                                                errors={errors.package_no}
                                            />
                                        </div> */}
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-6">
                                            <FormSelect
                                                name="advertisment_quarter"
                                                label="Triwulan"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.advertisment_quarter}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                <option value="Q1">Q1</option>
                                                <option value="Q2">Q2</option>
                                                <option value="Q3">Q3</option>
                                                <option value="Q4">Q4</option>
                                            </FormSelect>
                                        </div>
                                        <div className="col-lg-6">
                                            <FormSelect
                                                name="advertisment_year"
                                                label="Tahun"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.advertisment_year}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                                <option value="2027">2027</option>
                                            </FormSelect>
                                        </div>
                                    </div>

                                    <FormInput
                                        name="general_description"
                                        type="textarea"
                                        label="Paket Pengadaan"
                                        className="form-control"
                                        placeholder="Paket Pengadaan"
                                        register={register}
                                        rules={{ required: true }}
                                        readOnly={false}
                                        errors={errors.general_description}
                                    />

                                    <div className="row">
                                        <div className="col-lg-4">
                                            <FormSelect
                                                name="procurement_method"
                                                label="Metode"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.procurement_method}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                {
                                                    category_method.map((item, index) => {
                                                        return <option value={item.method} key={index}>{item.method}</option>
                                                    })
                                                }
                                            </FormSelect>
                                        </div>
                                        <div className="col-lg-4">
                                            <FormSelect
                                                name="review"
                                                label="Review"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.review}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                {
                                                    category_review.map((item, index) => {
                                                        return <option value={item.review} key={index}>{item.review}</option>
                                                    })
                                                }
                                            </FormSelect>
                                        </div>
                                        <div className="col-lg-4">
                                            <FormInput
                                                name="type_proposal"
                                                type="text"
                                                label="Tipe Proposal"
                                                className="form-control"
                                                placeholder="Tipe Proposal"
                                                register={register}
                                                rules={{ required: false }}
                                                readOnly={false}
                                                errors={errors.type_proposal}
                                            />
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="font-weight-bolder">Nilai Estimasi (Rp.):</label>
                                                <Controller
                                                    control={control}
                                                    name="est_value_idr"
                                                    rules={{ required: true }}
                                                    render={({ field: { ref, ...rest } }) => (
                                                        <NumericFormat
                                                            id="est_value_idr"
                                                            className="form-control"
                                                            thousandSeparator=","
                                                            decimalSeparator="."
                                                            decimalScale={2}
                                                            getInputRef={ref}
                                                            {...rest}
                                                        />
                                                    )}
                                                />
                                                {errors.est_value_idr && <span className="form-text text-danger">* Nilai Estimasi</span>}
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <FormInput
                                                name="status"
                                                type="text"
                                                label="Status"
                                                className="form-control form-control-solid border"
                                                placeholder=""
                                                register={register}
                                                rules={{ required: false }}
                                                readOnly={true}
                                                errors={errors.status}
                                            />
                                        </div>
                                    </div>

                                    <FormInput
                                        name="comments"
                                        type="textarea"
                                        label="Keterangan (Opsional)"
                                        className="form-control"
                                        placeholder="Keterangan"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.comments}
                                    />
                                    <div className="d-flex justify-content-between">
                                        <ButtonCancel to="/procurement" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>

            </Container>
        </MainContent>
    )
}

export default ProcurementPackageCreate