import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import FormUploadPlanning from './FormUploadPlanning';
import FormUploadImplementation from './FormUploadImplementation';
import FormUploadDisbursement from './FormUploadDisbursement';
import FormUploadFinish from './FormUploadFinish';
import HeaderFormVerification from './HeaderFormVerification';
import { apiProcurementPackageShow } from 'app/services/apiProcurement';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';

const data_status = [
    { navigate: '1', status: '' },
    { navigate: '1', status: null },
    { navigate: '1', status: 'Usulan' },
    { navigate: '1', status: 'Persiapan' },
    { navigate: '2', status: 'Kontrak' },
    { navigate: '3', status: 'Pencairan' },
    { navigate: '4', status: 'Selesai' },
];

function MainFormVerification() {
    const dispatch = useDispatch();
    const { id } = useParams();
    const { package_show } = useSelector(state => state.procurement);
    const { status } = package_show;
    const [navigate, setNavigate] = useState('1');

    useEffect(() => {
        dispatch(apiProcurementPackageShow({ id }));
    }, [dispatch, id]);

    useEffect(() => {
        //? utk cek status paket & navigasi aktif
        const filteredStatus = data_status.filter(item => item.status === status);
        if (filteredStatus.length > 0) {
            setNavigate(filteredStatus[0].navigate);
        }
    }, [status]);

    return (
        <MainContent>
            <SubHeader active_page="Procurement" menu_name="Aktivitas" modul_name="Tahap Verifikasi Dokumen">
                <span className="font-weight-bolder text-muted">Paket Pengadaan {package_show.institution_code} - Tahun {package_show.advertisment_year}</span>
            </SubHeader>
            <Container>
                <HeaderFormVerification navigate={navigate} />
                <Card>
                    <CardBody>
                        {navigate === '1' && <FormUploadPlanning packages={package_show} setNavigate={setNavigate} />}
                        {navigate === '2' && <FormUploadImplementation packages={package_show} setNavigate={setNavigate} />}
                        {navigate === '3' && <FormUploadDisbursement packages={package_show} setNavigate={setNavigate} />}
                        {navigate === '4' && <FormUploadFinish packages={package_show} setNavigate={setNavigate} />}
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default MainFormVerification