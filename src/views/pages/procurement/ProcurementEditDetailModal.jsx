import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { ButtonSubmit } from 'views/components/button';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import { apiProcurementPackageDetailUpdate } from 'app/services/apiProcurement';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function ProcurementEditDetailModal({ package_detail, apiProcurementPackageDetailList }) {
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        reset({ 
            id: package_detail.id,
            package_no: package_detail.package_no,
            package_no_lot: package_detail.package_no_lot,
            general_description: package_detail.general_description,
            est_value_usd: package_detail.est_value_usd,
            est_value_jpy: package_detail.est_value_jpy,
            est_value_idr: package_detail.est_value_idr,
        });
    }, [dispatch, package_detail]);

    const onSubmitPackageDetailUpdate = async (data) => {
        try {
            const { payload } = await dispatch(apiProcurementPackageDetailUpdate(data))
            if (payload.status === 200) {
                dispatch(apiProcurementPackageDetailList({ package_no: package_detail.package_no }));
                SwalAlertSuccess('Action Success', 'Success insert data package!');
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalProcurementPackageEditDetail" modal_size="modal-lg">
            <ModalHeader title={`Package Detail - ${package_detail.package_no}`} />
            <form onSubmit={handleSubmit(onSubmitPackageDetailUpdate)} className="form">
                <ModalBody>
                    <div className="mb-10">
                        <input type="hidden" {...register('id', { required: true })} name="id" />
                        <input type="hidden" {...register('package_no', { required: true })} name="package_no" />
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">No Package Lot:</label>
                            <div className="col-lg-6">
                                <input type="text" {...register('package_no_lot', { required: true })} className={`form-control ${errors.package_no_lot && 'is-invalid'}`} placeholder="Enter No Package Lot" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">General Description :</label>
                            <div className="col-lg-6">
                                <textarea
                                    name="general_description"
                                    rows="3"
                                    cols="30"
                                    readOnly={false}
                                    className={`form-control ${errors.general_description && 'is-invalid'}`}
                                    {...register('general_description', { required: true })}
                                ></textarea>
                                <span className="form-text text-muted">Please enter General Description</span>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">Est. Value USD:</label>
                            <div className="col-lg-6">
                                <input type="number" {...register('est_value_usd', { required: true })} className={`form-control ${errors.est_value_usd && 'is-invalid'}`} placeholder="Enter Est. Value USD" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">Est. Value JPY:</label>
                            <div className="col-lg-6">
                                <input type="number" {...register('est_value_jpy', { required: true })} className={`form-control ${errors.est_value_jpy && 'is-invalid'}`} placeholder="Enter Est. Value JPY" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">Est. Value IDR:</label>
                            <div className="col-lg-6">
                                <input type="number" {...register('est_value_idr', { required: true })} className={`form-control ${errors.est_value_idr && 'is-invalid'}`} placeholder="Enter Est. Value IDR" />
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ProcurementEditDetailModal