import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { NumericFormat } from "react-number-format";

import ListDocsUploaded from './ListDocsUploaded';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { ButtonSubmit } from 'views/components/button';
import {
    apiListDocsUploaded,
    apiInsertDocsUploaded,
    apiProcurement_UpdateStatus,
    apiProcurementStatusContract,
} from 'app/services/apiProcurement';
import { authUser } from 'app/slice/sliceAuth';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiContractDetail, apiContractUpdate } from 'app/services/apiContract';

function FormUploadImplementation({ packages, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { control, register, formState: { errors }, handleSubmit, reset } = useForm();
    const {
        register: register2,
        formState: { errors: errors2 },
        handleSubmit: handleSubmit2,
    } = useForm();
    const { contract_detail } = useSelector(state => state.contract);
    const { docs_uploaded_list } = useSelector(state => state.master);
    const { package_no, institution_code, procurement_method } = packages;

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'procurement', status: 'Pelaksanaan', method: procurement_method }));
    }, [dispatch, procurement_method]);

    useEffect(async () => {
        const { payload } = await dispatch(apiContractDetail({ package_no }));
        if (payload.status !== 200) return;
        reset({
            contract_no: payload?.data?.contract_no,
            contract_no_adb: payload?.data?.contract_no_adb,
            contract_vendor: payload?.data?.contract_vendor,
            contract_date: (payload?.data?.contract_date)?.split('T')[0],
            contract_value: payload?.data?.contract_value,
            contract_periode: payload?.data?.contract_periode,
            contract_last_date: (payload?.data?.contract_last_date)?.split('T')[0],
        });
    }, [dispatch, package_no]);

    useEffect(() => {
        reset({ package_no, institution: institution_code });
    }, [reset, package_no, institution_code]);

    const onSubmitStatusContract = async (data) => {
        try {
            const { payload } = await dispatch(apiContractUpdate(data))
            if (payload.status === 200) {
                dispatch(apiProcurementStatusContract({ package_no, contract_no: data.contract_no }))
                SwalAlertSuccess('Sukses', 'Berhasil simpan data.');
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiInsertDocsUploaded(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil unggah dokumen.');
                dispatch(apiListDocsUploaded({ package_no, status: 'Pelaksanaan' }));
                setLoading('');
                setFileDoc('');
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    const onChangeGetPeriode = (e) => {
        let start_date = new Date(e.target.value);
        const periode = document.getElementById('contract_periode');
        const contract_value = document.getElementById('contract_value');
        const month = Number(periode.value) + Number(start_date.getMonth());
        start_date.setMonth(start_date.getMonth() + month);
        reset({
            contract_value: contract_value?.value,
            contract_last_date: start_date.toISOString().substring(0, 10),
        });
    }

    const onSubmitNextBtn = () => {
        if (contract_detail) {
            setNavigate('3');
            dispatch(apiProcurement_UpdateStatus({ package_no, status: 'Pelaksanaan' }));
        }
        else {
            SwalAlertError('Gagal!', '*Wajib isi kontrak & unggah kelengkapan dokumen kontrak.');
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmitStatusContract)} id='form1' className="mb-4">
                <h3 className="font-size-lg text-dark font-weight-bold mb-5">2.1 Form Kontrak:</h3>
                <input type="hidden" {...register("package_no", { required: true })} />
                <input type="hidden" {...register("institution", { required: true })} />
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">No Kontrak PKS:</label>
                    <div className="col-lg-6">
                        <input type="text" {...register("contract_no", { required: true })} className="form-control" placeholder="No Kontrak PKS" />
                        {errors.contract_no && <span className="form-text text-danger">* No Kontrak PKS.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">No Kontrak ADB:</label>
                    <div className="col-lg-6">
                        <input type="text" {...register("contract_no_adb", { required: false })} className="form-control" placeholder="No Kontrak ADB" />
                        {errors.contract_no_adb && <span className="form-text text-danger">* No Kontrak ADB.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Nama Penyedia:</label>
                    <div className="col-lg-6">
                        <input type="text" {...register("contract_vendor", { required: true })} className="form-control" placeholder="Nama Penyedia" />
                        {errors.contract_vendor && <span className="form-text text-danger">* Nama Penyedia.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Nilai Kontrak (Rp.):</label>
                    <div className="col-lg-6">
                        <Controller
                            control={control}
                            name="contract_value"
                            rules={{ required: true }}
                            render={({ field: { ref, ...rest } }) => (
                                <NumericFormat
                                    id="contract_value"
                                    className="form-control"
                                    thousandSeparator=","
                                    decimalSeparator="."
                                    decimalScale={2}
                                    getInputRef={ref}
                                    {...rest}
                                />
                            )}
                        />
                        {errors.contract_value && <span className="form-text text-danger">* Nilai Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Periode Kontrak (Bulan):</label>
                    <div className="col-lg-6">
                        <input
                            type="text"
                            {...register("contract_periode", { required: true })}
                            id="contract_periode"
                            className="form-control"
                            placeholder="Periode Kontrak"
                        />
                        {errors.contract_periode && <span className="form-text text-danger">* Periode Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Tanggal Kontrak:</label>
                    <div className="col-lg-6">
                        <input
                            type="date"
                            {...register("contract_date", { required: true })}
                            className="form-control"
                            placeholder="Tanggal Kontrak"
                            onChange={(e) => onChangeGetPeriode(e)}
                        />
                        {errors.contract_date && <span className="form-text text-danger">* Tanggal Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Tanggal Akhir Kontrak:</label>
                    <div className="col-lg-6">
                        <input type="date" {...register("contract_last_date", { required: true })} id="contract_last_date" className="form-control" placeholder="Tanggal Akhir Kontrak" />
                        {errors.contract_last_date && <span className="form-text text-danger">* Tanggal Akhir Kontrak.</span>}
                    </div>
                </div>
                {
                    //? hanya PMU yg bisa edit contract
                    auth.institution === 'PMU' &&
                    <div className="form-group row">
                        <label className="col-lg-3 col-form-label text-right" />
                        <div className="col-lg-6">
                            <ButtonSubmit />
                        </div>
                    </div>
                }
            </form>

            <div className="separator separator-dashed separator-border-2 my-4" />
            <h3 className="font-size-lg text-dark font-weight-bold my-4">2.2 Unggah Dokumen Kelengkapan Kontrak:</h3>
            <form onSubmit={handleSubmit2(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                <input type="hidden" defaultValue={package_no} {...register2("package_no", { required: true })} />
                <input type="hidden" defaultValue="Pelaksanaan" {...register2("status", { required: true })} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen :</label>
                            <select name="document_name" className="form-control form-control-md" {...register2("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors2.document_name && <span className="form-text text-danger">Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen :</label>
                            <div className={`custom-file ${file_doc && 'hide'}`}>
                                <input type="file" {...register2("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                            </div>
                            <div className={`${(!file_doc) && 'hide'}`}>
                                <div className="form-control alert alert-custom alert-light-dark fade show p-2" role="alert">
                                    <div className="alert-text text-truncate" title={file_doc.name}>{file_doc.name} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(file_doc.size)})</div>
                                    <div className="alert-close">
                                        <button type="button" onClick={() => setFileDoc('')} className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i className="ki ki-close" title="Hapus Berkas" /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {errors2.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register2("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={package_no} status='Pelaksanaan' />

            <div className="separator separator-dashed separator-border-2 my-4" />
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('1')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div>
        </div>
    )
}

export default FormUploadImplementation