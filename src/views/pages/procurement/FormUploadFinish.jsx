import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import ListDocsUploaded from './ListDocsUploaded';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiListDocsUploaded,
    apiProcurementStatusFinish,
    apiProcurement_UpdateStatus,
} from 'app/services/apiProcurement';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';

function FormUploadFinish({ packages, setNavigate }) {
    const dispatch = useDispatch();
    const history = useHistory()
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { docs_uploaded_list } = useSelector(state => state.master);
    const { package_no, procurement_method } = packages;

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'procurement', status: 'Selesai', method: procurement_method }));
    }, [dispatch, procurement_method]);

    useEffect(() => {
        reset({ package_no });
    }, [reset, package_no]);

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiProcurementStatusFinish(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!');
                dispatch(apiListDocsUploaded({ package_no, status: 'Selesai' }));
                setLoading('');
                setFileDoc('');
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
            setLoading('');
        }
    }

    const onSubmitNextBtn = () => {
        if (package_no) {
            dispatch(apiProcurement_UpdateStatus({ package_no, status: 'Selesai' }));
            history.push("/procurement");
        }
        else {
            SwalAlertError('Gagal.', '*Wajib Unggah Dokumen Realisasi Anggaran.');
        }
    }

    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold my-4">4.1. Unggah Dokumen Realisasi Anggaran:</h3>
            <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                <input type="hidden" defaultValue={package_no} {...register("package_no", { required: true })} />
                <input type="hidden" defaultValue="Selesai" {...register("status", { required: true })} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen * :</label>
                            <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen * :</label>
                            <div className={`custom-file ${file_doc && 'hide'}`}>
                                <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                            </div>
                            <div className={`${(!file_doc) && 'hide'}`}>
                                <div className="form-control alert alert-custom alert-light-dark fade show p-2" role="alert">
                                    <div className="alert-text text-truncate" title={file_doc.name}>{file_doc.name} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(file_doc.size)})</div>
                                    <div className="alert-close">
                                        <button type="button" onClick={() => setFileDoc('')} className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i className="ki ki-close" title="Hapus Berkas" /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={package_no} status='Selesai' />

            <div className="separator separator-dashed separator-border-2 my-4" />
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selesai</button>
            </div>
        </div>
    )
}

export default FormUploadFinish