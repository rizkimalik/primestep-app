import React, { useEffect } from 'react';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, Selection } from 'devextreme-react/data-grid'

import { ButtonRefresh, ButtonCreate } from 'views/components/button';
import { authUser } from 'app/slice/sliceAuth';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card'
import { apiIssuesChallengesList, apiIssuesChallengesDelete } from 'app/services/apiIssuesChallenges';

function IssuesChallengesMain() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { issues_challenges } = useSelector(state => state.issues_challenges);

    useEffect(() => {
        dispatch(apiIssuesChallengesList({ user_level: auth.user_level, institution: auth.institution }))
    }, [dispatch, auth]);

    function handlerDeleteData(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiIssuesChallengesDelete({ id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Success Delete.",
                        text: `deleted from database!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiIssuesChallengesList({ user_level: auth.user_level, institution: auth.institution }))
                }
            }
        });
    }

    function componentButtonActions(data) {
        const { id } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <Link to={`/issue-challenge/${id}`} className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                    <i className="fas fa-edit fa-sm"></i>
                </Link>
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" onClick={() => handlerDeleteData(id)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Issues Challenges" menu_name="Implementation issues and challenges" modul_name="Proposed actions and solutions">
                <ButtonRefresh onClick={() => dispatch(apiIssuesChallengesList({ user_level: auth.user_level, institution: auth.institution }))} />
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <div className="d-flex justify-content-between mb-2">
                            <h3>Implementation Issues and Challenges</h3>
                            <ButtonCreate text="Add Issues / Challenge" to="/issue-challenge/create" />
                        </div>
                        <DataGrid
                            dataSource={issues_challenges}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Selection mode="single" />
                            <Column caption="Action" dataField="id" fixed={false} cellRender={componentButtonActions} />
                            <Column caption="Package No" dataField="package_no" />
                            <Column caption="Contract No" dataField="contract_no" />
                            <Column caption="Issue Detail" dataField="issue_detail" width={250} />
                            <Column caption="Solving Solution" dataField="solving_solution" width={250} />
                            <Column caption="Institution" dataField="institution" />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default IssuesChallengesMain