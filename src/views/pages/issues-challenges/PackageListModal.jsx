import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, Paging, Selection, Pager } from 'devextreme-react/data-grid'
import { ButtonRefresh } from 'views/components/button';
import { apiProcurementPackageList } from 'app/services/apiProcurement';
import { authUser } from 'app/slice/sliceAuth';
import { IconMark } from 'views/components/icon';

function PackageListModal({ setPackageNo }) {
    const dispatch = useDispatch();
    const refCloseModal = useRef();
    const auth = useSelector(authUser);
    const { package_list } = useSelector(state => state.procurement);

    useEffect(() => {
        dispatch(apiProcurementPackageList({ user_level: auth.user_level, institution: auth.institution }))
    }, [dispatch]);

    function OnSelectionChanged(data) {
        setPackageNo({ 
            package_no: data.package_no, 
            contract_no: data.contract_no, 
            institution: data.institution_code,
        });
        refCloseModal.current.click();
    }

    function componentButtonActions(data) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={() => OnSelectionChanged(data)}
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> {data.package_no}
                </button>
            </div>
        )
    }

    return (
        <div className="modal fade" id="modalPackageList" tabIndex={-1} role="dialog" aria-labelledby="modalPackageList" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalPackageListLabel">Reference Package No.</h5>
                        <button type="button" ref={refCloseModal} className="close" data-toggle="modal" data-target="#modalPackageList" aria-label="Close">
                            <i aria-hidden="true" className="ki ki-close"></i>
                        </button>
                    </div>
                    <div className="modal-body">
                        <span className="text-muted">Click row select data.</span>
                        <DataGrid
                            dataSource={package_list}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            hoverStateEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            columnMinWidth={80}
                        >
                            <Selection mode="single" />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Package No" dataField="package_no" cellRender={({ data }) => componentButtonActions(data)} />
                            <Column caption="General Description" dataField="general_description" width={250} />
                            <Column caption="Status" dataField="status" />
                            <Column caption="Adv. Quarter" dataField="advertisment_quarter" />
                            <Column caption="Adv. Year" dataField="advertisment_year" />
                            <Column caption="Procurement Method" dataField="procurement_method" />
                            <Column caption="Review" dataField="review" />
                            <Column caption="Bidding Procedure" dataField="type_proposal" />
                        </DataGrid>
                    </div>
                    <div className="modal-footer p-2">
                        <ButtonRefresh onClick={() => dispatch(apiProcurementPackageList({ user_level: auth.user_level, institution: auth.institution }))} />
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PackageListModal