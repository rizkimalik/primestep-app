import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import PackageListModal from './PackageListModal';
import { FormInput, FormSelect, FormGroup } from 'views/components/form';
import { Card, CardBody } from 'views/components/card';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiIssuesChallengesInsert } from 'app/services/apiIssuesChallenges';
import { apiMasterInstitution } from 'app/services/apiMasterData';

function IssuesChallengesCreate() {
    const dispatch = useDispatch();
    const { id } = useParams();
    const { institution } = useSelector(state => state.master)
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const [package_no, setPackageNo] = useState('');

    useEffect(() => {
        dispatch(apiMasterInstitution());
    }, [dispatch]);

    useEffect(() => {
        reset({ 
            package_no: package_no.package_no, 
            contract_no: package_no.contract_no,
            institution: package_no.institution 
        });
    }, [reset, package_no]);

    const onSubmitUpdateData = async (data) => {
        try {
            const { payload } = await dispatch(apiIssuesChallengesInsert(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!')
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Issues Challenges" menu_name="Add New Issue" modul_name="" />
            <Container>
                <PackageListModal setPackageNo={setPackageNo} />
                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Add Issue / Challange</h3>
                                <form onSubmit={handleSubmit(onSubmitUpdateData)} className="form">
                                    <input type="hidden" name="id" defaultValue={id} />
                                    <div className="row">
                                        <div className="col-lg-4">
                                            <FormGroup label="Package No">
                                                <div className="input-group">
                                                    <input type="text" name="package_no" {...register("package_no", { required: false })} className="form-control" placeholder="Search Package No..." />
                                                    <div className="input-group-append">
                                                        <button type="button" className="btn btn-light" data-toggle="modal" data-target="#modalPackageList">
                                                            <i className="fas fa-search fa-sm" />
                                                        </button>
                                                    </div>
                                                </div>
                                            </FormGroup>
                                        </div>
                                        <div className="col-lg-4">
                                            <FormInput
                                                name="contract_no"
                                                type="text"
                                                label="Contract No"
                                                className="form-control"
                                                placeholder="Enter Contract No"
                                                register={register}
                                                rules={{ required: false }}
                                                readOnly={false}
                                                errors={errors.contract_no}
                                            />
                                        </div>
                                        <div className="col-lg-4">
                                            <FormSelect
                                                name="institution"
                                                label="Institution"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.institution}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- select institution --</option>
                                                {
                                                    institution.map((item, index) => {
                                                        return <option value={item.institution_code} key={index}>{item.institution_code}</option>
                                                    })
                                                }
                                            </FormSelect>
                                        </div>
                                    </div>

                                    <FormInput
                                        name="issue_detail"
                                        type="textarea"
                                        label="Issue Detail"
                                        className="form-control"
                                        placeholder="Enter Issues Detail"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.issue_detail}
                                    />
                                    <FormInput
                                        name="solving_solution"
                                        type="textarea"
                                        label="Solving Solution"
                                        className="form-control"
                                        placeholder="Enter Solving Solution"
                                        register={register}
                                        rules=""
                                        readOnly={false}
                                        errors={errors.solving_solution}
                                    />
                                    <div className="d-flex justify-content-between">
                                        <ButtonCancel to="/issue-challenge" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default IssuesChallengesCreate