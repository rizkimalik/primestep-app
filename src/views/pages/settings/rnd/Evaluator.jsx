import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Lookup, Pager, Paging, } from 'devextreme-react/data-grid';

import SideMenu from './SideMenu';
import ModalEvaluator from './ModalEvaluator';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { apiEvaluator_List } from 'app/services/apiEvaluator';
import { apiEvaluator_Delete } from 'app/services/apiEvaluator';
import { apiMasterCategoryResearch } from 'app/services/apiMasterData';

function Evaluator() {
    const dispatch = useDispatch();
    const [evaluator_id, setEvaluatorID] = useState('');
    const { evaluator_list } = useSelector(state => state.evaluator)
    const { category_focus } = useSelector(state => state.master)

    useEffect(() => {
        dispatch(apiMasterCategoryResearch());
        dispatch(apiEvaluator_List());
    }, [dispatch])

    function formatWhatsappNumber(phoneNumber) {
        // Cek apakah nomor telepon diawali dengan 0
        if (phoneNumber?.charAt(0) === '0') {
            // Ganti 0 dengan 62
            return phoneNumber?.replace('0', '62');
        } else {
            // Kembalikan nomor telepon tanpa perubahan
            return phoneNumber;
        }
    }

    function handlerDeleteData(row) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiEvaluator_Delete({ id: row.id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Berhasil Hapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiEvaluator_List());
                }
            }
        });
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(row)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <button onClick={() => setEvaluatorID(row.id)} data-toggle="modal" data-target="#modalEvaluator" className="btn btn-sm btn-clean btn-icon btn-hover-warning" title="Edit Data">
                    <i className="fas fa-edit fa-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Startup & Research" modul_name="Evaluator " />
            <Container>
                <ModalEvaluator id={evaluator_id} />
                <div className="row">
                    <div className="col-md-12 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between mb-4">
                                            <h3>Daftar Evaluator</h3>
                                            <button type="button" onClick={() => setEvaluatorID('')} data-toggle="modal" data-target="#modalEvaluator" className="btn btn-primary font-weight-bold btn-sm">
                                                <i className="fa fa-plus fa-sm" />
                                                Tambah Evaluator
                                            </button>
                                        </div>
                                        <DataGrid
                                            dataSource={evaluator_list}
                                            remoteOperations={true}
                                            rowAlternationEnabled={true}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={80}
                                        >
                                            <HeaderFilter visible={false} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Column caption="Aksi" dataField="id" cellRender={componentButtonActions} />
                                            <Column caption="Nama Evaluator" dataField="name" />
                                            <Column caption="NIDN" dataField="username" />
                                            <Column caption="Institusi" dataField="institution_name" />
                                            <Column caption="Telp" dataField="email_address" />
                                            <Column caption="Whatsapp" dataField="whatsapp" cellRender={({ data }) => {
                                                return <a href={`https://wa.me/${formatWhatsappNumber(data.email_address)}`} target="_blank" className="text-success"><i className="fab fa-whatsapp fa-sm text-success" /> {formatWhatsappNumber(data.email_address)}</a>
                                            }} />
                                            <Column caption="Bidang Fokus" dataField="focus_area">
                                                <Lookup
                                                    dataSource={category_focus}
                                                    valueExpr="id"
                                                    displayExpr="focus_name"
                                                />
                                            </Column>
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Evaluator