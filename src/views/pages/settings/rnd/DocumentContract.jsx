import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Lookup, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import SideMenu from './SideMenu';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ButtonRefresh } from 'views/components/button';
import { apiDocumentStatusList } from 'app/services/apiDocumentStatus';

function DocumentContract() {
    const dispatch = useDispatch();
    const { document_status } = useSelector(state => state.document_status);

    useEffect(() => {
        dispatch(apiDocumentStatusList({ modul: 'research-startup'}));
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Startup & Research" modul_name="Document & Status" />
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <SideMenu />
                    </div>
                    <div className="col-md-9 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between mb-0">
                                            <h3>Dokumen Kontrak</h3>
                                            <ButtonRefresh onClick={() => dispatch(apiDocumentStatusList({ modul: 'research-startup'}))} />
                                        </div>

                                        <DataGrid
                                            dataSource={document_status}
                                            remoteOperations={{
                                                filtering: true,
                                                sorting: true,
                                                paging: true,
                                                summary: true
                                            }}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={100}
                                            onInitNewRow={(e) => {
                                                e.data.modul = 'research-startup';
                                            }}
                                        >
                                            <HeaderFilter visible={true} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Editing
                                                mode="row"
                                                refreshMode='reshape'
                                                allowAdding={true}
                                                allowDeleting={true}
                                                allowUpdating={true}
                                            />
                                            <Toolbar>
                                                <Item name="addRowButton" showText="always" location="after" widget="dxButton" options={{ icon: 'plus', text: 'Tambah Dokumen' }} />
                                            </Toolbar>
                                            <Column caption="Actions" type="buttons">
                                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                                    <i className="fas fa-trash-alt icon-sm"></i>
                                                </Button>
                                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                                    <i className="fas fa-edit icon-sm"></i>
                                                </Button>
                                            </Column>
                                            <Column caption="Modul" dataField="modul" width={180} allowEditing={false} visible={false} />
                                            <Column caption="Tahap Verifikasi Dokumen" dataField="status" >
                                                <Lookup
                                                    dataSource={[
                                                        { status: 'Kontrak' },
                                                        { status: 'Pencairan' },
                                                        { status: 'Finish' },
                                                    ]}
                                                    valueExpr="status"
                                                    displayExpr="status"
                                                />
                                            </Column>
                                            <Column caption="Nama Dokumen" dataField="document_name" />
                                            <Column caption="User Level" dataField="user_level" >
                                                <Lookup
                                                    dataSource={[
                                                        { user_level: 'PMU' },
                                                        { user_level: 'PIU' },
                                                    ]}
                                                    valueExpr="user_level"
                                                    displayExpr="user_level"
                                                />
                                            </Column>
                                            <Column caption="Tahap Pencairan" dataField="document_section" >
                                                <Lookup
                                                    dataSource={[
                                                        { document_section: '' },
                                                        { document_section: '70%' },
                                                        { document_section: '30%' },
                                                    ]}
                                                    valueExpr="document_section"
                                                    displayExpr="document_section"
                                                />
                                            </Column>
                                            <Column caption="Upload Dokumen" dataField="upload_document" dataType="boolean" width={100} />
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default DocumentContract