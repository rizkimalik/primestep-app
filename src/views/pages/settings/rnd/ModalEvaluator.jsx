import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiMasterCategoryResearch } from 'app/services/apiMasterData';
import { apiEvaluator_List, apiEvaluator_InsertUpdate, apiEvaluator_Detail } from 'app/services/apiEvaluator';
import UserResetPassword from '../user/UserResetPassword';

function ModalEvaluator({ id }) {
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { category_focus } = useSelector(state => state.master)

    useEffect(() => {
        dispatch(apiMasterCategoryResearch());
    }, [dispatch]);

    useEffect(() => {
        reset();
        reset({ id });
    }, [reset, id]);

    useEffect(async () => {
        reset();
        if (id) {
            const { payload } = await dispatch(apiEvaluator_Detail({ id }));
            reset({
                name: payload.data.name,
                focus_area: payload.data.focus_area,
                institution_name: payload.data.institution_name,
                email_address: payload.data.email_address,
                username: payload.data.username,
                password: payload.data.password,
            });
        } else {
            reset();
        }
    }, [reset, id]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiEvaluator_InsertUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data.')
                dispatch(apiEvaluator_List());
                reset();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalEvaluator" modal_size="modal-lg">
            <ModalHeader title="Evaluator Profile" />
            <UserResetPassword userid={id} />

            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form" id="form-evaluator" encType="multipart/form-data">
                <ModalBody>
                    <input type="hidden" {...register("id", { required: false })} />
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Nama Evaluator :</label>
                                <input type="text" {...register("name", { required: true, maxLength: 100 })} className="form-control" placeholder="Nama & Gelar" />
                                {errors.name && <span className="form-text text-danger">* Nama Evaluator</span>}
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Telp / Whatsapp :</label>
                                <input type="text" {...register("email_address", { required: true })} className="form-control" placeholder="Telp" />
                                {errors.email_address && <span className="form-text text-danger">* Telp / Whatsapp</span>}
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-lg-6">
                            <label className="font-weight-bolder">Bidang Fokus :</label>
                            <select
                                {...register("focus_area", { required: true })}
                                className="form-control form-control-md"
                            >
                                <option value="">-- Pilih --</option>
                                {
                                    category_focus.map((item, index) => {
                                        return <option value={item.id} key={index}>{index + 1}. {item.focus_name}</option>
                                    })
                                }
                            </select>
                            {errors.focus_area && <span className="form-text text-danger">* Bidang Fokus</span>}
                        </div>
                        <div className="col-lg-6">
                            <label className="font-weight-bolder">Institusi:</label>
                            <input type="text" {...register("institution_name", { required: true })} className="form-control" placeholder="Lembaga Institusi" />
                            {errors.institution_name && <span className="form-text text-danger">* Institusi</span>}
                        </div>
                    </div>

                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                    <h5 className="text-dark font-weight-bold my-4">Login</h5>
                    <div className="form-group row">
                        <div className="col-lg-6">
                            <label className="font-weight-bolder">NIDN / Username:</label>
                            <input type="text" {...register("username", { required: true, maxLength: 100 })} className="form-control" placeholder="Username" />
                            <span className="form-text text-muted">NIDN digunakan sebagai login aplikasi</span>
                            {errors.username && <span className="form-text text-danger">* Username login</span>}
                        </div>
                        <div className="col-lg-6">
                            <label className="font-weight-bolder">Password:</label>
                            {
                                id === ''
                                    ? <div>
                                        <input type="password" {...register("password", { required: true })} className="form-control" placeholder="Password" />
                                        {errors.password && <span className="form-text text-danger">* Login password</span>}
                                    </div>
                                    : <button type="button" className="btn btn-light-dark form-control" data-toggle="modal" data-target="#modalResetPassword">Reset Password</button>
                            }

                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalEvaluator
