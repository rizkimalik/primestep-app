import React from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody } from 'views/components/card'

function SideMenu() {
    return (
        <Card>
            <CardBody className="px-5">
                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                    <div className="navi-section mb-2 font-size-h3 pb-0">Startup & Research</div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/bidang-fokus" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Bidang Fokus</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/framework" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Framework (Kerangka)</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/document-startup" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Dokumen Startup </span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/document-research" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Dokumen Research </span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/document-contract" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Dokumen Kontrak</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/evaluator" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Daftar Evaluator</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/rnd/pagu" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">PAGU Kontrak</span>
                        </Link>
                    </div>
                </div>
            </CardBody>
        </Card>
    )
}

export default SideMenu