import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ButtonRefresh } from 'views/components/button';
import { apiGenderActionMaster } from 'app/services/apiGenderAction';

function GenderActionPlan() {
    const dispatch = useDispatch();
    const { gender_action_master } = useSelector(state => state.gender_action);

    useEffect(() => {
        dispatch(apiGenderActionMaster());
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Gender Action Plan" modul_name="Master Data" />
            <Container>
                <div className="row">
                    <div className="col-md-12 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between">
                                            <h3>Gender Action Plan</h3>
                                            <ButtonRefresh onClick={() => dispatch(apiGenderActionMaster())} />
                                        </div>

                                        <DataGrid
                                            dataSource={gender_action_master}
                                            remoteOperations={{
                                                filtering: true,
                                                sorting: true,
                                                paging: true,
                                                summary: true
                                            }}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={100}
                                        >
                                            <HeaderFilter visible={true} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Editing
                                                mode="row"
                                                refreshMode='reshape'
                                                allowAdding={true}
                                                allowDeleting={true}
                                                allowUpdating={true}
                                            />
                                            <Toolbar>
                                                <Item name="addRowButton" showText="always" location="after" widget="dxButton" options={{ icon: 'plus', text: 'Add New' }} />
                                            </Toolbar>
                                            <Column caption="Actions" type="buttons">
                                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                                    <i className="fas fa-trash-alt icon-sm"></i>
                                                </Button>
                                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                                    <i className="fas fa-edit icon-sm"></i>
                                                </Button>
                                            </Column>
                                            <Column caption="Sort" dataField="sort_code" />
                                            <Column caption="Target % " dataField="target_percentase" dataType="number" />
                                            <Column caption="Description" dataField="gender_action_plan" />
                                            <Column caption="Group Name" dataField="group_name" />
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default GenderActionPlan