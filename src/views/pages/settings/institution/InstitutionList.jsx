import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ButtonRefresh } from 'views/components/button';
import { apiInstitutionList } from 'app/services/apiInstitution';

function InstitutionList() {
    const dispatch = useDispatch();
    const { institutions } = useSelector(state => state.institution);

    useEffect(() => {
        dispatch(apiInstitutionList());
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Institution" menu_name="Framework PRIMESTeP" modul_name="Data List">
                <ButtonRefresh onClick={() => dispatch(apiInstitutionList())} />
            </SubHeader>
            <Container>
                <div className="card card-custom border">
                    <div className="card-body p-5">
                        <h3>Institution - Framework PRIMESTeP</h3>
                        <DataGrid
                            dataSource={institutions}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Editing
                                mode="row"
                                refreshMode='reshape'
                                allowAdding={true}
                                allowDeleting={true}
                                allowUpdating={true}
                            />
                            <Toolbar>
                                <Item name="addRowButton" showText="always" location="after" widget="dxButton" options={{ icon: 'plus', text: 'Add Institution' }} />
                            </Toolbar>
                            <Column caption="Actions" type="buttons">
                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                    <i className="fas fa-trash-alt icon-sm"></i>
                                </Button>
                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                    <i className="fas fa-edit icon-sm"></i>
                                </Button>
                            </Column>
                            <Column caption="Institution" dataField="institution_code" />
                            <Column caption="Institution Name" dataField="institution_name" />
                        </DataGrid>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default InstitutionList