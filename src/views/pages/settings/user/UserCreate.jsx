import React, { useEffect } from 'react';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { apiMasterUserLevel, apiMasterInstitution } from 'app/services/apiMasterData';
import { apiUserInsert } from 'app/services/apiUser';

function UserCreate() {
    const dispatch = useDispatch();
    const history = useHistory();
    const { user_level, institution } = useSelector(state => state.master)
    const { register, formState: { errors }, handleSubmit } = useForm();

    useEffect(() => {
        dispatch(apiMasterUserLevel())
        dispatch(apiMasterInstitution())
    }, [dispatch]);

    const onSubmitCreateUser = async (data) => {
        try {
            const { payload } = await dispatch(apiUserInsert(data));
            if (payload.status === 200) {
                Swal.fire({
                    title: "Insert Success.",
                    text: "Success into application!",
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                    timer: 1500
                });
                history.push('/user');
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="User Create" menu_name="Management User" modul_name="User Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Create New User" subtitle="Form add new user login application." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateUser)} className="form">
                        <CardBody>
                            <div className="form-group row">
                                <div className="col-lg-6">
                                    <label>Full Name:</label>
                                    <input type="text" {...register("name", { required: true, maxLength: 100 })} className="form-control" placeholder="Enter full name" />
                                    {errors.name && <span className="form-text text-danger">Please enter your full name</span>}
                                </div>
                                <div className="col-lg-6">
                                    <label>Email / Telp:</label>
                                    <input type="text" {...register("email_address", { required: true })} className="form-control" placeholder="Enter email" />
                                    {errors.email_address && <span className="form-text text-danger">Please enter your email / telp</span>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-lg-6">
                                    <label>Username:</label>
                                    <input type="text" {...register("username", { required: true, maxLength: 100 })} className="form-control" placeholder="Enter username" />
                                    {errors.username && <span className="form-text text-danger">Please enter your username</span>}
                                </div>
                                <div className="col-lg-6">
                                    <label>Password:</label>
                                    <input type="password" {...register("password", { required: true })} className="form-control" placeholder="Enter password" />
                                    {errors.password && <span className="form-text text-muted">Please enter your password</span>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-lg-6">
                                    <label>User Level:</label>
                                    <select className="form-control" {...register("user_level", { required: true })}>
                                        <option value="">-- User Level --</option>
                                        {
                                            user_level?.map((item) => {
                                                return <option value={item.level_code} key={item.id}>{item.level_name}</option>
                                            })
                                        }
                                    </select>
                                    {errors.user_level && <span className="form-text text-danger">Please enter User Level</span>}
                                </div>
                                <div className="col-lg-6">
                                    <label>Institution:</label>
                                    <select className="form-control" {...register("institution")}>
                                        <option value="">-- Select Institution --</option>
                                        {
                                            institution?.map((item) => {
                                                return <option value={item.institution_code} key={item.id}>{item.institution_code} - {item.institution_name}</option>
                                            })
                                        }
                                    </select>
                                    {errors.institution && <span className="form-text text-danger">Please select Institution</span>}
                                </div>
                            </div>
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/user" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default UserCreate
