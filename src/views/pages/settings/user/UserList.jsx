import React, { useEffect } from 'react'
import Swal from 'sweetalert2';
import DataGrid, { Column, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { useDispatch, useSelector } from 'react-redux';

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { authUser } from 'app/slice/sliceAuth';
import { apiUserList, apiUserDelete } from 'app/services/apiUser';

function UserList() {
    const dispatch = useDispatch();
    const { user_level } = useSelector(authUser);
    const { users } = useSelector(state => state.user);

    useEffect(() => {
        dispatch(apiUserList());
    }, [dispatch])


    async function deleteUserHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const res = await dispatch(apiUserDelete({ id }))
                const data = res.data.data;
                dispatch(apiUserList());
                
                Swal.fire(
                    data.message,
                    "Your data has been deleted.",
                    "success"
                )
            }
        });
    }

    function componentButtonActions(data) {
        const { id, user_level } = data.row.data;
        return (
            <div className={`d-flex align-items-end justify-content-center ${user_level === 'Admin' ? 'hide' : ''}`}>
                <ButtonEdit to={`/user/${id}/edit`} />
                <ButtonDelete onClick={() => deleteUserHandler(id)} />
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Management User" modul_name="">
                <ButtonCreate to="/user/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Management User" subtitle="User login system or interface designed for users." />
                        <CardToolbar>
                            <ButtonRefresh onClick={() => dispatch(apiUserList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={users}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <GroupPanel visible={true} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {
                                (user_level === 'Admin' || user_level === 'SPV') &&
                                <Column caption="Actions" dataField="id" width={150} cellRender={componentButtonActions} />
                            }
                            <Column caption="Username" dataField="username" />
                            <Column caption="Name" dataField="name" />
                            <Column caption="Level" dataField="user_level" />
                            <Column caption="Institution" dataField="institution" />
                            <Column caption="Email / Telp" dataField="email_address" />
                            <Column caption="Login" dataField="login" cellRender={(data) => {
                                return data.value === 1 ? <span className="text-success">Online</span> : <span className="text-danger">Offline</span>;
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default UserList
