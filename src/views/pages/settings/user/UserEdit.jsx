import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserResetPassword from './UserResetPassword';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { useDispatch, useSelector } from 'react-redux';
import { authUser } from 'app/slice/sliceAuth';
import { apiUserShow, apiUserUpdate } from 'app/services/apiUser';
import { apiMasterUserLevel, apiMasterInstitution } from 'app/services/apiMasterData';

function UserEdit() {
    let { id } = useParams();
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { user_level, institution } = useSelector(state => state.master)
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        dispatch(apiMasterUserLevel());
        dispatch(apiMasterInstitution());
    }, [dispatch]);

    useEffect(async () => {
        const { payload } = await dispatch(apiUserShow({ id }))
        if (payload.status === 200) {
            const {
                name,
                username,
                email_address,
                user_level,
                institution
            } = payload.data[0];
            reset({
                id: id,
                name,
                username,
                email_address,
                user_level,
                institution
            });
        }
    }, [id, reset, dispatch]);

    const onSubmitUpdateUser = async (data) => {
        try {
            const { payload } = await dispatch(apiUserUpdate(data));
            if (payload.status === 200) {
                Swal.fire({
                    title: "Update Success.",
                    text: "Success into application!",
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                    timer: 1500
                });
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="User Edit" menu_name="Management User" modul_name="User Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Update User" subtitle="Form update user login." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitUpdateUser)} className="form">
                        <CardBody>
                            <input type="hidden" {...register("id")} />
                            <div className="form-group row">
                                <div className="col-lg-6">
                                    <label>Full Name:</label>
                                    <input type="text" className="form-control" placeholder="Enter full name" {...register("name", { required: true, maxLength: 100 })} />
                                    {errors.name && <span className="form-text text-danger">Please enter your full name</span>}
                                </div>
                                <div className="col-lg-6">
                                    <label>Email / Telp:</label>
                                    <input type="text" className="form-control" placeholder="Enter email" {...register("email_address", { required: true })} />
                                    {errors.email_address && <span className="form-text text-danger">Please enter your email / telp</span>}
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-lg-6">
                                    <label>Username:</label>
                                    <input type="text" {...register("username", { required: true, maxLength: 100 })} className="form-control" placeholder="Enter username" />
                                    {errors.username && <span className="form-text text-danger">Please enter your username</span>}
                                </div>
                                <div className="col-lg-6">
                                    <label>Password:</label><br />
                                    <button type="button" className="btn btn-dark" data-toggle="modal" data-target="#modalResetPassword">Reset Password</button>
                                </div>
                            </div>

                            <section className={auth.user_level === 'Admin' ? 'show' : 'hide'}>
                                <div className="form-group row">
                                    <div className="col-lg-6">
                                        <label>User Level:</label>
                                        <select className="form-control" {...register("user_level", { required: true })}>
                                            <option value="">-- User Level --</option>
                                            {
                                                user_level?.map((item) => {
                                                    return <option value={item.level_code} key={item.id}>{item.level_name}</option>
                                                })
                                            }
                                        </select>
                                        {errors.user_level && <span className="form-text text-danger">Please enter User Level</span>}
                                    </div>
                                    <div className="col-lg-6">
                                        <label>Institution:</label>
                                        <select className="form-control" {...register("institution")}>
                                            <option value="">-- Select Institution --</option>
                                            {
                                                institution?.map((item) => {
                                                    return <option value={item.institution_code} key={item.id}>{item.institution_code} - {item.institution_name}</option>
                                                })
                                            }
                                        </select>
                                        {errors.institution && <span className="form-text text-danger">Please select Institution</span>}
                                    </div>
                                </div>
                            </section>
                        </CardBody>
                        <CardFooter>
                            {
                                (auth.user_level === 'Admin' || auth.user_level === 'PMU') &&
                                <ButtonCancel to="/user" />
                            }
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>

                <UserResetPassword userid={id} />
            </Container>
        </MainContent>
    )
}

export default UserEdit
