import React from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody } from 'views/components/card'

function SideMenu() {
    return (
        <Card>
            <CardBody className="px-5">
                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                    <div className="navi-section mb-2 font-size-h3 pb-0">Penguatan Kelembagaan</div>
                    <div className="navi-item my-2">
                        <Link to="/setting/strengthen/document_status" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Dokumen Upload</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/strengthen/component" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Komponen</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/strengthen/category" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Kategori</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/strengthen/pagu" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">PAGU Kontrak</span>
                        </Link>
                    </div>
                   
                </div>
            </CardBody>
        </Card>
    )
}

export default SideMenu