import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Lookup, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import SideMenu from './SideMenu';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { apiPaguContract } from 'app/services/apiPaguContract';

function PaguContract() {
    const dispatch = useDispatch();
    const { pagu_contract_list } = useSelector(state => state.pagu_contract);

    useEffect(() => {
        dispatch(apiPaguContract({ modul: 'strengthen' }));
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Strengthen Capability Building" modul_name="PAGU Kontrak" />
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <SideMenu />
                    </div>
                    <div className="col-md-9 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between mb-0">
                                            <h3>PAGU Anggaran Kontrak</h3>
                                        </div>

                                        <DataGrid
                                            dataSource={pagu_contract_list}
                                            remoteOperations={{
                                                filtering: true,
                                                sorting: true,
                                                paging: true,
                                                summary: true
                                            }}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={100}
                                            onInitNewRow={(e) => {
                                                e.data.modul = 'strengthen';
                                            }}
                                        >
                                            <HeaderFilter visible={true} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Editing
                                                mode="row"
                                                refreshMode='reshape'
                                                allowAdding={true}
                                                allowDeleting={true}
                                                allowUpdating={true}
                                            />
                                            <Toolbar>
                                                <Item name="addRowButton" showText="always" location="after" widget="dxButton" options={{ icon: 'plus', text: 'Tambah PAGU' }} />
                                            </Toolbar>
                                            <Column caption="Actions" type="buttons">
                                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                                    <i className="fas fa-trash-alt icon-sm"></i>
                                                </Button>
                                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                                    <i className="fas fa-edit icon-sm"></i>
                                                </Button>
                                            </Column>
                                            <Column caption="Modul" dataField="modul" width={180} allowEditing={false} visible={false} />
                                            <Column caption="Institusi" dataField="institution" >
                                                <Lookup
                                                    dataSource={[
                                                        { institution: 'UI' },
                                                        { institution: 'UGM' },
                                                        { institution: 'ITB' },
                                                        { institution: 'IPB' },
                                                        { institution: 'PMU' },
                                                    ]}
                                                    valueExpr="institution"
                                                    displayExpr="institution"
                                                />
                                            </Column>
                                            <Column caption="Tahun Alokasi" dataField="year_alocation" >
                                                <Lookup
                                                    dataSource={[
                                                        { year: '2023' },
                                                        { year: '2024' },
                                                        { year: '2025' },
                                                        { year: '2026' },
                                                        { year: '2027' },
                                                    ]}
                                                    valueExpr="year"
                                                    displayExpr="year"
                                                />
                                            </Column>
                                            <Column
                                                caption="PAGU Alokasi"
                                                dataField="total_alocation"
                                                format="fixedPoint"
                                                dataType="number"
                                                cellRender={({ value }) => {
                                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                                }}
                                            />
                                            <Column caption="Periode Usulan" dataField="periode_usulan" dataType="date" />
                                            {/* <Column caption="Periode Usulan" dataField="periode_usulan" dataType="date" cellRender={({ value }) => {
                                                 return <span>{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(value))}</span>
                                             }} /> */}
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default PaguContract