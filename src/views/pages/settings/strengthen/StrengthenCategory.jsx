import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Lookup, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import SideMenu from './SideMenu';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ButtonRefresh } from 'views/components/button';
import { apiStrengthenCategory } from 'app/services/apiStrengthenCategory';

function StrengthenCategory() {
    const dispatch = useDispatch();
    const { strengthen_category } = useSelector(state => state.strengthen_category);

    useEffect(() => {
        dispatch(apiStrengthenCategory());
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Strengthen Capability Building" modul_name="Kategori Penguatan Kelembagaan" />
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <SideMenu />
                    </div>
                    <div className="col-md-9 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between">
                                            <h3>Kategori Penguatan Kelembagaan</h3>
                                            <ButtonRefresh onClick={() => dispatch(apiStrengthenCategory())} />
                                        </div>

                                        <DataGrid
                                            dataSource={strengthen_category}
                                            remoteOperations={{
                                                filtering: true,
                                                sorting: true,
                                                paging: true,
                                                summary: true
                                            }}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={100}
                                        >
                                            <HeaderFilter visible={true} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Editing
                                                mode="row"
                                                refreshMode='reshape'
                                                allowAdding={true}
                                                allowDeleting={true}
                                                allowUpdating={true}
                                            />
                                            <Toolbar>
                                                <Item name="addRowButton" showText="always" location="before" widget="dxButton" options={{ icon: 'plus', text: 'Tambah Data' }} />
                                            </Toolbar>
                                            <Column caption="Opsi" type="buttons">
                                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                                    <i className="fas fa-trash-alt icon-sm"></i>
                                                </Button>
                                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                                    <i className="fas fa-edit icon-sm"></i>
                                                </Button>
                                            </Column>
                                            <Column caption="Tipe" dataField="category_header">
                                                <Lookup
                                                    dataSource={[
                                                        { tipe: 'Investment Costs' },
                                                        { tipe: 'Recurrent Costs' },
                                                    ]}
                                                    valueExpr="tipe"
                                                    displayExpr="tipe"
                                                />
                                            </Column>
                                            <Column caption="Kategori" dataField="category_name" />
                                            <Column caption="Kode" dataField="category_code" />
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default StrengthenCategory