import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import MenuModul from './MenuModul';
import Icons from 'views/components/Icons';
import DataGrid, { Column, Paging, MasterDetail, Selection } from 'devextreme-react/data-grid';
import { authUser } from 'app/slice/sliceAuth';
import { apiMenuAccess, apiModulAccess, apiMenu, apiMenuAccessDelete, apiMenuAccessInsert } from 'app/services/apiMenu';

const Menu = (props) => {
    const dispatch = useDispatch();
    const { level_code } = props.data.data;
    const { menu_access } = useSelector(state => state.mainmenu);

    // useEffect(() => {
    //     dispatch(apiMenuAccess({ user_level: level_code }));
    // }, [dispatch, level_code]);


    async function deleteHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiMenuAccessDelete({ id }))
                dispatch(apiMenuAccess({ user_level: level_code }));

                Swal.fire(
                    payload.data.message,
                    "Your data has been deleted.",
                    "success"
                )
            }
        });
    }

    function componentButtonActions(data) {
        const { id } = data.row.data;

        return <div className="d-flex align-items-end justify-content-center">
            <button type="button" onClick={() => deleteHandler(id)} className="btn btn-icon btn-light btn-hover-danger btn-sm mx-1" data-toggle="tooltip" title="User Delete">
                <Icons iconName="trash" className="svg-icon svg-icon-sm svg-icon-danger" />
            </button>
        </div>
    }

    return (
        <div>
            <FormCreate user_level={level_code} />
            <DataGrid
                dataSource={menu_access}
                keyExpr="id"
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                hoverStateEnabled={true}
                onSelectionChanged={({ selectedRowsData }) => dispatch(apiModulAccess({ user_level: selectedRowsData[0]?.user_level, menu_id: selectedRowsData[0]?.menu_id }))}
            >
                <MasterDetail
                    enabled={true}
                    component={MenuModul}
                />
                <Selection mode="single" />
                <Paging enabled={false} />
                <Column dataField="menu_id" caption="ID" width={100} />
                <Column dataField="menu_name" caption="Menu" />
                <Column caption="Actions" dataField="id" width={150} cellRender={componentButtonActions} />
            </DataGrid>
        </div>
    )
}

//? component Form Create
function FormCreate({ user_level }) {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const { username } = useSelector(authUser);
    const { menu } = useSelector(state => state.mainmenu);

    useEffect(() => {
        dispatch(apiMenu())
    }, [dispatch])

    const onSubmitCreate = async (data) => {
        try {
            data.user_level = user_level;
            data.user_create = username;
            data.access = 'menu';
            const { payload } = await dispatch(apiMenuAccessInsert(data));
            if (payload.status === 200) {
                Swal.fire({
                    title: "Success.",
                    text: payload.data,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                    timer: 1500
                });
                dispatch(apiMenuAccess({ user_level }))
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmitCreate)}>
            <div className="form-group row">
                <div className="col-lg-3">
                    <label>Menu {user_level}:</label>
                    <select className="form-control" {...register("menu_id", { required: true })}>
                        <option>-- Select Menu --</option>
                        {
                            menu?.map((item) => {
                                return <option value={item.menu_id} key={item.menu_id}>{item.menu_name}</option>
                            })
                        }
                    </select>
                </div>
                <div className="col-lg-9">
                    <button type="submit" className="btn btn-primary btn-sm mt-8">Add Menu</button>
                </div>
            </div>
        </form>
    )
}

export default Menu
