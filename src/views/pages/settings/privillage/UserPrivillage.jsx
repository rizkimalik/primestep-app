import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle } from 'views/components/card';
import DataGrid, { Column, MasterDetail, Selection } from 'devextreme-react/data-grid';
import { apiMasterUserLevel } from 'app/services/apiMasterData';
import Menu from './Menu';
import { apiMenuAccess } from 'app/services/apiMenu';

function UserPrivillage() {
    const dispatch = useDispatch();
    const { user_level } = useSelector(state => state.master)

    useEffect(() => {
        dispatch(apiMasterUserLevel())
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Setting Privillage" menu_name="Privillage" modul_name="User Menu" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Setting Privillage" subtitle="Menu access user." />
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={user_level}
                            keyExpr="id"
                            remoteOperations={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            hoverStateEnabled={true}
                            onSelectionChanged={({ selectedRowsData }) => dispatch(apiMenuAccess({ user_level: selectedRowsData[0]?.level_code }))}
                        >
                            <MasterDetail
                                enabled={true}
                                component={Menu}
                            />
                            <Selection mode="single" />
                            <Column dataField="level_name" caption="Level User" />
                            <Column dataField="level_code" caption="Level Code" />
                            <Column dataField="description" />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default UserPrivillage;
