import React, { useEffect } from 'react';
import DataGrid, { Column, Paging } from 'devextreme-react/data-grid';
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import Icons from 'views/components/Icons';
import { authUser } from 'app/slice/sliceAuth';
import { apiMenuAccessDelete, apiModulAccess, apiModul } from 'app/services/apiMenu';
import { apiMenuAccessInsert } from 'app/services/apiMenu';

const MenuModul = (props) => {
    const dispatch = useDispatch();
    const { user_level, menu_id, menu_name } = props.data.data;
    const { modul_access } = useSelector(state => state.mainmenu);

    async function deleteHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiMenuAccessDelete({ id }))
                dispatch(apiModulAccess({ user_level, menu_id }));

                Swal.fire(
                    payload.data.message,
                    "Your data has been deleted.",
                    "success"
                )
            }
        });
    }

    function componentButtonActions(data) {
        const { id } = data.row.data;

        return <div className="d-flex align-items-end justify-content-center">
            <button type="button" onClick={() => deleteHandler(id)} className="btn btn-icon btn-light btn-hover-danger btn-sm mx-1" data-toggle="tooltip" title="User Delete">
                <Icons iconName="trash" className="svg-icon svg-icon-sm svg-icon-danger" />
            </button>
        </div>
    }

    return (
        <div>
            <FormCreate
                user_level={user_level}
                menu_id={menu_id}
                menu_name={menu_name}
            />
            <DataGrid
                dataSource={modul_access}
                keyExpr="id"
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <Paging enabled={false} />
                <Column dataField="menu_modul_id" caption="ID" width={100} />
                <Column dataField="menu_modul_name" caption="Menu" />
                <Column caption="Actions" dataField="id" width={150} cellRender={componentButtonActions} />
            </DataGrid>
        </div>
    )
}

//? component Form Create
function FormCreate({ user_level, menu_id, menu_name }) {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const { username } = useSelector(authUser);
    const { modul } = useSelector(state => state.mainmenu);

    useEffect(() => {
        dispatch(apiModul({ menu_id }))
    }, [dispatch, menu_id]);

    const onSubmitCreate = async (data) => {
        try {
            data.user_level = user_level;
            data.user_create = username;
            data.access = 'modul';
            const { payload } = await dispatch(apiMenuAccessInsert(data));
            if (payload.status === 200) {
                Swal.fire({
                    title: "Success.",
                    text: payload.data,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                    timer: 1500
                });
                dispatch(apiModulAccess({ user_level, menu_id }))
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <form onSubmit={handleSubmit(onSubmitCreate)}>
            <div className="form-group row">
                <div className="col-lg-3">
                    <label>Modul {menu_name}:</label>
                    <input type="hidden" value={menu_id} {...register("menu_id", { required: true })} />
                    <select className="form-control" {...register("menu_modul_id", { required: true })}>
                        <option>-- Select Modul --</option>
                        {
                            modul?.map((item) => {
                                return <option value={item.menu_modul_id} key={item.menu_modul_id}>{item.menu_modul_name}</option>
                            })
                        }
                    </select>
                </div>
                <div className="col-lg-9">
                    <button type="submit" className="btn btn-primary btn-sm mt-8">Add Modul</button>
                </div>
            </div>
        </form>
    )
}

export default MenuModul
