import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Lookup, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';

import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { ButtonRefresh } from 'views/components/button';
import { apiDMF_IndicatorMaster } from 'app/services/apiDMF';
import { apiMaster_DMFPerformance } from 'app/services/apiMasterData';

function DMFIndicator() {
    const dispatch = useDispatch();
    const { dmf_indicator_list } = useSelector(state => state.dmf);
    const { dmf_performance } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiDMF_IndicatorMaster());
        dispatch(apiMaster_DMFPerformance());
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="DMF Indicator" modul_name="Master Data" />
            <Container>
                <div className="row">
                    <div className="col-md-12 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                <div className="card card-custom border">
                                    <div className="card-body p-5">
                                        <div className="d-flex justify-content-between">
                                            <h3>DMF Indicator</h3>
                                            <ButtonRefresh onClick={() => dispatch(apiDMF_IndicatorMaster())} />
                                        </div>

                                        <DataGrid
                                            dataSource={dmf_indicator_list}
                                            remoteOperations={{
                                                filtering: true,
                                                sorting: true,
                                                paging: true,
                                                summary: true
                                            }}
                                            allowColumnReordering={true}
                                            allowColumnResizing={true}
                                            columnAutoWidth={true}
                                            showBorders={true}
                                            showColumnLines={true}
                                            showRowLines={true}
                                            wordWrapEnabled={true}
                                            columnMinWidth={80}
                                        >
                                            <HeaderFilter visible={true} />
                                            <FilterRow visible={true} />
                                            <Paging defaultPageSize={10} />
                                            <Pager
                                                visible={true}
                                                displayMode='full'
                                                allowedPageSizes={[10, 20, 50]}
                                                showPageSizeSelector={true}
                                                showInfo={true}
                                                showNavigationButtons={true} />
                                            <Editing
                                                mode="row"
                                                refreshMode='reshape'
                                                allowAdding={true}
                                                allowDeleting={true}
                                                allowUpdating={true}
                                            />
                                            <Toolbar>
                                                <Item name="addRowButton" showText="always" location="after" widget="dxButton" options={{ icon: 'plus', text: 'Add New' }} />
                                            </Toolbar>
                                            <Column caption="Actions" type="buttons">
                                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                                    <i className="fas fa-trash-alt icon-sm"></i>
                                                </Button>
                                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                                    <i className="fas fa-edit icon-sm"></i>
                                                </Button>
                                            </Column>
                                            <Column caption="Modul" dataField="submodul">
                                                <Lookup dataSource={[{ submodul: 'OUTCOME' }, { submodul: 'OUTPUT' }]} valueExpr="submodul" displayExpr="submodul" />
                                            </Column>
                                            <Column caption="Performance" dataField="performance_id" width={250}>
                                                <Lookup dataSource={dmf_performance} valueExpr="id" displayExpr="performance" />
                                            </Column>
                                            <Column caption="Index" dataField="number" />
                                            <Column caption="Indicator" dataField="indicator" width={250} />
                                            <Column caption="Target capaian 5 tahun" dataField="target_all" dataType="number" />
                                            <Column caption="Target 5 tahun IPB" dataField="target_ipb" dataType="number" />
                                            <Column caption="Target 5 tahun ITB" dataField="target_itb" dataType="number" />
                                            <Column caption="Target 5 tahun UGM" dataField="target_ugm" dataType="number" />
                                            <Column caption="Target 5 tahun UI" dataField="target_ui" dataType="number" />
                                            <Column caption="Target Rate" dataField="rate">
                                                <Lookup dataSource={[{ rate: 'Jumlah' }, { rate: 'Persentase' }]} valueExpr="rate" displayExpr="rate" />
                                            </Column>
                                            <Column caption="Comments" dataField="comments" />
                                        </DataGrid>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default DMFIndicator