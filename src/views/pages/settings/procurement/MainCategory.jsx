import React, { useState } from 'react'
import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import PackageCategoryList from './PackageCategoryList';
import DocumentStatusList from './DocumentStatusList';
import ProcurementMethodList from './ProcurementMethodList';
import ProcurementReviewList from './ProcurementReviewList';

function MainCategory() {
    const [navigate, setNavigate] = useState('Package');

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Data Categories" modul_name={navigate} />
            <Container>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <Card>
                            <CardBody className="px-5">
                                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                                    <div className="navi-section mb-2 font-size-h3 pb-0">Procurement</div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabPackage" onClick={() => setNavigate('Package')} className={`navi-link ${navigate === 'Package' ?? 'active'}`}>
                                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Package Categories</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabMethod" onClick={() => setNavigate('Method')} className={`navi-link ${navigate === 'Method' ?? 'active'}`}>
                                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Procurement Method</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabReview" onClick={() => setNavigate('Review')} className={`navi-link ${navigate === 'Review' ?? 'active'}`}>
                                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Procurement Review</span>
                                        </a>
                                    </div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabDocuments" onClick={() => setNavigate('Documents')} className={`navi-link ${navigate === 'Documents' ?? 'active'}`}>
                                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Documents Status</span>
                                        </a>
                                    </div>

                                    <div className="navi-section mt-2 mb-2 font-size-h3 pb-0">Financial</div>
                                    <div className="navi-item my-2">
                                        <a data-toggle="tab" href="#tabServices" onClick={() => setNavigate('Services')} className={`navi-link ${navigate === 'Services' ?? 'active'}`}>
                                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                                            <span className="navi-text font-weight-bolder font-size-lg">Services Categories</span>
                                        </a>
                                    </div>

                                </div>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-md-9 col-sm-12 p-0">
                        <div className="tab-content border rounded">
                            <div className="tab-pane fade active show" id="tabPackage" role="tabpanel" aria-labelledby="tabPackage">
                                {navigate === 'Package' && <PackageCategoryList />}
                            </div>
                            <div className="tab-pane fade" id="tabMethod" role="tabpanel" aria-labelledby="tabMethod">
                                {navigate === 'Method' && <ProcurementMethodList />}
                            </div>
                            <div className="tab-pane fade" id="tabDocuments" role="tabpanel" aria-labelledby="tabDocuments">
                                {navigate === 'Documents' && <DocumentStatusList />}
                            </div>
                            <div className="tab-pane fade" id="tabReview" role="tabpanel" aria-labelledby="tabReview">
                                {navigate === 'Review' && <ProcurementReviewList />}
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default MainCategory