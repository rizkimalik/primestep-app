import React from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody } from 'views/components/card'

function SideMenu() {
    return (
        <Card>
            <CardBody className="px-5">
                <div className="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                    <div className="navi-section mb-2 font-size-h3 pb-0">Procurement</div>
                    <div className="navi-item my-2">
                        <Link to="/setting/category" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Kategori Paket</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/method" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Procurement Method</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/review" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Procurement Review</span>
                        </Link>
                    </div>
                    <div className="navi-item my-2">
                        <Link to="/setting/document_status" className={`navi-link`}>
                            <span className="navi-icon mr-4"><i className="fas fa-poll fa-md" /></span>
                            <span className="navi-text font-weight-bolder font-size-lg">Dokumen Procurement</span>
                        </Link>
                    </div>
                </div>
            </CardBody>
        </Card>
    )
}

export default SideMenu