import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import OutputPivot from './OutputPivot';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { apiDMF_PerformanceHeader, apiDMF_IndicatorHeader } from 'app/services/apiDMF';

function Output() {
    const dispatch = useDispatch();
    const { dmf_performance_header, dmf_indicator_header } = useSelector(state => state.dmf);
    const performance_id = dmf_performance_header[0]?.id;

    useEffect(() => {
        dispatch(apiDMF_PerformanceHeader({ submodul: 'OUTPUT' }));
    }, [dispatch]);

    useEffect(() => {
        if (performance_id) {
            dispatch(apiDMF_IndicatorHeader({ submodul: 'OUTPUT', performance_id }));
        }
    }, [dispatch, performance_id]);

    return (
        <MainContent>
            <SubHeader active_page="DMF Performance Indicator" menu_name="Output" modul_name="Summary Data">
                <Link to={`/dmf/output/create/0`} className="btn btn-sm btn-light-primary">
                    <i className="fas fa-edit fa-sm" />
                    Tambah Data
                </Link>
                <Link to={`/dmf/output/data`} className="btn btn-sm btn-light-warning ml-2">
                    <i className="fas fa-database fa-sm" />
                    Data Detail
                </Link>
                <Link to="/setting/dmf/indicator" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    DMF Indicator
                </Link>
            </SubHeader>
            <Container>
                <div className="alert alert-custom alert-white alert-shadow gutter-b mb-5" role="alert">
                    <div className="alert-icon">
                        <span className="svg-icon svg-icon-primary svg-icon-xl">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3" />
                                    <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fillRule="nonzero" />
                                </g>
                            </svg>
                        </span>
                    </div>
                    <div className="alert-text">
                        <select className="form-control" onChange={(e) => dispatch(apiDMF_IndicatorHeader({ submodul: 'OUTPUT', performance_id: e.target.value }))}>
                            {
                                dmf_performance_header.map((item, index) => {
                                    return <option value={item.id} key={index}>{index + 1}. {item.performance}</option>
                                })
                            }
                        </select>
                    </div>
                </div>

                {
                    dmf_indicator_header.map((item, index) => {
                        return <div className="card card-custom gutter-b border" key={index}>
                            <div className="card-body p-5">
                                <OutputPivot indicator={item} />
                            </div>
                        </div>
                    })
                }

            </Container>
        </MainContent>
    )
}

export default Output