import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import PivotGrid, { FieldChooser } from 'devextreme-react/pivot-grid';
import PivotGridDataSource from 'devextreme/ui/pivot_grid/data_source';
import { Column, DataGrid } from 'devextreme-react/data-grid'

import { apiDMF_DataPivot } from 'app/services/apiDMF';

function OutputPivot({ indicator }) {
    const dispatch = useDispatch();
    const [dmf_output_pivot, setDMFOutputPivot] = useState([]);

    useEffect(async () => {
        const { payload } = await dispatch(apiDMF_DataPivot({ submodul: 'OUTPUT', performance_id: indicator.performance_id, indicator_id: indicator.id }));
        if (payload.status === 200) {
            setDMFOutputPivot(payload.data);
        }
        else {
            setDMFOutputPivot([]);
        }
    }, [dispatch, indicator]);

    const dataSource = new PivotGridDataSource({
        fields: [
            {
                caption: 'Jumlah',
                dataField: 'Jumlah',
                area: 'row',
                selector(data) {
                    return `Jumlah`;
                },
            },
            {
                caption: 'STP',
                width: 80,
                dataField: 'institution',
                area: 'column',
                expanded: true,
            },
            // {
            //     caption: 'Gender',
            //     dataField: 'gender',
            //     width: 80,
            //     area: 'row',
            // },
            {
                dataField: 'record_date',
                dataType: 'date',
                area: 'column',
            },
            {
                caption: 'Target',
                dataField: 'amount',
                dataType: 'number',
                summaryType: 'sum',
                area: 'data',
            },
        ],
        store: dmf_output_pivot,
    });

    const sum_total_all = () => {
        const sum_all = dmf_output_pivot.reduce((total, row) => total + row.amount, 0);
        return sum_all;
    }

    const sum_by_stp = (institution) => {
        const sum_stp = dmf_output_pivot.filter(row => row.institution === institution).reduce((total, row) => total + row.amount, 0);
        return sum_stp;
    }

    const result_summary = [{
        name: 'Hasil Target 5 tahun',
        target_ui: sum_by_stp('UI') > Number(indicator.target_ui) ? 'Tercapai' : 'Tidak Tercapai',
        target_ugm: sum_by_stp('UGM') > Number(indicator.target_ugm) ? 'Tercapai' : 'Tidak Tercapai',
        target_itb: sum_by_stp('ITB') > Number(indicator.target_itb) ? 'Tercapai' : 'Tidak Tercapai',
        target_ipb: sum_by_stp('IPB') > Number(indicator.target_ipb) ? 'Tercapai' : 'Tidak Tercapai',
    }];

    return (
        <div>
            <div className="d-flex justify-content-between align-items-center mb-2">
                <p className="font-weight-bolder text-truncate" style={{ width: '70%' }}>{indicator.number}. {indicator.indicator}. </p>
                <p>
                    <span className="label label-light font-weight-bold label-inline">
                        Total Semua: {sum_total_all()}, Target: {Number(indicator.target_all)}
                    </span>
                    {
                        sum_total_all() > Number(indicator.target_all)
                            ? <span className="label label-light-success font-weight-bold label-inline ml-2">Tercapai</span>
                            : <span className="label label-light-warning font-weight-bold label-inline ml-2">Tidak Tercapai</span>
                    }
                </p>
            </div>

            <PivotGrid
                id="dmf_output_pivot"
                dataSource={dataSource}
                allowSortingBySummary={true}
                allowSorting={true}
                allowFiltering={true}
                allowExpandAll={true}
                showBorders={true}
                showColumnTotals={false}
                showColumnGrandTotals={false}
                showRowTotals={false}
                showRowGrandTotals={false}
            >
                <FieldChooser enabled={false} />
            </PivotGrid>
            <br />

            <DataGrid
                dataSource={result_summary}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <Column caption="Status STP" dataField="name" width={150} />
                <Column caption="IPB" dataField="target_ipb" cellRender={({ value }) => value === 'Tercapai' ? <span className="font-weight-bolder text-success">{value}</span> : <span className="font-weight-bolder">{value}</span>} />
                <Column caption="ITB" dataField="target_itb" cellRender={({ value }) => value === 'Tercapai' ? <span className="font-weight-bolder text-success">{value}</span> : <span className="font-weight-bolder">{value}</span>} />
                <Column caption="UGM" dataField="target_ugm" cellRender={({ value }) => value === 'Tercapai' ? <span className="font-weight-bolder text-success">{value}</span> : <span className="font-weight-bolder">{value}</span>} />
                <Column caption="UI" dataField="target_ui" cellRender={({ value }) => value === 'Tercapai' ? <span className="font-weight-bolder text-success">{value}</span> : <span className="font-weight-bolder">{value}</span>} />
            </DataGrid>
        </div>
    )
}

export default OutputPivot