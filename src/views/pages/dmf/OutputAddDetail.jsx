import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';

import { Card, CardBody } from 'views/components/card';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiMasterInstitution, apiMaster_DMFPerformance, apiMaster_DMFIndicator } from 'app/services/apiMasterData';
import { apiDMF_DetailInsert } from 'app/services/apiDMF';

function OutputAddDetail() {
    const dispatch = useDispatch();
    const { performance_id } = useParams();
    const { institution, dmf_performance, dmf_indicator } = useSelector(state => state.master)
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        dispatch(apiMasterInstitution());
        dispatch(apiMaster_DMFPerformance({ submodul: 'OUTPUT' }));
    }, [dispatch]);

    useEffect(() => {
        if (performance_id !== '0') {
            dispatch(apiMaster_DMFIndicator({ submodul: 'OUTPUT', performance_id }));
        }
    }, [dispatch, performance_id]);

    useEffect(() => {
        if (performance_id !== '0') {
            reset({ performance_id });
        }
    }, [reset, performance_id]);

    const onSubmitUpdateData = async (data) => {
        try {
            data.year = new Date(data.record_date).getFullYear()
            const { payload } = await dispatch(apiDMF_DetailInsert(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!')
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="DMF Performance Indicator" menu_name="Output" modul_name="Add Record">
                <Link to="/dmf/output" className="btn btn-sm btn-light-info">
                    <i className="fas fa-poll-h fa-sm" />
                    Summary Data
                </Link>
                <Link to="/dmf/output/data" className="btn btn-sm btn-light-warning ml-2">
                    <i className="fas fa-database fa-sm" />
                    Data Detail
                </Link>
                <Link to="/setting/dmf/indicator" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    DMF Indicator
                </Link>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Tambah Output Baru</h3>
                                <form onSubmit={handleSubmit(onSubmitUpdateData)} encType="multipart/form-data" className="form">
                                    <input type="hidden" defaultValue="OUTPUT" {...register('submodul')} />
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">
                                            * Performance:
                                        </label>
                                        <div className="col-lg-6">
                                            <select
                                                {...register("performance_id", { required: true })}
                                                className="form-control"
                                                onChange={(e) => dispatch(apiMaster_DMFIndicator({ submodul: 'OUTPUT', performance_id: e.target.value }))}
                                            >
                                                <option value="">-- select performance --</option>
                                                {
                                                    dmf_performance.map((item, index) => {
                                                        return <option value={item.id} key={index} data={item}>{item.performance}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.performance_id && <span className="form-text text-danger">Please select performance.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">
                                            <Link to="/setting/dmf/indicator"><i className="fas fa-edit fa-sm text-primary" /> </Link>
                                            * Indicator:
                                        </label>
                                        <div className="col-lg-6">
                                            <select
                                                {...register("indicator_id", { required: true })}
                                                className="form-control"
                                            >
                                                <option value="">-- select indicator --</option>
                                                {
                                                    dmf_indicator.map((item, index) => {
                                                        return <option value={item.id} key={index} data={item}>{item.number}. {item.indicator}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.indicator_id && <span className="form-text text-danger">Please select indicator.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* List of STP:</label>
                                        <div className="col-lg-6">
                                            <select className="form-control" {...register("institution", { required: true })}>
                                                <option value="">-- select stp --</option>
                                                {
                                                    institution.map((item, index) => {
                                                        return <option value={item.institution_code} key={index}>{item.institution_code}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.institution && <span className="form-text text-danger">Please select STP.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* Amount:</label>
                                        <div className="col-lg-6">
                                            <input type="number" {...register("amount", { required: true })} className="form-control" placeholder="Enter Amount" />
                                            {errors.amount && <span className="form-text text-danger">Please Enter Amount.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* Date Record:</label>
                                        <div className="col-lg-6">
                                            <input type="date" {...register("record_date", { required: true })} className="form-control" placeholder="Enter Date Record" />
                                            {errors.record_date && <span className="form-text text-danger">Please Enter Date Record.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">Gender:</label>
                                        <div className="col-lg-6">
                                            <select className="form-control" {...register("gender", { required: false })}>
                                                <option value="">-- select gender --</option>
                                                <option value="Pria">Pria</option>
                                                <option value="Wanita">Wanita</option>
                                                <option value="Pria & Wanita">Pria & Wanita</option>
                                            </select>
                                            {errors.gender && <span className="form-text text-danger">Please select Gender.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">Catatan:</label>
                                        <div className="col-lg-6">
                                            <textarea className="form-control" {...register("comments", { required: false })} rows="5" placeholder="Catatan"></textarea>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="d-flex justify-content-between mt-10">
                                        <ButtonCancel to="/dmf/output" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default OutputAddDetail