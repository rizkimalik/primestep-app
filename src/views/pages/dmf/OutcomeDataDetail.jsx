import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Lookup, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { apiMasterInstitution, apiMaster_DMFPerformance, apiMaster_DMFIndicator } from 'app/services/apiMasterData';
import { apiDMF_DetailData } from 'app/services/apiDMF';

function OutcomeDataDetail() {
    const dispatch = useDispatch();
    const { dmf_data_detail } = useSelector(state => state.dmf);
    const { institution, dmf_performance, dmf_indicator } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiMasterInstitution());
        dispatch(apiDMF_DetailData({ submodul: 'OUTCOME' }));
        dispatch(apiMaster_DMFPerformance({ submodul: 'OUTCOME' }));
        dispatch(apiMaster_DMFIndicator({ submodul: 'OUTCOME' }));
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="DMF Performance Indicator" menu_name="Outcome" modul_name="Data Detail">
                <Link to="/dmf/outcome" className="btn btn-sm btn-light-info ml-2">
                    <i className="fas fa-poll-h fa-sm" />
                    Summary Data
                </Link>
                <Link to="/setting/dmf/indicator" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    DMF Indicator
                </Link>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody className="p-5">
                        <div className="d-flex justify-content-between mb-2">
                            <h3>List Data Outcome</h3>
                            <Link to={`/dmf/outcome/create/0`} className="btn btn-sm btn-light-primary">
                                <i className="fas fa-edit fa-sm" />
                                Tambah Data
                            </Link>
                        </div>
                        <DataGrid
                            dataSource={dmf_data_detail}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Editing
                                mode="row"
                                refreshMode='reshape'
                                allowAdding={false}
                                allowDeleting={true}
                                allowUpdating={true}
                            />
                            <Column caption="Actions" type="buttons">
                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                    <i className="fas fa-trash-alt icon-sm"></i>
                                </Button>
                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                    <i className="fas fa-edit icon-sm"></i>
                                </Button>
                            </Column>
                            <Column caption="Package" dataField="package_no" allowEditing={false} />
                            <Column caption="STP" dataField="institution">
                                <Lookup
                                    dataSource={institution}
                                    valueExpr="institution_code"
                                    displayExpr="institution_code"
                                />
                            </Column>
                            <Column caption="Performance" dataField="performance_id" width={250}>
                                <Lookup
                                    dataSource={dmf_performance}
                                    valueExpr="id"
                                    displayExpr="performance"
                                />
                            </Column>
                            <Column caption="Indicator" dataField="indicator_id" width={250}>
                                <Lookup
                                    dataSource={dmf_indicator}
                                    valueExpr="id"
                                    displayExpr="indicator"
                                />
                            </Column>
                            <Column caption="Gender" dataField="gender" >
                                <Lookup
                                    dataSource={[{ gender: 'Pria' }, { gender: 'Wanita' }, { gender: 'Pria & Wanita' }]}
                                    valueExpr="gender"
                                    displayExpr="gender"
                                />
                            </Column>
                            <Column caption="Amount" dataField="amount" dataType="number" />
                            <Column caption="Date Record" dataField="record_date" dataType="date" />
                            <Column caption="Catatan" dataField="comments" width={250} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default OutcomeDataDetail