import React, { useState } from 'react'
import { useDispatch } from 'react-redux';

import Icons from 'views/components/Icons';
import { setAuth } from 'app/slice/sliceAuth';
import { apiAuthLogin } from 'app/services/apiAuth';
import { SwalAlertError } from 'views/components/SwalAlert';

function Login() {
    const dispatch = useDispatch();
    const [errorUsername, setErrorUsername] = useState(false);
    const [errorPassword, setErrorPassword] = useState(false);
    const [loading, setLoading] = useState('');
    const [fields, setFields] = useState({
        username: '',
        password: '',
        remember: ''
    });

    const onHandleChange = (event) => {
        const name = event.target.name;

        setFields({
            ...fields,
            [name]: event.target.value
        });
        setErrorUsername(false);
        setErrorPassword(false);
        setLoading('');
    };

    const AuthLogin = async (e) => {
        e.preventDefault();
        setLoading('spinner spinner-white spinner-left');

        try {
            const { payload } = await dispatch(apiAuthLogin(fields))
            if (payload.status === 200) {
                setLoading('');
                dispatch(setAuth(payload.data));
                window.location.reload();
            }
            else {
                SwalAlertError('Login Gagal.', 'Invalid Username & Password');
            }
        }
        catch (error) {
            SwalAlertError('Login Gagal.', 'Invalid Username & Password');
        }
    }


    return (
        <div className="card card-custom gutter-b card-border" id="kt_blockui_card">
            <div className="card-body">
                <div className="mb-5">
                    <h3>Login Project Management</h3>
                </div>

                <form onSubmit={AuthLogin} className="form">
                    <div className="form-group validated">
                        <label>Username</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <Icons iconName="user" className="svg-icon svg-icon-primary svg-icon-sm" />
                                </span>
                            </div>
                            <input
                                type="text"
                                name="username"
                                value={fields.username}
                                className={errorUsername ? 'form-control form-control-lg is-invalid' : 'form-control form-control-lg'}
                                autoComplete="username"
                                placeholder="Username"
                                required={true}
                                onChange={onHandleChange}
                                autoFocus={true}
                            />
                        </div>
                    </div>

                    <div className="form-group validated mt-4">
                        <label>Password</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <Icons iconName="key" className="svg-icon svg-icon-primary svg-icon-sm" />
                                </span>
                            </div>
                            <input
                                type="password"
                                name="password"
                                value={fields.password}
                                className={errorPassword ? 'form-control form-control-lg is-invalid' : 'form-control form-control-lg'}
                                autoComplete="current-password"
                                placeholder="Password"
                                required={true}
                                onChange={onHandleChange}
                            />
                        </div>
                    </div>

                    <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
                        <div className="checkbox-inline">
                            <label className="checkbox m-0 text-muted">
                                <input type="checkbox" name="remember" />
                                <span />Remember me
                            </label>
                        </div>
                    </div>

                    <div className="d-flex justify-content-between align-items-center mt-5">
                        <button type="submit" className={`btn btn-lg btn-primary btn-block ${loading}`}>
                            Sign In
                        </button>
                    </div>
                </form>

            </div>
        </div>
    )
}

export default Login
