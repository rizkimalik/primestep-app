import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import StatusPersiapan from './status/StatusPersiapan';
import HeaderFormVerification from './HeaderFormVerification';
import StatusVerificationDocument from './status/StatusVerificationDocument';
import { setStoreData } from 'app/slice/sliceAuth';
import { Card, CardBody } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { data_status } from './status/data_status';

function MainFormVerification() {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const [navigate, setNavigate] = useState('1');
    const { store_data } = useSelector(state => state.auth);
    const { information_status } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        const piu = new URLSearchParams(search).get('piu');
        const year = new URLSearchParams(search).get('year');
        const proposal_no = `SCB${year}-${piu}`;

        dispatch(setStoreData({ ...store_data, institution: piu, year, proposal_no }));
    }, [search]);

    useEffect(() => {
        //? utk cek strp paket & navigasi aktif
        if (!information_status) return;
        const filteredStatus = data_status.filter(item => item.status === information_status.status);
        if (filteredStatus.length > 0) {
            setNavigate(filteredStatus[0].navigate);
        }
    }, [information_status]);

    return (
        <MainContent>
            <SubHeader active_page="Strengthen Capability Building" menu_name="Verifikasi Program Penguatan Kelembagaan dan Manajemen" modul_name="">
                <span className="font-weight-bolder text-muted">Komponen {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <HeaderFormVerification navigate={navigate} />
                        <Card>
                            <CardBody>
                                {navigate === '1' && <StatusPersiapan setNavigate={setNavigate} />}
                                {navigate === '2' && <StatusVerificationDocument setNavigate={setNavigate} />}
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default MainFormVerification