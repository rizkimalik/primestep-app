import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { apiStrengthenCapability_VerifiedList } from 'app/services/apiStrengthenCapability';
import { authUser } from 'app/slice/sliceAuth';

function MainVerifiedList() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { store_data } = useSelector(state => state.auth);
    const { strengthen_verified } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiStrengthenCapability_VerifiedList({ institution: store_data.institution, adv_year: store_data.year }));
        }
        else {
            dispatch(apiStrengthenCapability_VerifiedList({ institution: auth.institution, adv_year: store_data.year }));
        }
    }, [dispatch, auth, store_data]);

    return (
        <DataGrid
            dataSource={strengthen_verified}
            remoteOperations={true}
            rowAlternationEnabled={true}
            allowColumnReordering={true}
            allowColumnResizing={true}
            columnAutoWidth={true}
            showBorders={true}
            showColumnLines={true}
            showRowLines={true}
            wordWrapEnabled={true}
            columnMinWidth={80}
        >
            <HeaderFilter visible={true} />
            <FilterRow visible={true} />
            <Paging defaultPageSize={10} />
            <Pager
                visible={true}
                displayMode='full'
                allowedPageSizes={[5, 10, 20]}
                showPageSizeSelector={true}
                showInfo={true}
                showNavigationButtons={true} />
            <Column caption="Komponen Biaya" dataField="component" width={250} />
            <Column caption="Triwulan Kegiatan" dataField="quarter" cellRender={({ value, data }) => {
                return <span>{value} / {data.adv_year}</span>
            }} />
            <Column
                dataField="est_value_idr"
                caption="Anggaran"
                cellRender={({ value }) => {
                    return <span>
                        {new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}
                        <input type="hidden" name="anggaran" defaultValue={value} />
                    </span>
                }}
            />
            <Column caption="Indikator DMF Yang Didukung" dataField="indicator_id" />
            <Column caption="Volume" dataField="volume" />
            <Column caption="Satuan" dataField="unit" />
        </DataGrid>
    )
}

export default MainVerifiedList