import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

// import StatusWithdrawalApp from './status/StatusWithdrawalApp';
import StatusContract from './status/StatusContract';
import StatusDisbursement from './status/StatusDisbursement';
import StatusFinish from './status/StatusFinish';
import HeaderFormContract from './HeaderFormContract';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { setStoreData } from 'app/slice/sliceAuth';
import { status_verification } from './status/data_status';


function MainFormContract() {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const [navigate, setNavigate] = useState('3');
    const { store_data } = useSelector(state => state.auth);
    const { information_status } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        const piu = new URLSearchParams(search).get('piu');
        const year = new URLSearchParams(search).get('year');
        const proposal_no = `SCB${year}-${piu}`;

        dispatch(setStoreData({ ...store_data, institution: piu, year, proposal_no }));
    }, [dispatch, search]);

    useEffect(() => {
        //? utk cek strp paket & navigasi aktif
        if (!information_status) return;
        const filteredStatus = status_verification.filter(item => item.status === information_status.status);
        if (filteredStatus.length > 0) {
            setNavigate(filteredStatus[0].navigate);
        }
    }, [information_status]);

    return (
        <MainContent>
            <SubHeader active_page="Strengthen Capability Building" menu_name="Kontrak Penguatan Kelembagaan" modul_name="">
                <span className="font-weight-bolder text-muted">Status {information_status.status} {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <HeaderFormContract navigate={navigate} />
                        <Card>
                            <CardBody>
                                {navigate === '3' && <StatusContract proposal_no={store_data.proposal_no} setNavigate={setNavigate} />}
                                {/* {navigate === '4' && <StatusWithdrawalApp proposal_no={store_data.proposal_no} setNavigate={setNavigate} />} */}
                                {navigate === '4' && <StatusDisbursement proposal_no={store_data.proposal_no} setNavigate={setNavigate} />}
                                {navigate === '5' && <StatusFinish proposal_no={store_data.proposal_no} setNavigate={setNavigate} />}
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default MainFormContract