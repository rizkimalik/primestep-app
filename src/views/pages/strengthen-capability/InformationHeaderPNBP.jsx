import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { apiStrengthenPNBP_InformationStatus } from 'app/services/apiStrengthenCapability';

const logo_ui = './assets/image/logo-ui.png';
const logo_itb = './assets/image/logo-itb.png';
const logo_ipb = './assets/image/logo-ipb.png';
const logo_ugm = './assets/image/logo-ugm.png';
const logo_pmu = './assets/image/logo-pmu.png';

function InformationHeaderPNBP({ auth, store_data, setStoreData, apiStrengthenPNBP_List }) {
    const dispatch = useDispatch();
    const { information_status_pnbp } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiStrengthenPNBP_InformationStatus({ institution: store_data.institution, adv_year: store_data.year }));
        }
        else {
            dispatch(apiStrengthenPNBP_InformationStatus({ institution: auth.institution, adv_year: store_data.year }));
        }
    }, [dispatch, auth, store_data]);

    const onClickBucket = (value_piu) => {
        dispatch(setStoreData({...store_data, institution: value_piu}));
        dispatch(apiStrengthenPNBP_List({ auth, institution: value_piu, adv_year: store_data.year }));
        dispatch(apiStrengthenPNBP_InformationStatus({ institution: value_piu, adv_year: store_data.year }));
    }

    const onChangeYear = (year) => {
        dispatch(setStoreData({...store_data, year}));
        dispatch(apiStrengthenPNBP_List({ auth, institution: store_data.institution, adv_year: year }));
        dispatch(apiStrengthenPNBP_InformationStatus({ institution: store_data.institution, adv_year: year }));
    }

    return (
        <div className="mb-4">
            {
                (auth.institution === 'PMU') &&
                <ul className="dashboard-tabs nav nav-pills nav-light-primary row row-paddingless mb-4 p-0 flex-column flex-sm-row" role="tablist">
                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a href="#tab_all_messages" onClick={() => onClickBucket('UI')} className={`nav-link d-flex flex-grow-1 rounded flex-column align-items-center bg-light-warning ${store_data.institution === 'UI' && 'active'}`} data-toggle="pill">
                            <div className="d-flex flex-row">
                                <span className="nav-icon py-2 w-auto">
                                    <img src={logo_ui} height={45} width={45} alt="logo_ui" />
                                </span>
                            </div>
                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                UI
                            </span>
                        </a>
                    </li>
                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a href="#tab_mentions" onClick={() => onClickBucket('UGM')} className={`nav-link  d-flex flex-grow-1 rounded flex-column align-items-center bg-light-danger ${store_data.institution === 'UGM' && 'active'}`} data-toggle="pill">
                            <div className="d-flex flex-row">
                                <span className="nav-icon py-2 w-auto">
                                    <img src={logo_ugm} height={45} width={45} alt="logo_ugm" />
                                </span>
                            </div>
                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                UGM
                            </span>
                        </a>
                    </li>
                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a href="#tab_feeds" onClick={() => onClickBucket('ITB')} className={`nav-link  d-flex flex-grow-1 rounded flex-column align-items-center bg-light-primary ${store_data.institution === 'ITB' && 'active'}`} data-toggle="pill">
                            <div className="d-flex flex-row">
                                <span className="nav-icon py-2 w-auto">
                                    <img src={logo_itb} height={45} width={45} alt="logo_itb" />
                                </span>
                            </div>
                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                ITB
                            </span>
                        </a>
                    </li>
                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a href="#tab_feeds" onClick={() => onClickBucket('IPB')} className={`nav-link d-flex flex-grow-1 rounded flex-column align-items-center bg-light-success ${store_data.institution === 'IPB' && 'active'}`} data-toggle="pill">
                            <div className="d-flex flex-row">
                                <span className="nav-icon py-2 w-auto">
                                    <img src={logo_ipb} height={45} width={45} alt="logo_ipb" />
                                </span>
                            </div>
                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                IPB
                            </span>
                        </a>
                    </li>
                    <li className="nav-item d-flex col-sm flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a href="#tab_feeds" onClick={() => onClickBucket('PMU')} className={`nav-link d-flex flex-grow-1 rounded flex-column align-items-center bg-light-info ${store_data.institution === 'PMU' && 'active'}`} data-toggle="pill">
                            <div className="d-flex flex-row">
                                <span className="nav-icon py-2 w-auto">
                                    <img src={logo_pmu} height={45} width={45} alt="logo_pmu" />
                                </span>
                            </div>
                            <span className="nav-text font-size-lg py-2 font-weight-bolder text-center">
                                PMU
                            </span>
                        </a>
                    </li>
                </ul>
            }

            <div className="mb-4">
                <div className="d-flex justify-content-between mb-2">
                    <h3>1. GOI / PNBP - {store_data.institution}</h3>
                    <div className="d-flex">
                        <div className="dropdown">
                            <button className="btn btn-light-danger btn-sm dropdown-toggle font-weight-bolder" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Tahun {store_data.year}
                            </button>
                            <div className="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                <a onClick={() => onChangeYear(2023)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">1. Tahun 2023</a>
                                <a onClick={() => onChangeYear(2024)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">2. Tahun 2024</a>
                                <a onClick={() => onChangeYear(2025)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">3. Tahun 2025</a>
                                <a onClick={() => onChangeYear(2026)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">4. Tahun 2026</a>
                                <a onClick={() => onChangeYear(2027)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">5. Tahun 2027</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Usulan Anggaran</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(information_status_pnbp.sum_value))}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Jumlah Komponen</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <div className="col-lg-6 col-form-label font-weight-bolder">
                        {
                            information_status_pnbp.count_value > 0
                                ? <span className="label label-rounded label-success font-weight-bolder">
                                    {information_status_pnbp.count_value}
                                </span>
                                : <span className="label label-rounded label-dark font-weight-bolder">
                                    {information_status_pnbp.count_value}
                                </span>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default InformationHeaderPNBP