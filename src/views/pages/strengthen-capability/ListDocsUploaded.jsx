import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons';
import ModalDocsUpload from './popup/ModalDocsUpload';
import ModalVerificationDocument from './popup/ModalVerificationDocument';
import ConvertTextToLinks from 'views/components/ConvertTextToLink';
import { urlAttachment } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiDocument_Upload,
    apiDocument_UploadList,
    apiDocument_UploadDelete,
} from 'app/services/apiDocument';
import { apiStrengthen_StatusRevisiDocument } from 'app/services/apiStrengthenCapability';
import { ButtonRefresh } from 'views/components/button';

function ListDocsUploaded({ package_no, status, document_for, document_section }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [document_data, setDocumentData] = useState('');
    const { document_list } = useSelector(state => state.document);

    useEffect(() => {
        dispatch(apiDocument_UploadList({ package_no, status, document_for }))
    }, [dispatch, package_no, status]);

    async function handlerDeleteData(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                await dispatch(apiDocument_UploadDelete({ id }))
                await dispatch(apiDocument_UploadList({ package_no, status, document_for }))
                Swal.fire({
                    title: "Sukses Menghapus.",
                    text: `Data telah terhapus!`,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            }
        });
    }

    const onUploadDocumentRevisi = async (file, data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: file,
                package_no: data.package_no,
                document_for: data.document_for,
                document_name: data.document_name,
                document_section: data.document_section,
                status: status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil', 'Berhasil upload proposal.');
                await dispatch(apiStrengthen_StatusRevisiDocument({ id: data.id })); // reset status dokumen
                await dispatch(apiDocument_UploadList({ package_no, status, document_for }))
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    function componentButtonActions(data) {
        const { id, approved } = data.row.data;
        return (
            <div>
                {
                    auth.user_level === 'PIU'
                        ? <div className="d-flex align-items-end justify-content-center">
                            <button type="button" onClick={() => handlerDeleteData(id)} className="btn btn-clean btn-icon btn-hover-danger btn-sm mx-1" title="Hapus Data" aria-expanded="false" disabled={approved === 1 ? true : false}>
                                <i className="fas fa-trash-alt fa-sm"></i>
                            </button>
                            <button type="button" onClick={() => setDocumentData(data.row.data)} data-toggle="modal" data-target="#modalDocsUpload" className="btn btn-sm btn-clean btn-icon btn-hover-warning mx-1" title="Edit Data" aria-expanded="false" disabled={approved === 1 ? true : false}>
                                <i className="fas fa-edit fa-sm"></i>
                            </button>
                        </div>
                        : <div className="d-flex align-items-end justify-content-center">
                            <button type="button" onClick={() => handlerDeleteData(id)} className="btn btn-clean btn-icon btn-hover-danger btn-sm mx-1" title="Hapus Data" aria-expanded="false">
                                <i className="fas fa-trash-alt fa-sm"></i>
                            </button>
                            <button type="button" onClick={() => setDocumentData(data.row.data)} data-toggle="modal" data-target="#modalDocsUpload" className="btn btn-sm btn-clean btn-icon btn-hover-warning mx-1" title="Edit Data" aria-expanded="false">
                                <i className="fas fa-edit fa-sm"></i>
                            </button>
                        </div>
                }
            </div>
        )
    }

    return (
        <div>
            {document_data && <ModalDocsUpload document_data={document_data} status={status} />}
            {document_data && <ModalVerificationDocument document_data={document_data} status={status} />}
            <div className="d-flex justify-content-end">
                <ButtonRefresh onClick={() => dispatch(apiDocument_UploadList({ package_no, status, document_for }))} />
            </div>
            <DataGrid
                dataSource={document_section ? document_list.filter(row => row.document_section === document_section) : document_list}
                remoteOperations={true}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={80}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                {
                    (auth.user_level !== 'PIU')
                        ? <Column caption="Aksi" dataField="id" width={100} cellRender={componentButtonActions} />
                        : (auth.user_level === 'PIU' && status !== 'Finish')
                            ? <Column caption="Aksi" dataField="id" width={100} cellRender={componentButtonActions} /> : ''
                }

                {
                    (status === 'Kontrak' || status === 'Pencairan')
                        ? <Column caption="Nama Dokumen" dataField="document_name" cellRender={({ value, data }) => {
                            return <a href="#" onClick={() => { setDocumentData(data); }} data-toggle="modal" data-target="#modalVerificationDocument" className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</a>
                        }} />
                        : <Column caption="Nama Dokumen" dataField="document_name" />
                }
                {
                    (auth.user_level !== 'PIU')
                        ? <Column caption="Tautan Berkas" dataField="filename" cellRender={({ value, data }) => {
                            return <a href={urlAttachment + '/' + data.document_for + '/' + value} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {value} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(data.filesize)})</a>
                        }} />
                        : (auth.user_level === 'PIU' && status !== 'Finish')
                            ? <Column caption="Tautan Berkas" dataField="filename" cellRender={({ value, data }) => {
                                return <a href={urlAttachment + '/' + data.document_for + '/' + value} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {value} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(data.filesize)})</a>
                            }} /> : ''
                }
                {/* <Column caption="Keterangan" dataField="description" /> */}
                <Column caption="Keterangan" dataField="description" cellRender={({ value }) => {
                    return <div dangerouslySetInnerHTML={{ __html: ConvertTextToLinks(value) }}></div>
                }} />
                <Column caption="Tanggal Unggah" dataField="created_at" cellRender={({ value }) => {
                    return <span>{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(value))}</span>
                }} />
                {
                    (status === 'Kontrak' || status === 'Pencairan' || status === 'Finish') &&
                    <Column caption="Status" dataField="approved" cellRender={({ value }) => {
                        return value === 1
                            ? <span className="label label-inline label-success">Lengkap</span>
                            : value === 0
                                ? <span className="label label-inline label-warning">Revisi</span>
                                : <span className="label label-inline">Menunggu</span>
                    }} />
                }
                {
                    auth.user_level === 'PIU' &&
                    (status === 'Kontrak' || status === 'Pencairan') &&
                    <Column caption="Revisi Berkas" dataField="revisi" alignment="center" cellRender={({ data }) => {
                        return data.approved === 0 && <button onClick={() => document.getElementById('file-document-revisi').click()} className="btn btn-sm btn-clean btn-link-warning" title="Unggah Revisi Berkas">
                            <input type="file" id="file-document-revisi" className="hide" onChange={(e) => onUploadDocumentRevisi(e.target.files, data)} />
                            <i className="fas fa-upload fa-sm" /> Revisi Berkas
                        </button>
                    }} />
                }
            </DataGrid>
        </div>
    )
}

export default ListDocsUploaded