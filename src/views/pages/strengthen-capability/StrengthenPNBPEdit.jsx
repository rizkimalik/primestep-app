import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';

import { authUser } from 'app/slice/sliceAuth';
import { Card, CardBody } from 'views/components/card';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiMaster_StrengthenCategory,
} from 'app/services/apiMasterData';
import {
    apiStrengthenPNBP_Show,
    apiStrengthenPNBP_Update,
} from 'app/services/apiStrengthenCapability';
import ModalDMFIndicator from './popup/ModalDMFIndicator';

const list_program = [
    { bentuk_program: 'Peningkataan Kualitas dan Kapasitas STP' },
    { bentuk_program: 'Peningkatan Riset Inovasi' },
    { bentuk_program: 'Pengembangan dan Pembinaan Start-Up' },
];


function StrengthenPNBPEdit() {
    const { id } = useParams();
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { store_data } = useSelector(state => state.auth);
    const { strengthen_category } = useSelector(state => state.master);
    const { register, formState: { errors }, handleSubmit, control, reset } = useForm();

    useEffect(() => {
        reset({
            institution: store_data.institution,
            adv_year: store_data.year,
        });
    }, [reset]);

    useEffect(() => {
        dispatch(apiMaster_StrengthenCategory())
    }, [dispatch]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStrengthenPNBP_Show({ id }));
        if (payload.status === 200) {
            reset({
                id: payload.data.id,
                bentuk_program: payload.data.bentuk_program,
                institution: payload.data.institution,
                package_no: payload.data.package_no,
                category_id: payload.data.category_id,
                indicator_id: payload.data.indicator_id,
                quarter: payload.data.quarter,
                adv_year: payload.data.adv_year,
                est_value_idr: payload.data.est_value_idr,
            });
        }
    }, [reset, id]);

    const groupedOptions = {};
    // Group the options by category_header
    strengthen_category.forEach(option => {
        if (!groupedOptions[option.category_header]) {
            groupedOptions[option.category_header] = [];
        }
        groupedOptions[option.category_header].push(option);
    });

    const onSubmitUpdateData = async (data) => {
        try {
            //? Usulan anggaran tidak lebih dari PAGU alokasi
            const { payload } = await dispatch(apiStrengthenPNBP_Update(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Aksi Sukses', 'Berhasil simpan data');
                reset();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="GOI / PNBP" menu_name="Penerimaan Negara Bukan Pajak" modul_name="">
                <span className="font-weight-bolder text-muted">Komponen Kegiatan {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <ModalDMFIndicator reset={reset} />

                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Data Komponen</h3>
                                <form onSubmit={handleSubmit(onSubmitUpdateData)} className="form">
                                    <input type="hidden" {...register("id", { required: true })} />
                                    <input type="hidden" {...register("institution", { required: true })} />
                                    <input type="hidden" {...register("adv_year", { required: true })} />
                                    <input type="hidden" {...register("username", { required: true })} defaultValue={auth.username} />
                                    <div className="row mb-10">
                                        <div className="col-lg-12">
                                            <div className="form-group row">
                                                <label className="col-lg-3 col-form-label text-right font-weight-bolder">1. Pelaksanaan Kegiatan:</label>
                                                <div className="col-lg-6">
                                                    <select className="form-control" {...register("quarter", { required: true })}>
                                                        <option value="">-- Pilih --</option>
                                                        <option value="Q1">Q1</option>
                                                        <option value="Q2">Q2</option>
                                                        <option value="Q3">Q3</option>
                                                        <option value="Q4">Q4</option>
                                                    </select>
                                                    {errors.quarter && <span className="form-text text-danger">* 1. Pelaksanaan Kegiatan</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group row">
                                                <label className="col-lg-3 col-form-label text-right font-weight-bolder">2. Bentuk Program:</label>
                                                <div className="col-lg-6">
                                                    <select className="form-control" {...register("bentuk_program", { required: true })}>
                                                        <option value="">-- Pilih --</option>
                                                        {
                                                            list_program.map((item, index) => {
                                                                return <option value={item.bentuk_program} key={index}>{item.bentuk_program}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.bentuk_program && <span className="form-text text-danger">* 2. Bentuk Program</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group row">
                                                <label className="col-lg-3 col-form-label text-right font-weight-bolder">3. Indikator DMF:</label>
                                                <div className="col-lg-6">
                                                    <div className="input-group">
                                                        <input type="text" {...register("indicator_id", { required: true })} className="form-control" placeholder="Indikator DMF" />
                                                        <div className="input-group-append">
                                                            <button type="button" data-toggle="modal" data-target="#modalDMFIndicator" className="btn btn-primary">Pilih</button>
                                                        </div>
                                                    </div>
                                                    {errors.indicator_id && <span className="form-text text-danger">* 3.Pilih Indikator DMF</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group row">
                                                <label className="col-lg-3 col-form-label text-right font-weight-bolder">4. Kategori Pengeluaran:</label>
                                                <div className="col-lg-6">
                                                    <select className="form-control" {...register("category_id", { required: true })}>
                                                        <option value="">-- Pilih --</option>
                                                        {Object.keys(groupedOptions).map(header => (
                                                            <optgroup label={header} key={header}>
                                                                {groupedOptions[header].map(option => (
                                                                    <option value={option.id} key={option.id}>
                                                                        {option.category_name}
                                                                    </option>
                                                                ))}
                                                            </optgroup>
                                                        ))}
                                                    </select>
                                                    {errors.category_id && <span className="form-text text-danger">* 4. Kategori Pengeluaran</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group row">
                                                <label className="col-lg-3 col-form-label text-right font-weight-bolder">5. Anggaran (Rp.):</label>
                                                <div className="col-lg-6">
                                                    <Controller
                                                        control={control}
                                                        name="est_value_idr"
                                                        rules={{ required: true }}
                                                        render={({ field: { ref, ...rest } }) => (
                                                            <NumericFormat
                                                                id="est_value_idr"
                                                                className="form-control"
                                                                thousandSeparator=","
                                                                decimalSeparator="."
                                                                decimalScale={2}
                                                                getInputRef={ref}
                                                                {...rest}
                                                            />
                                                        )}
                                                    />
                                                    {errors.est_value_idr && <span className="form-text text-danger">* 5.Anggaran</span>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-between">
                                        <ButtonCancel to="/goi-pnbp" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>

            </Container>
        </MainContent>
    )
}

export default StrengthenPNBPEdit