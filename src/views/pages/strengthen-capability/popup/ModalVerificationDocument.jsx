import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import Icons from 'views/components/Icons';
import { urlAttachment } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { ButtonSubmit } from 'views/components/button';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { apiDocument_UploadList } from 'app/services/apiDocument';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import {
    apiStrengthen_StatusVerificationDocument,
    apiStrengthen_HistoryVerificationDocument
} from 'app/services/apiStrengthenCapability';

function ModalVerificationDocument({ document_data, status }) {
    const auth = useSelector(authUser);
    const dispatch = useDispatch();
    const [is_recomend, setRecomend] = useState(true);
    const [history_document, setHistoryDocument] = useState([]);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        reset({
            id: document_data.id,
            approved: document_data.approved,
            approved_comment: document_data.approved_comment,
        });
    }, [reset, document_data]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStrengthen_HistoryVerificationDocument({
            package_no: document_data.package_no,
            document_for: document_data.document_for,
            document_name: document_data.document_name
        }));
        if (payload.status === 200) {
            setHistoryDocument(payload.data);
        }
    }, [dispatch, document_data]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiStrengthen_StatusVerificationDocument(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data.')
                await dispatch(apiDocument_UploadList({ package_no: document_data.package_no, status, document_for: 'strengthen' }))
                reset();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onClickHistoryDocument = async () => {
        try {
            const { payload } = await dispatch(apiStrengthen_HistoryVerificationDocument({
                package_no: document_data.package_no,
                document_for: document_data.document_for,
                document_name: document_data.document_name
            }));
            if (payload.status === 200) {
                setHistoryDocument(payload.data);
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalVerificationDocument" modal_size="modal-lg">
            <ModalHeader title="Verifikasi Dokumen Pengusulan Banpem" />
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form">
                <ModalBody>
                    <h3>{document_data.document_name}</h3>
                    <div className="row">
                        <div className="col-lg-12">
                            <ul className="nav nav-tabs justify-content-end mt-0">
                                <li className={`nav-item ${auth.user_level === 'PIU' && 'hide'}`}>
                                    <a className="nav-link font-weight-bolder" data-toggle="tab" href="#form_verification">Verifikasi Dokumen</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => onClickHistoryDocument()} className="nav-link font-weight-bolder active" data-toggle="tab" href="#history_verification">Riwayat Verifikasi</a>
                                </li>
                            </ul>
                            <div className="tab-content mt-5">
                                <div className={`tab-pane fade ${auth.user_level === 'PIU' && 'hide'}`} id="form_verification" role="tabpanel" aria-labelledby="form_verification">
                                    <input type="hidden" {...register('id', { required: true })} />
                                    <input type="hidden" {...register('approved_evaluator', { required: true })} value={auth.username} />
                                    <div className="form-group">
                                        <label className="font-weight-bolder">Apakah berkas ini Lengkap?</label>
                                        <div className="radio-inline">
                                            <label className="radio">
                                                <input type="radio" {...register('approved', { required: true })} value={1} defaultChecked={document_data.approved === 1 ? true : false} onClick={() => setRecomend(true)} />
                                                <span /> Lengkap
                                            </label>
                                            <label className="radio">
                                                <input type="radio" {...register('approved', { required: true })} value={0} defaultChecked={document_data.approved === 0 ? true : false} onClick={() => setRecomend(false)} />
                                                <span /> Revisi
                                            </label>
                                        </div>
                                        {errors.approved && <span className="form-text text-danger">* Verifikasi Lengkap / Revisi</span>}
                                    </div>
                                    <div className="form-group">
                                        <label className="font-weight-bolder">Komentar (PMU) :</label>
                                        <textarea {...register('approved_comment', { required: is_recomend ? false : true, minLength: is_recomend ? 0 : 20 })} className="form-control" rows="10" placeholder="Komentar"></textarea>
                                        {errors.approved_comment && <span className="form-text text-danger">* Komentar minimal 20 huruf</span>}
                                    </div>
                                </div>
                                <div className="tab-pane fade active show" id="history_verification" role="tabpanel" aria-labelledby="history_verification">
                                    {
                                        history_document.map((item, index) => {
                                            return <div className="d-flex flex-column align-items-start border rounded p-4 mb-4" key={index}>
                                                <div className="d-flex flex-column flex-grow-1">
                                                    <div className="mb-4">
                                                        {
                                                            item.approved === 0
                                                                ? <span className="label label-inline label-warning">Revisi</span>
                                                                : item.approved === 1
                                                                    ? <span className="label label-inline label-success">Rekomendasi</span>
                                                                    : <span className="label label-inline">Menunggu</span>
                                                        }
                                                        <a href={urlAttachment + '/' + item.document_for + '/' + item.filename} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold mx-4"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {item.filename} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(item.filesize)})</a>
                                                    </div>
                                                    <small className="text-muted mb-2">
                                                        Verifikator: {item.approved_evaluator}
                                                        <span className="mx-4">{new Intl.DateTimeFormat('id-ID',
                                                            {
                                                                year: 'numeric',
                                                                month: 'short',
                                                                day: 'numeric',
                                                                hour: "numeric",
                                                                minute: "numeric",
                                                                second: "numeric",
                                                                timeZoneName: 'short',
                                                            })
                                                            .format(new Date(item.updated_at))}
                                                        </span>
                                                    </small>
                                                    <p>{item.approved_comment}</p>
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    {(auth.user_level === 'Admin' || auth.user_level === 'PMU') && <ButtonSubmit />}
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalVerificationDocument