import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';

import { ButtonSubmit } from 'views/components/button';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiDocument_Update, apiDocument_UploadList } from 'app/services/apiDocument';

function ModalDocsUpload({ document_data, status }) {
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        reset({
            id: document_data.id,
            description: document_data.description,
        });
    }, [reset, document_data]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiDocument_Update(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data.')
                dispatch(apiDocument_UploadList({ package_no: document_data.package_no, status, document_for: 'strengthen' }))
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalDocsUpload" modal_size="modal-lg">
            <ModalHeader title="Dokumen Detail" />
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form">
                <ModalBody>
                    <h3>{document_data.document_name}</h3>
                    <input type="hidden" {...register('id', { required: true })} />
                    <div className="row mt-6">
                        <div className="col-lg-12">
                            <div className="form-group">
                                <label className="font-weight-bolder">Keterangan :</label>
                                <textarea {...register('description', { required: false })} className="form-control" rows="10" placeholder="Keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalDocsUpload