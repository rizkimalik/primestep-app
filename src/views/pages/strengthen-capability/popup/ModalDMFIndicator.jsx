import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter } from 'devextreme-react/data-grid';

import { IconMark } from 'views/components/icon';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { ButtonRefresh } from 'views/components/button';
import { apiDMFIndicator_List } from 'app/services/apiStrengthenCapability';

function ModalDMFIndicator({ reset }) {
    const dispatch = useDispatch();
    const { dmf_indicator_list } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        dispatch(apiDMFIndicator_List());
    }, [dispatch])

    const componentButtonActions = (data) => {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={() => reset({ indicator_id: row.number })}
                    data-dismiss="modal"
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> Pilih Indikator
                </button>
            </div>
        )
    }

    return (
        <Modal id="modalDMFIndicator" modal_size="modal-lg">
            <ModalHeader title="DMF Indicator" />
            <ModalBody className="p-4">
                <DataGrid
                    dataSource={dmf_indicator_list}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    height={440}
                    wordWrapEnabled={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Column caption="Pilih" dataField="id" alignment="center" width={150} cellRender={componentButtonActions} />
                    <Column caption="DMF" dataField="number" width={75} />
                    <Column caption="Indikator" dataField="indicator" />
                </DataGrid>
            </ModalBody>
            <ModalFooter>
                <ButtonRefresh onClick={() => dispatch(apiDMFIndicator_List())} />
            </ModalFooter>
        </Modal>
    )
}

export default ModalDMFIndicator