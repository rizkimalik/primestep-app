import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';

import { authUser } from 'app/slice/sliceAuth';
import { NumericFormat } from 'react-number-format';
import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import { ValidateDisbursementValue } from 'views/components/ValidateDisbursementPercent';
import { apiDisbursement_Perquarter } from 'app/services/apiStrengthenCapability';
import { apiDisbursementUpdateInsert, apiDisbursementShow } from 'app/services/apiDisbursement';

function ModalDisbursement({ id, contract_detail, apiDisbursementList }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();
    const { disbursement_list } = useSelector(state => state.disbursement);

    useEffect(() => {
        reset();
        reset({ id, contract_no: contract_detail.contract_no, institution: contract_detail.institution });
    }, [reset, id, contract_detail]);

    useEffect(async () => {
        reset();
        if (id) {
            const { payload } = await dispatch(apiDisbursementShow({ id }));
            reset({
                id: payload.data.id,
                contract_no: payload.data.contract_no,
                disbursement_percent: payload.data.disbursement_percent,
                disbursement_value: payload.data.disbursement_value,
                disbursement_date: (payload?.data?.disbursement_date)?.slice(0, 10),
            });
        } else {
            reset();
        }
    }, [reset, id]);

    function currency_to_number(currencyString) {
        if (typeof currencyString !== 'string') {
            return currencyString; // or any other appropriate value
        }
        const numericString = currencyString.replace(/[^0-9.]/g, '');
        const numericValue = parseFloat(numericString);
        return numericValue;
    }

    const onSubmitUpdateInsert = async (data) => {
        const current_value = ValidateDisbursementValue(disbursement_list);
        const new_value = currency_to_number(data.disbursement_value);
        // const total = current_value + new_value;
        
        let total = 0;
        if (data.id === "") {
            total = current_value + new_value;
        } else {
            total = new_value;
        }

        try {
            if (total <= currency_to_number(contract_detail?.contract_value)) {
                const { payload } = await dispatch(apiDisbursementUpdateInsert(data))
                if (payload.status === 200) {
                    SwalAlertSuccess('Aksi Berhasil', 'Berhasil input data.');
                    dispatch(apiDisbursementList({ contract_no: contract_detail.contract_no }))
                    reset();
                }
                else {
                    SwalAlertError('Aksi Gagal.', 'Silahkan cobal kembali.');
                }
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Jumlah Pencairan harus kurang dari atau sama Nilai Kontrak.');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan cobal kembali, ${error.message}.`);
        }
    }

    const sum_disbursement_perquartal = async (e) => {
        const get_quarter = "Q" + e.target.value;
        const { payload } = await dispatch(apiDisbursement_Perquarter({ 
            institution: contract_detail.institution, 
            year: new Date(contract_detail.contract_date).getFullYear(), 
            quarter: get_quarter,
        }));
        // console.log(payload);
        reset({ disbursement_value: Number(payload.data[0].sum_value_perquarter) });
    }

    return (
        <Modal id="modalTambahPencairan" modal_size="modal-md">
            <ModalHeader title="Tahap Pencairan" />
            <form onSubmit={handleSubmit(onSubmitUpdateInsert)} className="form">
                <ModalBody>
                    <div className="mb-2">
                        <input type="hidden" {...register('id', { required: false })} />
                        <input type="hidden" {...register('contract_no', { required: true })} />
                        <input type="hidden" {...register('institution', { required: true })} />
                        <div className="form-group row">
                            <label className="col-lg-4 col-form-label text-right">Tahap Pencairan:</label>
                            <div className="col-lg-6">
                                <select
                                    {...register('disbursement_percent', { required: true })}
                                    // onChange={(e) => sum_disbursement_perquartal(e)}
                                    className="form-control"
                                >
                                    <option value="">-- Pilih  --</option>
                                    <option value="1">Q1</option>
                                    <option value="2">Q2</option>
                                    <option value="3">Q3</option>
                                    <option value="4">Q4</option>
                                </select>
                                {errors.disbursement_percent && <span className="form-text text-danger">Pilih Tahap Pencairan.</span>}
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-4 col-form-label text-right">Jumlah Pencairan (Rp.):</label>
                            <div className="col-lg-6">
                                <Controller
                                    control={control}
                                    name="disbursement_value"
                                    rules={{
                                        required: true,
                                        validate: (value) => {
                                            if (currency_to_number(value) > currency_to_number(contract_detail?.contract_value)) {
                                                return 'Value must be less than or equal to Usulan';
                                            }
                                            return undefined;
                                        }
                                    }}
                                    render={({ field: { ref, ...rest } }) => (
                                        <NumericFormat
                                            id="disbursement_value"
                                            className="form-control"
                                            thousandSeparator=","
                                            decimalSeparator="."
                                            decimalScale={2}
                                            getInputRef={ref}
                                            {...rest}
                                        />
                                    )}
                                />
                                {errors.disbursement_value && <span className="form-text text-danger">* Jumlah Pencairan harus kurang dari atau sama Nilai Kontrak.</span>}
                            </div>
                        </div>
                        <div className={`form-group row ${auth.user_level === 'PIU' && 'hide'}`}>
                            <label className="col-lg-4 col-form-label text-right">Tanggal SPP:</label>
                            <div className="col-lg-6">
                                <input type="date" {...register('disbursement_date', { required: false })} className={`form-control ${errors.disbursement_date && 'is-invalid'}`} />
                                {errors.disbursement_date && <span className="form-text text-danger">* Tanggal SPP.</span>}
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalDisbursement