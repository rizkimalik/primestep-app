
export const data_status = [
    { navigate: '1', status: '' },
    { navigate: '1', status: null },
    { navigate: '1', status: 'Persiapan-Dokumen' },
    { navigate: '2', status: 'Verifikasi-Dokumen' },
    { navigate: '2', status: 'Kontrak' },
    // { navigate: '2', status: 'Withdrawal' },
    { navigate: '2', status: 'Pencairan-Q1' },
    { navigate: '2', status: 'Pencairan-Q2' },
    { navigate: '2', status: 'Pencairan-Q3' },
    { navigate: '2', status: 'Pencairan-Q4' },
    { navigate: '2', status: 'Finish' },
];


export const status_verification = [
    { navigate: '3', status: '' },
    { navigate: '3', status: null },
    { navigate: '3', status: 'Verifikasi-Dokumen' },
    { navigate: '3', status: 'Kontrak' },
    // { navigate: '4', status: 'Withdrawal' },
    { navigate: '4', status: 'Pencairan-Q1' },
    { navigate: '4', status: 'Pencairan-Q2' },
    { navigate: '4', status: 'Pencairan-Q3' },
    { navigate: '4', status: 'Pencairan-Q4' },
    { navigate: '5', status: 'Finish' },
];

