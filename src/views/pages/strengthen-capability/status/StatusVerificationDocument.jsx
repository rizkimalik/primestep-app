import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons';
import ModalVerificationDocument from '../popup/ModalVerificationDocument';
import { urlAttachment } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { apiDocument_UploadList, apiDocument_Upload } from 'app/services/apiDocument';
import {
    apiUpdate_StatusProgress,
    apiStrengthen_StatusRevisiDocument,
    apiCheck_VerificationDocument,
} from 'app/services/apiStrengthenCapability';
import ConvertTextToLinks from 'views/components/ConvertTextToLink';

function StatusVerificationDocument({ setNavigate }) {
    const dispatch = useDispatch();
    const history = useHistory();
    const auth = useSelector(authUser);
    const [document_data, setDocumentData] = useState('');
    const [modal_show, setModalShow] = useState('');
    const { store_data } = useSelector(state => state.auth);
    const { document_list } = useSelector(state => state.document);

    useEffect(() => {
        dispatch(apiDocument_UploadList({ package_no: store_data.proposal_no, status: 'Persiapan-Dokumen', document_for: 'strengthen' }))
    }, [dispatch, store_data]);

    const onUploadDocumentRevisi = async (file, data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: file,
                package_no: data.package_no,
                document_for: data.document_for,
                document_name: data.document_name,
                status: 'Persiapan-Dokumen',
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil', 'Berhasil upload proposal.');
                await dispatch(apiStrengthen_StatusRevisiDocument({ id: data.id })); // reset status
                await dispatch(apiDocument_UploadList({ package_no: data.package_no, status: 'Persiapan-Dokumen', document_for: 'strengthen' }))
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitNextBtn = async () => {
        const { payload } = await dispatch(apiCheck_VerificationDocument({ package_no: store_data.proposal_no, document_for: 'strengthen', status: 'Persiapan-Dokumen' }));
        if (payload?.data > 0){
            SwalAlertError('Aksi Gagal.', '* Verifikasi dokumen Banpem.');
        }
        else {
            await dispatch(apiUpdate_StatusProgress({ proposal_no: store_data.proposal_no, status: 'Verifikasi-Dokumen' }));
            history.push(`/strengthen-capability`);
        }
    }

    return (
        <div>
            {modal_show === 'Document' && document_data && <ModalVerificationDocument document_data={document_data} status="Persiapan-Dokumen" />}
            <h3 className="font-size-lg text-dark font-weight-bold mb-4">2.1 Verifikasi Dokumen Pengusulan Banpem</h3>

            <DataGrid
                dataSource={document_list}
                remoteOperations={true}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={80}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Nama Dokumen" dataField="document_name" cellRender={({ value, data }) => {
                    return <a href="#" onClick={() => { setModalShow('Document'); setDocumentData(data); }} data-toggle="modal" data-target="#modalVerificationDocument" className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</a>
                }} />
                <Column caption="Tautan Berkas" dataField="filename" cellRender={({ value, data }) => {
                    return <a href={urlAttachment + '/' + data.document_for + '/' + value} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {value} ({(data.filesize / 1000).toFixed(2)} kb)</a>
                }} />
                <Column caption="Keterangan" dataField="description" cellRender={({ value }) => {
                    return <div dangerouslySetInnerHTML={{ __html: ConvertTextToLinks(value) }}></div>
                }} />
                <Column caption="Tanggal Unggah" dataField="created_at" cellRender={({ value }) => {
                    return <span>{new Intl.DateTimeFormat('id-ID', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(value))}</span>
                }} />
                <Column caption="Status" dataField="approved" cellRender={({ value }) => {
                    return value === 1
                        ? <span className="label label-inline label-success">Lengkap</span>
                        : value === 0
                            ? <span className="label label-inline label-warning">Revisi</span>
                            : <span className="label label-inline">Menunggu</span>
                }} />
                {
                    auth.user_level === 'PIU' &&
                    <Column caption="Revisi Berkas" dataField="revisi" alignment="center" cellRender={({ data }) => {
                        return data.approved === 0 && <button onClick={() => document.getElementById('file-document-revisi').click()} className="btn btn-sm btn-clean btn-link-warning" title="Unggah Revisi Berkas">
                            <input type="file" id="file-document-revisi" className="hide" onChange={(e) => onUploadDocumentRevisi(e.target.files, data)} />
                            <i className="fas fa-upload fa-sm" /> Revisi Berkas
                        </button>
                    }} />
                }
            </DataGrid>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('1')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                {
                    auth.institution === 'PMU' &&
                    <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Konfirmasi</button>
                }
            </div>
        </div>
    )
}

export default StatusVerificationDocument