import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Column, DataGrid, Pager, Paging } from 'devextreme-react/data-grid';

import ListDocsUploaded from '../ListDocsUploaded';
import ModalDisbursement from '../popup/ModalDisbursement';
// import ValidateDisbursementPercent from 'views/components/ValidateDisbursementPercent';
import { ValidateDocumentUpload } from 'views/components/ValidateDocumentUpload';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDisbursementList, apiDisbursementDelete } from 'app/services/apiDisbursement';
import { apiContractDetail } from 'app/services/apiContract';
import { apiDocument_Upload, apiDocument_UploadList } from 'app/services/apiDocument';
import { apiCheck_VerificationDocument, apiUpdate_StatusProgress, apiStrengthenCapability_InformationStatus } from 'app/services/apiStrengthenCapability';

function StatusDisbursement({ proposal_no, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const [id, setID] = useState('');
    const [accordion, setAccordion] = useState('Q1');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { store_data } = useSelector(state => state.auth);
    const { contract_detail } = useSelector(state => state.contract);
    const { disbursement_list } = useSelector(state => state.disbursement);
    const { document_list } = useSelector(state => state.document);
    const { docs_uploaded_list } = useSelector(state => state.master);
    const { information_status } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        reset({ proposal_no });
    }, [reset, proposal_no]);

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'strengthen', status: 'Pencairan', user_level: auth.user_level }));
    }, [dispatch]);

    useEffect(async () => {
        const { payload } = await dispatch(apiContractDetail({ package_no: proposal_no }));
        if (payload.status === 200) {
            dispatch(apiDisbursementList({ contract_no: payload.data.contract_no }));
        }
    }, [dispatch, proposal_no]);

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.proposal_no,
                document_for: 'strengthen',
                document_name: data.document_name,
                document_section: accordion,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil unggah dokumen.');
                await dispatch(apiDocument_UploadList({ package_no: proposal_no, status: 'Pencairan', document_for: 'strengthen' }));
                setLoading('');
                setFileDoc('');
                reset();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    async function deleteDataHandler(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                await dispatch(apiDisbursementDelete({ id }))
                await dispatch(apiDisbursementList({ contract_no: contract_detail.contract_no }));
                Swal.fire({
                    title: "Sukses Menghapus.",
                    text: `Data telah terhapus!`,
                    buttonsStyling: false,
                    icon: "success",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            }
        });
    }

    /**
     * Handles the submission of the next button for a specific document section.
     * @param {type} document_section - The document section to be processed
     * @return {type} Description of the return value = Q1, Q2, Q3, Q4
    */
    const onSubmitNextBtn = async (document_section) => {
        const isValidDocumentUpload = await ValidateDocumentUpload(
            docs_uploaded_list.filter(row => row.document_section === document_section),
            document_list.filter(row => row.institution === auth.institution && row.document_section === document_section)
        );
        const { payload } = await dispatch(apiCheck_VerificationDocument({ package_no: proposal_no, document_for: 'strengthen', status: 'Pencairan' }));

        if (payload?.data > 0) {
            SwalAlertError('Aksi Gagal.', `* Verifikasi dokumen kelengkapan Pencairan ${document_section}.`);
        }
        else if (isValidDocumentUpload === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Pencairan ${document_section}.`);
        }
        else {
            await dispatch(apiUpdate_StatusProgress({ proposal_no, status: `Pencairan-${document_section}` }));
            await dispatch(apiStrengthenCapability_InformationStatus({ institution: auth.institution, adv_year: store_data.year }));
            setNavigate('5');
        }
    }


    return (
        <div>
            {contract_detail && <ModalDisbursement id={id} contract_detail={contract_detail} apiDisbursementList={apiDisbursementList} />}
            <div className="mb-4">
                <h3 className="font-size-lg text-dark font-weight-bold">4.1 Informasi Kontrak:</h3>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Nomor PKS PIU </label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{contract_detail?.contract_no}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Nomor PKS PMU </label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{contract_detail?.contract_no_piu}</label>
                </div>
                <div className="form-group row m-0">
                    <label className="col-lg-2 col-form-label">Nilai Kontrak (Rp.)</label>
                    <label className="col-lg-1 col-form-label text-right">:</label>
                    <label className="col-lg-6 col-form-label font-weight-bolder">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(contract_detail ? contract_detail?.contract_value : 0))}</label>
                </div>
            </div>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <h3 className="font-size-lg text-dark font-weight-bold mb-2">4.2 Tahap Pencairan:</h3>
            <div className={`d-flex justify-content-end mb-2`}>
                <button type="button" onClick={() => setID('')} data-toggle="modal" data-target="#modalTambahPencairan" className="btn btn-sm btn-primary">Tambah Pencairan</button>
            </div>
            <DataGrid
                dataSource={disbursement_list}
                remoteOperations={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                columnMinWidth={80}
                onInitNewRow={(e) => {
                    e.data.contract_no = contract_no;
                }}
            >
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Aksi" dataField="id" width={80} alignment="center" cellRender={({ data }) => {
                    return <div>
                        <button onClick={() => deleteDataHandler(data.id)} className="btn btn-sm btn-clean btn-icon btn-hover-danger">
                            <i className="fas fa-trash-alt icon-sm"></i>
                        </button>
                        <button onClick={() => setID(data.id)} data-toggle="modal" data-target="#modalTambahPencairan" className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                            <i className="fas fa-edit icon-sm"></i>
                        </button>
                    </div>
                }} />
                <Column caption="Nomor Kontrak" dataField="contract_no" visible={false} allowEditing={false} />
                <Column caption="Tahap Pencairan" dataField="disbursement_percent" alignment="center" cellRender={({ value }) => {
                    return "Q" + value;
                }} />
                <Column
                    caption="Jumlah Pencairan"
                    dataField="disbursement_value"
                    format="fixedPoint"
                    alignment="center"
                    cellRender={({ value }) => {
                        return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                    }}
                />
                <Column caption="Tanggal SPP" dataField="disbursement_date" dataType="date" alignment="center" />
            </DataGrid>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <div className="accordion accordion-light accordion-toggle-arrow" id="accordionDisbursement">
                <div className="card">
                    <div className="card-header" id="headingQ1">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseDisbursementQ1" aria-expanded="true" onClick={() => setAccordion('Q1')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">4.3 Unggah Dokumen Kelengkapan Pencairan Q1:</h3>
                        </div>
                    </div>
                    {/*collapse show */}
                    <div id="collapseDisbursementQ1" className="collapse " data-parent="#accordionDisbursement">
                        <div className="card-body">
                            {
                                accordion === 'Q1' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Pencairan" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.filter(row => row.document_section === 'Q1').map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Pencairan' document_for='strengthen' document_section="Q1" />

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q1')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ2">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseDisbursementQ2" onClick={() => setAccordion('Q2')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">4.4 Unggah Dokumen Kelengkapan Pencairan Q2 :</h3>
                        </div>
                    </div>
                    <div id="collapseDisbursementQ2" className="collapse" data-parent="#accordionDisbursement">
                        <div className="card-body">
                            {
                                accordion === 'Q2' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Pencairan" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.filter(row => row.document_section === 'Q2').map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Pencairan' document_for='strengthen' document_section="Q2" />

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q2')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ3">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseDisbursementQ3" onClick={() => setAccordion('Q3')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">4.5 Unggah Dokumen Kelengkapan Pencairan Q3 :</h3>
                        </div>
                    </div>
                    <div id="collapseDisbursementQ3" className="collapse" data-parent="#accordionDisbursement">
                        <div className="card-body">
                            {
                                accordion === 'Q3' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Pencairan" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.filter(row => row.document_section === 'Q3').map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Pencairan' document_for='strengthen' document_section="Q3" />

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q3')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ4">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseDisbursementQ4" onClick={() => setAccordion('Q4')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">4.6 Unggah Dokumen Kelengkapan Pencairan Q4 :</h3>
                        </div>
                    </div>
                    <div id="collapseDisbursementQ4" className="collapse" data-parent="#accordionDisbursement">
                        <div className="card-body">
                            {
                                accordion === 'Q4' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Pencairan" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.filter(row => row.document_section === 'Q4').map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Pencairan' document_for='strengthen' document_section="Q4" />

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q4')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

            {/* <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn('Q1')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div> */}
        </div>
    )
}

export default StatusDisbursement