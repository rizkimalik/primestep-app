import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import ListDocsUploaded from '../ListDocsUploaded';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDocument_Upload, apiDocument_UploadList } from 'app/services/apiDocument';
import { apiUpdate_StatusProgress } from 'app/services/apiStrengthenCapability';
import { ValidateDocumentUpload } from 'views/components/ValidateDocumentUpload';

function StatusFinish({ proposal_no, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const history = useHistory();
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const [accordion, setAccordion] = useState('Q1');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { document_list } = useSelector(state => state.document);
    const { docs_uploaded_list } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'strengthen', status: 'Finish' }));
    }, [dispatch]);

    useEffect(() => {
        reset({ proposal_no });
    }, [reset, proposal_no]);

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.proposal_no,
                document_for: 'strengthen',
                document_name: data.document_name,
                document_section: accordion,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil unggah dokumen.');
                await dispatch(apiDocument_UploadList({ package_no: data.proposal_no, status: 'Finish' }));
                setLoading('');
                setFileDoc('');
                reset();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    /* const onSubmitNextBtn = async (document_section) => {
        const isValidDocumentUpload = await ValidateDocumentUpload(
            // docs_uploaded_list.filter(row => row.document_section === document_section),
            docs_uploaded_list,
            document_list.filter(row => row.institution === auth.institution && row.document_section === document_section)
        );

        if (isValidDocumentUpload === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Realisasi Anggaran ${document_section}.`);
        }
        else {
            await dispatch(apiUpdate_StatusProgress({ proposal_no, status: `Finish-${document_section}` }));
            history.push(`/strengthen-capability/contract`);
        }
    } */

    const onSubmitNextBtn = async () => {
        const DocQ1 = await ValidateDocumentUpload(docs_uploaded_list,document_list.filter(row => row.institution === auth.institution && row.document_section === 'Q1'));
        const DocQ2 = await ValidateDocumentUpload(docs_uploaded_list,document_list.filter(row => row.institution === auth.institution && row.document_section === 'Q2'));
        const DocQ3 = await ValidateDocumentUpload(docs_uploaded_list,document_list.filter(row => row.institution === auth.institution && row.document_section === 'Q3'));
        const DocQ4 = await ValidateDocumentUpload(docs_uploaded_list,document_list.filter(row => row.institution === auth.institution && row.document_section === 'Q4'));

        if (DocQ1 === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Realisasi Anggaran Q1.`);
        }
        else if (DocQ2 === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Realisasi Anggaran Q2.`);
        }
        else if (DocQ3 === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Realisasi Anggaran Q3.`);
        }
        else if (DocQ4 === false) {
            SwalAlertError('Aksi Gagal.', `* Unggah dokumen kelengkapan Realisasi Anggaran Q4.`);
        }
        else {
            await dispatch(apiUpdate_StatusProgress({ proposal_no, status: `Finish` }));
            history.push(`/strengthen-capability/contract`);
        }
    }


    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold my-4">Realisasi Anggaran Penguatan Kelembagaan:</h3>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <div className="accordion accordion-light accordion-toggle-arrow" id="accordionFinish">
                <div className="card">
                    <div className="card-header" id="headingQ1">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseFinishQ1" aria-expanded="true" onClick={() => setAccordion('Q1')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">5.1 Unggah Dokumen Kelengkapan Realisasi Anggaran Q1:</h3>
                        </div>
                    </div>
                    {/*collapse show */}
                    <div id="collapseFinishQ1" className="collapse " data-parent="#accordionFinish">
                        <div className="card-body">
                            {
                                accordion === 'Q1' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data" className={`${auth.institution !== 'PMU' && 'hide'}`}>
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Finish" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            // docs_uploaded_list.filter(row => row.document_section === 'Q1').map((item, index) => {
                                                            docs_uploaded_list.map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Finish' document_for='strengthen' document_section="Q1" />

                                    {/* <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q1')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div> */}
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ2">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseFinishQ2" onClick={() => setAccordion('Q2')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">5.2 Unggah Dokumen Kelengkapan Realisasi Anggaran Q2 :</h3>
                        </div>
                    </div>
                    <div id="collapseFinishQ2" className="collapse" data-parent="#accordionFinish">
                        <div className="card-body">
                            {
                                accordion === 'Q2' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data" className={`${auth.institution !== 'PMU' && 'hide'}`}>
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Finish" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Finish' document_for='strengthen' document_section="Q2" />

                                    {/* <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q2')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div> */}
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ3">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseFinishQ3" onClick={() => setAccordion('Q3')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">5.3 Unggah Dokumen Kelengkapan Realisasi Anggaran Q3 :</h3>
                        </div>
                    </div>
                    <div id="collapseFinishQ3" className="collapse" data-parent="#accordionFinish">
                        <div className="card-body">
                            {
                                accordion === 'Q3' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data" className={`${auth.institution !== 'PMU' && 'hide'}`}>
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Finish" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Finish' document_for='strengthen' document_section="Q3" />

                                    {/* <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q3')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div> */}
                                </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header" id="headingQ4">
                        <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseFinishQ4" onClick={() => setAccordion('Q4')}>
                            <h3 className="font-size-lg text-dark font-weight-bold">5.4 Unggah Dokumen Kelengkapan Realisasi Anggaran Q4 :</h3>
                        </div>
                    </div>
                    <div id="collapseFinishQ4" className="collapse" data-parent="#accordionFinish">
                        <div className="card-body">
                            {
                                accordion === 'Q4' &&
                                <div>
                                    <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data" className={`${auth.institution !== 'PMU' && 'hide'}`}>
                                        <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                                        <input type="hidden" defaultValue="Finish" {...register("status", { required: true })} />
                                        <div className="form-group row mb-0">
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>1. Nama Dokumen * :</label>
                                                    <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                                        <option value="">-- Pilih Dokumen --</option>
                                                        {
                                                            docs_uploaded_list.map((item, index) => {
                                                                return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>2. Unggah Dokumen * :</label>
                                                    <div className={`custom-file`}>
                                                        <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                                        <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                                    </div>
                                                    {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>3. Keterangan (Opsional) :</label>
                                                    <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="form-group">
                                                    <label>4. Unggah :</label>
                                                    <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                                        <i className="fa fa-upload fa-sm" />
                                                        Unggah Berkas
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ListDocsUploaded package_no={proposal_no} status='Finish' document_for='strengthen' document_section="Q4" />

                                    {/* <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-end">
                                        <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                                        <button type="button" onClick={() => onSubmitNextBtn('Q4')} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                                    </div> */}
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                {
                    auth.institution === 'PMU' &&
                    <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selesai</button>
                }
            </div>
        </div>
    )
}

export default StatusFinish