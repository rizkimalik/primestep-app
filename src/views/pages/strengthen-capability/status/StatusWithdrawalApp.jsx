import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import { 
    apiStrengthenCapability_InformationStatus,
    apiStrengthen_StatusWithdrawal,
    apiUpdate_StatusProgress,
 } from 'app/services/apiStrengthenCapability';

function StatusWithdrawalApp({ proposal_no, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { information_status } = useSelector(state => state.strengthen_capability);
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        reset({ proposal_no });
    }, [reset, proposal_no]);

    useEffect(() => {
        let status_wa = false;
        if (information_status.status === 'Withdrawal' || information_status.status === 'Pencairan' || information_status.status === 'Finish') {
            status_wa = true
        }
        reset({ status_wa });
    }, [reset, information_status]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiStrengthen_StatusWithdrawal(data))
            if (payload.status === 200) {
                await dispatch(apiStrengthenCapability_InformationStatus({ auth, institution: store_data.institution, adv_year: store_data.year }));
                SwalAlertSuccess('Sukses', 'Berhasil simpan data.');
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitNextBtn = () => {
        if (information_status.status === 'Withdrawal' || information_status.status === 'Pencairan' || information_status.status === 'Finish') {
            setNavigate('5');
            dispatch(apiUpdate_StatusProgress({ proposal_no, status: 'Withdrawal-App' }));
        } else {
            SwalAlertError('Gagal.', '*Status Withdrawal belum selesai.');
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmitInsertData)} encType="multipart/form-data" className="mb-4">
                <h3 className="font-size-lg text-dark font-weight-bold my-4">4.1 Withdrawal App:</h3>
                <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                <input type="hidden" defaultValue="Withdrawal-App" {...register("status", { required: true })} />

                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Status :</label>
                    <div className="col-lg-2">
                        <span className="switch switch-outline switch-icon switch-primary">
                            <label>
                                <input type="checkbox" {...register("status_wa", { required: true })} />
                                <span />
                            </label>
                        </span>
                        {errors.status_wa && <span className="form-text text-danger">* Status</span>}
                        <span className="form-text text-dark">Belum / Sudah selesai</span>
                    </div>
                </div>

                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <div className="d-flex justify-content-between">
                    <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                    <div>
                        {auth.institution === 'PMU' && <ButtonSubmit className="btn btn-success font-weight-bold btn-lg" />}
                        <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default StatusWithdrawalApp