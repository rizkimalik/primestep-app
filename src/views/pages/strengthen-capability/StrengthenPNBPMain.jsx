import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import InformationHeaderPNBP from './InformationHeaderPNBP';
import { Card, CardBody } from 'views/components/card'
import { ButtonCreate } from 'views/components/button';
import { SwalAlertError } from 'views/components/SwalAlert';
import { authUser, setStoreData } from 'app/slice/sliceAuth';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import {
    apiStrengthenPNBP_List,
    apiStrengthenPNBP_Delete,
    apiStrengthenPNBP_InformationStatus,
    apiPNBP_ExportExcel,
} from 'app/services/apiStrengthenCapability';

function StrengthenPNBPMain() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { store_data } = useSelector(state => state.auth);
    const { goi_pnbp } = useSelector(state => state.strengthen_capability);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiStrengthenPNBP_List({ auth, institution: store_data.institution, adv_year: store_data.year }));
        }
        else {
            dispatch(apiStrengthenPNBP_List({ auth, institution: auth.institution, adv_year: store_data.year }));
        }
    }, [dispatch, auth, store_data]);


    function handlerDeleteData(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiStrengthenPNBP_Delete({ id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Sukses Menghapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiStrengthenPNBP_List({ auth, institution: store_data.institution, adv_year: store_data.year }))
                    dispatch(apiStrengthenPNBP_InformationStatus({ institution: store_data.institution, adv_year: store_data.year }));
                }
            }
        });
    }

    const onExportExcel = async () => {
        const { payload } = await dispatch(apiPNBP_ExportExcel({ institution: store_data.institution, adv_year: store_data.year }))
        if (payload?.data?.length > 0) {
            const data_export = payload.data;
            const workbook = new Workbook();
            const worksheet = workbook.addWorksheet('Main sheet');
            worksheet.columns = [
                { header: 'Pelaksanaan Kegiatan', key: 'quarter' },
                { header: 'Tahun Pelaksanaan', key: 'adv_year' },
                { header: 'Bentuk Program', key: 'bentuk_program' },
                { header: 'Indikator DMF', key: 'indicator_id' },
                { header: 'Kategori Pengeluaran', key: 'category_name' },
                { header: 'Anggaran', key: 'est_value_idr' },
            ]
            worksheet.addRows(data_export);
            worksheet.autoFilter = 'A1:J1';
            worksheet.eachRow(function (row, rowNumber) {
                row.eachCell((cell, colNumber) => {
                    if (rowNumber === 1) {
                        cell.fill = {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: { argb: 'f5b914' }
                        }
                    }
                })
                row.commit();
            });

            workbook.xlsx.writeBuffer().then((buffer) => {
                saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `GOI/PNBP - ${store_data.institution} ${store_data.year}.xlsx`);
            });
        } else {
            SwalAlertError('Aksi Gagal', 'Gagal cetak ke berkas excel.')
        }
    }

    function componentButtonActions(data) {
        const { id } = data.row.data;

        return (
            <div className="d-flex align-items-end justify-content-center">
                <Link to={`/goi-pnbp/edit/${id}`} title="Edit Data" className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                    <i className="fas fa-edit fa-sm"></i>
                </Link>
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(id)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="GOI / PNBP" menu_name="Penerimaan Negara Bukan Pajak" modul_name="">
                <span className="font-weight-bolder text-muted">Komponen {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <InformationHeaderPNBP
                            auth={auth}
                            store_data={store_data}
                            setStoreData={setStoreData}
                            apiStrengthenPNBP_List={apiStrengthenPNBP_List}
                        />

                        <div className="d-flex justify-content-between mb-2">
                            <h3>2. Daftar Komponen</h3>
                            <div>
                                <button type="button" onClick={() => onExportExcel()} className="btn btn-sm btn-light-success font-weight-bolder mr-2"><i className="fas fa-print icon-sm"></i> Cetak Excel</button>
                                <ButtonCreate text="Tambah Komponen" to={`/goi-pnbp/create?piu=${store_data.institution}&year=${store_data.year}`} className={`btn btn-primary font-weight-bolder btn-sm ml-2`} />
                            </div>
                        </div>
                        <DataGrid
                            dataSource={goi_pnbp}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Aksi" dataField="id" fixed={false} allowFiltering={false} alignment="center" cellRender={componentButtonActions} />
                            <Column caption="Pelaksanaan Kegiatan" dataField="quarter" cellRender={({ value, data }) => {
                                return <span>{value} / {data.adv_year}</span>
                            }} />
                            <Column caption="Bentuk Program" dataField="bentuk_program" />
                            <Column caption="Indikator DMF" dataField="indicator_id" />
                            <Column caption="Kategori Pengeluaran" dataField="category_name" />
                            <Column
                                dataField="est_value_idr"
                                caption="Anggaran"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default StrengthenPNBPMain