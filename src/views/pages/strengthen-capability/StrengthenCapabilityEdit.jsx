import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';

import { authUser } from 'app/slice/sliceAuth';
import { Card, CardBody } from 'views/components/card';
import { FormInput, FormSelect } from 'views/components/form';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { currency_to_number } from 'views/components/CurrencyToNumber';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiMaster_StrengthenComponent,
    apiMaster_StrengthenCategory,
    apiMaster_PaguContract,
} from 'app/services/apiMasterData';
import {
    apiStrengthenCapability_Show,
    apiStrengthenCapability_Update,
    apiStrengthenCapability_InformationStatus,
} from 'app/services/apiStrengthenCapability';

const list_strengthen = [
    { strengthen: 'Peningkataan Kualitas dan Kapasitas STP' },
    { strengthen: 'Peningkatan Riset Inovasi' },
    { strengthen: 'Pengembangan dan Pembinaan Start-Up' },
];


function StrengthenCapabilityEdit() {
    const { id } = useParams();
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [pagu_contract, setPaguContract] = useState('');
    const [current_alocation, setCurrentAlocation] = useState(0);
    const { store_data } = useSelector(state => state.auth);
    const { information_status } = useSelector(state => state.strengthen_capability);
    const { strengthen_component, strengthen_category } = useSelector(state => state.master);
    const { register, formState: { errors }, handleSubmit, control, reset } = useForm();

    useEffect(() => {
        reset({
            institution: store_data.institution,
            adv_year: store_data.year,
        });
    }, [reset]);

    useEffect(() => {
        dispatch(apiMaster_StrengthenComponent())
        dispatch(apiMaster_StrengthenCategory())
    }, [dispatch]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStrengthenCapability_Show({ id }));
        if (payload.status === 200) {
            reset({
                id: payload.data.id,
                component: payload.data.component,
                strengthen: payload.data.strengthen,
                institution: payload.data.institution,
                package_no: payload.data.package_no,
                category_id: payload.data.category_id,
                indicator_id: payload.data.indicator_id,
                quarter: payload.data.quarter,
                adv_year: payload.data.adv_year,
                volume: payload.data.volume,
                unit: payload.data.unit,
                est_value_idr: payload.data.est_value_idr,
            });
            setCurrentAlocation(payload.data.est_value_idr)
        }
    }, [reset, id]);

    useEffect(async () => {
        await dispatch(apiStrengthenCapability_InformationStatus({ institution: store_data.institution, adv_year: store_data.year }));

        const { payload } = await dispatch(apiMaster_PaguContract({ modul: 'strengthen', institution: store_data.institution, year_alocation: store_data.year }));
        if (payload.status === 200) {
            setPaguContract(payload.data)
        }
    }, [dispatch, store_data]);

    const groupedOptions = {};
    // Group the options by category_header
    strengthen_category.forEach(option => {
        if (!groupedOptions[option.category_header]) {
            groupedOptions[option.category_header] = [];
        }
        groupedOptions[option.category_header].push(option);
    });

    const onChangeComponent = (e) => {
        reset({ unit: e.target.selectedOptions[0].getAttribute('unit') })
    }

    const onSubmitUpdateData = async (data) => {
        try {
            //? Usulan anggaran tidak lebih dari PAGU alokasi
            if ((information_status.sum_value - current_alocation) + currency_to_number(data.est_value_idr) <= pagu_contract.total_alocation) {
                const { payload } = await dispatch(apiStrengthenCapability_Update(data))
                if (payload.status === 200) {
                    SwalAlertSuccess('Aksi Sukses', 'Berhasil simpan data');
                    reset();
                }
                else {
                    SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi');
                }
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Usulan anggaran tidak lebih dari PAGU alokasi');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Strengthen Capability Building" menu_name="Penguatan Kelembagaan" modul_name="Data Detail" />
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Data Detail Komponen</h3>
                                <form onSubmit={handleSubmit(onSubmitUpdateData)} className="form">
                                    <input type="hidden" {...register("id", { required: true })} />
                                    <input type="hidden" {...register("institution", { required: true })} />
                                    <input type="hidden" {...register("adv_year", { required: true })} />
                                    <input type="hidden" {...register("username", { required: true })} defaultValue={auth.username} />
                                    <div className="row mb-10">
                                        <div className="col-lg-6 pr-10">
                                            <FormSelect
                                                name="quarter"
                                                label="1. Pelaksanaan Kegiatan"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.quarter}
                                                onChange={() => ''}
                                            >
                                                <option value="Q1">Q1</option>
                                                <option value="Q2">Q2</option>
                                                <option value="Q3">Q3</option>
                                                <option value="Q4">Q4</option>
                                            </FormSelect>
                                            <FormSelect
                                                name="strengthen"
                                                label="2. Bentuk Program"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.strengthen}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                {
                                                    list_strengthen.map((item, index) => {
                                                        return <option value={item.strengthen} key={index}>{item.strengthen}</option>
                                                    })
                                                }
                                            </FormSelect>
                                            <div className="form-group">
                                                <label className="font-weight-bolder">3. Komponen Biaya :</label>
                                                <div className="input-group">
                                                    <select
                                                        {...register("component", { required: true })}
                                                        className="form-control"
                                                        onChange={(e) => onChangeComponent(e)}
                                                    >
                                                        <option value="">-- Pilih --</option>
                                                        {
                                                            strengthen_component.map((item, index) => {
                                                                return <option value={item.component} unit={item.unit} key={index}>{index + 1}. {item.component}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                                {errors.component && <span className="form-text text-danger">* 3.Komponen Biaya</span>}
                                            </div>
                                            <FormInput
                                                name="unit"
                                                type="text"
                                                label="4. Satuan"
                                                className="form-control"
                                                placeholder="Satuan"
                                                register={register}
                                                rules={{ required: true }}
                                                readOnly={false}
                                                errors={errors.unit}
                                            />
                                        </div>
                                        <div className="col-lg-6 pl-10">
                                            <FormInput
                                                name="volume"
                                                type="text"
                                                label="5. Volume / Jumlah"
                                                className="form-control"
                                                placeholder="Volume / Jumlah"
                                                register={register}
                                                rules={{ required: true, pattern: { value: /^(0|[0-9]\d*)(\.\d+)?$/, } }}
                                                readOnly={false}
                                                errors={errors.volume}
                                            />
                                            <div className="form-group">
                                                <label className="font-weight-bolder">6. Indikator DMF yang didukung:</label>
                                                <div className="input-group">
                                                    <input type="text" {...register("indicator_id", { required: true })} className="form-control" placeholder="Indikator DMF" />
                                                    <div className="input-group-append">
                                                        <button type="button" data-toggle="modal" data-target="#modalDMFIndicator" className="btn btn-primary">Pilih</button>
                                                    </div>
                                                </div>
                                                {errors.indicator_id && <span className="form-text text-danger">* 6.Pilih Indikator DMF</span>}
                                            </div>
                                            <FormSelect
                                                name="category_id"
                                                label="7. Kategori Pengeluaran"
                                                className="form-control"
                                                register={register}
                                                rules={{ required: true }}
                                                errors={errors.category_id}
                                                onChange={() => ''}
                                            >
                                                <option value="">-- Pilih --</option>
                                                {Object.keys(groupedOptions).map(header => (
                                                    <optgroup label={header} key={header}>
                                                        {groupedOptions[header].map(option => (
                                                            <option value={option.id} key={option.id}>
                                                                {option.category_name}
                                                            </option>
                                                        ))}
                                                    </optgroup>
                                                ))}
                                            </FormSelect>
                                            <div className="form-group mb-4">
                                                <label className="font-weight-bolder">8. Anggaran (Rp.):</label>
                                                <Controller
                                                    control={control}
                                                    name="est_value_idr"
                                                    rules={{ required: true }}
                                                    render={({ field: { ref, ...rest } }) => (
                                                        <NumericFormat
                                                            id="est_value_idr"
                                                            className="form-control"
                                                            thousandSeparator=","
                                                            decimalSeparator="."
                                                            decimalScale={2}
                                                            getInputRef={ref}
                                                            {...rest}
                                                        />
                                                    )}
                                                />
                                                {errors.est_value_idr && <span className="form-text text-danger">* 8.Anggaran</span>}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="separator separator-dashed separator-border-2 my-4"></div>
                                    <div className="d-flex justify-content-between">
                                        <ButtonCancel to="/strengthen-capability" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>

            </Container>
        </MainContent>
    )
}

export default StrengthenCapabilityEdit