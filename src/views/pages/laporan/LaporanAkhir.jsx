import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import ListDocsUploaded from '../strengthen-capability/ListDocsUploaded';
import { authUser } from 'app/slice/sliceAuth';
import { useForm } from 'react-hook-form';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDocument_Upload } from 'app/services/apiDocument';

function LaporanAkhir() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { store_data } = useSelector(state => state.auth);
    const { docs_uploaded_list } = useSelector(state => state.master);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'laporan', status: 'Laporan-Akhir' }));
    }, [dispatch]);

    const onSubmitForm = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.package_no,
                document_for: data.modul,
                document_name: data.document_name,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                await dispatch(apiDocument_UploadList({ package_no: data.package_no, status: data.status, document_for: data.modul }));
                SwalAlertSuccess('Berhasil', 'Berhasil simpan data.');
                setLoading('');
                setFileDoc('');
                reset();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Laporan" menu_name="Laporan Akhir" modul_name="">
                {/* <span className="font-weight-bolder text-muted">Tahun {store_data.year}</span> */}
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <h3 className="font-size-lg text-dark font-weight-bold mb-4">Unggah Dokumen Laporan Akhir</h3>
                        <form onSubmit={handleSubmit(onSubmitForm)} encType="multipart/form-data">
                            <input type="hidden" {...register("modul", { required: true, value: "laporan" })} />
                            <input type="hidden" {...register("package_no", { required: true, value: "" })} />
                            <input type="hidden" {...register("status", { required: true, value: "Persiapan-Usulan" })} />
                            <div className="form-group row">
                                <div className="col-lg-3">
                                    <div className="form-group">
                                        <label>1. Nama Dokumen * :</label>
                                        <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                            <option value="">-- Pilih Dokumen --</option>
                                            {
                                                docs_uploaded_list.map((item, index) => {
                                                    return <option value={item.document_name} key={index}>{item.document_name}</option>
                                                })
                                            }
                                        </select>
                                        {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                    <div className="form-group">
                                        <label>2. Unggah Dokumen :</label>
                                        <div className={`custom-file`}>
                                            <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                            <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                                        </div>
                                        {errors.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                    <div className="form-group">
                                        <label>3. Keterangan (Opsional) :</label>
                                        <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                    <div className="form-group">
                                        <label>4. Unggah :</label>
                                        <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                            <i className="fa fa-upload fa-sm" />
                                            Unggah Berkas
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <ListDocsUploaded package_no="" status="Persiapan-Usulan" document_for="laporan" />
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default LaporanAkhir