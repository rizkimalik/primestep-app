import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card'
import { apiLaporanDokumen_RND } from 'app/services/apiLaporanDokumen';

function DokumenRND() {
    const dispatch = useDispatch();
    const [piu, setPIU] = useState('UI');
    const [riset_inovasi, setRisetInovasi] = useState([]);
    const [inkubasi_startup, setInkubasiStartup] = useState([]);
    const [akselerasi_startup, setAkselerasiStartup] = useState([]);
    const [proposed_year, setProposedYear] = useState(new Date().getFullYear());

    useEffect(async () => {
        const result1 = await dispatch(apiLaporanDokumen_RND({ modul: 'research', piu, proposed_year }));
        const result2 = await dispatch(apiLaporanDokumen_RND({ modul: 'startup-incubation', piu, proposed_year }));
        const result3 = await dispatch(apiLaporanDokumen_RND({ modul: 'startup-acceleration', piu, proposed_year }));
        setRisetInovasi(result1.payload.data);
        setInkubasiStartup(result2.payload.data);
        setAkselerasiStartup(result3.payload.data);
    }, [dispatch, proposed_year, piu]);

    return (
        <MainContent>
            <SubHeader active_page="Laporan Dokumen" menu_name="Research and Development" modul_name="">
                <ul className="nav nav-pills nav-pills-sm nav-dark-75 mx-5">
                    <li className="nav-item">
                        <Link to="#ui" className={`nav-link btn-sm font-weight-bolder ${piu == 'UI' && 'active'}`} data-toggle="tab" onClick={() => setPIU('UI')}>UI</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#ugm" className={`nav-link btn-sm font-weight-bolder ${piu == 'UGM' && 'active'}`} data-toggle="tab" onClick={() => setPIU('UGM')}>UGM</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#itb" className={`nav-link btn-sm font-weight-bolder ${piu == 'ITB' && 'active'}`} data-toggle="tab" onClick={() => setPIU('ITB')}>ITB</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="#ipb" className={`nav-link btn-sm font-weight-bolder ${piu == 'IPB' && 'active'}`} data-toggle="tab" onClick={() => setPIU('IPB')}>IPB</Link>
                    </li>
                </ul>

                <div className="dropdown">
                    <button className="btn btn-danger btn-sm dropdown-toggle font-weight-bolder" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Tahun {proposed_year}
                    </button>
                    <div className="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                        <a onClick={() => setProposedYear(2023)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">1. Tahun 2023</a>
                        <a onClick={() => setProposedYear(2024)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">2. Tahun 2024</a>
                        <a onClick={() => setProposedYear(2025)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">3. Tahun 2025</a>
                        <a onClick={() => setProposedYear(2026)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">4. Tahun 2026</a>
                        <a onClick={() => setProposedYear(2027)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">5. Tahun 2027</a>
                    </div>
                </div>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <div className="mb-2">
                            <h3>Laporan Dokumen Research and Development</h3>
                            <p className="text-muted">Unggahan dokumen proposal perjudul & dokumen gabungan penelitian, inkubasi startup & akselerasi startup yang harus diupload pihak PIU</p>
                        </div>
                        <div className="separator separator-dashed separator-border-2 my-4"></div>

                        <h3>1. Dokumen Riset Inovasi</h3>
                        <DataGrid
                            dataSource={riset_inovasi}
                            remoteOperations={true}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {/* <Column caption="Nomor Paket" dataField="package_no" /> */}
                            <Column caption="Judul Proposal" dataField="proposal_name" />
                            <Column caption="Usulan Perjudul" dataField="total_usulan_perjudul" alignment="center" width={120} />
                            <Column caption="Dokumen Gabungan" alignment="center">
                                <Column caption="Kelengkapan Proposal" dataField="total_persiapan_usulan" width={120} />
                                <Column caption="Kontrak" dataField="total_kontrak" width={120} />
                                <Column caption="Pencairan" dataField="total_pencairan" width={120} />
                            </Column>
                        </DataGrid>
                        <div className="separator separator-dashed separator-border-2 my-4"></div>

                        <h3>2. Dokumen Inkubasi Startup</h3>
                        <DataGrid
                            dataSource={inkubasi_startup}
                            remoteOperations={true}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {/* <Column caption="Nomor Paket" dataField="package_no" /> */}
                            <Column caption="Judul Proposal" dataField="proposal_name" />
                            <Column caption="Usulan Perjudul" dataField="total_usulan_perjudul" alignment="center" width={120} />
                            <Column caption="Dokumen Gabungan" alignment="center">
                                <Column caption="Kelengkapan Proposal" dataField="total_persiapan_usulan" width={120} />
                                <Column caption="Kontrak" dataField="total_kontrak" width={120} />
                                <Column caption="Pencairan" dataField="total_pencairan" width={120} />
                            </Column>
                        </DataGrid>
                        <div className="separator separator-dashed separator-border-2 my-4"></div>

                        <h3>3. Dokumen Akselerasi Startup</h3>
                        <DataGrid
                            dataSource={akselerasi_startup}
                            remoteOperations={true}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {/* <Column caption="Nomor Paket" dataField="package_no" /> */}
                            <Column caption="Judul Proposal" dataField="proposal_name" />
                            <Column caption="Usulan Perjudul" dataField="total_usulan_perjudul" alignment="center" width={120} />
                            <Column caption="Dokumen Gabungan" alignment="center">
                                <Column caption="Kelengkapan Proposal" dataField="total_persiapan_usulan" width={120} />
                                <Column caption="Kontrak" dataField="total_kontrak" width={120} />
                                <Column caption="Pencairan" dataField="total_pencairan" width={120} />
                            </Column>
                        </DataGrid>
                        <div className="separator separator-dashed separator-border-2 my-4"></div>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default DokumenRND