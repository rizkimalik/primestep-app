import React, { useEffect, useState } from 'react'
import { useParams, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import HeaderFormCreate from './header/HeaderFormCreate';
import StepFormProposal from './step/StepFormProposal';
import StepFormLuaran from './step/StepFormLuaran';
import StepFormRAB from './step/StepFormRAB';
import StepFormTargetPendapatan from './step/StepFormTargetPendapatan';
import StepFormMitraPendukung from './step/StepFormMitraPendukung';
import StepFormKonfiramasi from './step/StepFormKonfiramasi';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { RandomNumber } from 'views/components/RandomString';
import { setStoreData } from 'app/slice/sliceAuth';
import { apiStartupResearch_Detail } from 'app/services/apiStartupResearch';
import { data_status } from './step/data_step';


function MainFormCreate() {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const startup = new URLSearchParams(search).get('startup');
    const [navigate, setNavigate] = useState('1');
    const [data_detail, setDataDetail] = useState('');
    const [proposal_no, setProposalNo] = useState('');
    const [package_no, setPackageNo] = useState('');
    const [load_data, setLoadData] = useState(0);
    const { modul } = useParams();
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        const piu = new URLSearchParams(search).get('piu');
        const year = new URLSearchParams(search).get('year');

        setProposalNo(`RND${year}-${piu}`);
        setPackageNo(`RND${year}-${piu}${RandomNumber(3)}`);
        dispatch(setStoreData({ ...store_data, institution: piu, year }))
    }, [search]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStartupResearch_Detail({ package_no }));
        setDataDetail(payload.data);
    }, [dispatch, package_no, load_data]);

    //? pake akselerasi
    useEffect(async () => {
        const startup = new URLSearchParams(search).get('startup');
        if (startup === 'acceleration') {
            const { payload } = await dispatch(apiStartupResearch_Detail({ package_no: store_data.package_no }));
            setDataDetail(payload.data);
        }
    }, [dispatch, store_data, search]);

    useEffect(() => {
        //? utk cek strp paket & navigasi aktif
        if (!data_detail) return;
        const filteredStatus = data_status.filter(item => item.status === data_detail.status);
        if (filteredStatus.length > 0) {
            setNavigate(filteredStatus[0].navigate);
        }
    }, [data_detail]);


    return (
        <MainContent>
            <SubHeader active_page="Research and Development" menu_name="Form Usulan" modul_name={startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'}>
                <span className="font-weight-bolder text-muted">{startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <HeaderFormCreate navigate={navigate} modul={modul} />
                        <Card>
                            <CardBody>
                                {navigate === '1' && <StepFormProposal modul={modul} proposal_no={proposal_no} package_no={package_no} data_detail={data_detail} setNavigate={setNavigate} setLoadData={setLoadData} startup={startup} />}
                                {(navigate === '2' && modul === 'startup') && <StepFormTargetPendapatan modul={modul} package_no={package_no} data_detail={data_detail} setNavigate={setNavigate} setLoadData={setLoadData} startup={startup} />}
                                {(navigate === '2' && modul === 'research') && <StepFormLuaran modul={modul} package_no={package_no} data_detail={data_detail} setNavigate={setNavigate} setLoadData={setLoadData} startup={startup} />}
                                {navigate === '3' && <StepFormRAB modul={modul} package_no={package_no} data_detail={data_detail} setNavigate={setNavigate} startup={startup} />}
                                {navigate === '4' && <StepFormMitraPendukung modul={modul} package_no={package_no} setNavigate={setNavigate} startup={startup} />}
                                {navigate === '5' && <StepFormKonfiramasi modul={modul} package_no={package_no} setNavigate={setNavigate} startup={startup} />}
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default MainFormCreate