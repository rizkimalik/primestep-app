import React from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';

import { IconMark } from 'views/components/icon';
import { authUser } from 'app/slice/sliceAuth';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiStartupResearch_SearchUsulan } from 'app/services/apiStartupResearch';
import { apiStartupResearch_Detail } from 'app/services/apiStartupResearch';

function ModalUsulanLanjutan({ reset, institution }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const { usulan_list } = useSelector(state => state.startup_research);

    const onClickSearch = ({ search_value }) => {
        dispatch(apiStartupResearch_SearchUsulan({ institution, search_value }))
    }

    const onClickSelected = async (row) => {
        const { payload } = await dispatch(apiStartupResearch_Detail({ package_no: row.package_no }));
        if (payload.status !== 200) return;
        const data = payload.data;
        reset({
            jenis_usulan: '1', //? flag usulan 1=lanjutan, 0=baru
            piu: data?.piu,
            proposal_name: data?.proposal_name,
            category_focus: data?.category_focus,
            category_framework: data?.category_framework,
            proposed_year: data?.proposed_year,
            value_idr: data?.value_idr,
            type: data?.type,
            tkt_value: data?.tkt_value,
            tkt_duration: data?.tkt_duration,
            tkt_category: data?.tkt_category,
            product_development: data?.product_development,
            // package_no: data?.package_no,
            // proposal_no: data?.proposal_no,
            // type_corporation: data?.type_corporation,
            // base_income_lastyear: data?.base_income_lastyear,
            // product_featured: data?.product_featured,
            // product_description: data?.product_description,
            //? data leader
            leader_name: data?.leader?.leader_name,
            title_position: data?.leader?.title_position,
            identity_id: data?.leader?.identity_id,
            gender: data?.leader?.gender,
            email: data?.leader?.email,
            phone_number: data?.leader?.phone_number,
            graduation: data?.leader?.graduation,
            address: data?.leader?.address,
        });
    }

    const componentButtonActions = (data) => {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={() => onClickSelected(row)}
                    data-dismiss="modal"
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> Pilih Usulan
                </button>
            </div>
        )
    }

    return (
        <Modal id="modalUsulanLanjutan" modal_size="modal-lg">
            <ModalHeader title="Daftar Usulan" />
            <ModalBody className="p-4">
                <div className="row">
                    <div className="col-lg-6">
                        <form onSubmit={handleSubmit(onClickSearch)} className="form">
                            <div className="form-group">
                                <label>Cari usulan berdasarkan NIDN/NIDK atau Nama Ketua Peneliti</label>
                                <div className="input-group">
                                    <input type="text" {...register("search_value", { required: true })} className="form-control" placeholder="Cari..." />
                                    <div className="input-group-append">
                                        <button className="btn btn-primary" type="submit"><i className="fas fa-search fa-sm"></i></button>
                                    </div>
                                </div>
                                {errors.search_value && <span className="form-text text-danger">* Masukan NIDN/NIDK atau Nama Ketua Peniliti</span>}
                            </div>
                        </form>
                    </div>
                </div>

                <DataGrid
                    dataSource={usulan_list}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    columnMinWidth={80}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={5} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[5, 10]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Pilih" dataField="id" cellRender={componentButtonActions} />
                    <Column caption="Judul Penelitian" dataField="proposal_name" />
                    <Column caption="Ketua Peneliti" dataField="leader_name" />
                    <Column caption="Nomor Identitas" dataField="identitas_id" />
                    <Column caption="Tahun Usulan" dataField="proposed_year" />
                </DataGrid>
            </ModalBody>
            <ModalFooter />
        </Modal>
    )
}

export default ModalUsulanLanjutan