import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';

import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import {
    apiInnovation_MemberInsertUpdate,
    apiInnovationMember,
    apiInnovation_MemberDetail,
} from 'app/services/apiStartupResearch';

function ModalMemberTeam({ id, package_no }) {
    const dispatch = useDispatch();
    const [golongan, setGolongan] = useState('');
    const {
        register: register_member,
        formState: { errors: errors_member },
        handleSubmit: handleSubmit_member,
        reset: reset_member,
    } = useForm();

    useEffect(() => {
        reset_member();
        reset_member({ id });
    }, [reset_member, id]);

    useEffect(async () => {
        reset_member();
        if (id) {
            const { payload } = await dispatch(apiInnovation_MemberDetail({ id }));
            setGolongan(payload.data.member_type);
            reset_member({
                member_name: payload.data.member_name,
                member_gender: payload.data.member_gender,
                member_type: payload.data.member_type,
                member_identity: payload.data.member_identity,
                member_roles: payload.data.member_roles,
                member_phone: payload.data.member_phone,
                member_email: payload.data.member_email,
                member_address: payload.data.member_address,
                partner_category: payload.data.partner_category,
                partner_name: payload.data.partner_name,
            });
        } else {
            reset_member();
        }
    }, [reset_member, id]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiInnovation_MemberInsertUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data anggota tim.')
                dispatch(apiInnovationMember({ package_no }));
                reset_member();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }


    return (
        <Modal id="modalTambahAnggota" modal_size="modal-lg">
            <ModalHeader title="Tambah Anggota Tim" />
            <form onSubmit={handleSubmit_member(onSubmitInsertData)} className="form" id="form-team">
                <ModalBody>
                    <input type="hidden" {...register_member("id", { required: false })} />
                    <input type="hidden" {...register_member("package_no", { required: true })} value={package_no} />
                    {errors_member.package_no && <span className="form-text text-danger">* No Paket Kosong</span>}

                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">1. Jenis Anggota :</label>
                                <select {...register_member("member_type", { required: true })} className="form-control" onChange={(e) => setGolongan(e.target.value)}>
                                    <option value="">-- Pilih --</option>
                                    <option value="Mahasiswa">1. Mahasiswa</option>
                                    <option value="Dosen">2. Dosen</option>
                                    <option value="Tendik">3. Tendik</option>
                                    <option value="Mitra">4. Mitra</option>
                                </select>
                                {errors_member.member_type && <span className="form-text text-danger">* Pilih Jenis Anggota</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">2. Nomor {golongan === 'Mahasiswa' ? 'NIM' : golongan === 'Dosen' ? 'NIDN/NIDK' : golongan === 'Tendik' ? 'NIP/NIK' : 'NIK'} :</label>
                                <input type="text" {...register_member("member_identity", { required: true })} className="form-control" placeholder="Nomor Identitas" />
                                {errors_member.member_identity && <span className="form-text text-danger">* Nomor {golongan === 'Mahasiswa' ? 'NIM' : golongan === 'Dosen' ? 'NIDN/NIDK' : golongan === 'Tendik' ? 'NIP/NIK' : 'NIK'}</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">3. Nama Lengkap :</label>
                                <input type="text" {...register_member("member_name", { required: true })} className="form-control" placeholder="Nama Lengkap" />
                                {errors_member.member_name && <span className="form-text text-danger">* Nama Lengkap</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">4. Nomor HP :</label>
                                <input type="text" {...register_member("member_phone", { required: true, pattern: { value: /^(0|[0-9]\d*)(\.\d+)?$/, } })} className="form-control" placeholder="Nomor HP" required />
                                {errors_member.member_phone && <span className="form-text text-danger">* Nomor HP</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">5. Email :</label>
                                <input type="email" {...register_member("member_email", { required: true, pattern: { value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, } })} className="form-control" placeholder="Email" />
                                {errors_member.member_email && <span className="form-text text-danger">* Email</span>}
                            </div>
                        </div>

                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">6. Jenis Kelamin :</label>
                                <select {...register_member("member_gender", { required: true })} className="form-control" >
                                    <option value="">-- Pilih --</option>
                                    <option value="Pria">1. Pria</option>
                                    <option value="Wanita">2. Wanita</option>
                                </select>
                                {errors_member.member_gender && <span className="form-text text-danger">* Jenis Kelamin</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">7. Peran Anggota :</label>
                                <input type="text" {...register_member("member_roles", { required: true })} className="form-control" placeholder="Peran Anggota" />
                                {errors_member.member_roles && <span className="form-text text-danger">* Peran Anggota</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">8. Alamat :</label>
                                <textarea {...register_member("member_address", { required: true })} className="form-control" rows="2" placeholder="Alamat"></textarea>
                                {errors_member.member_address && <span className="form-text text-danger">* Alamat</span>}
                            </div>
                            {
                                golongan === 'Mitra' &&
                                <div className="">
                                    <div className="form-group">
                                        <label className="font-weight-bolder">9. Mitra :</label>
                                        <select {...register_member("partner_category", { required: true })} className="form-control" >
                                            <option value="">-- Pilih --</option>
                                            <option value="Industri">1. Mitra Industri</option>
                                            <option value="Perguruan Tinggi">2. Mitra Perguruan Tinggi</option>
                                            <option value="Lembaga">3. Mitra Lembaga</option>
                                        </select>
                                        {errors_member.partner_category && <span className="form-text text-danger">* Pilih Mitra</span>}
                                    </div>
                                    <div className="form-group">
                                        <label className="font-weight-bolder">10. Nama Instansi Asal :</label>
                                        <input type="text" {...register_member("partner_name", { required: true })} className="form-control" placeholder="Nama Instansi" />
                                        {errors_member.partner_name && <span className="form-text text-danger">* Nama Instansi</span>}
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalMemberTeam