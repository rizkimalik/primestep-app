import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';

import Icons from 'views/components/Icons';
import { urlAttachment } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { ButtonSubmit } from 'views/components/button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiStartupResearch_StartupList,
    apiStartupResearch_ResearchList,
    apiStartupResearch_StatusVerificationProposal,
    apiStartupResearch_HistoryVerificationProposal,
} from 'app/services/apiStartupResearch';
import ListDocsUploaded from '../ListDocsUploaded';

function ModalVerificationProposal({ proposal_data, modul, step }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [is_recomend, setRecomend] = useState(true);
    const [history_proposal, setHistoryProposal] = useState([]);
    const { proposal_document } = proposal_data;
    const { store_data } = useSelector(state => state.auth);
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();

    useEffect(() => {
        reset({
            id: proposal_document?.id,
            package_no: proposal_data?.package_no,
            recommend: proposal_data?.recommend,
            recommend_budget: proposal_data?.recommend_budget,
            recommend_comment: proposal_document?.recommend_comment,
        });
    }, [reset, proposal_document]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStartupResearch_HistoryVerificationProposal({ package_no: proposal_data.package_no }));
        if (payload.status === 200) {
            setHistoryProposal(payload.data);
        }
    }, [dispatch, proposal_data]);

    function currency_to_number(currencyString) {
        if (typeof currencyString !== 'string') {
            return currencyString; // or any other appropriate value
        }
        const numericString = currencyString.replace(/[^0-9.]/g, '');
        const numericValue = parseFloat(numericString);
        return numericValue;
    }

    const onClickHistoryProposal = async () => {
        try {
            const { payload } = await dispatch(apiStartupResearch_HistoryVerificationProposal({ package_no: proposal_data.package_no }))
            if (payload.status === 200) {
                setHistoryProposal(payload.data);
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiStartupResearch_StatusVerificationProposal(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan data.')
                if (modul === 'startup') {
                    await dispatch(apiStartupResearch_StartupList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                }
                else {
                    await dispatch(apiStartupResearch_ResearchList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                }
                reset();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalVerificationProposal" modal_size="modal-lg">
            <ModalHeader title={`Verifikasi Pengusulan Judul Proposal`} />
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form">
                <ModalBody>
                    <div className="row mb-0">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Judul {modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} :</label>
                                <p className="border-0 pl-0 text-primary font-weight-bolder">{proposal_data.proposal_name}</p>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Kategori {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.focus_name}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Bidang Fokus {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.framework}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Anggaran Usulan :</label>
                                <span className="form-control border-0 pl-0">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(proposal_data.value_idr))}</span>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Nama {modul === 'startup' ? 'Perintis' : 'Peneliti'} :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.leader_name}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">TKT :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.tkt_value}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Tahun Usulan :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.proposed_year}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Durasi {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                <span className="form-control border-0 pl-0">{proposal_data.tkt_duration} Tahun</span>
                            </div>
                        </div>
                    </div>
                    {
                        step === 'verification' &&
                        <div>
                            {/* <div className="separator separator-dashed separator-border-2 my-4"></div> */}
                            <ul className="nav nav-tabs justify-content-end mt-0">
                                <li className={`nav-item ${auth.user_level === 'PIU' && 'hide'}`}>
                                    <a className="nav-link font-weight-bolder" data-toggle="tab" href="#form_verification">Form Verifikasi</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => onClickHistoryProposal()} className="nav-link font-weight-bolder active" data-toggle="tab" href="#history_verification">Riwayat Verifikasi</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => ''} className="nav-link font-weight-bolder" data-toggle="tab" href="#dokumen_pengusulan">Dokumen Pengusulan</a>
                                </li>
                            </ul>
                            <div className="tab-content mt-5">
                                <div className={`tab-pane fade ${auth.user_level === 'PIU' && 'hide'}`} id="form_verification" role="tabpanel" aria-labelledby="form_verification">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <input type="hidden" {...register('id', { required: true })} />
                                            <input type="hidden" {...register('package_no', { required: true })} />
                                            <input type="hidden" {...register('recommend_evaluator', { required: true })} value={auth.username} />
                                            <div className="form-group">
                                                <label className="font-weight-bolder">Apakah Judul Proposal ini direkomendasi?</label>
                                                <div className="radio-inline">
                                                    <label className="radio">
                                                        <input type="radio" {...register('recommend', { required: true })} value={1} defaultChecked={proposal_data.recommend === 1} onClick={() => setRecomend(true)} />
                                                        <span /> Rekomendasi
                                                    </label>
                                                    <label className="radio">
                                                        <input type="radio" {...register('recommend', { required: true })} value={0} defaultChecked={proposal_data.recommend === 0} onClick={() => setRecomend(false)} />
                                                        <span /> Revisi
                                                    </label>
                                                    <label className="radio">
                                                        <input type="radio" {...register('recommend', { required: true })} value={2} defaultChecked={proposal_data.recommend === 2} onClick={() => setRecomend(true)} />
                                                        <span /> Ditolak
                                                    </label>
                                                </div>
                                                {errors.recommend && <span className="form-text text-danger">* Konfirmasi Rekomendasi / Revisi</span>}
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="font-weight-bolder">Rekomendasi Anggaran (Rp.) :</label>
                                                <Controller
                                                    control={control}
                                                    name="recommend_budget"
                                                    rules={{
                                                        required: true,
                                                        validate: (value) => {
                                                            if (currency_to_number(value) > currency_to_number(proposal_data.value_idr)) {
                                                                return 'Value must be less than or equal to Usulan';
                                                            }
                                                            return undefined;
                                                        }
                                                    }}
                                                    render={({ field: { ref, ...rest } }) => (
                                                        <NumericFormat
                                                            {...rest}
                                                            getInputRef={ref}
                                                            id="recommend_budget"
                                                            className="form-control"
                                                            thousandSeparator=","
                                                            decimalSeparator="."
                                                            decimalScale={2}
                                                        />
                                                    )}
                                                />
                                                {errors.recommend_budget && <span className="form-text text-danger">* Rekomendasi Anggaran harus kurang dari atau sama dengan Nilai Usulan</span>}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label className="font-weight-bolder">Komentar Evaluator : </label>
                                                <textarea {...register('recommend_comment', { required: is_recomend ? false : true, minLength: is_recomend ? 0 : 20 })} className="form-control" rows="10" placeholder="Komentar rekomendasi"></textarea>
                                                {errors.recommend_comment && <span className="form-text text-danger">* Komentar minimal 20 huruf</span>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane fade active show" id="history_verification" role="tabpanel" aria-labelledby="history_verification">
                                    {
                                        history_proposal.map((item, index) => {
                                            return <div className="d-flex flex-column align-items-start border rounded p-4 mb-4" key={index}>
                                                <div className="d-flex flex-column flex-grow-1">
                                                    <div className="mb-4">
                                                        {
                                                            item.recommend === 0
                                                                ? <span className="label label-inline label-warning">Revisi</span>
                                                                : item.recommend === 1
                                                                    ? <span className="label label-inline label-success">Rekomendasi</span>
                                                                    : item.recommend === 2
                                                                        ? <span className="label label-inline label-danger">Ditolak</span>
                                                                        : <span className="label label-inline">Menunggu</span>
                                                        }
                                                        <a href={urlAttachment + '/' + item.document_for + '/' + item.filename} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold mx-4"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {item.filename} ({new Intl.NumberFormat("en", { notation: "compact", style: "unit", unit: "byte", unitDisplay: "narrow", }).format(item.filesize)})</a>
                                                    </div>
                                                    <small className="text-muted mb-2">
                                                        {
                                                            auth.user_level !== 'PIU' && 
                                                            <span>Evaluator: {item.evaluator_name}</span>
                                                        }
                                                        <span className="mx-4">{new Intl.DateTimeFormat('id-ID',
                                                            {
                                                                year: 'numeric',
                                                                month: 'short',
                                                                day: 'numeric',
                                                                hour: "numeric",
                                                                minute: "numeric",
                                                                second: "numeric",
                                                                timeZoneName: 'short',
                                                            })
                                                            .format(new Date(item.updated_at))}
                                                        </span>
                                                    </small>
                                                    <span className="mb-2">
                                                        Rekomendasi Anggaran:
                                                        <span className="text-dark font-weight-bolder ml-2">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(item.recommend_budget)}</span>
                                                    </span>
                                                    <p>{item.recommend_comment}</p>
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                                <div className="tab-pane fade" id="dokumen_pengusulan" role="tabpanel" aria-labelledby="dokumen_pengusulan">
                                    <ListDocsUploaded package_no={proposal_data.package_no} status='Persiapan-Usulan' document_for={modul} />

                                </div>
                            </div>
                        </div>
                    }
                </ModalBody>
                <ModalFooter>
                    {step === 'verification' && auth.user_level === 'Evaluator' && <ButtonSubmit />}
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalVerificationProposal