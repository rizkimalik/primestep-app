import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'views/components/modal';
import { ButtonSubmit } from 'views/components/button';
import { NumericFormat } from 'react-number-format';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiDisbursementUpdateInsert, apiDisbursementShow } from 'app/services/apiDisbursement';
import ValidateDisbursementPercent from 'views/components/ValidateDisbursementPercent';

function ModalDisbursement({ id, contract_detail, apiDisbursementList }) {
    const dispatch = useDispatch();
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();
    const { disbursement_list } = useSelector(state => state.disbursement);

    useEffect(() => {
        reset();
        reset({ id, contract_no: contract_detail.contract_no, institution: contract_detail.institution });
    }, [reset, id, contract_detail]);

    useEffect(async () => {
        reset();
        if (id) {
            const { payload } = await dispatch(apiDisbursementShow({ id }));
            reset({
                id: payload.data.id,
                contract_no: payload.data.contract_no,
                disbursement_percent: payload.data.disbursement_percent,
                disbursement_value: payload.data.disbursement_value,
                disbursement_date: (payload.data.disbursement_date).slice(0, 10),
            });
        } else {
            reset();
        }
    }, [reset, id]);

    const onSubmitUpdateInsert = async (data) => {
        const current = ValidateDisbursementPercent(disbursement_list);
        const percent = Number(data.disbursement_percent);
        const total = current + percent;

        try {
            if (total <= 100) {
                const { payload } = await dispatch(apiDisbursementUpdateInsert(data))
                if (payload.status === 200) {
                    SwalAlertSuccess('Aksi Berhasil', 'Berhasil input data.');
                    dispatch(apiDisbursementList({ contract_no: contract_detail.contract_no }))
                    reset();
                }
                else {
                    SwalAlertError('Aksi Gagal.', 'Silahkan cobal kembali.');
                }
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Persentase pencairan maksimal 100%.');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan cobal kembali, ${error.message}.`);
        }
    }

    const CountValuePercent = (e) => {
        const percent = e.target.value;
        const result = Number(contract_detail?.contract_value) * percent / 100;
        reset({ disbursement_value: result });
    }

    return (
        <Modal id="modalTambahPencairan" modal_size="modal-md">
            <ModalHeader title="Tahap Pencairan" />
            <form onSubmit={handleSubmit(onSubmitUpdateInsert)} className="form">
                <ModalBody>
                    <div className="mb-2">
                        <input type="hidden" {...register('id', { required: false })} />
                        <input type="hidden" {...register('contract_no', { required: true })} />
                        <input type="hidden" {...register('institution', { required: true })} />
                        <div className="form-group row">
                            <label className="col-lg-4 col-form-label text-right">Pencairan (%):</label>
                            <div className="col-lg-6">
                                <select
                                    {...register('disbursement_percent', { required: true })}
                                    onChange={(e) => CountValuePercent(e)}
                                    className="form-control"
                                >
                                    <option value="">-- Pilih  --</option>
                                    <option value="70">70 %</option>
                                    <option value="30">30 %</option>
                                </select>
                                {errors.disbursement_percent && <span className="form-text text-danger">Pilih Persentase % Pencairan.</span>}

                                {/* <input type="number" onKeyUp={(e) => CountValuePercent(e)} {...register('disbursement_percent', { required: true })} className={`form-control ${errors.disbursement_percent && 'is-invalid'}`} placeholder="Persentase %" /> */}
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-4 col-form-label text-right">Jumlah Pencairan (Rp.):</label>
                            <div className="col-lg-6">
                                <Controller
                                    control={control}
                                    name="disbursement_value"
                                    rules={{ required: true }}
                                    render={({ field: { ref, ...rest } }) => (
                                        <NumericFormat
                                            id="disbursement_value"
                                            className="form-control"
                                            thousandSeparator=","
                                            decimalSeparator="."
                                            decimalScale={2}
                                            getInputRef={ref}
                                            {...rest}
                                        />
                                    )}
                                />
                                {errors.disbursement_value && <span className="form-text text-danger">Silahkan input Jumlah Pencairan.</span>}
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-4 col-form-label text-right">Tanggal SPP:</label>
                            <div className="col-lg-6">
                                <input type="date" {...register('disbursement_date', { required: true })} className={`form-control ${errors.disbursement_date && 'is-invalid'}`} placeholder="Enter Est. Value USD" />
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalDisbursement