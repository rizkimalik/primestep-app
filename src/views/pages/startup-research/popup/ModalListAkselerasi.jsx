import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter } from 'devextreme-react/data-grid';

import { IconMark } from 'views/components/icon';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { ButtonRefresh } from 'views/components/button';
import { apiStartup_AccelerationSelect } from 'app/services/apiStartupResearch';

function ModalListAkselerasi({ auth, store_data, setStoreData }) {
    const dispatch = useDispatch();
    const history = useHistory();
    const { startup_acceleration_select } = useSelector(state => state.startup_research);

    useEffect(() => {
        dispatch(apiStartup_AccelerationSelect({ auth, piu: store_data.institution }));
    }, [dispatch, auth, store_data]);

    const onSelectAcceleration = ({ piu, proposed_year, package_no }) => {
        dispatch(setStoreData({ ...store_data, package_no }));
        setTimeout(() => {
            history.push(`/startup-research/create/startup?piu=${piu}&year=${proposed_year}&startup=acceleration`);
            // history.push(`/startup-research/create/startup?piu=${piu}&year=${Number(proposed_year) + 1}&startup=acceleration`);
        }, 500);
    }

    const componentButtonActions = (data) => {
        const { piu, package_no, proposed_year } = data.row.data;

        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    data-dismiss="modal"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={() => onSelectAcceleration({ piu, proposed_year, package_no })}
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> Pilih Akselerasi
                </button>
            </div>
        )
    }

    return (
        <Modal id="modalListAkselerasi" modal_size="modal-xl">
            <ModalHeader title="Daftar Akselerasi Startup" />
            <ModalBody className="p-4">
                <DataGrid
                    dataSource={startup_acceleration_select}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    height={440}
                    wordWrapEnabled={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Column caption="Aksi" dataField="id" alignment="center" width={150} cellRender={componentButtonActions} />
                    <Column caption="Judul Proposal" dataField="proposal_name" width={300} />
                    <Column caption="Bidang Fokus" dataField="focus_name" />
                    <Column caption="Nama Ketua" dataField="leader_name" />
                    <Column
                        caption="Nilai Kontrak"
                        dataField="contract_value"
                        format="fixedPoint"
                        cellRender={({ value }) => {
                            return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                        }}
                    />
                    <Column caption="Tahun Usulan" dataField="proposed_year" />
                    <Column
                        caption="Usulan Dana"
                        dataField="value_idr"
                        format="fixedPoint"
                        cellRender={({ value }) => {
                            return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                        }}
                    />
                    <Column caption="TKT" dataField="tkt_value" />
                    <Column caption="Durasi Kegiatan" dataField="tkt_duration" cellRender={({ value }) => {
                        return <span>{value} Tahun</span>
                    }} />
                </DataGrid>
            </ModalBody>
            <ModalFooter>
                <ButtonRefresh onClick={() => dispatch(apiStartup_AccelerationSelect({ auth, piu: store_data.institution }))} />
            </ModalFooter>
        </Modal>
    )
}

export default ModalListAkselerasi