import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import SyaratBukitLuaran from '../information/SyaratBukitLuaran';
import SyaratKategoriLuaran from '../information/SyaratKategoriLuaran';
import { urlAttachment } from 'app/config';
import { ButtonSubmit } from 'views/components/button';
import { RandomNumber } from 'views/components/RandomString';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import {
    apiInnovation_OuterInsertUpdate,
    apiInnovation_OuterList,
    apiInnovation_OuterDetail,
    apiInnovation_OuterDocument,
} from 'app/services/apiStartupResearch';

function ModalLuaran({ id, package_no, data_detail }) {
    const dispatch = useDispatch();
    const tahun_list = [{ value: 1, tkt: 0 }, { value: 2, tkt: 1 }, { value: 3, tkt: 2 }];
    const [tahun_ke, setTahunke] = useState(Number(data_detail.tkt_value));
    const [file_doc, setFileDoc] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        reset();
        reset({ id });
    }, [reset, id]);

    useEffect(() => {
        reset({ tkt_value: tahun_ke });
    }, [reset, tahun_ke]);

    useEffect(async () => {
        reset();
        if (id) {
            const { payload } = await dispatch(apiInnovation_OuterDetail({ id }));
            reset({
                package_no: payload.data.package_no,
                outer_code: payload.data.outer_code,
                institution: payload.data.institution,
                tkt_value: payload.data.tkt_value,
                outer_in_year: payload.data.outer_in_year,
                outer_year: payload.data.outer_year,
                outer_name: payload.data.outer_name,
                outer_category: payload.data.outer_category,
                description: payload.data.description,
                document_file: payload.data.documentation,
            });
            setFileDoc({ name: payload.data.documentation })
        } else {
            reset();
        }
    }, [reset, id]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiInnovation_OuterInsertUpdate(data))
            if (payload.status === 200) {
                await dispatch(apiInnovation_OuterDocument(data)); //? insert dokumentasi foto/video
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan Luaran wajib tahunan.')
                dispatch(apiInnovation_OuterList({ package_no }));
                reset();
                setFileDoc('');
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalLuaran" modal_size="modal-lg">
            <ModalHeader title="Luaran Wajib Tahunan" />
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form" id="form-luaran" encType="multipart/form-data">
                <ModalBody>
                    <SyaratBukitLuaran />
                    <input type="hidden" {...register("id", { required: false })} />
                    <input type="hidden" {...register("institution", { required: true })} defaultValue={store_data.institution} />
                    <input type="hidden" {...register("package_no", { required: true })} defaultValue={package_no} />
                    <input type="hidden" {...register("outer_code", { required: true })} defaultValue={RandomNumber(10)} />
                    <input type="hidden" {...register("tkt_value", { required: true })} />

                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label>
                                    <span className="font-weight-bolder">Kategori Luaran :</span>
                                    <SyaratKategoriLuaran />
                                </label>
                                <select
                                    {...register("outer_category", { required: true })}
                                    className="form-control"
                                >
                                    <option value="">-- Pilih --</option>
                                    <option value="Purwarupa">1. Purwarupa</option>
                                    <option value="Produk">2. Produk</option>
                                </select>
                                {errors.outer_category && <span className="form-text text-danger">* Kategori Luaran</span>}
                            </div>
                        </div>
                        <div className="col-lg-6">
                            {/* <div className="form-group">
                                <label className="font-weight-bolder">Luaran :</label>
                                <input type="text" {...register("outer_name", { required: true })} className="form-control" placeholder="Luaran" />
                                {errors.outer_name && <span className="form-text text-danger">* Luaran</span>}
                            </div> */}
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Tahun ke :</label>
                                <select
                                    {...register("outer_in_year", { required: true })}
                                    className="form-control form-control-md"
                                    onChange={(e) => setTahunke(Number(data_detail.tkt_value) + Number(e.target.value) - 1)}
                                >
                                    <option value="">-- Pilih --</option>
                                    {
                                        //? filter tahun berdasarkan jumah tahun usulan
                                        tahun_list.filter(v => v.value <= data_detail.tkt_duration).map((item, index) => {
                                            return <option value={item.value} key={index}>{item.value}</option>
                                        })
                                    }
                                </select>
                                {errors.outer_in_year && <span className="form-text text-danger">* Pilih Tahun Kegiatan</span>}
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Tahun :</label>
                                <select
                                    {...register("outer_year", { required: true })}
                                    className="form-control form-control-md"
                                >
                                    <option value="">-- Pilih --</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                </select>
                                {errors.outer_year && <span className="form-text text-danger">* Pilih Tahun</span>}
                            </div>
                        </div>

                    </div>

                    
                    {/* <div className="row">
                        <div className="col-lg-12">
                            <div className="form-group">
                                <label className="font-weight-bolder">Keterangan (opsional) :</label>
                                <textarea {...register("description", { required: false })} className="form-control" rows="2" placeholder="Keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Unggah Dokumentasi Produk :</label>
                                <div className={`custom-file ${file_doc && 'hide'}`}>
                                    <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                    <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                                </div>
                                {
                                    file_doc &&
                                    <div>
                                        <a href={urlAttachment + '/usulan-luaran/' + file_doc.name} className="form-control font-weight-bold" target="_blank"><i className="fas fa-photo-video fa-sm text-primary text-truncate"></i> {file_doc.name}</a>
                                        <button type="button" onClick={() => setFileDoc('')} className="btn btn-light-danger btn-sm mt-2">Unggah Ulang</button>
                                    </div>
                                }
                                {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumentasi produk (foto/video).</span>}
                            </div>
                        </div>
                    </div> */}

                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalLuaran
