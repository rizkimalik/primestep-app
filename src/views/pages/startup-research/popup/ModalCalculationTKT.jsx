import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid } from 'devextreme-react/data-grid';

import { Card, CardBody } from 'views/components/card';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiMaster_CategoryTKT, apiMaster_IndicatorTKT } from 'app/services/apiMasterData';
import { apiHistoryTKT_Insert, apiHistoryTKT_List } from 'app/services/apiStartupResearch';

function ModalCalculationTKT({ value_tkt, setValueTKT, modul, duration_year, package_no, institution, data_detail, load_tkt, setLoadTKT, reset }) {
    const dispatch = useDispatch();
    const [tkt_no, setNoTKT] = useState(7);
    const [tkt_percent, setPercentTKT] = useState(0);
    const [category_tkt, setCategoryTKT] = useState('');
    const [history_tkt, setHistoryTKT] = useState([]);
    const { tkt_category, tkt_indicator } = useSelector(state => state.master)

    const dataObj = history_tkt.reduce((obj, item) => {
        obj[item.indicator_id] = item;
        return obj;
    }, {}); // fungsi duplicate data, ambil last data change
    const data_fields = Object.values(dataObj).reverse();

    useEffect(() => {
        dispatch(apiMaster_CategoryTKT())
    }, [dispatch]);

    useEffect(() => {
        // jika tahun 1 -> min tkt 6, tahun 2-3 -> tkt min 5
        // startup default 7
        if (modul === 'research') {
            setNoTKT(duration_year === '1' ? 6 : 5);
        }
    }, [duration_year, modul]);

    useEffect(() => {
        if (data_detail) {
            setNoTKT(data_detail ? data_detail?.tkt_value : 7);
            setCategoryTKT(data_detail?.tkt_category);
        }
    }, [value_tkt, data_detail]);

    useEffect(() => {
        // load indicator tkt > data default
        if (category_tkt) {
            dispatch(apiMaster_IndicatorTKT({ category_tkt, tkt_no }))
        }
    }, [dispatch, category_tkt, tkt_no]);

    useEffect(() => {
        // load indicator tkt > data detail
        if (data_detail) {
            dispatch(apiMaster_IndicatorTKT({ category_tkt: data_detail.tkt_category, tkt_no }))
        }
    }, [dispatch, data_detail, tkt_no]);

    useEffect(() => {
        setTimeout(async () => {
            if (data_detail) {
                const { payload } = await dispatch(apiHistoryTKT_List({ package_no: data_detail.package_no, category_tkt: data_detail.tkt_category }))
                if (payload.status === 200) {
                    let values = 0;
                    const result_data = payload.data;
                    const indicators = document.getElementsByName('percent');
                    for (var i = 0; i < indicators.length; i++) {
                        let indicator_id = indicators[i].getAttribute('indicator-id');
                        const matchingItem = result_data.find((item) => item.indicator_id === parseInt(indicator_id));
                        // console.log(matchingItem);
                        if (matchingItem) {
                            indicators[i].value = matchingItem.percentase;
                            indicators[i].selected = true;
                        } else {
                            console.warn(`No matching item found for indicator_id: ${indicator_id}`);
                        }

                        values += parseFloat(matchingItem?.percentase) || 0;
                    }

                    let result_percent = (values / tkt_indicator.length)
                    setPercentTKT(result_percent);
                }
            }
        }, 1000);
    }, [dispatch, data_detail, load_tkt]);

    const onChangeIndicatorTKT = (value) => {
        dispatch(apiMaster_IndicatorTKT({ category_tkt: value, tkt_no }));
        setCategoryTKT(value);
        // setTKTCategory(value);
        reset({ tkt_category: value });
    }

    const onClickNextTKT = () => {
        setNoTKT(Number(tkt_no) + 1);
        setPercentTKT(0);
        setLoadTKT(i => i + 1);
    }

    const onChangePercentValues = (e) => {
        const percentase = e.target.value;
        const indicator_id = e.target.getAttribute('indicator-id');
        setHistoryTKT(current => [...current, { institution, package_no, category_tkt, indicator_id, percentase }]);

        const percent = document.getElementsByName('percent');
        let values = 0;
        for (var i = 0; i < percent.length; i++) {
            values += parseFloat(percent[i].value) || 0;
        }
        let result_percent = (values / tkt_indicator.length)
        setPercentTKT(result_percent);
    }

    const onSubmitCalculationTKT = async (e) => {
        if (modul === 'research') {
            if (tkt_no === 5) {
                setValueTKT(tkt_no);
            } else if (tkt_no === 6 && duration_year === '1') {
                setValueTKT(tkt_no);
            } else {
                setValueTKT(tkt_percent < 100 ? (tkt_no - 1) : tkt_no);
            }
        }
        else {
            if (tkt_no === 7) {
                setValueTKT(tkt_no);
            } else {
                setValueTKT(tkt_percent < 100 ? (tkt_no - 1) : tkt_no);
            }
        }
        // setValueTKT(tkt_percent < 100 ? (tkt_no - 1) : tkt_no);

        const { payload } = await dispatch(apiHistoryTKT_Insert({ data_fields }));
        if (!payload) return;
        if (payload.status === 200) {
            Swal.fire("Berhasil simpan data TKT.");
        } else {
            Swal.fire("Gagal simpan data TKT.");
        }
    }


    return (
        <Modal id="modalCalculationTKT" modal_size="modal-lg">
            <ModalHeader title='Perhitungan TKT' />
            <form className="form" id="form-tkt">
                <ModalBody>
                    <div className="row">
                        <div className="col-lg-12">
                            {/* <div className="form-group">
                                <label className="font-weight-bolder">{modul === 'startup' ? '1. Produk Unggulan' : '1. Penelitian yang dikembangkan'}</label>
                                <input type="text" defaultValue={data_detail?.product_development} onKeyUp={(e) => setProductDevelopment(e.target.value)} className="form-control" placeholder={modul === 'startup' ? '1. Produk Unggulan' : '1. Penelitian yang dikembangkan'} />
                            </div> */}
                            <div className="form-group">
                                <label className="font-weight-bolder">Kategori TKT :</label>
                                <select
                                    name="category_tkt"
                                    className="form-control"
                                    defaultValue={data_detail?.tkt_category}
                                    onChange={(e) => onChangeIndicatorTKT(e.target.value)}
                                >
                                    <option value="">-- Pilih Kategori TKT --</option>
                                    {
                                        tkt_category.map((item, index) => {
                                            return <option value={item.id} key={index} selected={item.id === data_detail?.tkt_category ? true : false}>{index + 1}. {item.jenis_tkt}</option>
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="font-weight-bolder mb-2">Capaian Indikator TKT</div>
                    <Card>
                        <CardBody className="p-5 border border-primary rounded text-primary">
                            <i className="fas fa-info-circle text-primary"></i> Level TKT saat ini <b>{tkt_no}</b> dari <b>9</b>
                        </CardBody>
                    </Card>
                    {
                        // (tkt_indicator.length > 0 && data_detail?.tkt_category)
                        tkt_indicator.length > 0
                            ? <DataGrid
                                dataSource={tkt_indicator}
                                remoteOperations={true}
                                allowColumnReordering={true}
                                allowColumnResizing={true}
                                columnAutoWidth={true}
                                showBorders={true}
                                showColumnLines={true}
                                showRowLines={true}
                                wordWrapEnabled={true}
                                columnMinWidth={80}
                            >
                                <Column caption="Indikator" dataField="indicator" />
                                <Column caption="Persentase (%) " dataField="percent" width={120} cellRender={({ value, data }) => {
                                    return <select
                                        className="form-control form-control-sm"
                                        indicator-id={data.id}
                                        name="percent"
                                        onChange={(e) => onChangePercentValues(e)}
                                    >
                                        <option value="0">0%</option>
                                        <option value="20">20%</option>
                                        <option value="40">40%</option>
                                        <option value="60">60%</option>
                                        <option value="80">80%</option>
                                        <option value="100">100%</option>
                                    </select>
                                }} />
                            </DataGrid>
                            : <h3>Pilih Kategori TKT</h3>
                    }
                </ModalBody>
                <ModalFooter>
                    {
                        (tkt_no < 9 && tkt_percent >= 100) &&
                        <button type="button" onClick={() => onClickNextTKT()} className="btn btn-sm btn-primary">Lanjut TKT</button>
                    }
                    <button type="button" onClick={(e) => onSubmitCalculationTKT(e)} className="btn btn-sm btn-success ml-2" data-dismiss="modal">Selesai</button>
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalCalculationTKT