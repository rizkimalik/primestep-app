import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import Icons from 'views/components/Icons';
import { authUser } from 'app/slice/sliceAuth';
import { urlAttachment } from 'app/config';
import { ButtonSubmit } from 'views/components/button';
import { RandomNumber } from 'views/components/RandomString';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiDocument_Upload } from 'app/services/apiDocument';
import {
    apiInnovation_PartnerList,
    apiInnovation_PartnerDetail,
    apiInnovation_PartnerInsertUpdate,
} from 'app/services/apiStartupResearch';


function ModalMitraPendukung({ id, package_no, data_detail }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        reset();
        reset({ id });
    }, [reset, id]);

    useEffect(async () => {
        reset();
        if (id) {
            const { payload } = await dispatch(apiInnovation_PartnerDetail({ id }));
            reset({
                package_no: payload.data.package_no,
                partner_code: payload.data.partner_code,
                institution: payload.data.institution,
                year: payload.data.year,
                partner_name: payload.data.partner_name,
                partner_institution: payload.data.partner_institution,
                partner_address: payload.data.partner_address,
                partner_email: payload.data.partner_email,
                partner_country: payload.data.partner_country,
                year_contribution_1: payload.data.year_contribution_1,
                year_contribution_2: payload.data.year_contribution_2,
                year_contribution_3: payload.data.year_contribution_3,
            });
            setFileDoc({ name: payload.data.document })
        } else {
            reset();
        }
    }, [reset, id]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiInnovation_PartnerInsertUpdate(data))
            if (payload.status === 200) {
                await dispatch(apiDocument_Upload({
                    document_file: data.document_file,
                    package_no: data.partner_code,
                    document_for: 'usulan-mitra',
                    document_name: 'Surat Pernyataan Dukungan Mitra',
                    status: 'Mitra-Pendukung',
                    description: '-',
                    institution: auth.institution,
                    user_upload: auth.username,
                })); //? insert dokument
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan Mitra Pendukung.')
                dispatch(apiInnovation_PartnerList({ package_no }));
                reset();
                setFileDoc('');
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <Modal id="modalMitraPendukung" modal_size="modal-lg">
            <ModalHeader title="Informasi Mitra Pendukung" />
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form" encType="multipart/form-data">
                <ModalBody>
                    <h3 className="font-weight-bolder mb-2">1. Mitra Pendukung</h3>
                    <input type="hidden" {...register("id", { required: false })} />
                    <input type="hidden" {...register("package_no", { required: true })} defaultValue={package_no} />
                    <input type="hidden" {...register("partner_code", { required: true })} defaultValue={RandomNumber(10)} />
                    <input type="hidden" {...register("institution", { required: true })} defaultValue={store_data.institution} />
                    <input type="hidden" {...register("year", { required: true })} defaultValue={store_data.year} />

                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Nama Mitra :</label>
                                <input type="text" {...register("partner_name", { required: true })} className="form-control" placeholder="Nama Mitra" />
                                {errors.partner_name && <span className="form-text text-danger">* Nama Mitra</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Nama Institusi :</label>
                                <input type="text" {...register("partner_institution", { required: true })} className="form-control" placeholder="Nama Institusi" />
                                {errors.partner_institution && <span className="form-text text-danger">* Institusi</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Alamat Mitra :</label>
                                <textarea {...register("partner_address", { required: true })} className="form-control" rows="2" placeholder="Alamat Mitra"></textarea>
                                {errors.partner_address && <span className="form-text text-danger">* Alamat Mitra</span>}
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Alamat Surel :</label>
                                <input type="email" {...register("partner_email", { required: true })} className="form-control" placeholder="Email" />
                                {errors.partner_email && <span className="form-text text-danger">* Email</span>}
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Negara :</label>
                                <input type="text" {...register("partner_country", { required: true })} className="form-control" placeholder="Negara" />
                                {errors.partner_country && <span className="form-text text-danger">* Negara</span>}
                            </div>
                        </div>
                    </div>

                    <h3 className="font-weight-bolder mb-2">2. Kontribusi</h3>
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="form-group">
                                <label className="font-weight-bolder">Kontribusi Tahun 1 :</label>
                                <textarea {...register("year_contribution_1", { required: false })} className="form-control" rows="2" placeholder="Tahun 1"></textarea>
                                {errors.year_contribution_1 && <span className="form-text text-danger">* Kontribusi Tahun 1</span>}
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="form-group">
                                <label className="font-weight-bolder">Kontribusi Tahun 2 :</label>
                                <textarea {...register("year_contribution_2", { required: false })} className="form-control" rows="2" placeholder="Tahun 2"></textarea>
                                {errors.year_contribution_2 && <span className="form-text text-danger">* Kontribusi Tahun 2</span>}
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="form-group">
                                <label className="font-weight-bolder">Kontribusi Tahun 3 :</label>
                                <textarea {...register("year_contribution_3", { required: false })} className="form-control" rows="2" placeholder="Tahun 3"></textarea>
                                {errors.year_contribution_3 && <span className="form-text text-danger">* Kontribusi Tahun 3</span>}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label className="font-weight-bolder">Unggah Surat Pernyataan Dukungan Mitra :</label>
                                <div className={`custom-file ${file_doc && 'hide'}`}>
                                    <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                    <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                                </div>
                                {
                                    file_doc &&
                                    <div>
                                        <a href={urlAttachment + '/usulan-mitra/' + file_doc.name} className="form-control font-weight-bold" target="_blank"><Icons iconName="attachment" className="svg-icon svg-icon-primary svg-icon-md" /> {file_doc.name}</a>
                                        <button type="button" onClick={() => setFileDoc('')} className="btn btn-light-danger btn-sm mt-2">Unggah Ulang</button>
                                    </div>
                                }
                                {errors.document_file && <span className="form-text text-danger">* Silahkan unggah Surat Pernyataan Dukungan.</span>}
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <ButtonSubmit />
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default ModalMitraPendukung