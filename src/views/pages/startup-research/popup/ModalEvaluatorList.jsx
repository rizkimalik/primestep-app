import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Lookup, Pager, Paging } from 'devextreme-react/data-grid';

import { IconMark } from 'views/components/icon';
import { apiEvaluator_List } from 'app/services/apiEvaluator';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'views/components/modal'
import { apiMasterCategoryResearch } from 'app/services/apiMasterData';
import {
    apiStartupResearch_AssignEvaluator,
    apiStartupResearch_StartupList,
    apiStartupResearch_ResearchList,
} from 'app/services/apiStartupResearch';
import { authUser } from 'app/slice/sliceAuth';

function ModalEvaluatorList({ modul, proposal_data }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { evaluator_list } = useSelector(state => state.evaluator)
    const { category_focus } = useSelector(state => state.master)
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        dispatch(apiMasterCategoryResearch());
        dispatch(apiEvaluator_List());
    }, [dispatch])

    function formatWhatsappNumber(phoneNumber) {
        // Cek apakah nomor telepon diawali dengan 0
        if (phoneNumber.charAt(0) === '0') {
            // Ganti 0 dengan 62
            return phoneNumber.replace('0', '62');
        } else {
            // Kembalikan nomor telepon tanpa perubahan
            return phoneNumber;
        }
    }

    const componentButtonActions = (data) => {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button
                    type="button"
                    className="btn btn-sm btn-light-primary py-1 px-2"
                    onClick={() => {
                        dispatch(apiStartupResearch_AssignEvaluator({ package_no: proposal_data.package_no, evaluator: row.username }));
                        if (modul === 'startup') {
                            dispatch(apiStartupResearch_StartupList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                        }
                        else {
                            dispatch(apiStartupResearch_ResearchList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                        }
                    }}
                    data-dismiss="modal"
                >
                    <IconMark className="svg-icon svg-icon-sm p-0" /> Pilih Evaluator
                </button>
            </div>
        )
    }

    return (
        <Modal id="modalEvaluatorList" modal_size="modal-lg">
            <ModalHeader title="Daftar Evaluator" />
            <ModalBody className="p-4">
                <DataGrid
                    dataSource={evaluator_list}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    columnMinWidth={100}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={5} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[5, 10]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Pilih" dataField="id" cellRender={componentButtonActions} />
                    <Column caption="Nama Evaluator" dataField="name" />
                    <Column caption="Insititusi" dataField="institution_name" />
                    <Column caption="Bidang Fokus" dataField="focus_area">
                        <Lookup
                            dataSource={category_focus}
                            valueExpr="id"
                            displayExpr="focus_name"
                        />
                    </Column>
                    <Column caption="Whatsapp" dataField="whatsapp" cellRender={({ data }) => {
                        return <a href={`https://wa.me/${formatWhatsappNumber(data.email_address)}`} target="_blank" className="text-success"><i className="fab fa-whatsapp fa-sm text-success" /> {formatWhatsappNumber(data.email_address)}</a>
                    }} />
                    <Column caption="Telp" dataField="email_address" />
                </DataGrid>
            </ModalBody>
            <ModalFooter />
        </Modal>
    )
}

export default ModalEvaluatorList