import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, } from 'devextreme-react/data-grid';
import { useForm } from 'react-hook-form';

import ModalLuaran from '../popup/ModalLuaran';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { ButtonSubmit } from 'views/components/button';
import {
    apiInnovation_OuterList,
    apiInnovation_OuterDelete,
    apiUpdateStatus_TahapUsulan,
    apiInnovation_OutertUpdatePublication,
    apiInnovation_OutertDetailPublication,
} from 'app/services/apiStartupResearch';


function StepFormLuaran({ data_detail, package_no, setNavigate }) {
    const dispatch = useDispatch();
    const [outer_id, setOuterID] = useState('');
    const [outer_publication, setOuterPublication] = useState('');
    const { innovation_outers } = useSelector(state => state.startup_research);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const outer_publikasi = [
        { value: 'Publikasi pada jurnal ber-reputasi internasional' },
        { value: 'Book Chapter dalam buku terindeks scopus' },
        // { value: 'Buku hasil penelitian ber-ISBN' },
    ];

    useEffect(async () => {
        let mounted = true;
        if (mounted) {
            await dispatch(apiInnovation_OuterList({ package_no }));
            const { payload } = await dispatch(apiInnovation_OutertDetailPublication({ package_no }));
            setOuterPublication(payload?.data?.outer_publication)
        }
        return () => (mounted = false);
    }, [dispatch, package_no]);

    useEffect(() => {
        reset({ outer_publication })
    }, [reset, outer_publication]);

    function handlerDeleteData(row) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiInnovation_OuterDelete({ id: row.id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Berhasil Hapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiInnovation_OuterList({ package_no }));
                }
            }
        });
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(row)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <button onClick={() => setOuterID(row.id)} data-toggle="modal" data-target="#modalLuaran" className="btn btn-sm btn-clean btn-icon btn-hover-warning" title="Edit Data">
                    <i className="fas fa-edit fa-sm"></i>
                </button>
            </div>
        )
    }

    const onSubmitUpdatePublikasi = async (data) => {
        try {
            const { payload } = await dispatch(apiInnovation_OutertUpdatePublication(data))
            if (payload.status === 200) {
                dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Luaran' }));
                SwalAlertSuccess('Berhasil.', 'Berhasil simpan Luaran wajib publikasi.')
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitNextBtn = () => {
        if (package_no && innovation_outers.length > 0) {
            setNavigate('3');
            dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-RAB' }));
        } else {
            SwalAlertError('Gagal.', '*Wajib isi Luaran Tahunan dan Publikasi.');
        }
    }

    return (
        <div>
            <ModalLuaran id={outer_id} package_no={package_no} data_detail={data_detail} />

            <div className="row">
                <div className="col-lg-12">
                    <div className="d-flex justify-content-between mb-4">
                        <h3 className="text-dark font-weight-bold">2.1 Luaran Wajib Tahunan</h3>
                        <button type="button" onClick={() => setOuterID('')} data-toggle="modal" data-target="#modalLuaran" className="btn btn-primary font-weight-bold btn-sm">
                            <i className="fa fa-plus fa-sm" />
                            Tambah Luaran
                        </button>
                    </div>

                    <DataGrid
                        dataSource={innovation_outers}
                        remoteOperations={true}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                        wordWrapEnabled={true}
                        columnMinWidth={80}
                    >
                        <HeaderFilter visible={false} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            displayMode='full'
                            allowedPageSizes={[10, 20, 50]}
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Aksi" dataField="id" cellRender={componentButtonActions} />
                        <Column caption="Tahun" dataField="outer_year" />
                        <Column caption="TKT" dataField="tkt_value" />
                        <Column caption="Kategori Luaran" dataField="outer_category" />
                        {/* <Column caption="Luaran" dataField="outer_name" /> */}
                        {/* <Column caption="Keterangan (Opsional)" dataField="description" /> */}
                    </DataGrid>
                </div>
            </div>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <form onSubmit={handleSubmit(onSubmitUpdatePublikasi)} className="form" id="form-luaran-publikasi">
                <input type="hidden" {...register("package_no", { required: true })} defaultValue={package_no} />
                <div className="row">
                    <div className="col-lg-12">
                        <div className="d-flex justify-content-between mb-4">
                            <h3 className="text-dark font-weight-bold">2.2 Luaran Publikasi</h3>
                        </div>
                        <div className="form-group row">
                            <div className="col-lg-4">
                                <div className="form-group">
                                    <label>Luaran Publikasi :</label>
                                    <select
                                        {...register("outer_publication", { required: true })}
                                        className="form-control form-control-md"
                                    >
                                        <option value="">-- Pilih --</option>
                                        {
                                            outer_publikasi.map((item, index) => {
                                                return <option value={item.value} key={index}>{index + 1}. {item.value}</option>
                                            })
                                        }
                                    </select>
                                    {errors.outer_publication && <span className="form-text text-danger">* Pilih Luaran Publikasi</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <div className="d-flex justify-content-between">
                    <ButtonSubmit className="btn btn-success font-weight-bold btn-lg" />
                    <div className="d-flex justify-content-end">
                        <button type="button" onClick={() => setNavigate('1')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                        <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                    </div>
                </div>
            </form>

        </div>
    )
}

export default StepFormLuaran