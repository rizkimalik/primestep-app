import React, { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';
import { useDispatch } from 'react-redux';

import SyaratLegalitas from '../information/SyaratLegalitas';
import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiUpdateStatus_TahapUsulan,
    apiOuterIncubation_InsertUpdate,
    apiOuterIncubation_Detail,
} from 'app/services/apiStartupResearch';
import { FormInput } from 'views/components/form';
import SyaratSertifikat from '../information/SyaratSertifikat';


function StepFormTargetPendapatan({ package_no, setNavigate, setLoadData }) {
    const dispatch = useDispatch();
    const [target_luaran, setTargetLuaran] = useState('');
    const { register, formState: { errors }, handleSubmit, control, reset } = useForm();

    useEffect(async () => {
        if (!package_no) return;
        const { payload } = await dispatch(apiOuterIncubation_Detail({ package_no }));
        if (!payload) return;
        setTargetLuaran(payload.data);
        reset({
            jumlah_produksi: payload?.data?.jumlah_produksi,
            jumlah_penjualan: payload?.data?.jumlah_penjualan,
            jumlah_pendapatan: payload?.data?.jumlah_pendapatan,
            jumlah_profit: payload?.data?.jumlah_profit,
            legalitas_izin_usaha: payload?.data?.legalitas_izin_usaha,
            sertifikasi_izin_produk: payload?.data?.sertifikasi_izin_produk,
            jumlah_tenaga_kerja: payload?.data?.jumlah_tenaga_kerja,
            bisnis_model: payload?.data?.bisnis_model,
            mitra_industri: payload?.data?.mitra_industri,
            supply_chain: payload?.data?.supply_chain,
            target_pasar: payload?.data?.target_pasar,
            pangsa_pasar: payload?.data?.pangsa_pasar,
            harga_produksi_jual: payload?.data?.harga_produksi_jual,
        });
    }, [dispatch, package_no]);


    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiOuterIncubation_InsertUpdate(data))
            if (payload.status === 200) {
                await dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Target' }));
                SwalAlertSuccess('Berhasil', 'Berhasil simpan data.')
                setLoadData(i => i + 1);

                const { payload } = await dispatch(apiOuterIncubation_Detail({ package_no }));
                if (!payload) return;
                setTargetLuaran(payload.data);
            } else {
                SwalAlertError('Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitNextBtn = () => {
        if (package_no && target_luaran) {
            setNavigate('3');
            dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-RAB' }));
        }
        else {
            SwalAlertError('Gagal.', '*Wajib isi Target Pendapatan.');
        }
    }


    return (
        <div>
            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form mb-4" id="form-proposal">
                <div className="d-flex justify-content-between mb-4">
                    <h3 className="text-dark font-weight-bold">2.1 Target Luaran Wajib</h3>
                </div>
                <input type="hidden" {...register('package_no')} value={package_no} />

                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="font-weight-bolder">1. Jumlah produksi selama 1 tahun :</label>
                            <Controller
                                control={control}
                                name="jumlah_produksi"
                                rules={{ required: true }}
                                render={({ field: { ref, ...rest } }) => (
                                    <NumericFormat
                                        id="jumlah_produksi"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        {...rest}
                                    />
                                )}
                            />
                            {errors.jumlah_produksi && <span className="form-text text-danger">* 1. Jumlah produksi selama 1 tahun</span>}
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">2. Jumlah penjualan selama 1 tahun :</label>
                            <Controller
                                control={control}
                                name="jumlah_penjualan"
                                rules={{ required: true }}
                                render={({ field: { ref, ...rest } }) => (
                                    <NumericFormat
                                        id="jumlah_penjualan"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        {...rest}
                                    />
                                )}
                            />
                            {errors.jumlah_penjualan && <span className="form-text text-danger">* 2. Jumlah penjualan selama 1 tahun</span>}
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">3. Jumlah pendapatan/omset selama 1 tahun :</label>
                            <Controller
                                control={control}
                                name="jumlah_pendapatan"
                                rules={{ required: true }}
                                render={({ field: { ref, ...rest } }) => (
                                    <NumericFormat
                                        id="jumlah_pendapatan"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        {...rest}
                                    />
                                )}
                            />
                            {errors.jumlah_pendapatan && <span className="form-text text-danger">* 3. Jumlah pendapatan</span>}
                            <span className="form-text text-muted">(hanya pendapatan dari produk yang diajukan, bujan pendapatan keseluruhan perusahaan)</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">4. Jumlah profit selama 1 tahun :</label>
                            <Controller
                                control={control}
                                name="jumlah_profit"
                                rules={{ required: true }}
                                render={({ field: { ref, ...rest } }) => (
                                    <NumericFormat
                                        id="jumlah_profit"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        {...rest}
                                    />
                                )}
                            />
                            {errors.jumlah_profit && <span className="form-text text-danger">* 4. Jumlah profit</span>}
                            <span className="form-text text-muted">(hanya pendapatan dari produk yang diajukan, bujan pendapatan keseluruhan perusahaan)</span>
                        </div>

                        <div className="form-group">
                            <label>
                                <span className="font-weight-bolder">5. Legalitas badan usaha dan perizin usaha :</span>
                                <SyaratLegalitas />
                            </label>
                            <textarea
                                {...register("legalitas_izin_usaha", { required: true })}
                                className="form-control"
                                rows={1}
                                placeholder="Legalitas"
                            />
                            {errors.legalitas_izin_usaha && <span className="form-text text-danger">* 5. Legalitas badan usaha dan perizin usaha</span>}
                        </div>

                        <div className="form-group">
                            <label>
                                <span className="font-weight-bolder">6. Sertifikasi dan izin produk :</span>
                                <SyaratSertifikat />
                            </label>
                            <textarea
                                {...register("sertifikasi_izin_produk", { required: true })}
                                className="form-control"
                                rows={1}
                                placeholder="Sertifikasi"
                            />
                            {errors.sertifikasi_izin_produk && <span className="form-text text-danger">* 6. Sertifikasi dan izin produk</span>}
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="font-weight-bolder">7. Jumlah tenaga kerja :</label>
                            <Controller
                                control={control}
                                name="jumlah_tenaga_kerja"
                                rules={{ required: true }}
                                render={({ field: { ref, ...rest } }) => (
                                    <NumericFormat
                                        id="jumlah_tenaga_kerja"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        {...rest}
                                    />
                                )}
                            />
                            {errors.jumlah_tenaga_kerja && <span className="form-text text-danger">* 7. Jumlah tenaga kerja</span>}
                        </div>
                        <FormInput
                            name="bisnis_model"
                            type="text"
                            label="8. Business Model yang prospektif"
                            className="form-control"
                            placeholder="Business Model"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.bisnis_model}
                        />
                        <FormInput
                            name="mitra_industri"
                            type="text"
                            label="9. Mitra industri untuk kerjasama produksi"
                            className="form-control"
                            placeholder="Mitra industri"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.mitra_industri}
                        />
                        <FormInput
                            name="supply_chain"
                            type="text"
                            label="10. Matriks supply chain produksi usaha"
                            className="form-control"
                            placeholder="Supply chain"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.supply_chain}
                        />
                        <FormInput
                            name="target_pasar"
                            type="text"
                            label="11. Memiliki target pasar global/internasional"
                            className="form-control"
                            placeholder="Target pasar"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.target_pasar}
                        />
                        <FormInput
                            name="pangsa_pasar"
                            type="text"
                            label="12. Memiliki pangsa pasar dengan potensi pertumbuhan yang besar"
                            className="form-control"
                            placeholder="Pangsa pasar"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.pangsa_pasar}
                        />
                        <FormInput
                            name="harga_produksi_jual"
                            type="text"
                            label="13. Penetapan biaya produksi dan harga jual kompetitif"
                            className="form-control"
                            placeholder="Biaya produksi dan harga jual"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.harga_produksi_jual}
                        />
                    </div>
                </div>


                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <div className="d-flex justify-content-between">
                    <ButtonSubmit className="btn btn-success font-weight-bold btn-lg" />
                    <div className="d-flex justify-content-end">
                        <button type="button" onClick={() => setNavigate('1')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                        <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default StepFormTargetPendapatan