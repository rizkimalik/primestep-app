import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { apiRAB_Detail } from 'app/services/apiRAB';
import { SwalAlertError } from 'views/components/SwalAlert';
import {
    apiStartupResearch_Detail,
    apiUpdateStatus_TahapUsulan,
    apiInnovation_OutertDetailPublication,
} from 'app/services/apiStartupResearch';

function StepFormKonfiramasi({ modul, package_no, setNavigate, startup }) {
    const dispatch = useDispatch();
    const history = useHistory();
    const [luaran, setLuaran] = useState('');
    const [rab_detail, setDetailRAB] = useState('');
    const [data_detail, setDataDetail] = useState('');
    const { innovation_outers } = useSelector(state => state.startup_research);

    useEffect(async () => {
        const { payload } = await dispatch(apiStartupResearch_Detail({ package_no }));
        if (payload.status !== 200) return;
        setDataDetail(payload.data);
    }, [dispatch, package_no]);

    useEffect(async () => {
        const { payload } = await dispatch(apiRAB_Detail({ package_no, modul }));
        setDetailRAB(payload.data);
    }, [dispatch, package_no]);

    useEffect(async () => {
        const { payload } = await dispatch(apiInnovation_OutertDetailPublication({ package_no }));
        setLuaran(payload.data);
    }, [dispatch, package_no]);

    const onSubmitNextBtn = () => {
        if (package_no) {
            dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Konfirmasi' }));
            if (startup === 'acceleration') {
                history.push(`/startup-acceleration`);
            }
            else {
                history.push(`/${modul}`);
            }
        }
        else {
            SwalAlertError('Gagal.', '*Usulan tidak lengkap.');
        }
    }

    return (
        <div>
            <div className="mb-0">
                <div className="d-flex justify-content-between mb-2">
                    <h3>5.1 Informasi Usulan Proposal</h3>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="font-weight-bolder">Judul {startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} :</label>
                            <p className="border-0 pl-0 text-primary font-weight-bolder">{data_detail.proposal_name}</p>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">Kategori {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                            <span className="form-control border-0 pl-0">{data_detail.category_framework_name}</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">Bidang Fokus {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                            <span className="form-control border-0 pl-0">{data_detail.category_focus_name}</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">Anggaran Usulan :</label>
                            <span className="form-control border-0 pl-0">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(data_detail.value_idr))}</span>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="font-weight-bolder">Nama {modul === 'startup' ? 'Perintis' : 'Peneliti'} :</label>
                            <span className="form-control border-0 pl-0">{data_detail.leader_name}</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">TKT :</label>
                            <span className="form-control border-0 pl-0">{data_detail.tkt_value}</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">Tahun Usulan :</label>
                            <span className="form-control border-0 pl-0">{data_detail.proposed_year}</span>
                        </div>
                        <div className="form-group">
                            <label className="font-weight-bolder">Durasi {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                            <span className="form-control border-0 pl-0">{data_detail.tkt_duration} Tahun</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <div className="row">
                <div className="col-lg-6">
                    <div className="d-flex justify-content-between mb-2">
                        <h3>5.2 Informasi {modul === 'startup' ? 'Target Pendapatan' : 'Luaran'}</h3>
                    </div>
                    {
                        modul === 'startup' &&
                        <div className="form-group">
                            <label className="font-weight-bolder">Target Pendapatan :</label>
                            <span className="form-control border-0 pl-0">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(data_detail.target_income)}</span>
                        </div>
                    }
                    {
                        modul === 'research' &&
                        <div>
                            <div className="form-group mb-0">
                                <label className="font-weight-bolder">Luaran Wajib Tahunan :</label>
                                <span className="form-control border-0 pl-0">{innovation_outers.length}</span>
                            </div>
                            <div className="form-group">
                                <label className="font-weight-bolder">Luaran Wajib Publikasi :</label>
                                <span className="form-control border-0 pl-0">{luaran.outer_publication}</span>
                            </div>
                        </div>
                    }
                </div>
                <div className="col-lg-6">
                    <div className="d-flex justify-content-between mb-2">
                        <h3>5.3 Informasi RAB</h3>
                    </div>
                    <div className="form-group">
                        <label className="font-weight-bolder">Total Anggaran :</label>
                        <span className="form-control border-0 pl-0">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(rab_detail.total_expenditure)}</span>
                    </div>
                </div>
            </div>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Konfirmasi</button>
            </div>
        </div>
    )
}

export default StepFormKonfiramasi