import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, } from 'devextreme-react/data-grid';

import ModalMitraPendukung from '../popup/ModalMitraPendukung';
import {
    apiInnovation_PartnerList,
    apiInnovation_PartnerDelete,
    apiUpdateStatus_TahapUsulan,
} from 'app/services/apiStartupResearch';
import { SwalAlertError } from 'views/components/SwalAlert';

function StepFormMitraPendukung({ modul, data_detail, package_no, setNavigate }) {
    const dispatch = useDispatch();
    const [partner_id, setPartnerID] = useState('');
    const { innovation_partners } = useSelector(state => state.startup_research);

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            dispatch(apiInnovation_PartnerList({ package_no }));
        }
        return () => (mounted = false);
    }, [dispatch, package_no]);


    function handlerDeleteData(row) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiInnovation_PartnerDelete({ id: row.id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Berhasil Hapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiInnovation_PartnerList({ package_no }));
                }
            }
        });
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(row)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <button onClick={() => setPartnerID(row.id)} data-toggle="modal" data-target="#modalMitraPendukung" className="btn btn-sm btn-clean btn-icon btn-hover-warning" title="Edit Data">
                    <i className="fas fa-edit fa-sm"></i>
                </button>
            </div>
        )
    }

    const onSubmitNextBtn = () => {
        if (modul === 'research' || modul === 'startup') {
            if (package_no && (modul === 'research' ? innovation_partners.length > 0 : true)) {
                setNavigate('5');
                dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Konfirmasi' }));
            } else {
                SwalAlertError('Gagal.', '*Wajib isi Dokumen Pendukung, beserta informasi mitra pendukung.');
            }
        } else {}
    }

    return (
        <div>
            <ModalMitraPendukung id={partner_id} package_no={package_no} data_detail={data_detail} />
            <div className="row">
                <div className="col-lg-12">
                    <div className="d-flex justify-content-between mb-2">
                        <h3 className="text-dark font-weight-bold">4.1 Dokumen Pendukung</h3>
                        <button type="button" onClick={() => setPartnerID('')} data-toggle="modal" data-target="#modalMitraPendukung" className="btn btn-primary font-weight-bold btn-sm">
                            <i className="fa fa-plus fa-sm" /> Tambah Mitra
                        </button>
                    </div>
                    <p>Dokumen Pendukung dilengkapi dengan informasi mengenai mitra pendukung.</p>

                    <DataGrid
                        dataSource={innovation_partners}
                        remoteOperations={true}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                        wordWrapEnabled={true}
                        columnMinWidth={80}
                    >
                        <HeaderFilter visible={false} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            displayMode='full'
                            allowedPageSizes={[10, 20, 50]}
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Aksi" dataField="id" width={80} cellRender={componentButtonActions} />
                        <Column caption="Nama Mitra" dataField="partner_name" />
                        <Column caption="Institusi" dataField="partner_institution" />
                        <Column caption="Alamat Mitra" dataField="partner_address" />
                    </DataGrid>
                </div>
            </div>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('3')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div>
        </div>
    )
}

export default StepFormMitraPendukung