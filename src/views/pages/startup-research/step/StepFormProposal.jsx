import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';

import MemberTeam from './MemberTeam';
import FormUsulanResearch from './FormUsulanResearch';
import FormUsulanStartup from './FormUsulanStartup';
import ModalCalculationTKT from '../popup/ModalCalculationTKT';
import ModalMemberTeam from '../popup/ModalMemberTeam';
import ModalUsulanLanjutan from '../popup/ModalUsulanLanjutan';
import FormKetuaResearch from './FormKetuaResearch';
import FormKetuaStartup from './FormKetuaStartup';
import SyaratJabatanGelar from '../information/SyaratJabatanGelar';
import SyaratUsulan from '../information/SyaratUsulan';
import { authUser } from 'app/slice/sliceAuth';
import { FormInput, FormSelect } from 'views/components/form';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiStartupResearch_Insert, apiUpdateStatus_TahapUsulan } from 'app/services/apiStartupResearch';
import { apiMasterInstitution, apiMasterCategoryResearch, apiMaster_CategoryFramework } from 'app/services/apiMasterData';



function StepFormProposal({ modul, proposal_no, package_no, data_detail, setNavigate, setLoadData, startup }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [modal, setModal] = useState('');
    const [pekerjaan, setPekerjaan] = useState('');
    const [value_tkt, setValueTKT] = useState('');
    const [member_id, setMemberID] = useState('');
    // const [tkt_category, setTKTCategory] = useState('');
    // const [product_development, setProductDevelopment] = useState('');
    const [duration_year, setDurationYear] = useState(1);
    const [load_tkt, setLoadTKT] = useState(0);
    const { register, formState: { errors }, handleSubmit, control, reset } = useForm();
    const { store_data } = useSelector(state => state.auth);
    const { category_focus, category_framework } = useSelector(state => state.master)

    useEffect(() => {
        dispatch(apiMasterInstitution());
        dispatch(apiMasterCategoryResearch());
    }, [dispatch]);

    useEffect(() => {
        //? load default framework sesuai stp
        dispatch(apiMaster_CategoryFramework({ institution: store_data.institution }));
    }, [dispatch, store_data]);

    useEffect(() => {
        //? load set data existing
        if (startup) {
            setTimeout(() => {
                if (!data_detail) return;
                reset({
                    // package_no: data_detail?.package_no,
                    // proposal_no: data_detail?.proposal_no,
                    piu: data_detail?.piu,
                    proposal_name: data_detail?.proposal_name,
                    category_focus: data_detail?.category_focus,
                    category_framework: data_detail?.category_framework,
                    proposed_year: data_detail?.proposed_year,
                    value_idr: data_detail?.value_idr,
                    type: data_detail?.type,
                    type_corporation: data_detail?.type_corporation,
                    base_income_lastyear: data_detail?.base_income_lastyear,
                    // product_development: data_detail?.product_development,
                    product_featured: data_detail?.product_featured,
                    product_description: data_detail?.product_description,
                    tkt_value: data_detail?.tkt_value,
                    tkt_duration: data_detail?.tkt_duration,
                    tkt_category: data_detail?.tkt_category,
                    jenis_usulan: 1,
                    //? data leader
                    leader_name: data_detail?.leader?.leader_name,
                    title_position: data_detail?.leader?.title_position,
                    identity_id: data_detail?.leader?.identity_id,
                    gender: data_detail?.leader?.gender,
                    email: data_detail?.leader?.email,
                    phone_number: data_detail?.leader?.phone_number,
                    graduation: data_detail?.leader?.graduation,
                    address: data_detail?.leader?.address,
                });
            }, 1000);
        }
        else {
            setTimeout(() => {
                if (!data_detail) return;
                reset({
                    piu: data_detail?.piu,
                    package_no: data_detail?.package_no,
                    proposal_no: data_detail?.proposal_no,
                    proposal_name: data_detail?.proposal_name,
                    category_focus: data_detail?.category_focus,
                    category_framework: data_detail?.category_framework,
                    proposed_year: data_detail?.proposed_year,
                    value_idr: data_detail?.value_idr,
                    type: data_detail?.type,
                    type_corporation: data_detail?.type_corporation,
                    base_income_lastyear: data_detail?.base_income_lastyear,
                    // product_development: data_detail?.product_development,
                    product_featured: data_detail?.product_featured,
                    product_description: data_detail?.product_description,
                    tkt_value: data_detail?.tkt_value,
                    tkt_duration: data_detail?.tkt_duration,
                    tkt_category: data_detail?.tkt_category,
                    jenis_usulan: data_detail?.jenis_usulan ? 1 : 0,
                    //? data leader
                    leader_name: data_detail?.leader?.leader_name,
                    title_position: data_detail?.leader?.title_position,
                    identity_id: data_detail?.leader?.identity_id,
                    gender: data_detail?.leader?.gender,
                    email: data_detail?.leader?.email,
                    phone_number: data_detail?.leader?.phone_number,
                    graduation: data_detail?.leader?.graduation,
                    address: data_detail?.leader?.address,
                });
            }, 1000);
        }

    }, [reset, data_detail, startup]);

    useEffect(() => {
        if (!data_detail) return;
        dispatch(apiMaster_CategoryFramework({ institution: data_detail.piu })); //? set framework
    }, [dispatch, data_detail]);

    useEffect(() => {
        const value_idr = document.getElementById('value_idr');
        const base_income_lastyear = document.getElementById('base_income_lastyear');
        reset({
            tkt_value: value_tkt,  //? set tkt setelah isi form popup calculasi
            value_idr: value_idr?.value, //? set ulang agar tdk error
            base_income_lastyear: base_income_lastyear?.value, //? set ulang agar tdk error
        });
    }, [reset, value_tkt]);

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiStartupResearch_Insert(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Berhasil', 'data berhasil disimpan.')
                setLoadData(i => i + 1);  // reset data paket
            }
            else if (payload.status === 201) {
                SwalAlertError('Peneliti Terdaftar', 'Nama Peneliti atau NIDN sudah terdaftar, silahkan pilih usulan lanjutan.')
            }
            else {
                SwalAlertError('Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitNextBtn = () => {
        if (package_no && data_detail) {
            setNavigate('2');
            if (modul === 'startup') {
                dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Target' }));
            } else {
                dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Luaran' }));
            }
        }
        else {
            SwalAlertError('Gagal.', '*Wajib isi form usulan, informasi ketua dan anggota tim.');
        }
    }


    return (
        <div>
            <ModalMemberTeam id={member_id} package_no={package_no} />
            {
                modul === 'research' &&
                modal === 'usulan-lanjutan' &&
                <ModalUsulanLanjutan reset={reset} institution={store_data.institution} />
            }
            {
                modal === 'calculate-tkt' &&
                <ModalCalculationTKT
                    load_tkt={load_tkt}
                    setLoadTKT={setLoadTKT}
                    modul={modul}
                    package_no={package_no}
                    value_tkt={value_tkt}
                    duration_year={duration_year}
                    data_detail={data_detail}
                    institution={store_data.institution}
                    setValueTKT={setValueTKT}
                    reset={reset}
                    // setTKTCategory={setTKTCategory}
                    // setProductDevelopment={setProductDevelopment}
                />
            }


            <form onSubmit={handleSubmit(onSubmitInsertData)} className="form mb-4" id="form-proposal">
                <input type="text" {...register("jenis_usulan", { required: false })} defaultValue="0" className="hide" />
                <input type="hidden" {...register("package_no", { required: true })} defaultValue={package_no} />
                <input type="hidden" {...register("proposal_no", { required: true })} defaultValue={proposal_no} />
                <input type="hidden" {...register("type", { required: true })} defaultValue={modul === 'startup' ? 'S' : 'R'} />
                <input type="hidden" {...register("username", { required: true })} defaultValue={auth.username} />
                <input type="hidden" {...register("tkt_category", { required: true })} /> {/* setdata > popup TKT */}

                <div className="d-flex justify-content-between mb-4">
                    <h3 className="text-dark font-weight-bold">1.1 Informasi Usulan {startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'}</h3>
                </div>
                {
                    modul === 'research' &&
                    <div className="row mb-4">
                        <div className="col-lg-6">
                            <button type="button" onClick={() => setModal('usulan-lanjutan')} className="btn btn-light-success btn-sm font-weight-bolder" data-toggle="modal" data-target="#modalUsulanLanjutan">
                                Usulan Lanjutan
                            </button>
                        </div>
                    </div>
                }

                {
                    modul === "research"
                        ? <FormUsulanResearch
                            modul={modul}
                            FormInput={FormInput}
                            FormSelect={FormSelect}
                            Controller={Controller}
                            control={control}
                            NumericFormat={NumericFormat}
                            register={register}
                            errors={errors}
                            store_data={store_data}
                            category_framework={category_framework}
                            category_focus={category_focus}
                            setDurationYear={setDurationYear}
                            setLoadTKT={setLoadTKT}
                            setModal={setModal}
                            setValueTKT={setLoadTKT}
                        />
                        : <FormUsulanStartup
                            modul={modul}
                            FormInput={FormInput}
                            FormSelect={FormSelect}
                            Controller={Controller}
                            control={control}
                            NumericFormat={NumericFormat}
                            register={register}
                            errors={errors}
                            store_data={store_data}
                            category_framework={category_framework}
                            category_focus={category_focus}
                            setDurationYear={setDurationYear}
                            setLoadTKT={setLoadTKT}
                            setModal={setModal}
                            setValueTKT={setLoadTKT}
                        />
                }

                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <h3 className="text-dark font-weight-bold my-4">1.2 Informasi Ketua {modul === 'startup' ? 'Startup' : 'Peneliti'}</h3>
                {modul === 'research' && <SyaratUsulan />}
                {
                    modul === "research"
                        ? <FormKetuaResearch
                            modul={modul}
                            FormInput={FormInput}
                            FormSelect={FormSelect}
                            SyaratJabatanGelar={SyaratJabatanGelar}
                            register={register}
                            errors={errors}
                            setPekerjaan={setPekerjaan}
                            pekerjaan={pekerjaan}
                        />
                        : <FormKetuaStartup
                            modul={modul}
                            FormInput={FormInput}
                            FormSelect={FormSelect}
                            SyaratJabatanGelar={SyaratJabatanGelar}
                            register={register}
                            errors={errors}
                            setPekerjaan={setPekerjaan}
                            pekerjaan={pekerjaan}
                        />
                }


                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <MemberTeam package_no={package_no} modul={modul} setMemberID={setMemberID} />

                <div className="justify-content-start">
                    {errors.tkt_category && <span className="form-text text-danger">*Kategori TKT kosong, silahkan pilih kategori pada saat ukur TKT.</span>}
                    {/* {errors.product_development && <span className="form-text text-danger">*{modul === 'startup' ? 'Produk Unggulan' : 'Penelitian yang dikembangkan'} kosong, silahkan isi pada saat ukur TKT.</span>} */}
                </div>

                <div className="separator separator-dashed separator-border-2 my-4"></div>
                <div className="d-flex justify-content-between">
                    <ButtonSubmit className="btn btn-success font-weight-bold btn-lg" />
                    <div className="d-flex justify-content-end">
                        <ButtonCancel to={`/${modul}`} className="btn btn-light-danger font-weight-bold btn-lg" />
                        <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                    </div>
                </div>


            </form>
        </div>
    )
}

export default StepFormProposal
