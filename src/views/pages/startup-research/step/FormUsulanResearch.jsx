import React from 'react'

function FormUsulanResearch({ modul, FormInput, FormSelect, Controller, control, NumericFormat, register, errors, store_data, category_framework, category_focus, setDurationYear, setLoadTKT, setModal, setValueTKT }) {
    return (
        <div className="row">
            <div className="col-lg-6">
                <div className="form-group">
                    <label className="font-weight-bolder">1. Perguruan Tinggi :</label>
                    <input type="text" {...register("piu", { required: true, value: store_data.institution })} className="form-control" disabled={true} />
                    {errors.piu && <span className="form-text text-danger">* 1. Pilih Perguruan Tinggi.</span>}
                </div>
                <FormInput
                    type="textarea"
                    name="proposal_name"
                    label={`2. Judul ${modul === 'startup' ? 'Rintisan' : 'Penelitian'}`}
                    className="form-control"
                    placeholder={`Judul ${modul === 'startup' ? 'Rintisan' : 'Penelitian'}`}
                    register={register}
                    rules={{ required: true }}
                    readOnly={false}
                    rows={2}
                    errors={errors.proposal_name}
                />
                <FormSelect
                    name="category_framework"
                    label={`3. Kategori ${modul === 'startup' ? 'Rintisan' : 'Penelitian'}`}
                    className="form-control"
                    register={register}
                    rules={{ required: true }}
                    errors={errors.category_framework}
                    onChange={() => ''}
                >
                    <option value="">-- Pilih --</option>
                    {
                        category_framework?.map((item, index) => {
                            return <option value={item.id} key={index}>{index + 1}. {item.framework}</option>
                        })
                    }
                </FormSelect>
                <FormSelect
                    name="category_focus" //ganti > category_focus
                    label="4. Bidang Fokus"
                    className="form-control"
                    register={register}
                    rules={{ required: true }}
                    errors={errors.category_focus}
                    onChange={() => ''}
                >
                    <option value="">-- Pilih --</option>
                    {
                        category_focus.map((item, index) => {
                            return <option value={item.id} key={index}>{index + 1}. {item.focus_name}</option>
                        })
                    }
                </FormSelect>
            </div>
            <div className="col-lg-6">
                <FormSelect
                    name="proposed_year"
                    label="5. Tahun Usulan"
                    className="form-control"
                    register={register}
                    rules={{ required: true }}
                    errors={errors.proposed_year}
                    onChange={() => ''}
                >
                    <option value="">-- Pilih --</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                </FormSelect>
                <div className="form-group">
                    <label className="font-weight-bolder">6. Usulan Dana (Rp.):</label>
                    <Controller
                        control={control}
                        name="value_idr"
                        rules={{ required: true }}
                        render={({ field: { ref, ...rest } }) => (
                            <NumericFormat
                                {...rest}
                                getInputRef={ref}
                                id="value_idr"
                                className="form-control"
                                thousandSeparator=","
                                decimalSeparator="."
                                decimalScale={2}
                            />
                        )}
                    />
                    {errors.value_idr && <span className="form-text text-danger">* 6. Usulan Dana</span>}
                </div>
                <div className="form-group">
                    <label className="font-weight-bolder">7. Durasi Kegiatan :</label>
                    <div className="input-group">
                        <select
                            className="form-control"
                            {...register("tkt_duration", { required: true })}
                            onChange={(e) => setDurationYear(e.target.value)}
                        >
                            <option value="">-- Pilih --</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                        <div className="input-group-append"><span className="input-group-text">Tahun</span></div>
                    </div>
                    {errors.tkt_duration && <span className="form-text text-danger">* 7. Durasi Kegiatan</span>}
                </div>
                <div className="form-group">
                    <label className="font-weight-bolder">8. TKT saat ini:</label>
                    <div className="input-group">
                        <input type="text" onKeyUp={(e) => setValueTKT(e.target.value)} {...register("tkt_value", { required: true })} className="form-control" readOnly={true} />
                        <div className="input-group-append">
                            <button type="button" onClick={() => { setModal('calculate-tkt'); setLoadTKT(i => i + 1) }} data-toggle="modal" data-target="#modalCalculationTKT" className="btn btn-primary">Ukur</button>
                        </div>
                    </div>
                    {errors.tkt_value && <span className="form-text text-danger">* 8. TKT saat ini</span>}
                </div>
                <FormInput
                    name="product_featured"
                    type="text"
                    label="9. Penelitian yang dikembangkan"
                    className="form-control"
                    placeholder="Penelitian yang dikembangkan"
                    register={register}
                    rules={{ required: true }}
                    readOnly={false}
                    errors={errors.product_featured}
                />
            </div>
        </div>
    )
}

export default FormUsulanResearch