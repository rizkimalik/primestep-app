import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';

import { apiInnovationMember, apiInnovation_MemberDelete } from 'app/services/apiStartupResearch';

function MemberTeam({ modul, package_no, setMemberID }) {
    const dispatch = useDispatch();
    const { innovation_members } = useSelector(state => state.startup_research);

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            dispatch(apiInnovationMember({ package_no }));
        }
        return () => (mounted = false);
    }, [dispatch, package_no]);

    function handlerDeleteData(row) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiInnovation_MemberDelete({ id: row.id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Berhasil Hapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiInnovationMember({ package_no }));
                }
            }
        });
    }

    function componentButtonActions(data) {
        const row = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <button type="button" className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(row)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button>
                <button type="button" onClick={() => setMemberID(row.id)} data-toggle="modal" data-target="#modalTambahAnggota" className="btn btn-sm btn-clean btn-icon btn-hover-warning" title="Edit Data">
                    <i className="fas fa-edit fa-sm"></i>
                </button>
            </div>
        )
    }


    return (
        <div className="row mb-5">
            <div className="col-lg-12">
                <div className="d-flex justify-content-between mb-4">
                    <h3 className="text-dark font-weight-bold">1.3 Informasi Anggota {modul === 'startup' ? 'Startup' : 'Peneliti'}</h3>
                    <button type="button" onClick={() => setMemberID('')} data-toggle="modal" data-target="#modalTambahAnggota" className="btn btn-primary btn-sm">Tambah Anggota</button>
                </div>
                {/* <MemberTeamModal /> Modal ada di StepFormProposal */}

                <DataGrid
                    dataSource={innovation_members}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                    wordWrapEnabled={true}
                    columnMinWidth={100}
                >
                    <HeaderFilter visible={false} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        displayMode='full'
                        allowedPageSizes={[10, 20, 50]}
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Aksi" dataField="id" cellRender={componentButtonActions} />
                    <Column caption="Nama Anggota" dataField="member_name" />
                    <Column caption="Nomor Identitas" dataField="member_identity" />
                    <Column caption="Jenis Anggota" dataField="member_type" />
                    <Column caption="Peran Anggota" dataField="member_roles" />
                    <Column caption="Jenis Kelamin" dataField="member_gender" />
                </DataGrid>
            </div>
        </div>
    )
}

export default MemberTeam