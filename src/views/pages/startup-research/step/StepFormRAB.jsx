import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, } from 'devextreme-react/data-grid';

import Icons from 'views/components/Icons';
import { urlAttachment } from 'app/config';
import { apiUpdateStatus_TahapUsulan } from 'app/services/apiStartupResearch';
import { apiRAB_List, apiRAB_UploadExcel, apiRAB_Detail } from 'app/services/apiRAB';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function StepFormRAB({ modul, package_no, setNavigate }) {
    const dispatch = useDispatch();
    const [file_excel, setFileExcel] = useState('');
    const [loading, setLoading] = useState('');
    const [rab_detail, setDetailRAB] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { rab_list } = useSelector(state => state.rab);
    const { store_data } = useSelector(state => state.auth);


    useEffect(async () => {
        setFileExcel(''); //? reset file
        await dispatch(apiRAB_List({ package_no }));
        const { payload } = await dispatch(apiRAB_Detail({ package_no, modul }));
        setDetailRAB(payload.data);
        if (payload.data.rab_file) {
            setFileExcel({ name: payload.data.rab_file })
        }
    }, [dispatch, package_no])

    const onSubmitDataRAB = async (data) => {
        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiRAB_UploadExcel(data))
            if (payload.status === 200) {
                setLoading('');
                dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-RAB' }));
                SwalAlertSuccess('Berhasil', 'Berhasil simpan data.');
                dispatch(apiRAB_List({ package_no }));
            }
            else {
                SwalAlertError('Gagal.', 'Silahkan coba lagi, dokumen RAB tidak sesuai Contoh.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Gagal.', `Silahkan coba lagi, dokumen RAB tidak sesuai Contoh.`);
            setLoading('');
        }
    }

    const onClickRefresh = async () => {
        const { payload } = await dispatch(apiRAB_Detail({ package_no, modul }));
        setDetailRAB(payload.data);
    }

    const onSubmitNextBtn = () => {
        if (package_no && rab_detail) {
            setNavigate('4');
            dispatch(apiUpdateStatus_TahapUsulan({ package_no, status: 'Usulan-Mitra-Pendukung' }));
        } else {
            SwalAlertError('Gagal.', '*Wajib isi Rencana Anggaran Belanja (RAB).');
        }
    }

    return (
        <div>
            <div className="d-flex justify-content-between mb-4">
                <h3 className="text-dark font-weight-bold">3.1 Rencana Anggaran Belanja</h3>
            </div>

            <form onSubmit={handleSubmit(onSubmitDataRAB)} encType="multipart/form-data">
                <input type="hidden" {...register("modul", { required: true })} defaultValue={modul} />
                <input type="hidden" {...register("year", { required: true })} defaultValue={store_data.year} />
                <input type="hidden" {...register("institution", { required: true })} defaultValue={store_data.institution} />
                <input type="hidden" {...register("package_no", { required: true })} defaultValue={package_no} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Unduh Contoh Excel </label>
                            <a href={modul === 'startup' ? "./assets/doc/Template-RAB-Startup.xlsx" : "./assets/doc/Template-RAB-Riset.xlsx"} className="form-control font-weight-bolder"> <i className="fa fa-download fa-sm text-primary"></i> Unduh Template</a>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label>2. Unggah Dokumen *</label>
                            <div className={`custom-file ${file_excel && 'hide'}`}>
                                <input type="file" {...register("excel_file", { required: true })} className="custom-file-input" onChange={(e) => setFileExcel(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">{file_excel ? file_excel.name : 'Unggah Excel'}</label>
                            </div>
                            {
                                file_excel &&
                                <div className="d-flex justify-content-between">
                                    <a href={urlAttachment + '/usulan-rab/' + file_excel.name} className="form-control ml-2 font-weight-bold text-success text-truncate" target="_blank">
                                        <Icons iconName="attachment" className="svg-icon svg-icon-success svg-icon-md" /> {file_excel.name}</a>
                                    <button type="button" onClick={() => setFileExcel('')} title="Unggah Ulang" className="btn btn-clean btn-icon btn-hover-danger btn-md"><i className="far fa-times-circle fa-md" /></button>
                                </div>
                            }
                            {errors.excel_file && <span className="form-text text-danger">* Unggah berkas excel RAB.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`}><i className="fa fa-upload fa-sm" /> Tambah RAB </button>
                        </div>
                    </div>
                </div>
            </form>

            <DataGrid
                dataSource={rab_list}
                remoteOperations={true}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={80}
            >
                <HeaderFilter visible={false} />
                <FilterRow visible={false} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[10, 20, 50]}
                    showPageSizeSelector={true}
                    showInfo={false}
                    showNavigationButtons={true} />
                {/* <Editing
                    mode="row"
                    refreshMode='reshape'
                    allowAdding={false}
                    allowDeleting={false}
                    allowUpdating={false}
                /> */}
                <Column caption="ID" dataField="id" visible={false} />
                <Column caption="No" dataField="no" alignment="center" width={50} />
                <Column caption="Kelompok RAB" dataField="component" alignment="left" />
                <Column caption="Nominal Pengeluaran" dataField="expenditure" cellRender={({ value }) => {
                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                }} />
                {/* <Column caption="Aksi" type="buttons" alignment="center">
                    <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                        <i className="fas fa-trash-alt icon-sm"></i>
                    </Button>
                    <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                        <i className="fas fa-edit icon-sm"></i>
                    </Button>
                </Column> */}
            </DataGrid>

            <div className="d-flex justify-content-end mb-4">
                <span className="text-dark font-weight-bold bg-light-primary p-5">
                    <button type="button" onClick={() => onClickRefresh()} className="btn btn-clean btn-icon btn-sm"><i className="fas fa-sync fa-sm text-primary" /> </button>
                    TOTAL: {new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(rab_detail.total_expenditure)}
                </span>
            </div>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('2')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div>
        </div>
    )
}

export default StepFormRAB