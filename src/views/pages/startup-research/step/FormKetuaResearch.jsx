import React from 'react'

function FormKetuaResearch({ modul, FormInput, FormSelect, SyaratJabatanGelar, register, errors, setPekerjaan, pekerjaan }) {
    return (
        <div className="row">
            <div className="col-lg-6">
                {
                    modul === 'startup'
                        ? <div className="form-group">
                            <label className="font-weight-bolder">1. Pekerjaan :</label>
                            <select
                                className="form-control"
                                {...register("title_position", { required: true })}
                                onChange={(e) => setPekerjaan(e.target.value)}
                            >
                                <option value="">-- Pilih --</option>
                                <option value="Dosen">1. Dosen</option>
                                <option value="Tendik">2. Tendik</option>
                                <option value="Mahasiswa">3. Mahasiswa</option>
                                <option value="Alumni">4. Alumni</option>
                            </select>
                            {errors.title_position && <span className="form-text text-danger">* 1. Pekerjaan</span>}
                        </div>
                        : <div className="form-group">
                            <label>
                                <span className="font-weight-bolder">1. Jabatan & Gelar :</span>
                                <SyaratJabatanGelar />
                            </label>
                            <select
                                className="form-control"
                                {...register("title_position", { required: true })}
                            >
                                <option value="">-- Pilih --</option>
                                <option value="Guru Besar">1. Guru Besar dengan gelar Profesor</option>
                                <option value="Lektor">2. Lektor dengan gelar paling rendah Doktor</option>
                                <option value="Lektor Kepala">3. Lektor Kepala dengan gelar paling rendah Magister</option>
                            </select>
                            {errors.title_position && <span className="form-text text-danger">* 1. Jabatan & Gelar</span>}
                        </div>
                }
                <FormInput
                    name="identity_id"
                    type="text"
                    label={`2. Nomor ${pekerjaan === 'Alumni' ? 'NIK' : pekerjaan === 'Mahasiswa' ? 'NIM' : pekerjaan === 'Tendik' ? 'NIP/NIK' : 'NIDN/NIDK'}`}
                    className="form-control"
                    placeholder="Nomor Identitas"
                    register={register}
                    rules={{ required: true, pattern: { value: /^(0|[0-9]\d*)(\.\d+)?$/, } }}
                    readOnly={false}
                    errors={errors.identity_id}
                />
                <FormInput
                    name="leader_name"
                    type="text"
                    label="3. Nama Lengkap"
                    className="form-control"
                    placeholder="Nama"
                    register={register}
                    rules={{ required: true }}
                    readOnly={false}
                    errors={errors.leader_name}
                />
            </div>
            <div className="col-lg-6">
                <FormSelect
                    name="gender"
                    label="4. Jenis Kelamin"
                    className="form-control"
                    register={register}
                    rules={{ required: true }}
                    errors={errors.gender}
                    onChange={() => ''}
                >
                    <option value="">-- Pilih --</option>
                    <option value="Pria">1. Pria</option>
                    <option value="Wanita">2. Wanita</option>
                </FormSelect>
                <FormInput
                    name="email"
                    type="email"
                    label="5. Alamat Surel"
                    className="form-control"
                    placeholder="Email"
                    register={register}
                    rules={{ required: true, pattern: { value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, } }}
                    readOnly={false}
                    errors={errors.email}
                />
                <FormInput
                    name="phone_number"
                    type="text"
                    label="6. Nomor HP"
                    className="form-control"
                    placeholder="Nomor HP"
                    register={register}
                    rules={{ required: true, pattern: { value: /^(0|[0-9]\d*)(\.\d+)?$/, } }}
                    readOnly={false}
                    errors={errors.phone_number}
                />
            </div>
        </div>
    )
}

export default FormKetuaResearch