import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, Selection } from 'devextreme-react/data-grid';

import ModalVerificationProposal from './popup/ModalVerificationProposal';
import { authUser } from 'app/slice/sliceAuth';
import { apiStartupResearch_RecommendedList } from 'app/services/apiStartupResearch';

function MainRecommendedList({ setDataRekomendasi }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [proposal_data, setProposalData] = useState('');
    const [deffered, setDeffered] = useState(true);
    const { recommend_list } = useSelector(state => state.startup_research);
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiStartupResearch_RecommendedList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
        }
        else {
            dispatch(apiStartupResearch_RecommendedList({ auth, piu: auth.institution, proposed_year: store_data.year }));
        }
    }, [dispatch, auth, store_data]);

    const onSelectionChanged = useCallback(({ selectedRowsData }) => {
        setDeffered(false);
        setDataRekomendasi(selectedRowsData);
    }, []);

    return (
        <div>
            {proposal_data && <ModalVerificationProposal proposal_data={proposal_data} modul={proposal_data.type === 'S' ? 'startup' : 'research'} step="verification" />}
            <DataGrid
                dataSource={recommend_list}
                keyExpr='id'
                remoteOperations={true}
                rowAlternationEnabled={true}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={100}
                onSelectionChanged={onSelectionChanged}
                defaultSelectionFilter={["!", ['contract_no', '=', null]]} //auto checked
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[5, 10, 20]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Selection
                    mode="multiple"
                    selectAllMode="allPages"
                    showCheckBoxesMode="always"
                    deferred={deffered}
                />
                <Column caption="Judul Proposal" dataField="proposal_name" width={250} cellRender={({ value, data }) => {
                    return <a href="#" onClick={() => { setProposalData(data); }} data-toggle="modal" data-target="#modalVerificationProposal" className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</a>
                }} />
                <Column caption="Bidang Fokus" dataField="focus_name" />
                <Column caption="Nama Ketua" dataField="leader_name" />
                <Column
                    caption="Usulan Dana"
                    dataField="value_idr"
                    cellRender={({ value }) => {
                        return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                    }}
                />
                <Column
                    caption="Rekomendasi Dana"
                    dataField="recommend_budget"
                    cellRender={({ value }) => {
                        return <span>
                            {new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}
                            <input type="hidden" name="rekomendasi_anggaran" defaultValue={value} />
                        </span>
                    }}
                />
                <Column caption="Tahun Usulan" dataField="proposed_year" />
                <Column caption="TKT" dataField="tkt_value" />
                <Column caption="Durasi Kegiatan" dataField="tkt_duration" cellRender={({ value }) => {
                    return <span>{value} Tahun</span>
                }} />
            </DataGrid>
        </div>
    )
}

export default MainRecommendedList