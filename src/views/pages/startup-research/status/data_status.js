
export const data_status = [
    { navigate: '1', status: '' },
    { navigate: '1', status: null },
    { navigate: '1', status: 'Persiapan' },
    { navigate: '2', status: 'Verifikasi' },
    { navigate: '2', status: 'Kontrak' },
    { navigate: '2', status: 'Pencairan' },
    { navigate: '2', status: 'Finish' },
];


export const status_verification = [
    { navigate: '3', status: '' },
    { navigate: '3', status: null },
    { navigate: '3', status: 'Verifikasi' },
    { navigate: '3', status: 'Kontrak' },
    { navigate: '4', status: 'Pencairan' },
    { navigate: '5', status: 'Finish' },
    // { navigate: '4', status: 'Withdrawal' },
];

