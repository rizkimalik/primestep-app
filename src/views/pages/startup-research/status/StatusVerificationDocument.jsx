import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import ListDocsUploaded from '../ListDocsUploaded';
import { authUser } from 'app/slice/sliceAuth';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { apiDocument_UploadList, apiDocument_Upload } from 'app/services/apiDocument';
import {
    apiUpdateStatus_TahapVerification,
    apiCheck_VerificationDocument,
} from 'app/services/apiStartupResearch';
import { ValidateDocumentUpload } from 'views/components/ValidateDocumentUpload';

function StatusVerificationDocument({ modul, setNavigate, startup }) {
    const dispatch = useDispatch();
    const history = useHistory();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { document_list } = useSelector(state => state.document);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { store_data } = useSelector(state => state.auth);
    const { docs_uploaded_list } = useSelector(state => state.master);

    useEffect(() => {
        reset({ proposal_no: store_data.proposal_no })
    }, [reset, store_data]);

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul, status: 'Persiapan' }));
    }, [dispatch, modul]);

    const onSubmitForm = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            await dispatch(apiUpdateStatus_TahapVerification({ proposal_no: data.proposal_no, status: 'Persiapan-Dokumen' }));
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.proposal_no,
                document_for: data.modul,
                document_name: data.document_name,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                await dispatch(apiDocument_UploadList({ package_no: data.proposal_no, status: data.status, document_for: data.modul }));
                SwalAlertSuccess('Berhasil', 'Berhasil simpan data.');
                setLoading('');
                setFileDoc('');
                reset();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    const onSubmitNextBtn = async () => {
        const { payload } = await dispatch(apiCheck_VerificationDocument({ package_no: store_data.proposal_no, document_for: modul, status: 'Persiapan-Dokumen' }));
        const isValidDocumentUpload = await ValidateDocumentUpload(docs_uploaded_list, document_list);

        if (payload?.data > 0) {
            SwalAlertError('Aksi Gagal.', '* Verifikasi dokumen kelengkapan proposal.');
        }
        else if (isValidDocumentUpload === false) {
            SwalAlertError('Aksi Gagal.', '* Unggah dokumen kelengkapan proposal.');
        }
        else {
            await dispatch(apiUpdateStatus_TahapVerification({ proposal_no: store_data.proposal_no, status: 'Verifikasi-Dokumen' }));
            if (startup === 'acceleration') {
                history.push(`/startup-acceleration`);
            } else {
                history.push(`/${modul}`);
            }
        }
    }

    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold mb-4">2.1 Unggah Dokumen Kelengkapan Proposal</h3>
            <form onSubmit={handleSubmit(onSubmitForm)} encType="multipart/form-data" className={`${auth.user_level === 'Evaluator' && 'hide'}`}>
                <input type="hidden" {...register("proposal_no", { required: true })} />
                <input type="hidden" {...register("status", { required: true })} defaultValue="Persiapan-Dokumen" />
                <input type="hidden" {...register("modul", { required: true })} defaultValue={modul} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen * :</label>
                            <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen :</label>
                            <div className={`custom-file`}>
                                <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                            </div>
                            {errors.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                        </div>

                        {/* <div className="form-group">
                            <label>2. Unggah Dokumen * :</label>
                            <div className={`custom-file ${file_doc && 'hide'}`}>
                                <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">Pilih Berkas</label>
                            </div>
                            <div className={`${(!file_doc) && 'hide'}`}>
                                <div className="form-control alert alert-custom alert-light-dark fade show p-2" role="alert">
                                    <div className="alert-text text-truncate" title={file_doc.name}>{file_doc.name} ({file_doc.size / 1000} KB)</div>
                                    <div className="alert-close">
                                        <button type="button" onClick={() => setFileDoc('')} className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i className="ki ki-close" title="Hapus Berkas" /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {errors.document_file && <span className="form-text text-danger">* Silahkan unggah dokumen berkas.</span>}
                        </div> */}
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={store_data.proposal_no} status='Persiapan-Dokumen' document_for={modul} />

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('1')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                {
                    auth.institution === 'PMU' &&
                    <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selesai Verifikasi</button>
                }
            </div>
        </div>
    )
}

export default StatusVerificationDocument