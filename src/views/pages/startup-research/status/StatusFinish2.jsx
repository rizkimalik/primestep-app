import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import ListDocsUploaded from '../ListDocsUploaded';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import {
    apiUpdateStatus_TahapVerification,
} from 'app/services/apiStartupResearch';
import { authUser } from 'app/slice/sliceAuth';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDocument_Upload, apiDocument_UploadList } from 'app/services/apiDocument';

function StatusFinish({ proposal_no, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const history = useHistory();
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { document_list } = useSelector(state => state.document);
    const { docs_uploaded_list } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'research-startup', status: 'Finish' }));
    }, [dispatch]);

    useEffect(() => {
        reset({ proposal_no });
    }, [reset, proposal_no]);

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.proposal_no,
                document_for: 'startup-research',
                document_name: data.document_name,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil unggah dokumen.');
                await dispatch(apiDocument_UploadList({ package_no: proposal_no, status: 'Finish' }));
                setLoading('');
                setFileDoc('');
                reset();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    const onSubmitNextBtn = () => {
        if (document_list.length > 0) {
            dispatch(apiUpdateStatus_TahapVerification({ proposal_no, status: 'Finish' }));
            history.push(`/startup-research/contract`);
        } else {
            SwalAlertError('Gagal.', '* Verifikasi dokumen Banpem & proposal perjudul.');
        }
    }


    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold my-4">5.1 Unggah Dokumen SP2D :</h3>
            <form onSubmit={handleSubmit(onSubmitInsertDocsUploaded)} encType="multipart/form-data" className={`${auth.institution !== 'PMU' && 'hide'}`}>
                <input type="hidden" defaultValue={proposal_no} {...register("proposal_no", { required: true })} />
                <input type="hidden" defaultValue="Finish" {...register("status", { required: true })} />

                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen * :</label>
                            <select name="document_name" className="form-control form-control-md" {...register("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen :</label>
                            <div className={`custom-file`}>
                                <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                            </div>
                            {errors.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={proposal_no} status='Finish' document_for='startup-research' />

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <button type="button" onClick={() => setNavigate('4')} className="btn btn-light-danger font-weight-bold btn-lg">Kembali</button>
                {
                    auth.institution === 'PMU' &&
                    <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selesai</button>
                }
            </div>
        </div>
    )
}

export default StatusFinish