import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import ListDocsUploaded from '../ListDocsUploaded';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { apiDocument_UploadList, apiDocument_Upload } from 'app/services/apiDocument';

function StatusPersiapanDocument({ modul, package_no }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const { docs_uploaded_list } = useSelector(state => state.master);
    // const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const {
        register: register2,
        formState: { errors: errors2 },
        handleSubmit: handleSubmit2,
        reset: reset2,
    } = useForm();

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul, status: 'Usulan' }));
    }, [dispatch, modul]);

    const onSubmitForm = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.package_no,
                document_for: data.modul,
                document_name: data.document_name,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                await dispatch(apiDocument_UploadList({ package_no: data.package_no, status: data.status, document_for: data.modul }));
                SwalAlertSuccess('Berhasil', 'Berhasil simpan data.');
                setLoading('');
                setFileDoc('');
                reset2();
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold mb-4">Unggah Dokumen Pengusulan Perjudul</h3>
            <form onSubmit={handleSubmit2(onSubmitForm)} encType="multipart/form-data" className={`${auth.user_level === 'Evaluator' && 'hide'}`}>
                <input type="hidden" {...register2("modul", { required: true, value: modul })} />
                <input type="hidden" {...register2("package_no", { required: true, value: package_no })} />
                <input type="hidden" {...register2("status", { required: true, value: "Persiapan-Usulan" })} />
                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen * :</label>
                            <select name="document_name" className="form-control form-control-md" {...register2("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors2.document_name && <span className="form-text text-danger">* Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>2. Unggah Dokumen :</label>
                            <div className={`custom-file`}>
                                <input type="file" {...register2("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                            </div>
                            {errors2.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register2("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={package_no} status='Persiapan-Usulan' document_for={modul} />
        </div>
    )
}

export default StatusPersiapanDocument