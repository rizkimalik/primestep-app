import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';

import ModalEvaluatorList from '../popup/ModalEvaluatorList';
import { SwalAlertError } from 'views/components/SwalAlert';
import { apiMaster_DocUploadedList } from 'app/services/apiMasterData';
import { authUser } from 'app/slice/sliceAuth';
import { apiDocument_UploadList } from 'app/services/apiDocument';
import {
    apiStartupResearch_StartupList,
    apiStartupResearch_ResearchList,
    apiStartup_AccelerationList,
    apiCheck_VerificationProposal,
    apiUpdateStatus_TahapVerification,
} from 'app/services/apiStartupResearch';


// Form proposal (Usulan)
function StatusPersiapan({ modul, setNavigate, startup }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [proposal_data, setProposalData] = useState('');
    const [modal_show, setModalShow] = useState('');
    const { store_data } = useSelector(state => state.auth);
    const { startups, research, startup_acceleration } = useSelector(state => state.startup_research);
    const data_proposal = startup === 'acceleration' ? startup_acceleration : modul === 'startup' ? startups : research; //? data berdasarkan modul nya

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul, status: 'Usulan' }));
    }, [dispatch, modul]);

    useEffect(() => {
        if (startup === 'acceleration') {
            dispatch(apiStartup_AccelerationList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
        }
        else if (modul === 'startup') {
            dispatch(apiStartupResearch_StartupList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
        }
        else {
            dispatch(apiStartupResearch_ResearchList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
        }
    }, [dispatch, modul, store_data]);

    const onAssignEvaluator = async (data) => {
        const { payload } = await dispatch(apiDocument_UploadList({ package_no: data.package_no, status: 'Persiapan-Usulan', document_for: modul }))
        if (payload?.data.length > 0) {
            setModalShow('Evaluator');
            setProposalData(data);
            document.getElementById('btn-evaluator').click();
        }
        else {
            SwalAlertError('Aksi Gagal.', '* Unggah dokumen kelengkapan pengusulan perjudul.');
        }
    }

    const onSubmitNextBtn = async () => {
        const { payload } = await await dispatch(apiCheck_VerificationProposal({ institution: store_data.institution, year: store_data.year, modul }));

        if (store_data && payload?.data <= 0) {
            setNavigate('2');
            await dispatch(apiUpdateStatus_TahapVerification({ proposal_no: store_data.proposal_no, status: 'Persiapan-Dokumen' }));
        } else {
            SwalAlertError('Aksi Gagal.', '* Verifikasi dokumen & usulan perjudul.');
        }
    }


    return (
        <div>
            {
                modal_show === 'Evaluator' &&
                <div>
                    <ModalEvaluatorList proposal_data={proposal_data} modul={modul} />
                    <button type="button" id="btn-evaluator" data-toggle="modal" data-target="#modalEvaluatorList" className="hide">trigger modal evaluator</button>
                </div>
            }
            {/* {
                proposal_data &&
                modal_show === 'Proposal' &&
                <ModalVerificationProposal proposal_data={proposal_data} modul={modul} step="verification" />
            } */}

            <h3 className="font-size-lg text-dark font-weight-bold mb-4">1.1 Unggah Dokumen Pengusulan Perjudul</h3>
            <DataGrid
                dataSource={data_proposal}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true,
                    summary: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
                wordWrapEnabled={true}
                columnMinWidth={100}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={10} />
                <Pager
                    visible={true}
                    displayMode='full'
                    allowedPageSizes={[10, 20, 50]}
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Judul Proposal" dataField="proposal_name" width={300} cellRender={({ value, data }) => {
                    return <Link to={`/startup-research/verification/${modul}/${data.package_no}`} className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</Link>
                }} />
                <Column caption="Bidang Fokus" dataField="focus_name" />
                {/* <Column caption={`Nama ${modul === 'startup' ? 'Perintis' : 'Peneliti'}`} dataField="leader_name" /> */}
                <Column caption="Nama Ketua" dataField="leader_name" />
                {
                    (auth.user_level === 'Admin' || auth.user_level === 'PMU') &&
                    <Column caption="Evaluator" dataField="evaluator_name" cellRender={({ value, data }) => {
                        return value !== null
                            ? <a href='#' onClick={() => onAssignEvaluator(data)} data-toggle="modal" data-target="#modalEvaluatorList2" className="btn btn-sm btn-clean btn-light-primary"><i className="fas fa-user fa-sm" /> {value}</a>
                            : <a href='#' onClick={() => onAssignEvaluator(data)} data-toggle="modal" data-target="#modalEvaluatorList2" className="btn btn-sm btn-clean btn-light"><i className="fas fa-user fa-sm" /> Pilih Evaluator</a>
                    }} />
                    // : <Column caption="Evaluator" dataField="evaluator_name" />
                }
                <Column caption="Status" dataField="recommend" cellRender={({ data }) => {
                    return data.recommend === 0
                        ? <span className="label label-inline label-warning">Revisi</span>
                        : data.recommend === 1
                            ? <span className="label label-inline label-success">Rekomendasi</span>
                            : data.recommend === 2
                                ? <span className="label label-inline label-danger">Ditolak</span>
                                : <span className="label label-inline">Menunggu</span>
                }} />
            </DataGrid>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <Link to={`/${startup === 'acceleration' ? 'startup-acceleration' : modul}`} className="btn btn-light-danger font-weight-bold btn-lg ml-2">
                    Kembali
                </Link>
                {
                    auth.user_level === 'Evaluator'
                        ? <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selesai Verifikasi</button>
                        : <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
                }
            </div>
        </div>
    )
}

export default StatusPersiapan