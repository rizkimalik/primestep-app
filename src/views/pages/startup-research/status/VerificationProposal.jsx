import React, { useEffect, useState } from 'react'
import HtmlEditor, { Toolbar, Item } from 'devextreme-react/html-editor';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Controller, useForm } from 'react-hook-form';
import { NumericFormat } from 'react-number-format';
import { Link } from 'react-router-dom';

import StatusPersiapanDocument from './StatusPersiapanDocument';
import { authUser } from 'app/slice/sliceAuth';
import { ButtonSubmit } from 'views/components/button';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { Card, CardBody } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import {
    apiStartupResearch_Detail,
    apiStartupResearch_StartupList,
    apiStartupResearch_ResearchList,
    apiStartupResearch_StatusVerificationProposal,
    apiStartupResearch_HistoryVerificationProposal,
} from 'app/services/apiStartupResearch';
import { apiCheck_VerificationDocument } from 'app/services/apiStartupResearch';

function VerificationProposal() {
    const { modul, package_no } = useParams();
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [data_detail, setDataDetail] = useState('');
    const [is_recomend, setRecomend] = useState(parseInt(data_detail?.recommend) || 1);
    const [history_proposal, setHistoryProposal] = useState([]);
    const { store_data } = useSelector(state => state.auth);
    const { register, formState: { errors }, handleSubmit, reset, control } = useForm();

    useEffect(async () => {
        const { payload } = await dispatch(apiStartupResearch_Detail({ package_no }));
        if (payload.status !== 200) return;
        setDataDetail(payload.data);
        setRecomend(parseInt(payload.data.recommend));

        reset({
            package_no: payload.data?.package_no,
            recommend: payload.data?.recommend,
            recommend_budget: payload.data?.recommend_budget,
            recommend_comment: payload.data?.recommend_comment,
        });
    }, [dispatch, package_no]);

    useEffect(async () => {
        const { payload } = await dispatch(apiStartupResearch_HistoryVerificationProposal({ package_no }));
        if (payload.status === 200) {
            setHistoryProposal(payload.data);
        }
    }, [dispatch, package_no]);

    function currency_to_number(currencyString) {
        if (typeof currencyString !== 'string') {
            return currencyString; // or any other appropriate value
        }
        const numericString = currencyString.replace(/[^0-9.]/g, '');
        const numericValue = parseFloat(numericString);
        return numericValue;
    }

    const onClickHistoryProposal = async () => {
        try {
            const { payload } = await dispatch(apiStartupResearch_HistoryVerificationProposal({ package_no }))
            if (payload.status === 200) {
                setHistoryProposal(payload.data);
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitInsertData = async (data) => {
        try {
            const { payload } = await dispatch(apiCheck_VerificationDocument({ package_no: data.package_no, document_for: modul, status: 'Persiapan-Usulan' }));

            if (payload?.data > 0 && data.recommend == 1) {
                SwalAlertError('Aksi Gagal.', '* Verifikasi dokumen kelengkapan pengusulan.');
            }
            else {
                data.recommend_comment = document.querySelector('input[name=recommend_comment]').value;
                const verify = await dispatch(apiStartupResearch_StatusVerificationProposal(data))
                if (verify.payload.status === 200) {
                    SwalAlertSuccess('Berhasil.', 'Berhasil simpan data.');

                    const detail = await dispatch(apiStartupResearch_Detail({ package_no: data.package_no }));
                    setDataDetail(detail.payload.data);

                    if (modul === 'startup') {
                        await dispatch(apiStartupResearch_StartupList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                    }
                    else {
                        await dispatch(apiStartupResearch_ResearchList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
                    }
                    reset();
                } else {
                    SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                }
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Research and Development" menu_name="Verifikasi Startup & Penelitian" modul_name="">
                {/* <span className="font-weight-bolder text-muted">Tahun {store_data.year}</span> */}
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <div className="d-flex justify-content-between mb-2">
                            <h3 className="font-size-lg text-dark font-weight-bold mb-4">Informasi Detail Judul Proposal</h3>
                        </div>
                        <div className="row mb-0">
                            <div className="col-lg-6">
                                <div className="form-group">
                                    <label className="font-weight-bolder">Judul {modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} :</label>
                                    <p className="border-0 pl-0 text-primary font-weight-bolder">{data_detail.proposal_name}</p>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">Kategori {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.category_framework_name}</span>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">Bidang Fokus {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.category_focus_name}</span>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">Anggaran Usulan :</label>
                                    <span className="form-control border-0 pl-0">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(Number(data_detail.value_idr))}</span>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="form-group">
                                    <label className="font-weight-bolder">Nama {modul === 'startup' ? 'Perintis' : 'Peneliti'} :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.leader_name}</span>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">TKT :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.tkt_value}</span>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">Tahun Usulan :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.proposed_year}</span>
                                </div>
                                <div className="form-group">
                                    <label className="font-weight-bolder">Durasi {modul === 'startup' ? 'Rintisan' : 'Penelitian'} :</label>
                                    <span className="form-control border-0 pl-0">{data_detail.tkt_duration} Tahun</span>
                                </div>
                            </div>
                        </div>

                        <div>
                            <ul className="nav nav-tabs justify-content-end mt-0">
                                <li className="nav-item">
                                    <a onClick={() => onClickHistoryProposal()} className="nav-link font-weight-bolder " data-toggle="tab" href="#history_verification">Riwayat Verifikasi</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => ''} className="nav-link font-weight-bolder active" data-toggle="tab" href="#dokumen_pengusulan">Dokumen Pengusulan</a>
                                </li>
                            </ul>
                            <div className="tab-content mt-5">
                                <div className="tab-pane fade" id="history_verification" role="tabpanel" aria-labelledby="history_verification">
                                    {
                                        history_proposal.map((item, index) => {
                                            return <div className="d-flex flex-column align-items-start border rounded p-4 mb-4" key={index}>
                                                <div className="d-flex flex-column flex-grow-1">
                                                    <div className="mb-4">
                                                        {
                                                            item.recommend === 0
                                                                ? <span className="label label-inline label-warning">Revisi</span>
                                                                : item.recommend === 1
                                                                    ? <span className="label label-inline label-success">Rekomendasi</span>
                                                                    : item.recommend === 2
                                                                        ? <span className="label label-inline label-danger">Ditolak</span>
                                                                        : <span className="label label-inline">Menunggu</span>
                                                        }
                                                        {
                                                            auth.user_level !== 'PIU' &&
                                                            <small className="text-muted mx-4">
                                                                Evaluator: {item.evaluator_name}
                                                            </small>
                                                        }
                                                    </div>
                                                    <small className="text-muted mb-2">
                                                        <span>{new Intl.DateTimeFormat('id-ID',
                                                            {
                                                                year: 'numeric',
                                                                month: 'short',
                                                                day: 'numeric',
                                                                hour: "numeric",
                                                                minute: "numeric",
                                                                second: "numeric",
                                                                timeZoneName: 'short',
                                                            })
                                                            .format(new Date(item.created_at))}
                                                        </span>
                                                    </small>
                                                    <span className="mb-2">
                                                        Rekomendasi Anggaran:
                                                        <span className="text-dark font-weight-bolder ml-2">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(item.recommend_budget)}</span>
                                                    </span>
                                                    {/* <p>{item.recommend_comment}</p> */}
                                                    <p dangerouslySetInnerHTML={{ __html: item.recommend_comment }}></p>
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                                <div className="tab-pane fade active show" id="dokumen_pengusulan" role="tabpanel" aria-labelledby="dokumen_pengusulan">
                                    <p className="text-muted my-4">Dokumen Pengusulan dan Judul Proposal diverifikasi oleh Evaluator.</p>
                                    <StatusPersiapanDocument modul={modul} package_no={package_no} />

                                    <div className="separator separator-dashed separator-border-2 my-5"></div>
                                    <div className="d-flex justify-content-end">
                                        {
                                            auth.user_level === 'PIU' &&
                                            <Link to={`/startup-research/status/research?piu=${store_data.institution}&year=${store_data.year}`} className="btn btn-light-danger font-weight-bold btn-lg ml-2">
                                                Kembali
                                            </Link>
                                        }
                                    </div>

                                    <form onSubmit={handleSubmit(onSubmitInsertData)} className={`form ${auth.user_level === 'PIU' && 'hide'}`}>
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <input type="hidden" {...register('package_no', { required: true })} />
                                                <input type="hidden" {...register('recommend_evaluator', { required: true })} value={auth.username} />
                                                <div className="form-group">
                                                    <label className="font-weight-bolder">Apakah Judul Proposal ini direkomendasi?</label>
                                                    <div className="radio-inline">
                                                        <label className="radio">
                                                            <input type="radio" {...register('recommend', { required: true })} value={1} checked={is_recomend === 1} onClick={(event) => setRecomend(parseInt(event.target.value))} />
                                                            <span /> Rekomendasi
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" {...register('recommend', { required: true })} value={0} checked={is_recomend === 0} onClick={(event) => setRecomend(parseInt(event.target.value))} />
                                                            <span /> Revisi
                                                        </label>
                                                        <label className="radio">
                                                            <input type="radio" {...register('recommend', { required: true })} value={2} checked={is_recomend === 2} onClick={(event) => setRecomend(parseInt(event.target.value))} />
                                                            <span /> Ditolak
                                                        </label>
                                                    </div>
                                                    {errors.recommend && <span className="form-text text-danger">* Konfirmasi Rekomendasi / Revisi</span>}
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="font-weight-bolder">Rekomendasi Anggaran (Rp.) :</label>
                                                    <Controller
                                                        control={control}
                                                        name="recommend_budget"
                                                        rules={{
                                                            required: true,
                                                            validate: (value) => {
                                                                if (currency_to_number(value) > currency_to_number(data_detail.value_idr)) {
                                                                    return 'Value must be less than or equal to Usulan';
                                                                }
                                                                return undefined;
                                                            }
                                                        }}
                                                        render={({ field: { ref, ...rest } }) => (
                                                            <NumericFormat
                                                                {...rest}
                                                                getInputRef={ref}
                                                                id="recommend_budget"
                                                                className="form-control"
                                                                thousandSeparator=","
                                                                decimalSeparator="."
                                                                decimalScale={2}
                                                            />
                                                        )}
                                                    />
                                                    {errors.recommend_budget && <span className="form-text text-danger">* Rekomendasi Anggaran harus kurang dari atau sama dengan Nilai Usulan</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="font-weight-bolder">Komentar (Evaluator) : </label>
                                                    {/* <textarea {...register('recommend_comment', { required: is_recomend ? false : true, minLength: is_recomend ? 0 : 50 })} className="form-control" rows="10" placeholder="Komentar rekomendasi"></textarea> */}
                                                    <div dangerouslySetInnerHTML={{ __html: data_detail.recommend_comment }}></div>
                                                    <HtmlEditor
                                                        className="mt-4"
                                                        height={300}
                                                        valueType="html"
                                                        // defaultValue={data_detail.recommend_comment}
                                                        onValueChange={(value) => document.querySelector('input[name=recommend_comment]').value = value}
                                                    >
                                                        <Toolbar multiline={true}>
                                                            <Item
                                                                name="size"
                                                                acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                                                            />
                                                            <Item
                                                                name="font"
                                                                acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                                                            />
                                                            <Item name="separator" />
                                                            <Item name="bold" />
                                                            <Item name="italic" />
                                                            <Item name="underline" />
                                                            <Item name="separator" />
                                                            <Item name="alignLeft" />
                                                            <Item name="alignCenter" />
                                                            <Item name="alignRight" />
                                                            <Item name="alignJustify" />
                                                            <Item name="separator" />
                                                            <Item name="orderedList" />
                                                            <Item name="bulletList" />
                                                            <Item name="separator" />
                                                            <Item name="color" />
                                                            <Item name="background" />
                                                        </Toolbar>
                                                    </HtmlEditor>

                                                    {/* <input type="hidden" {...register('recommend_comment', { required: is_recomend ? false : true, minLength: is_recomend ? 0 : 50 })} className="form-control" name="recommend_comment" title="tampung data comment" readOnly /> */}
                                                    <input type="hidden" {...register('recommend_comment', { required: false})} className="form-control" name="recommend_comment" title="tampung data comment" />
                                                    {errors.recommend_comment && <span className="form-text text-danger">* Komentar minimal 50 huruf</span>}
                                                </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-end">
                                            {
                                                // data_detail.recommend !== 1 &&
                                                auth.user_level === 'Evaluator' &&
                                                <ButtonSubmit />
                                            }
                                            {
                                                auth.user_level === 'Evaluator'
                                                    ? <Link to={`/dashboard`} className="btn btn-light-danger font-weight-bold btn-lg ml-2">
                                                        Kembali
                                                    </Link>
                                                    : <Link to={`/startup-research/status/${modul}?piu=${store_data.institution}&year=${store_data.year}`} className="btn btn-light-danger font-weight-bold btn-lg ml-2">
                                                        Kembali
                                                    </Link>
                                            }
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default VerificationProposal