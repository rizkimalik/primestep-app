import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { NumericFormat } from "react-number-format";
import { Link } from 'react-router-dom';

import ListDocsUploaded from '../ListDocsUploaded';
import MainRecommendedList from '../MainRecommendedList';
import { ButtonSubmit } from 'views/components/button';
import { apiContractDetail, apiContractUpdate } from 'app/services/apiContract';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiMaster_DocUploadedList, apiMaster_PaguContract } from 'app/services/apiMasterData';
import { authUser } from 'app/slice/sliceAuth';
import { apiDocument_Upload, apiDocument_UploadList } from 'app/services/apiDocument';
import { ValidateDocumentUpload } from 'views/components/ValidateDocumentUpload';
import {
    apiStartupResearch_StatusContract,
    apiUpdateStatus_TahapVerification,
    apiCheck_VerificationDocument,
} from 'app/services/apiStartupResearch';

function StatusContract({ proposal_no, setNavigate }) {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const [file_doc, setFileDoc] = useState('');
    const [loading, setLoading] = useState('');
    const [pagu_contract, setPaguContract] = useState('');
    const [data_rekomendasi, setDataRekomendasi] = useState([]);
    const { store_data } = useSelector(state => state.auth);
    const { document_list } = useSelector(state => state.document);
    const { docs_uploaded_list } = useSelector(state => state.master);
    const { control, register, formState: { errors }, handleSubmit, reset } = useForm();
    const {
        register: register2,
        formState: { errors: errors2 },
        handleSubmit: handleSubmit2,
        reset: reset2,
    } = useForm();

    useEffect(() => {
        dispatch(apiMaster_DocUploadedList({ modul: 'research-startup', status: 'Kontrak', user_level: auth.user_level }));
    }, [dispatch]);

    useEffect(async () => {
        const { payload } = await dispatch(apiMaster_PaguContract({ modul: 'research-startup', institution: store_data.institution, year_alocation: store_data.year }));
        if (payload.status === 200) {
            setPaguContract(payload.data)
        }
    }, [dispatch, store_data]);

    useEffect(() => {
        reset({ proposal_no });
    }, [reset, proposal_no]);

    useEffect(() => {
        reset2({ proposal_no });
    }, [reset2, proposal_no]);

    useEffect(async () => {
        if (!proposal_no) return;
        const { payload } = await dispatch(apiContractDetail({ package_no: proposal_no }));
        if (!payload) return;
        reset({
            contract_no: payload?.data?.contract_no,
            contract_no_piu: payload?.data?.contract_no_piu,
            contract_no_adb: payload?.data?.contract_no_adb,
            contract_vendor: payload?.data?.contract_vendor,
            contract_date: (payload?.data?.contract_date)?.split('T')[0],
            contract_value: payload?.data?.contract_value,
            contract_periode: payload?.data?.contract_periode,
            contract_last_date: (payload?.data?.contract_last_date)?.split('T')[0],
        });
    }, [dispatch, proposal_no]);

    useEffect(() => {
        dispatch(apiDocument_UploadList({ package_no: proposal_no, status: 'Kontrak' }));
    }, [dispatch, proposal_no]);

    const onChangeGetPeriode = (e) => {
        let start_date = new Date(e.target.value);
        const periode = document.getElementById('contract_periode');
        const contract_value = document.getElementById('contract_value');
        const month = Number(periode.value) + Number(start_date.getMonth());
        start_date.setMonth(start_date.getMonth() + month);
        reset({
            contract_value: contract_value?.value,
            contract_last_date: start_date.toISOString().substring(0, 10),
        });
    }

    const onSubmitStatusContract = async (data) => {
        try {
            const { payload } = await dispatch(apiContractUpdate(data))
            if (payload.status === 200) {
                await dispatch(apiStartupResearch_StatusContract({ proposal_no, contract_no: data.contract_no, data_contract: data_rekomendasi }))
                SwalAlertSuccess('Sukses', 'Berhasil simpan data.');
            } else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
            }
        } catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
        }
    }

    const onSubmitInsertDocsUploaded = async (data) => {
        const maxFileSize = 40 * 1024 * 1024; // 20MB in bytes
        const fileSize = data.document_file[0].size;
        if (fileSize > maxFileSize) return SwalAlertError('Unggah Berkas Gagal.', 'Batas ukuran maximal 40 MB');

        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiDocument_Upload({
                document_file: data.document_file,
                package_no: data.proposal_no,
                document_for: 'startup-research',
                document_name: data.document_name,
                status: data.status,
                description: data.description,
                institution: auth.institution,
                user_upload: auth.username,
            })); //? insert dokument
            if (payload.status === 200) {
                SwalAlertSuccess('Sukses', 'Berhasil simpan data.');
                await dispatch(apiDocument_UploadList({ package_no: proposal_no, status: 'Kontrak' }));
                setLoading('');
                setFileDoc('');
                reset2();
            }
            else {
                SwalAlertError('Aksi Gagal.', 'Silahkan coba lagi.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Aksi Gagal.', `Silahkan coba lagi, ${error.message}.`);
            setLoading('');
        }
    }

    const onHitungNilaiKontrak = (e) => {
        e.preventDefault()

        let nilai_kontrak = 0;
        for (var i = 0; i < data_rekomendasi.length; i++) {
            nilai_kontrak += parseFloat(data_rekomendasi[i].recommend_budget) || 0;
        }

        if (nilai_kontrak <= pagu_contract.total_alocation) {
            reset({ contract_value: nilai_kontrak })
        } else {
            SwalAlertError('Aksi Gagal.', `Total Rekomendasi lebih dari PAGU Anggaran`);
        }
    }

    const onSubmitNextBtn = async () => {
        const isValidDocumentUpload = await ValidateDocumentUpload(docs_uploaded_list, document_list.filter(row => row.institution === auth.institution));
        const { payload } = await dispatch(apiCheck_VerificationDocument({ package_no: store_data.proposal_no, document_for: 'startup-research', status: 'Kontrak' }));

        if (payload?.data > 0) {
            SwalAlertError('Aksi Gagal.', '* Verifikasi dokumen kelengkapan kontrak.');
        }
        else if (isValidDocumentUpload === false) {
            SwalAlertError('Aksi Gagal.', '* Unggah dokumen kelengkapan kontrak.');
        }
        else {
            await dispatch(apiUpdateStatus_TahapVerification({ proposal_no, status: 'Kontrak' }));
            setNavigate('4');
        }
    }

    return (
        <div>
            <h3 className="font-size-lg text-dark font-weight-bold my-4">3.1 Daftar Usulan yang direkomendasikan :</h3>
            <MainRecommendedList setDataRekomendasi={setDataRekomendasi} />
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <form onSubmit={handleSubmit(onSubmitStatusContract)} className="mb-4">
                <h3 className="font-size-lg text-dark font-weight-bold mb-4">3.2 Form Kontrak:</h3>
                <input type="hidden" defaultValue={proposal_no} {...register("package_no", { required: true })} />
                <input type="hidden" defaultValue={store_data.institution} {...register("institution", { required: true })} />
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">PAGU Anggaran {pagu_contract?.year_alocation}:</label>
                    <div className="col-lg-6">
                        <span className="form-control">{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(pagu_contract?.total_alocation)}</span>
                    </div>
                </div>
                {
                    auth.institution === 'PMU' &&
                    <div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">No PKS PMU:</label>
                            <div className="col-lg-6">
                                <input type="text" {...register("contract_no_piu", { required: true })} className="form-control" placeholder="No PKS PMU" />
                                {errors.contract_no_piu && <span className="form-text text-danger">* No PKS PMU.</span>}
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-lg-3 col-form-label text-right">No Kontrak ADB:</label>
                            <div className="col-lg-6">
                                <input type="text" {...register("contract_no_adb", { required: false })} className="form-control" placeholder="No Kontrak ADB" />
                                {errors.contract_no_adb && <span className="form-text text-danger">* No Kontrak ADB.</span>}
                            </div>
                        </div>
                    </div>
                }
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">No PKS PIU:</label>
                    <div className="col-lg-6">
                        <input type="text" {...register("contract_no", { required: true })} className="form-control" placeholder="No PKS PIU" />
                        {errors.contract_no && <span className="form-text text-danger">* No PKS PIU.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Nilai Kontrak (Rp.):</label>
                    <div className="col-lg-6">
                        <Controller
                            control={control}
                            name="contract_value"
                            rules={{ required: true }}
                            render={({ field: { ref, ...rest } }) => (
                                <div className="input-group">
                                    <NumericFormat
                                        id="contract_value"
                                        className="form-control"
                                        thousandSeparator=","
                                        decimalSeparator="."
                                        decimalScale={2}
                                        getInputRef={ref}
                                        readOnly={true}
                                        {...rest}
                                    />
                                    <div className="input-group-append">
                                        <button onClick={(e) => onHitungNilaiKontrak(e)} className="btn btn-light-primary" type="button">= Total Rekomendasi</button>
                                    </div>
                                </div>
                            )}
                        />
                        {errors.contract_value && <span className="form-text text-danger">* Nilai Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Periode Kontrak (Bulan):</label>
                    <div className="col-lg-6">
                        <input
                            type="text"
                            {...register("contract_periode", { required: true })}
                            id="contract_periode"
                            className="form-control"
                            placeholder="Periode Kontrak"
                        />
                        {errors.contract_periode && <span className="form-text text-danger">* Periode Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Tanggal Mulai Kontrak:</label>
                    <div className="col-lg-6">
                        <input type="date" {...register("contract_date", { required: true })} onChange={(e) => onChangeGetPeriode(e)} className="form-control" placeholder="Tanggal Kontrak" />
                        {errors.contract_date && <span className="form-text text-danger">* Tanggal Mulai Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right">Tanggal Akhir Kontrak:</label>
                    <div className="col-lg-6">
                        <input type="date" {...register("contract_last_date", { required: true })} id="contract_last_date" className="form-control" placeholder="Tanggal Akhir Kontrak" />
                        {errors.contract_last_date && <span className="form-text text-danger">* Tanggal Akhir Kontrak.</span>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-right" />
                    <div className="col-lg-6">
                        <ButtonSubmit />
                    </div>
                </div>
            </form>
            <div className="separator separator-dashed separator-border-2 my-4"></div>

            <h3 className="font-size-lg text-dark font-weight-bold my-4">3.3 Dokumen Kelengkapan Kontrak :</h3>
            <form onSubmit={handleSubmit2(onSubmitInsertDocsUploaded)} encType="multipart/form-data">
                <input type="hidden" defaultValue={proposal_no} {...register2("proposal_no", { required: true })} />
                <input type="hidden" defaultValue="Kontrak" {...register2("status", { required: true })} />

                <div className="form-group row">
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>1. Nama Dokumen :</label>
                            <select name="document_name" className="form-control form-control-md" {...register2("document_name", { required: true })}>
                                <option value="">-- Pilih Dokumen --</option>
                                {
                                    docs_uploaded_list.map((item, index) => {
                                        return <option value={item.document_name} key={index}>{item.document_name}</option>
                                    })
                                }
                            </select>
                            {errors2.document_name && <span className="form-text text-danger">Silahkan pilih Dokumen.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                    <div className="form-group">
                            <label>2. Unggah Dokumen :</label>
                            <div className={`custom-file`}>
                                <input type="file" {...register2("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileDoc(e.target.files[0])} />
                                <label className="custom-file-label text-truncate" htmlFor="fileupload">{file_doc ? file_doc.name : 'Pilih Berkas'}</label>
                            </div>
                            {errors2.document_file && <span className="form-text text-danger">Silahkan unggah dokumen berkas.</span>}
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>3. Keterangan (Opsional) :</label>
                            <textarea rows="1" {...register2("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="form-group">
                            <label>4. Unggah :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Unggah Berkas
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <ListDocsUploaded package_no={proposal_no} status='Kontrak' document_for='startup-research' />

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            <div className="d-flex justify-content-end">
                <Link to={`/startup-research/contract`} className="btn btn-light-danger font-weight-bold btn-lg ml-2">
                    Kembali
                </Link>
                <button type="button" onClick={() => onSubmitNextBtn()} className="btn btn-primary font-weight-bold btn-lg ml-2">Selanjutnya</button>
            </div>
        </div>
    )
}

export default StatusContract