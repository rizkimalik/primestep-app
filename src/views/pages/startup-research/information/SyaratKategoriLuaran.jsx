import React from 'react'

function SyaratKategoriLuaran() {
    return (
        <div className="dropdown dropdown-inline ml-2">
            <a href="#" className="btn btn-link-success font-weight-bolder"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-question-circle fa-sm" />
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <ul className="navi navi-hover">
                    <li className="navi-header pb-1 border-bottom">
                        <span className="font-weight-bold text-dark mb-2">Kategori Luaran :</span>
                    </li>
                    <li className="navi-header pb-1">
                        <span className="font-weight-bold text-dark">1. Purwarupa:</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">a. Deskripsi & spesifikasi produk</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">b. Dokumentasi produk (foto / video)</span>
                        </span>
                    </li>
                    <li className="navi-header pb-1 mt-2">
                        <span className="font-weight-bold text-dark">2. Produk (Hanya untuk tahun ke-3):</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">a. Dokumen hasil uji produk lapangan / lingkungan terbatas</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">b. Dokumentasi pengujian (foto / video) </span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SyaratKategoriLuaran