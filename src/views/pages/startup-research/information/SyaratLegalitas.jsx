import React from 'react'

function SyaratLegalitas() {
    return (
        <div className="dropdown dropdown-inline ml-2">
            <a href="#" className="btn btn-link-success font-weight-bolder"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-question-circle fa-sm" />
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <ul className="navi navi-hover">
                    <li className="navi-header pb-1 border-bottom">
                        <span className="font-weight-bold mb-2">Legalitas badan usaha dan perizin usaha :</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">1. Nomor Pokok Wajib Pajak (NPWP)</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">2. Surat Izin Usaha Perdagangan (SIUP)</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">3. Surat Keterangan Domisili Perusahaan (SKDP)</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">4. Tanda Daftar Perusahaan (TDP)</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">5. Dokumen lain yang diperlukan</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SyaratLegalitas