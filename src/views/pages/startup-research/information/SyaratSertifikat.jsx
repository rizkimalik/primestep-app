import React from 'react'

function SyaratSertifikat() {
    return (
        <div className="dropdown dropdown-inline ml-2">
            <a href="#" className="btn btn-link-success font-weight-bolder"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-question-circle fa-sm" />
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <ul className="navi navi-hover">
                    <li className="navi-header pb-1 border-bottom">
                        <span className="font-weight-bold mb-2">Sertifikasi dan Izin Produk :</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">1. BPPOM</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">2. SNI</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">3. Sertifikat Lainnya</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SyaratSertifikat