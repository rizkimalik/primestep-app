import React from 'react'

function SyaratUsulan() {
    return (
        <div className="dropdown dropdown-inline mb-4">
            <span className="form-text text-muted">Sebelum mengisi, <br /></span>
            <a href="#" className="font-weight-bold" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Baca Persyaratan Pengusul Riset PRIME STeP 2024</a>
            <div className="dropdown-menu dropdown-menu-xxl dropdown-menu-center">
                <ul className="navi navi-hover">
                    <li className="navi-header pb-1 border-bottom">
                        <span className="font-weight-bold mb-2">Syarat Pengusul Penelitian PRIME STeP 2024</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">a. Ketua pengusul dengan jabatan fungsional Lektor yang berkualifikasi Guru Besar, Doktor atau Lektor Kepala yang berkualifikasi minimal Magister;</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">b. Telah memiliki paten/paten sederhana, perlindungan varietas tanaman, desain tata letak sirkuit terpadu, desain industri, indikasi geografis, rahasia dagang atau yang relevan dengan proposal yang diajukan. Dibuktikan dengan menyertakan nomor pendaftaran atau nomor sertifikat yang dikeluarkan oleh Lembaga yang berwenang;</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">c. Memiliki pengalaman riset paling sedikit 3 (tiga) tahun dan setidaknya 1 (satu) publikasi pada jurnal internasional bereputasi;</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">d. Telah memiliki mitra yang dibuktikan dengan surat pernyataan (dukungan) yang berisikan kesediaan sebagai pengguna hasil penelitian;</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">e. Tim pengusul terdiri dari unsur dosen, mahasiswa, mitra industri, mitra penelitian dan pihak lain yang terkait;</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SyaratUsulan