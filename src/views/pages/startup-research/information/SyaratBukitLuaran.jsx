import React from 'react'

function SyaratBukitLuaran() {
    return (
        <div className="dropdown dropdown-inline mb-4">
            <a href="#" className="font-weight-bold" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">* Syarat Bukti Luaran :</a>
            <div className="dropdown-menu dropdown-menu-xxl dropdown-menu-center">
                <ul className="navi navi-hover">
                    <li className="navi-header pb-1 border-bottom">
                        <span className="font-weight-bold mb-2">Bukti Luaran yang harus disubmit adalah:</span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">1. Deskripsi dan spesifikasi produk</span>
                        </span>
                    </li>
                    <li className="navi-item">
                        <span className="navi-link">
                            <span className="navi-text">2. Dokumentasi produk (foto/video)</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SyaratBukitLuaran