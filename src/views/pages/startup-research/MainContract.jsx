import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';

import { IconMark } from 'views/components/icon';
import { Card, CardBody } from 'views/components/card';
import { authUser, setStoreData } from 'app/slice/sliceAuth';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import {
    apiStartupResearch_ContractList,
    apiStartupResearch_InformationStatus
} from 'app/services/apiStartupResearch';

function MainContract() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const history = useHistory();
    const { contract_list } = useSelector(state => state.startup_research);
    const { store_data } = useSelector(state => state.auth);


    useEffect(() => {
        dispatch(apiStartupResearch_ContractList({ auth, proposed_year: store_data.year }));
    }, [dispatch, store_data]);

    const onChangeYear = (year) => {
        dispatch(setStoreData({ ...store_data, year }));
    }

    const onClickContract = (data) => {
        dispatch(setStoreData({ ...store_data, proposal_no: data.proposal_no }));
        dispatch(apiStartupResearch_InformationStatus({ auth, piu: data.piu, proposed_year: data.proposed_year }));
        history.push(`/startup-research/contract-status?piu=${data.piu}&year=${data.proposed_year}`);
    }


    return (
        <MainContent>
            <SubHeader active_page="Research and Development" menu_name="Kontrak Startup & Penelitian" modul_name="">
                <span className="font-weight-bolder text-muted">Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <div className="d-flex justify-content-between mb-2">
                            <h3>Daftar Kontrak Startup & Penelitian</h3>
                            <div className="dropdown">
                                <button className="btn btn-danger btn-sm dropdown-toggle font-weight-bolder" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tahun {store_data.year}
                                </button>
                                <div className="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                    <a onClick={() => onChangeYear(2023)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">1. Tahun 2023</a>
                                    <a onClick={() => onChangeYear(2024)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">2. Tahun 2024</a>
                                    <a onClick={() => onChangeYear(2025)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">3. Tahun 2025</a>
                                    <a onClick={() => onChangeYear(2026)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">4. Tahun 2026</a>
                                    <a onClick={() => onChangeYear(2027)} data-toggle="modal" className="dropdown-item font-weight-bolder" href="#">5. Tahun 2027</a>
                                </div>
                            </div>
                        </div>
                        <DataGrid
                            dataSource={contract_list}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={100}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Perguruan Tinggi" dataField="institution_name" cellRender={({ data }) => {
                                return <span>{data.institution_name} ({data.piu})</span>
                            }} />
                            <Column caption="Tahun Usulan" dataField="proposed_year" width={120} alignment="center" />
                            {/* <Column caption="Status" dataField="status" cellRender={({ value }) => {
                                if (value === 'Persiapan-Dokumen') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Verifikasi-Dokumen') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Kontrak') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Withdrawal-App') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Pencairan') {
                                    return <span className="label label-inline label-primary font-weight-bolder">{value}</span>
                                } else if (value === 'Finish') {
                                    return <span className="label label-inline label-success font-weight-bolder">{value}</span>
                                } else {
                                    return <span className="label label-inline font-weight-bolder">Usulan</span>
                                }
                            }} /> */}
                            <Column caption="Kontrak" dataField="proposal_no" alignment="center" cellRender={({ value, data }) => {
                                return <button type="button" onClick={() => onClickContract(data)} className="btn btn-sm btn-light-primary font-weight-bolder"><IconMark className="svg-icon svg-icon-sm p-0" /> Detail Kontrak</button>
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default MainContract
