import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging, Selection } from 'devextreme-react/data-grid';

import InnovationMember from './step/MemberTeam';
import InformationHeader from './header/InformationHeader';
import { ButtonCreate } from 'views/components/button';
import { Card, CardBody } from 'views/components/card';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { authUser, setStoreData } from 'app/slice/sliceAuth';
import { SwalAlertError } from 'views/components/SwalAlert';
import {
    apiStartupResearch_ResearchList,
    apiStartupResearch_Delete,
    apiExport_Excel,
} from 'app/services/apiStartupResearch';


function ResearchMain() {
    const dispatch = useDispatch();
    const auth = useSelector(authUser);
    const { research } = useSelector(state => state.startup_research);
    const { store_data } = useSelector(state => state.auth);
    const [count_value, setCount] = useState(0);

    useEffect(() => {
        if (auth.institution === 'PMU') {
            dispatch(apiStartupResearch_ResearchList({ auth, piu: store_data.institution, proposed_year: store_data.year }));
        }
        else {
            dispatch(apiStartupResearch_ResearchList({ auth, piu: auth.institution, proposed_year: store_data.year }));
        }
    }, [dispatch, auth, store_data]);

    function handlerDeleteData(id) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "anda ingin hapus data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, hapus sekarang!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiStartupResearch_Delete({ id }));
                if (payload.status === 200) {
                    Swal.fire({
                        title: "Sukses Menghapus.",
                        text: `Data telah terhapus!`,
                        buttonsStyling: false,
                        icon: "success",
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    dispatch(apiStartupResearch_ResearchList())
                }
            }
        });
    }

    const onExportExcel = async () => {
        const { payload } = await dispatch(apiExport_Excel({ modul: 'research', piu: store_data.institution, proposed_year: store_data.year }))
        if (payload?.data?.length > 0) {
            const data_export = payload.data;
            const workbook = new Workbook();
            const worksheet = workbook.addWorksheet('Main sheet');
            worksheet.columns = [
                { header: 'Judul Proposal', key: 'proposal_name' },
                { header: 'Bidang Fokus', key: 'focus_name' },
                { header: 'Kategori Penelitian', key: 'framework' },
                { header: 'Nama Ketua', key: 'leader_name' },
                { header: 'Nilai Kontrak', key: 'contract_value' },
                { header: 'Tahun Usulan', key: 'proposed_year' },
                { header: 'Usulan Dana', key: 'value_idr' },
                { header: 'TKT', key: 'tkt_value' },
                { header: 'Durasi Kegiatan', key: 'tkt_duration' },
                { header: 'Status', key: 'status_rekomendasi' },
            ]
            worksheet.addRows(data_export);
            worksheet.autoFilter = 'A1:I1';
            worksheet.eachRow(function (row, rowNumber) {
                row.eachCell((cell, colNumber) => {
                    if (rowNumber === 1) {
                        cell.fill = {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: { argb: 'f5b914' }
                        }
                    }
                })
                row.commit();
            });

            workbook.xlsx.writeBuffer().then((buffer) => {
                saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Riset Inovasi - ${store_data.institution} ${store_data.year}.xlsx`);
            });
        } else {
            SwalAlertError('Aksi Gagal', 'Gagal cetak ke berkas excel.')
        }
    }

    function componentButtonActions(data) {
        const { package_no, proposal_no } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <Link to={`/startup-research/edit/research/${package_no}`} onClick={() => dispatch(setStoreData({ ...store_data, package_no, proposal_no }))} title="Update Data" className="btn btn-sm btn-clean btn-icon btn-hover-warning">
                    <i className="fas fa-edit fa-sm"></i>
                </Link>
                {/* 
                <button className="btn btn-sm btn-clean btn-icon btn-hover-danger" title="Hapus Data" onClick={() => handlerDeleteData(id)}>
                    <i className="fas fa-trash-alt fa-sm"></i>
                </button> */}
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Research and Development" menu_name="Riset" modul_name="Data Riset / Penelitian">
                <span className="font-weight-bolder text-muted">Riset / Penelitian {store_data.institution} - Tahun {store_data.year}</span>
            </SubHeader>
            <Container>
                <Card>
                    <CardBody>
                        <InformationHeader
                            modul="research"
                            auth={auth}
                            store_data={store_data}
                            setStoreData={setStoreData}
                            setCount={setCount}
                            apiStartupResearchList={apiStartupResearch_ResearchList}
                        />
                        <div className="d-flex justify-content-between mb-2">
                            <h3>2. Daftar Riset / Penelitian</h3>
                            <div>
                                <button type="button" onClick={() => onExportExcel()} className="btn btn-sm btn-light-success font-weight-bolder mr-2"><i className="fas fa-print icon-sm"></i> Cetak Excel</button>
                                {
                                    count_value > 0 &&
                                    <Link to={`/startup-research/status/research?piu=${store_data.institution}&year=${store_data.year}`} className="btn btn-sm btn-success font-weight-bolder mr-2" title="Verifikasi Dokumen">
                                        <i className="flaticon2-correct icon-sm"></i> Verifikasi Dokumen
                                        {/* <i className="fas fa-upload fa-sm"></i> Verifikasi Dokumen */}
                                    </Link>
                                }
                                {auth.user_level !== 'Evaluator' && <ButtonCreate text="Tambah Usulan" to={`/startup-research/create/research?piu=${store_data.institution}&year=${store_data.year}`} />}
                            </div>
                        </div>
                        <DataGrid
                            dataSource={research}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            rowAlternationEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={100}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Selection mode="single" />
                            {
                                auth.user_level !== 'Evaluator' &&
                                <Column caption="Aksi" dataField="package_no" cellRender={componentButtonActions} />
                            }
                            {/* <Column caption="Judul Proposal" dataField="proposal_name" width={300} /> */}
                            <Column caption="Judul Proposal" dataField="proposal_name" width={300} cellRender={({ value, data }) => {
                                return <Link to={`/startup-research/verification/research/${data.package_no}`} className="text-primary font-weight-bolder"><i className="fas fa-edit fa-sm text-primary" /> {value}</Link>
                            }} />
                            <Column caption="Bidang Fokus" dataField="focus_name" />
                            <Column caption="Kategori Penelitian" dataField="framework" />
                            <Column caption="Nama Ketua" dataField="leader_name" />
                            <Column
                                caption="Nilai Kontrak"
                                dataField="contract_value"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                            <Column caption="Tahun Usulan" dataField="proposed_year" />
                            <Column
                                caption="Usulan Dana"
                                dataField="value_idr"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                            <Column
                                caption="Rekomendasi Dana"
                                dataField="recommend_budget"
                                format="fixedPoint"
                                cellRender={({ value }) => {
                                    return <span>{new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(value)}</span>
                                }}
                            />
                            <Column caption="TKT" dataField="tkt_value" />
                            <Column caption="Durasi Kegiatan" dataField="tkt_duration" cellRender={({ value }) => {
                                return <span>{value} Tahun</span>
                            }} />
                            <Column caption="Status" dataField="recommend" alignment="center" cellRender={({ data }) => {
                                return data.recommend === 0
                                    ? <div>
                                        <span className="label label-inline label-warning">Revisi</span>
                                        <Link to={`/startup-research/verification/research/${data.package_no}`} className="btn btn-sm btn-clean btn-link-warning" title="Unggah Revisi Berkas">
                                            <i className="fas fa-upload fa-sm" /> Revisi Berkas
                                        </Link>
                                    </div>
                                    : data.recommend === 1
                                        ? <span className="label label-inline label-success">Rekomendasi</span>
                                        : data.recommend === 2
                                            ? <span className="label label-inline label-danger">Ditolak</span>
                                            : <span className="label label-inline">Menunggu</span>
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

const MasterDetailView = (props) => {
    const { package_no } = props.data.data;
    return (
        <InnovationMember package_no={package_no} />
    );
}

export default ResearchMain