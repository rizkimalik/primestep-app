import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation, useParams } from 'react-router-dom';

import StatusPersiapan from './status/StatusPersiapan';
import StatusPersiapanDocument from './status/StatusPersiapanDocument';
import StatusVerificationDocument from './status/StatusVerificationDocument';
import HeaderFormVerification from './header/HeaderFormVerification';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { data_status } from './status/data_status';
import { setStoreData } from 'app/slice/sliceAuth';

function MainFormVerification() {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const startup = new URLSearchParams(search).get('startup');
    const [navigate, setNavigate] = useState('1');
    const { modul } = useParams();
    const { information_status } = useSelector(state => state.startup_research);
    const { store_data } = useSelector(state => state.auth);

    useEffect(() => {
        const piu = new URLSearchParams(search).get('piu');
        const year = new URLSearchParams(search).get('year');
        const proposal_no = `RND${year}-${piu}`;

        dispatch(setStoreData({ ...store_data, institution: piu, year, proposal_no }));
    }, [search]);

    useEffect(() => {
        //? utk cek strp paket & navigasi aktif
        if (!information_status) return;
        const filteredStatus = data_status.filter(item => item.status === information_status.status);
        if (filteredStatus.length > 0) {
            setNavigate(filteredStatus[0].navigate);
        }
    }, [information_status]);


    return (
        <MainContent>
            <SubHeader active_page="Research and Development" menu_name={startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} modul_name="Persiapan & Verifikasi Dokumen">
                <Link to={`/${modul}`} className="btn btn-light btn-sm font-weight-bolder">
                    {startup === 'acceleration' ? 'Startup Akselerasi' : modul === 'startup' ? 'Inkubasi Startup' : 'Riset / Penelitian'} {store_data.institution} - Tahun {store_data.year}
                </Link>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <HeaderFormVerification navigate={navigate} />
                        <Card>
                            <CardBody>
                                {navigate === '1' && <StatusPersiapan modul={modul} setNavigate={setNavigate} startup={startup} />}
                                {navigate === '1A' && <StatusPersiapanDocument modul={modul} setNavigate={setNavigate} startup={startup} />}
                                {navigate === '2' && <StatusVerificationDocument modul={modul} setNavigate={setNavigate} startup={startup} />}
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default MainFormVerification