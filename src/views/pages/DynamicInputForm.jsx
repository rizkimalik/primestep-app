import React, { useEffect, useState } from 'react';

const DynamicInputFields = () => {
    const [inputs, setInputs] = useState([{ type: 'text', value: '' }, { type: 'date', value: '' }]);

    const handleInputChange = (index, event) => {
        const newInputs = [...inputs];
        newInputs[index].value = event.target.value;
        setInputs(newInputs);
    };

    const handleAddInput = () => {
        // Determine the type based on the length of the inputs array
        // const type = inputs.length % 2 === 0 ? 'text' : 'date';
        // setInputs([...inputs, { type, value: '' }]);
        setInputs([...inputs, { type: 'text', value: '' }, { type: 'date', value: '' }]);
    };

    const handleRemoveInput = (index) => {
        const newInputs = [...inputs];
        newInputs.splice(index, 1);
        setInputs(newInputs);
    };

    return (
        <div>
            <button onClick={handleAddInput}>Add Input</button>

            {inputs.map((input, index) => (
                <div key={index}>
                    <input
                        type={input.type}
                        value={input.value}
                        onChange={(e) => handleInputChange(index, e)}
                    />
                    {/* <button onClick={() => handleRemoveInput(index)}>Remove</button> */}
                </div>
            ))}

            <code>{JSON.stringify(inputs)}</code>
        </div>
    );
};

export default DynamicInputFields;
