import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Lookup, Pager, Paging } from 'devextreme-react/data-grid'

import ModalDocumentGAP from './ModalDocumentGAP';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { apiGenderActionDetail, apiGenderActionHeader } from 'app/services/apiGenderAction';
import { apiMasterInstitution } from 'app/services/apiMasterData';

function GenderActionDetailList() {
    const dispatch = useDispatch();
    const [package_no, setPackageNo] = useState('');
    const { gender_action_detail, gender_action_header } = useSelector(state => state.gender_action);
    const { institution } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiGenderActionDetail())
        dispatch(apiMasterInstitution());
        dispatch(apiGenderActionHeader())
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Gender Action Plan" menu_name="Progress of GAP" modul_name="Data Detail">
                <Link to="/gender-action" className="btn btn-sm btn-light-info ml-2">
                    <i className="fas fa-poll-h fa-sm" />
                    Summary Data
                </Link>
                <Link to="/setting/gender_action" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    Gender Action Plan
                </Link>
            </SubHeader>
            <Container>
                {package_no && <ModalDocumentGAP package_no={package_no} />}
                <Card>
                    <CardBody className="p-5">
                        <div className="d-flex justify-content-between mb-2">
                            <h3>Gender Action Plan</h3>
                            <Link to={`/gender-action/create/0`} className="btn btn-sm btn-light-primary">
                                <i className="fas fa-edit fa-sm" />
                                Add Record
                            </Link>
                        </div>
                        <DataGrid
                            dataSource={gender_action_detail}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            wordWrapEnabled={true}
                            columnMinWidth={80}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Editing
                                mode="row"
                                refreshMode='reshape'
                                allowAdding={false}
                                allowDeleting={true}
                                allowUpdating={true}
                            />
                            <Column caption="Actions" type="buttons">
                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                    <i className="fas fa-trash-alt icon-sm"></i>
                                </Button>
                                <Button name="edit" cssClass="btn btn-sm btn-clean btn-icon btn-hover-warning">
                                    <i className="fas fa-edit icon-sm"></i>
                                </Button>
                            </Column>
                            <Column caption="Package" dataField="package_no" allowEditing={false} />
                            <Column caption="PIU" dataField="piu">
                                <Lookup
                                    dataSource={institution}
                                    valueExpr="institution_code"
                                    displayExpr="institution_code"
                                />
                            </Column>
                            <Column caption="GAP" dataField="gap_id" width={250}>
                                <Lookup
                                    dataSource={gender_action_header}
                                    valueExpr="id"
                                    displayExpr="gender_action_plan"
                                />
                            </Column>
                            <Column caption="Gender" dataField="gender" >
                                <Lookup
                                    dataSource={[{ gender: 'Pria' }, { gender: 'Wanita' }]}
                                    valueExpr="gender"
                                    displayExpr="gender"
                                />
                            </Column>
                            <Column caption="Amount" dataField="amount" dataType="number" />
                            <Column caption="Date Record" dataField="action_date" dataType="date" />
                            <Column caption="Catatan" dataField="comments" />
                            <Column caption="Report File" dataField="document_upload" alignment="center" allowEditing={false} cellRender={({ value, data }) => {
                                if (value === 1 || data.gap_id === 4) {
                                    return <button type="button" className="btn btn-sm btn-clean btn-icon btn-hover-info" data-toggle="modal" data-target="#ModalDocumentGAP" title="upload document / file laporan" onClick={() => setPackageNo(data.package_no)}>
                                        <i className="fas fa-file-alt"></i>
                                    </button>
                                }
                                else {
                                    return <span>-</span>
                                }
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default GenderActionDetailList