import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import GenderActionPivot from './GenderActionPivot';
import { ButtonRefresh } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials';
import { Card, CardBody } from 'views/components/card';
import { apiGenderActionHeader } from 'app/services/apiGenderAction';

function GenderActionPlantMain() {
    const dispatch = useDispatch();
    const { gender_action_header } = useSelector(state => state.gender_action);

    useEffect(() => {
        dispatch(apiGenderActionHeader())
    }, [dispatch]);

    return (
        <MainContent>
            <SubHeader active_page="Gender Action Plan" menu_name="Progress of GAP" modul_name="Summary Data">
                <Link to="/gender-action/detail" className="btn btn-sm btn-light-warning">
                    <i className="fas fa-database fa-sm" />
                    Data Detail
                </Link>
                <Link to="/setting/gender_action" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    Gender Action Plan
                </Link>
            </SubHeader>
            <Container>
                {
                    gender_action_header.map((item, index) => {
                        return <div key={index}>
                            <Card>
                                <CardBody className="p-5">
                                    <div className="d-flex justify-content-between align-items-center mb-2">
                                        <p className="font-weight-bolder text-truncate" style={{ width: '70%' }}>{item.sort_code}. {item.gender_action_plan} </p>
                                        <Link to={`/gender-action/create/${item.id}`} className="btn btn-sm btn-light-primary">
                                            <i className="fas fa-edit fa-sm" />
                                            Add Record
                                        </Link>
                                    </div>
                                    <GenderActionPivot detail_data={item} id={item.id} />
                                </CardBody>
                            </Card>
                        </div>
                    })
                }
            </Container>
        </MainContent>
    )
}

export default GenderActionPlantMain