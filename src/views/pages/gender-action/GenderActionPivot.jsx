import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Column, DataGrid } from 'devextreme-react/data-grid'

import PivotGrid, { FieldChooser } from 'devextreme-react/pivot-grid';
import PivotGridDataSource from 'devextreme/ui/pivot_grid/data_source';
import { apiGenderActionPivot } from 'app/services/apiGenderAction';

function GenderActionPivot({ detail_data, id }) {
    const dispatch = useDispatch();
    const [gender_action_pivot, setGenderActionPivot] = useState([]);
    // const { gender_action_pivot } = useSelector(state => state.gender_action);

    useEffect(async () => {
        // dispatch(apiGenderActionPivot({ gap_id: id }))
        const { payload } = await dispatch(apiGenderActionPivot({ gap_id: id }));
        if (payload.status === 200) {
            setGenderActionPivot(payload.data);
        }
        else {
            setGenderActionPivot([]);
        }
    }, [dispatch, id]);

    const dataSource = new PivotGridDataSource({
        fields: [
            {
                caption: 'PIU',
                width: 100,
                dataField: 'piu',
                area: 'row',
                expanded: false,
            },
            {
                caption: 'Gender',
                dataField: 'gender',
                width: 100,
                area: 'row',
                selector(data) {
                    return `${detail_data.group_name} ${data.gender}`;
                },
            },
            {
                dataField: 'action_date',
                dataType: 'date',
                area: 'column',
            },
            {
                caption: 'Gender Action',
                dataField: 'amount',
                dataType: 'number',
                summaryType: 'sum',
                area: 'data',
            },
        ],
        store: gender_action_pivot,
    });

    const count_ratio_year = (year) => {
        const jml_wanita = gender_action_pivot.filter(row => row.gender === 'Wanita' && row.year === year).reduce((total, row) => total + row.amount, 0);
        const jml_total = gender_action_pivot.filter(row => row.year === year).reduce((total, row) => total + row.amount, 0);
        const result = jml_wanita / jml_total * 100;
        return (!result ? 0 : result.toFixed(2)); // result persentase
    }

    const result_summary_gap = [{
        name: 'RATIO',
        result_2023: count_ratio_year('2023') + '%',
        result_2024: count_ratio_year('2024') + '%',
        result_2025: count_ratio_year('2025') + '%',
        result_2026: count_ratio_year('2026') + '%',
        result_2027: count_ratio_year('2027') + '%',
    }, {
        name: 'STATUS',
        result_2023: count_ratio_year('2023') > detail_data.target_percentase ? 'Achived' : 'Unachived',
        result_2024: count_ratio_year('2024') > detail_data.target_percentase ? 'Achived' : 'Unachived',
        result_2025: count_ratio_year('2025') > detail_data.target_percentase ? 'Achived' : 'Unachived',
        result_2026: count_ratio_year('2026') > detail_data.target_percentase ? 'Achived' : 'Unachived',
        result_2027: count_ratio_year('2027') > detail_data.target_percentase ? 'Achived' : 'Unachived',
    }];

    return (
        <div>
            <PivotGrid
                id="gender_action_pivot"
                dataSource={dataSource}
                allowSortingBySummary={true}
                allowSorting={true}
                allowFiltering={true}
                allowExpandAll={true}
                showBorders={true}
            >
                <FieldChooser enabled={false} />
            </PivotGrid>
            <br />

            <DataGrid
                dataSource={result_summary_gap}
                // remoteOperations={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <Column caption="Result" dataField="name" width={120} />
                <Column caption="2023" dataField="result_2023" cellRender={({ value }) => <span className="font-weight-bolder">{value}</span>} />
                <Column caption="2024" dataField="result_2024" cellRender={({ value }) => <span className="font-weight-bolder">{value}</span>} />
                <Column caption="2025" dataField="result_2025" cellRender={({ value }) => <span className="font-weight-bolder">{value}</span>} />
                <Column caption="2026" dataField="result_2026" cellRender={({ value }) => <span className="font-weight-bolder">{value}</span>} />
                <Column caption="2027" dataField="result_2027" cellRender={({ value }) => <span className="font-weight-bolder">{value}</span>} />
            </DataGrid>

            <div className="alert alert-custom alert-white alert-shadow gutter-b border mt-4 mb-0" role="alert">
                <div className="alert-text">
                    <span className="label label-inline label-pill label-danger label-rounded mr-2">NOTE:</span>
                    <code>Ratio = ( Jumlah Wanita / Jumlah Total ) x 100 % | Status = Ratio % {`>`} Target % (Achived) </code>
                </div>
            </div>

        </div>
    )
}

export default GenderActionPivot