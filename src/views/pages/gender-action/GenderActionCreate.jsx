import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';

import { Card, CardBody } from 'views/components/card';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { MainContent, SubHeader, Container } from 'views/layouts/partials';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiGenderActionInsert, apiGenderActionHeader, apiGenderActionMasterShow } from 'app/services/apiGenderAction';
import { apiMasterInstitution } from 'app/services/apiMasterData';

function GenderActionCreate() {
    const dispatch = useDispatch();
    const [file_upload, setFileUpload] = useState('');
    const { gap_id } = useParams();
    const { institution } = useSelector(state => state.master)
    const { gender_action_header, gender_action_master_show } = useSelector(state => state.gender_action);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        dispatch(apiMasterInstitution());
        dispatch(apiGenderActionHeader());
    }, [dispatch]);

    useEffect(() => {
        if (gap_id !== '0') {
            dispatch(apiGenderActionMasterShow({ id: gap_id }));
        }
    }, [dispatch, gap_id]);

    useEffect(() => {
        if (gap_id !== '0') {
            reset({ gap_id });
        }
    }, [reset, gap_id]);

    const onSubmitUpdateData = async (data) => {
        try {
            data.year = new Date(data.action_date).getFullYear()
            const { payload } = await dispatch(apiGenderActionInsert(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!')
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Gender Action Plan" menu_name="Progress of GAP" modul_name="Add Record">
                <Link to="/gender-action" className="btn btn-sm btn-light-info">
                    <i className="fas fa-poll-h fa-sm" />
                    Summary Data
                </Link>
                <Link to="/gender-action/detail" className="btn btn-sm btn-light-warning ml-2">
                    <i className="fas fa-database fa-sm" />
                    Data Detail
                </Link>
                <Link to="/setting/gender_action" className="btn btn-sm btn-light-danger ml-2">
                    <i className="fas fa-cog fa-sm" />
                    Gender Action Plan
                </Link>
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <Card>
                            <CardBody>
                                <h3 className="mb-4">Add Record of GAP</h3>
                                <form onSubmit={handleSubmit(onSubmitUpdateData)} encType="multipart/form-data" className="form">
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">
                                            <Link to="/setting/gender_action"><i className="fas fa-edit fa-sm text-primary" /> </Link>
                                            * Gender Action Plan:
                                        </label>
                                        <div className="col-lg-6">
                                            <select
                                                {...register("gap_id", { required: true })}
                                                className="form-control"
                                                onChange={(e) => dispatch(apiGenderActionMasterShow({ id: e.target.value }))}
                                            >
                                                <option value="">-- select action plan --</option>
                                                {
                                                    gender_action_header.map((item, index) => {
                                                        return <option value={item.id} key={index} data={item}>{item.sort_code}. {item.gender_action_plan}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.gap_id && <span className="form-text text-danger">Please select action.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* List of PIU:</label>
                                        <div className="col-lg-6">
                                            <select className="form-control" {...register("piu", { required: true })}>
                                                <option value="">-- select piu --</option>
                                                {
                                                    institution.map((item, index) => {
                                                        return <option value={item.institution_code} key={index}>{item.institution_code}</option>
                                                    })
                                                }
                                            </select>
                                            {errors.piu && <span className="form-text text-danger">Please select PIU.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* Gender:</label>
                                        <div className="col-lg-6">
                                            <select className="form-control" {...register("gender", { required: true })}>
                                                <option value="">-- select gender --</option>
                                                <option value="Pria">Pria</option>
                                                <option value="Wanita">Wanita</option>
                                            </select>
                                            {errors.gender && <span className="form-text text-danger">Please select Gender.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* Number of People:</label>
                                        <div className="col-lg-6">
                                            <input type="number" {...register("amount", { required: true })} className="form-control" placeholder="Enter Number of people" />
                                            {errors.amount && <span className="form-text text-danger">Please Enter Number.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">* Date Record:</label>
                                        <div className="col-lg-6">
                                            <input type="date" {...register("action_date", { required: true })} className="form-control" placeholder="Enter Date Record" />
                                            {errors.action_date && <span className="form-text text-danger">Please Enter Date Record.</span>}
                                        </div>
                                    </div>
                                    <div className={`form-group row ${gender_action_master_show?.upload_document !== 1 && 'hide'}`}>
                                        <label className="col-lg-3 col-form-label text-right">File Browser</label>
                                        <div className="col-lg-6">
                                            <div className="custom-file">
                                                <input type="file" {...register("document_file", { required: false })} className="custom-file-input" id="document_file" onChange={(e) => setFileUpload(e.target.files[0])} />
                                                <label className="custom-file-label" htmlFor="document_file">{file_upload ? file_upload.name : 'Upload file'}</label>
                                            </div>
                                            {errors.document_file && <span className="form-text text-danger">Please upload document file.</span>}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-lg-3 col-form-label text-right">Catatan:</label>
                                        <div className="col-lg-6">
                                            <textarea className="form-control" {...register("comments", { required: false })} rows="5" placeholder="Catatan"></textarea>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="d-flex justify-content-between mt-10">
                                        <ButtonCancel to="/gender-action" />
                                        <ButtonSubmit />
                                    </div>
                                </form>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default GenderActionCreate