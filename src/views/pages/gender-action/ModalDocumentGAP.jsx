import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Column, DataGrid, FilterRow, Paging, Pager, Editing, Button } from 'devextreme-react/data-grid'

import { urlAttachment } from 'app/config';
import { apiGenderAction_FileUpload, apiGenderAction_FileUploadList } from 'app/services/apiGenderAction';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function ModalDocumentGAP({ package_no }) {
    const dispatch = useDispatch();
    const [file_upload, setFileUpload] = useState('');
    const { gap_file_upload } = useSelector(state => state.gender_action);
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        dispatch(apiGenderAction_FileUploadList({ package_no }))
    }, [dispatch, package_no]);

    useEffect(() => {
        reset({ package_no });
    }, [reset, package_no]);

    const onSubmitUploadFile = async (data) => {
        try {
            const { payload } = await dispatch(apiGenderAction_FileUpload(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!');
                setFileUpload('');
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
        }
    }

    return (
        <div className="modal fade" id="ModalDocumentGAP" tabIndex={-1} role="dialog" aria-labelledby="ModalDocumentGAP" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Document Laporan.</h5>
                        <button type="button" className="close" data-toggle="modal" data-target="#ModalDocumentGAP" aria-label="Close">
                            <i aria-hidden="true" className="ki ki-close"></i>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={handleSubmit(onSubmitUploadFile)} encType="multipart/form-data" className="mb-4 p-4 rounded bg-light">
                            <input type="hidden" {...register("package_no", { required: true })} />
                            <input type="hidden" defaultValue="Complete" {...register("status", { required: true })} />
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="form-group">
                                        <label>File Browser</label>
                                        <div />
                                        <div className="custom-file">
                                            <input type="file" {...register("document_file", { required: true })} className="custom-file-input" onChange={(e) => setFileUpload(e.target.files[0])} />
                                            <label className="custom-file-label" htmlFor="fileupload">{file_upload ? file_upload.name : 'Upload file'}</label>
                                        </div>
                                        {errors.document_file && <span className="form-text text-danger">Please upload document file.</span>}
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <button type="submit" className={`btn btn-primary font-weight-bold btn-sm`}>Upload File</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <DataGrid
                            dataSource={gap_file_upload}
                            remoteOperations={{
                                filtering: true,
                                sorting: true,
                                paging: true,
                                summary: true
                            }}
                            hoverStateEnabled={true}
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                            columnMinWidth={80}
                        >
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                displayMode='full'
                                allowedPageSizes={[10, 20, 50]}
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Editing
                                mode="row"
                                refreshMode='reshape'
                                allowAdding={false}
                                allowDeleting={true}
                                allowUpdating={false}
                            />
                            <Column caption="Actions" type="buttons">
                                <Button name="delete" cssClass="btn btn-sm btn-clean btn-icon btn-hover-danger">
                                    <i className="fas fa-trash-alt icon-sm"></i>
                                </Button>
                            </Column>
                            <Column caption="Filename" dataField="filename" cellRender={({ value }) => {
                                return <a href={urlAttachment + '/gender_action_plan/' + value} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold">{value}</a>
                            }} />
                            <Column caption="Filesize" dataField="filesize" cellRender={({ value }) => {
                                return <span>{value / 1000} KB</span>
                            }} />
                            <Column caption="Created" dataField="created_at" dataType="datetime" />
                        </DataGrid>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default ModalDocumentGAP