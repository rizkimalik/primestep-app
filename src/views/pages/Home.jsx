import React from 'react'

import ticketing from 'assets/image/ticketing.png'
import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'

const Home = () => {

    return (
        <MainContent>
            <SubHeader active_page="Home" menu_name="Welcome" modul_name="Application" />
            <Container>
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <Card>
                            <CardBody className="d-flex align-items-center">
                                <div className="text-center">
                                    <p className="display-4 text-primary">Keurais Case Management</p>
                                    <p>Contact center solutions allow your contact center to connect with customers on any communication channel like voice call, video call, live chat, social media, and more. Provide customer service on any channel and seamlessly switch among any digital channels during an interaction, while maintaining context and relevant information across all channels as if it were a single conversation.</p>
                                    <img loading="lazy" src={ticketing} alt="Logo-Ticketing" style={{ width: '600px', height: 'auto' }} />
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Home
