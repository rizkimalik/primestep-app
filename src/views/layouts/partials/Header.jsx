import React, { useEffect } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import Icons from 'views/components/Icons';
import { axiosDefault } from 'app/config';
import { authUser } from 'app/slice/sliceAuth';
import { apiCheckAuthExpired, apiAuthLogout } from 'app/services/apiAuth';
import { apiNotificationProposal } from 'app/services/apiNotification';

function Header() {
    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const pathname = history.location.pathname;
    const { notification_proposal } = useSelector(state => state.notification);

    useEffect(() => {
        axiosDefault(user.token);
        async function onCheckAuth() {
            const { payload } = await dispatch(apiCheckAuthExpired({ token: user.token }));
            if (!payload) {
                console.log('auth token expired.');
                Swal.fire({
                    title: 'Auth token expired.',
                    text: 'Please re-login the application.',
                    buttonsStyling: false,
                    icon: "warning",
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    },
                }).then(() => {
                    localStorage.clear();
                    window.location.reload();
                });
            }
        }
        setTimeout(() => {
            onCheckAuth();
        }, 2000);
    }, [dispatch, user, pathname]);

    useEffect(async () => {
        if (user.user_level === 'PIU' || user.user_level === 'Evaluator') {
            await dispatch(apiNotificationProposal({ user }))
        }
    }, [dispatch, user, pathname]);

    async function onSignOut() {
        try {
            const { payload } = await dispatch(apiAuthLogout({ username: user.username }));
            if (payload.status === 200) {
                localStorage.clear();
                window.location.reload();
            }
        }
        catch (error) {
            console.log(error.message)
        }
    }

    return (
        <header id="kt_header" className="header header-fixed">
            <div className="container-fluid d-flex align-items-stretch justify-content-between">
                <div className="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu" className="header-menu header-menu-mobile header-menu-layout-default">
                        <ul className="menu-nav">
                            <li className="menu-item menu-item-submenu" data-menu-toggle="click">
                                <NavLink to="/dashboard" className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                                    <Icons iconName="layout-4-block" className="svg-icon svg-icon-xl svg-icon-info" />
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="topbar">
                    <div id="kt_header_mobile" className="header-mobile align-items-center header-mobile-fixed">
                        <div className="d-flex align-items-center">
                            <button className="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
                                <span />
                            </button>
                        </div>
                    </div>

                    {/* <div className="dropdown">
                        <div className="topbar-item">
                            <div className="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2">
                                <span className="label label-md label-light-success label-inline mr-1">{user.token ? 'Ready' : 'Not Ready'}</span>
                            </div>
                        </div>
                    </div> */}

                    <div className="dropdown">
                        <div className="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                            <div className="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-warning">
                                <Icons iconName="incoming-box" className="svg-icon svg-icon-xl svg-icon-warning" />
                                {notification_proposal.length > 0 && <span className="pulse-ring" />}
                            </div>
                        </div>
                        <div className="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                            <div className="d-flex flex-column flex-center py-2 bg-light-warning bgi-size-cover bgi-no-repeat rounded-top">
                                <h5 className="text-warning font-weight-bolder font-size-lg mt-2">
                                    Notifikasi <span className="btn btn-text btn-warning btn-sm font-weight-bold ml-2">{notification_proposal.length}</span>
                                </h5>
                            </div>
                            <div className="row row-paddingless">
                                <div className="col-12" style={{ height: 300, overflow: 'auto' }}>
                                    <div className="navi navi-hover scroll my-4 ps" data-scroll="true">
                                        {
                                            notification_proposal?.map((data, index) => {
                                                if (user.user_level === 'PIU') {
                                                    return <NavLink to="/startup" className="navi-item" key={index}>
                                                        <div className="navi-link">
                                                            <div className="navi-text d-flex flex-column">
                                                                <span className="font-weight-bold"><b>Revisi Usulan {data.type === 'S' ? 'Inkubasi Startup' : 'Penelitian'}</b></span>
                                                                <span className="d-flex text-inline text-muted">
                                                                    <small>{
                                                                        new Intl.DateTimeFormat('id-ID', {
                                                                            year: 'numeric',
                                                                            month: 'short',
                                                                            day: 'numeric',
                                                                            hour: "numeric",
                                                                            minute: "numeric",
                                                                            second: "numeric",
                                                                            timeZoneName: 'short',
                                                                        }).format(new Date(data.updated_at))
                                                                    }</small>
                                                                </span>
                                                                <span className="mt-2">Usulan {data.type === 'S' ? 'Inkubasi Startup' : 'Penelitian'} berjudul "{data.proposal_name}"</span>
                                                            </div>
                                                        </div>
                                                    </NavLink>
                                                } else {
                                                    return <NavLink to="/dashboard" className="navi-item" key={index}>
                                                        <div className="navi-link">
                                                            <div className="navi-text d-flex flex-column">
                                                                <span className="font-weight-bold"><b>Verifikasi Usulan {data.type === 'S' ? 'Inkubasi Startup' : 'Penelitian'}</b></span>
                                                                <span className="d-flex text-inline text-muted">
                                                                    <small className="mr-5">Institusi - {data.piu}</small>
                                                                    <small>{
                                                                        new Intl.DateTimeFormat('id-ID', {
                                                                            year: 'numeric',
                                                                            month: 'short',
                                                                            day: 'numeric',
                                                                            hour: "numeric",
                                                                            minute: "numeric",
                                                                            second: "numeric",
                                                                            timeZoneName: 'short',
                                                                        }).format(new Date(data.updated_at))
                                                                    }</small>
                                                                </span>
                                                                <span className="mt-2">Usulan {data.type === 'S' ? 'Inkubasi Startup' : 'Penelitian'} berjudul "{data.proposal_name}"</span>
                                                            </div>
                                                        </div>
                                                    </NavLink>
                                                }
                                            })
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="dropdown">
                        <div className="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                            <div className="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2">
                                <div className="d-none d-md-inline mx-2">
                                    <div className="d-flex flex-column align-items-end">
                                        <span className="text-dark-50 font-weight-bolder font-size-base">{user.name}</span>
                                        {
                                            user.user_level === 'PIU'
                                                ? <small className="text-muted font-size-xs">{user.user_level} - {user.institution}</small>
                                                : <small className="text-muted font-size-xs">{user.user_level}</small>
                                        }
                                    </div>
                                </div>
                                <span className="symbol symbol-lg-35 symbol-25 symbol-light-primary">
                                    <span className="symbol-label font-size-h5 font-weight-bold text-uppercase">{user.name.slice(0, 1)}</span>
                                </span>
                            </div>
                        </div>

                        <div className="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
                            <ul className="navi navi-hover py-4">
                                <li className="navi-item active">
                                    <Link to={`/user/${user.id}/edit`} className="navi-item">
                                        <div className="navi-link">
                                            <div className="symbol symbol-20 mr-3">
                                                <div className="symbol-label">
                                                    <Icons iconName="profile" className="svg-icon svg-icon-lg svg-icon-primary" />
                                                </div>
                                            </div>
                                            <div className="navi-text">
                                                <div className="font-weight-bold">Profile</div>
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <li className="navi-item active">
                                    <Link to="" className="navi-item" onClick={() => onSignOut()}>
                                        <div className="navi-link">
                                            <div className="symbol symbol-20 mr-3">
                                                <div className="symbol-label">
                                                    <Icons iconName="sign-out" className="svg-icon svg-icon-lg svg-icon-primary" />
                                                </div>
                                            </div>
                                            <div className="navi-text">
                                                <div className="font-weight-bold">Sign Out</div>
                                            </div>
                                        </div>
                                    </Link>
                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header
