import React from 'react';

function MainContent({children}) {
    return (
        <div className="content d-flex flex-column flex-column-fluid bg-light" id="kt_content">
            {children}
        </div>
    )
}

export default MainContent;
