import React from 'react';

function Guest({ children }) {
    return (
        <div className="d-flex flex-column flex-root vh-100">
            <div className="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid">
                <div className="content order-2 order-lg-1 d-flex flex-column w-100 pb-0 mb-0" style={{ backgroundColor: '#fff' }}>
                    <div className="d-flex flex-row justify-content-between text-center pt-md-5 pt-sm-5 px-lg-20 pt-5 px-0">
                        <div className="text-center">
                            <img loading="lazy" src="./assets/image/logo-adb.png" alt="Asian-Development-Bank" className="max-h-80px" />
                        </div>
                        
                    </div>
                    <div className="d-flex flex-column justify-content-center text-center pt-md-5 px-lg-20 px-0 mt-10">
                        <h3>
                            Promoting Research and Innovation through Modern and Efficient Science and Techno Park<br />
                        </h3>
                        <label>
                            PRIMESTeP berada dalam naungan Kementerian Pendidikan dan Kebudayaan demi menyediakan sistem pemantauan dan evaluasi untuk dana hibah dalam misi pengembangan STP. Dana hibah ini didukung oleh dana Pinjaman Hibah Luar Negeri (PHLN) dari Asian Development Bank. Lingkup pekerjaaan yang akan dilaksanakan meliputi tiga komponen kegiatan
                        </label>
                    </div>

                    <div className="row m-10">
                        <div className="col-md-6 col-xxl-3 border-right-0 border-right-md border-bottom border-bottom-xxl-0">
                            <div className="pt-30 pt-md-25 pb-15 px-5 text-center">
                                <div className="d-flex flex-center position-relative mb-25">
                                    <span className="svg svg-fill-primary opacity-4 position-absolute">
                                        <svg width={175} height={200}>
                                            <polyline points="87,0 174,50 174,150 87,200 0,150 0,50 87,0" />
                                        </svg>
                                    </span>
                                    <img loading="lazy" src="./assets/image/logo-ui.png" alt="logo_ui" className="max-h-80px" />
                                </div>
                                <h4 className="font-size-sm d-block font-weight-bold mb-7 text-dark-50">Universitas Indonesia</h4>
                                <div className="d-flex justify-content-center">
                                    <a href="https://www.ui.ac.id/" target="_blank" className="btn btn-warning btn-block">UI</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xxl-3 border-right-0 border-right-xxl border-bottom border-bottom-xxl-0">
                            <div className="pt-30 pt-md-25 pb-15 px-5 text-center">
                                <div className="d-flex flex-center position-relative mb-25">
                                    <span className="svg svg-fill-primary opacity-4 position-absolute">
                                        <svg width={175} height={200}>
                                            <polyline points="87,0 174,50 174,150 87,200 0,150 0,50 87,0" />
                                        </svg>
                                    </span>
                                    <img loading="lazy" src="./assets/image/logo-itb.png" alt="logo_itb" className="max-h-80px" />
                                </div>
                                <h4 className="font-size-sm d-block font-weight-bold mb-7 text-dark-50">Institut Teknologi Bandung</h4>
                                <div className="d-flex justify-content-center">
                                    <a href="https://www.itb.ac.id/" target="_blank" className="btn btn-primary btn-block">ITB</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xxl-3 border-right-0 border-right-md border-bottom border-bottom-md-0">
                            <div className="pt-30 pt-md-25 pb-15 px-5 text-center">
                                <div className="d-flex flex-center position-relative mb-25">
                                    <span className="svg svg-fill-primary opacity-4 position-absolute">
                                        <svg width={175} height={200}>
                                            <polyline points="87,0 174,50 174,150 87,200 0,150 0,50 87,0" />
                                        </svg>
                                    </span>
                                    <img loading="lazy" src="./assets/image/logo-ipb.png" alt="logo_ipb" className="max-h-80px" />
                                </div>
                                <h4 className="font-size-sm d-block font-weight-bold mb-7 text-dark-50">Institut Pertanian Bogor</h4>
                                <div className="d-flex justify-content-center">
                                    <a href="https://ipb.ac.id/" target="_blank" className="btn btn-success btn-block">IPB</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xxl-3">
                            <div className="pt-30 pt-md-25 pb-15 px-5 text-center">
                                <div className="d-flex flex-center position-relative mb-25">
                                    <span className="svg svg-fill-primary opacity-4 position-absolute">
                                        <svg width={175} height={200}>
                                            <polyline points="87,0 174,50 174,150 87,200 0,150 0,50 87,0" />
                                        </svg>
                                    </span>
                                    <img loading="lazy" src="./assets/image/logo-ugm.png" alt="logo_ugm" className="max-h-80px" />
                                </div>
                                <h4 className="font-size-sm d-block font-weight-bold mb-7 text-dark-50">Universitas Gajah Mada</h4>
                                <div className="d-flex justify-content-center">
                                    <a href="https://ugm.ac.id/id/" target="_blank" className="btn btn-danger btn-block">UGM</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="login-aside order-1 order-lg-2 d-flex flex-row-auto position-relative overflow-hidden" style={{ backgroundColor: '#eee' }}>
                    
                    <div className="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
                        <div className="d-flex flex-column-fluid flex-column flex-center">
                        <div className="text-center">
                            <img loading="lazy" src="./assets/image/logo-hitam.png" alt="Primestep" className="max-h-80px" />
                        </div>
                            <div className="login-form login-signin py-11">
                                {children}
                            </div>
                        </div>
                        <div className="d-flex justify-content-center align-items-center py-5">
                            <span>Hak Cipta © Tim PRIMESTeP 2023</span>
                        </div>
                        <div className="d-flex justify-content-center align-items-center py-7 py-lg-0">
                            <a href="https://primestep.kemdikbud.go.id" target="_blank" className="text-primary ml-5 font-weight-bolder font-size-lg">About Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Guest
